package com.bonch_dev.spinit

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.multidex.MultiDex
import com.bonch_dev.spinit.di.AppComponent
import com.bonch_dev.spinit.di.AppModule
import com.bonch_dev.spinit.di.DaggerAppComponent
import com.bonch_dev.spinit.domain.utils.Keys.DEV_TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.vk.sdk.VKSdk
import javax.inject.Inject
import javax.inject.Named

class App : Application() {

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences.Editor

    init {
        instance = this
    }

    companion object {
        var fireBaseToken = ""
        private var instance: App? = null
        lateinit var appComponent: AppComponent
        fun applicationContext(): Context = instance!!.applicationContext
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        initFB()
        initVK()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
        appComponent.inject(this)
    }

    private fun initVK() {
        VKSdk.initialize(applicationContext)
    }

    private fun initFB() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("APP", "getInstanceId failed", task.exception)
                return@OnCompleteListener
            } else {
                val token = task.result
                token?.let { fireBaseToken = it }
                sp.putString(DEV_TOKEN, fireBaseToken).apply()
                Log.e("firebasetoken", fireBaseToken)
            }
        })
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }
}