package com.bonch_dev.spinit.data.network.admin

import com.bonch_dev.spinit.domain.utils.Keys
import retrofit2.http.*

interface AdminArticleService {
    @GET("api/admin/posts/list")
    suspend fun getPostsList(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )

    @GET("api/admin/posts/create")
    suspend fun createPost(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )

    @PUT("api/admin/posts/{id}/update")
    suspend fun updatePost(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )

    @DELETE("api/admin/posts/{id}/delete")
    suspend fun deletePost(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )
}