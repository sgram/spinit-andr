package com.bonch_dev.spinit.data.network.admin

import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import retrofit2.http.*

interface AdminBroadcastService {
    @GET("api/admin/broadcasts/list")
    suspend fun getBroadcasts(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = APP_JSON
    )

    @POST("api/admin/broadcasts/create")
    suspend fun createBroadcast(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = APP_JSON
    )

    @PUT("api/admin/broadcasts/{id}/update")
    suspend fun updateBroadcast(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = APP_JSON,
        @Path("id") id: Int
    )

    @DELETE("api/admin/broadcasts/{id}/delete")
    suspend fun deleteBroadcast(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = APP_JSON,
        @Path("id") id: Int
    )
}