package com.bonch_dev.spinit.data.network.admin

import com.bonch_dev.spinit.domain.utils.Keys
import retrofit2.http.*

interface AdminCoursesService {
    @POST("api/admin/courses/{course_id}/teacherAdd")
    suspend fun addTeacherToCourse(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("course_id") id: Int
    )

    @POST("api/admin/courses/{course_id}/teacherDelete")
    suspend fun deleteTeacherFromCourse(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("course_id") id: Int
    )

    @POST("api/admin/lessons/{lesson_id}/themeCreate")
    suspend fun createLessonTheme(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("lesson_id") id: Int
    )

    @PUT("api/admin/themes/{theme_id}/update")
    suspend fun updateLessonTheme(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("theme_id") id: Int
    )

    @DELETE("api/admin/themes/{theme_id}/delete")
    suspend fun deleteLessonTheme(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("theme_id") id: Int
    )
}