package com.bonch_dev.spinit.data.network.admin

import com.bonch_dev.spinit.domain.utils.Keys
import retrofit2.http.*

interface AdminHomeworkService {
    @GET("api/admin/homeworks/list")
    suspend fun getHomeworkList(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )

    @POST("api/admin/homeworks/{id}/chat")
    suspend fun chat(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )

    @POST("api/admin/homeworks/{id}/answer")
    suspend fun answerHomework(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )

    @GET("api/admin/homeworks/{id}/status/change")
    suspend fun changeHomeworkStatus(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )

    @DELETE("api/admin/homeworks/{id}/delete")
    suspend fun deleteHomework(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("id") id: Int
    )
}