package com.bonch_dev.spinit.data.network.admin

import com.bonch_dev.spinit.domain.utils.Keys
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface AdminSettingsService {
    @GET("api/admin/users/staff")
    suspend fun getAdmins(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )

    @GET("api/admin/users/roles")
    suspend fun getRoles(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )

    @POST("api/admin/users/{user_id}/setRole")
    suspend fun setRole(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("user_id") id: Int
    )

    @POST("api/admin/users/{user_id}/setPosition")
    suspend fun setPosition(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON,
        @Path("user_id") id: Int
    )

    @POST("api/admin/users/find")
    suspend fun findUser(
        @Header(Keys.AUTHORIZATION) token: String,
        @Header(Keys.ACCEPT) access: String = Keys.APP_JSON
    )
}