package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.entity.response.article.LikedPost
import com.bonch_dev.spinit.domain.entity.response.article.PostData
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ArticleService {

    @GET("api/categories")
    suspend fun getCategories(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<List<ArticleCategory>>

    @GET("api/posts/{id}")
    suspend fun getArticle(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") postId: Int
    ): Response<FullArticle>

    @GET("api/posts")
    suspend fun getPosts(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Query("category") category: String? = null
    ): Response<PostData>

    @GET("api/posts/{post_id}/like")
    suspend fun getPostsLike(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("post_id") postId: String
    ): Response<LikedPost>

    @GET("api/posts/{post_id}/unlike")
    suspend fun getPostsUnlike(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("post_id") postId: String
    ): Response<Any>
}