package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.entity.response.GoogleSignInAccessTokenDataClass
import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.entity.response.ResponseData
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import retrofit2.Response
import retrofit2.http.*

interface AuthService {
    @FormUrlEncoded
    @POST("token")
    @Headers("Content-Type:application/x-www-form-urlencoded")
    suspend fun getAccessTokenGoogle(
        @Query("prompt") prompt: String = "consent",
        @Query("access_type") access_type: String = "offline",
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("redirect_uri") redirect_uri: String,
        @Field("code") authCode: String,
        @Field("id_token") id_token: String
    ): Response<GoogleSignInAccessTokenDataClass>

    @FormUrlEncoded
    @POST("api/auth/vk/mobile")
    suspend fun authVK(@Field("access_token") accessToken: String): Response<ResponseData>

    @GET("api/auth/google/mobile")
    suspend fun authGoogle(
        @Header(ACCEPT) accept: String = APP_JSON,
        @Query("access_token") token: String?,
        @Query("id") id: String?
    ): Response<ResponseData>

    @GET("api/auth/logout")
    suspend fun logout(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Query("token") deviceToken: String
    ): Response<Message>

    @POST("api/auth/register")
    suspend fun register(
        @Header(ACCEPT) access: String = APP_JSON,
        @Body regInfo: RegistrationProfileSend
    ): Response<Message>

    @POST("api/auth/login")
    suspend fun login(
        @Header(ACCEPT) access: String = APP_JSON,
        @Body loginData: EmailPass
    ): Response<ResponseData>
}