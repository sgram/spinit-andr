package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface BroadcastService {

    @GET("api/broadcasts")
    suspend fun getBroadCasts(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<List<Broadcast>>
}