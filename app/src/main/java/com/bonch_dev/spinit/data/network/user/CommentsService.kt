package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.entity.response.article.CommentData
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface CommentsService {

    @GET("api/posts/{id}/comments")
    suspend fun getComments(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<CommentData?>

    @Multipart
    @POST("api/posts/{id}/comment")
    suspend fun sendComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int,
        @Part("text") text: RequestBody
    ): Response<Message>

    @GET("/api/comments/{comment_id}/replies")
    suspend fun getReplies(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") commentID: Int?
    ): Response<CommentData?>

    @GET("api/comments/{comment_id}/like")
    suspend fun likeComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") id: Int?
    ): Response<Message>

    @GET("api/comments/{comment_id}/dislike")
    suspend fun unLikeComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") id: Int?
    ): Response<Message>

    @Multipart
    @POST("api/comments/{comment_id}/reply")
    suspend fun replyComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") id: Int?,
        @Part("text") text: RequestBody
    ): Response<CommentData?>

    @Multipart
    @POST("api/lessons/{lesson_id}/text/comment")
    suspend fun sendLessonTextComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("lesson_id") id: Int?,
        @Part("text") text: RequestBody
    ): Response<Message>

    @Multipart
    @POST("api/lessons/{lesson_id}/video/comment")
    suspend fun sendLessonVideoComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("lesson_id") id: Int?,
        @Part("text") text: RequestBody
    ): Response<Message>

    @GET("api/lessons/{lesson_id}/text/comments")
    suspend fun getLessonTextComments(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("lesson_id") id: Int?
    ): Response<CommentData?>

    @GET("api/lessons/{lesson_id}/video/comments")
    suspend fun getLessonVideoComments(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("lesson_id") id: Int?
    ): Response<CommentData?>

    @DELETE("api/comments/{comment_id}/delete")
    suspend fun deleteComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") id: Int
    ): Response<Message>

    @FormUrlEncoded
    @PUT("api/comments/{comment_id}/update")
    suspend fun updateComment(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("comment_id") id: Int,
        @Field("text") text: String
    ): Response<Message>
}