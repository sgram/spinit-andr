package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourse
import com.bonch_dev.spinit.domain.entity.response.lesson.Status
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import com.bonch_dev.spinit.domain.utils.Keys.CONTENT_TYPE
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface CoursesService {
    @GET("api/courses")
    suspend fun getCourses(
        @Header(CONTENT_TYPE) content: String = APP_JSON,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<List<Course>>

    @GET("api/courses/mycourses")
    suspend fun getMyCourses(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<MyCourse>

    @GET("api/courses/{course}/attend")
    suspend fun attendCourse(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("course") courseID: Int
    ): Response<Message>

    @GET("api/courses/{id}")
    suspend fun getCourseInfo(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<AboutCourseInfoData>

    @GET("api/courses/{id}/status")
    suspend fun getCourseStatus(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<Status>
}