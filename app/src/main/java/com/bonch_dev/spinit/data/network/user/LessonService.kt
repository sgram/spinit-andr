package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.lesson.*
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface LessonService {

    @GET("api/lessons/{id}/text")
    suspend fun getLessonText(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<LessonText>

    @GET("api/lessons/{id}/video")
    suspend fun getLessonVideo(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<Video>

    @GET("api/lessons/{id}/test")
    suspend fun getLessonTest(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<LessonTest>

    @GET("api/lessons/{id}/homework")
    suspend fun getLessonHomework(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<LessonHomework>

    @Multipart
    @POST("api/lessons/{id}/homework/send")
    suspend fun sendHomework(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int,
        @Part("text") text: RequestBody,
        @Part files: ArrayList<MultipartBody.Part>?
    ): Response<Homework>

    @GET("api/homeworks/{homework_id}/files/get")
    suspend fun getHomeworkDocs(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("homework_id") id: Int
    ): Response<Array<String>>

    @GET("api/lessons/{id}/status")
    suspend fun getLessonStatus(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int
    ): Response<LessonStatusData>

    @POST("api/lessons/{id}/test/send")
    suspend fun sendTest(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("id") ID: Int,
        @Body tasks: SendTest
    ): Response<CheckTest>
}