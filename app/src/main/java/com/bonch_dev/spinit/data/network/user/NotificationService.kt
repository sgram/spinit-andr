package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.entity.response.notification.NotificationObject
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface NotificationService {
    @GET("/api/auth/user/notifications")
    suspend fun getNotifications(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<NotificationObject>

    @GET("api/notifications/{notification_id}/read")
    suspend fun readNotification(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("notification_id") notificationID: Int
    ): Response<Message>

    @GET("api/notifications/{notification_id}/delete")
    suspend fun deleteNotification(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Path("notification_id") notificationID: String
    ): Response<String>
}