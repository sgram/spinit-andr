package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import com.bonch_dev.spinit.domain.utils.Keys.CONTENT_TYPE
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface UpdateInfoService {
    @PUT("api/users/update")
    suspend fun updateUserInfo(
        @Header(CONTENT_TYPE) content: String = APP_JSON,
        @Header(ACCEPT) access: String = APP_JSON,
        @Header(AUTHORIZATION) token: String,
        @Query("first_name") firstName: String? = "",
        @Query("last_name") lastName: String? = "",
        @Query("email") email: String? = "",
        @Query("phone_number") phoneNumber: String? = "",
        @Query("gender") gender: String? = ""
    ): Response<Message>

    @Multipart
    @POST("api/users/update-avatar")
    suspend fun changeAvatar(
        @Part file: MultipartBody.Part,
        @Header(ACCEPT) access: String = APP_JSON,
        @Header(AUTHORIZATION) token: String
    ): Response<Message>

    @Multipart
    @POST("api/users/update-password")
    suspend fun updatePassword(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Part("password") password: RequestBody,
        @Part("new_password") newPassword: RequestBody,
        @Part("new_password_confirmation") confirmPassword: RequestBody
    ): Response<Message>

    @GET("/api/users/reset-avatar")
    suspend fun resetAvatar(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<Message>

    @Multipart
    @POST("api/auth/find-reset")
    suspend fun resetPass(
        @Part("email") email: RequestBody
    ): Response<Message>
}