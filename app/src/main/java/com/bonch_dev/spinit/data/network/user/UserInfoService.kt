package com.bonch_dev.spinit.data.network.user

import com.bonch_dev.spinit.domain.entity.response.Message
import com.bonch_dev.spinit.domain.entity.response.userinfo.UserInfoData
import com.bonch_dev.spinit.domain.utils.Keys.ACCEPT
import com.bonch_dev.spinit.domain.utils.Keys.APP_JSON
import com.bonch_dev.spinit.domain.utils.Keys.AUTHORIZATION
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface UserInfoService {

    @GET("api/auth/user")
    suspend fun getUserInfo(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON
    ): Response<UserInfoData>

    @Multipart
    @POST("/api/auth/user/new-device")
    suspend fun sendDeviceToken(
        @Header(AUTHORIZATION) token: String,
        @Header(ACCEPT) access: String = APP_JSON,
        @Part("token") deviceToken: RequestBody
    ): Response<Message>
}