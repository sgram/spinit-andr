package com.bonch_dev.spinit.data.repository.article

import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.ArticleService
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.GET_CATEGORIES
import com.bonch_dev.spinit.domain.utils.Keys.GET_FULL_ARTICLE
import com.bonch_dev.spinit.domain.utils.Keys.GET_POSTS
import com.bonch_dev.spinit.domain.utils.Keys.LIKE_POST
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.UNLIKE_POST
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class ArticleRepository @Inject constructor(private val service: ArticleService) : IArticleRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String

    init {
        App.appComponent.inject(this)
    }

    private val scope = CoroutineScope(Dispatchers.IO)

    override fun getCategories(completion: DataCompletionHandler<List<ArticleCategory>?>) {
        val j = scope.launch {
            try {
                val response = service.getCategories(token = token)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(response.body(), null)
                    else
                        completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_CATEGORIES)
            }
        }
        activeJobs[GET_CATEGORIES] = j
    }

    override fun getPosts(category: String?, completion: DataCompletionHandler<List<Post>?>) {
        val j = scope.launch {
            try {
                val response = service.getPosts(token = token, category = category)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(response.body()?.data, null)
                    else
                        completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_POSTS)
            }
        }
        activeJobs[GET_POSTS] = j
    }

    override fun likePost(position: Int, completion: DataCompletionHandler<Boolean>) {
        Log.e("test", "BlogLike: $token")
        val j = scope.launch {
            try {
                val response = service.getPostsLike(token = token, postId = (position).toString())
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else
                        completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(LIKE_POST)
            }
        }
        activeJobs[LIKE_POST] = j
    }

    override fun unlikePost(position: Int, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.getPostsUnlike(token = token, postId = (position).toString())
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else
                        completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(UNLIKE_POST)
            }
        }
        activeJobs[UNLIKE_POST] = j
    }

    override fun getFullArticle(postId: Int, completion: DataCompletionHandler<FullArticle?>) {
        val j = scope.launch {
            try {
                val response = service.getArticle(token = token, postId = postId)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(response.body(), null)
                    else
                        completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_FULL_ARTICLE)
            }
        }
        activeJobs[GET_FULL_ARTICLE] = j
    }
}