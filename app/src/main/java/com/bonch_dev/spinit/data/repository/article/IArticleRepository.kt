package com.bonch_dev.spinit.data.repository.article

import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface IArticleRepository {
    fun getPosts(category: String?, completion: DataCompletionHandler<List<Post>?>)
    fun likePost(position: Int, completion: DataCompletionHandler<Boolean>)
    fun unlikePost(position: Int, completion: DataCompletionHandler<Boolean>)
    fun getFullArticle(postId: Int, completion: DataCompletionHandler<FullArticle?>)
    fun getCategories(completion: DataCompletionHandler<List<ArticleCategory>?>)
}