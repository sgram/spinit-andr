package com.bonch_dev.spinit.data.repository.auth

import com.bonch_dev.spinit.data.network.user.AuthService
import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.entity.response.ResponseData
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.SIGN_IN
import com.bonch_dev.spinit.domain.utils.Keys.SIGN_UP
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CasualAuthRepository @Inject constructor(private val service: AuthService) : ICasualAuthRepository {

    private val scope = CoroutineScope(Dispatchers.IO)

    override fun signIn(loginData: EmailPass, completion: DataCompletionHandler<ResponseData?>) {
        val j = scope.launch {
            try {
                val response = service.login(loginData = loginData)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(SIGN_IN)
            }
        }
        activeJobs[SIGN_IN] = j
    }

    override fun signUp(profile: RegistrationProfileSend, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.register(regInfo = profile)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(SIGN_UP)
            }
        }
        activeJobs[SIGN_UP] = j
    }
}