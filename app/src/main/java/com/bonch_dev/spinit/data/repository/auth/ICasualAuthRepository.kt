package com.bonch_dev.spinit.data.repository.auth

import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.entity.response.ResponseData
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface ICasualAuthRepository {
    fun signUp(profile: RegistrationProfileSend, completion: DataCompletionHandler<Boolean>)
    fun signIn(loginData: EmailPass, completion: DataCompletionHandler<ResponseData?>)
}