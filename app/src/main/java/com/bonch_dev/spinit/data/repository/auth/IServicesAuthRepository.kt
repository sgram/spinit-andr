package com.bonch_dev.spinit.data.repository.auth

import com.bonch_dev.spinit.domain.entity.response.GoogleSignInAccessTokenDataClass
import com.bonch_dev.spinit.domain.entity.response.ResponseData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import com.vk.sdk.VKAccessToken

interface IServicesAuthRepository {
    fun getAccessToken(authCode: String, idToken: String,completion:DataCompletionHandler<GoogleSignInAccessTokenDataClass?>)
    fun authGoogle(token: String?, id: String?, completion: DataCompletionHandler<ResponseData?>)
    fun authVK(res: VKAccessToken, completion: DataCompletionHandler<ResponseData?>)
}