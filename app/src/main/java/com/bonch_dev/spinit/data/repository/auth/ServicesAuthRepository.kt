package com.bonch_dev.spinit.data.repository.auth

import android.util.Log
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.data.network.user.AuthService
import com.bonch_dev.spinit.domain.entity.response.GoogleSignInAccessTokenDataClass
import com.bonch_dev.spinit.domain.entity.response.ResponseData
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.AUTH_GOOGLE
import com.bonch_dev.spinit.domain.utils.Keys.AUTH_VK
import com.bonch_dev.spinit.domain.utils.Keys.GOOGLE_ACCESS_TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.domain.utils.ResourceManager.getString
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import com.vk.sdk.VKAccessToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class ServicesAuthRepository @Inject constructor(private val service: AuthService) : IServicesAuthRepository {
    private val scope = CoroutineScope(Dispatchers.IO)

    override fun getAccessToken(
        authCode: String,
        idToken: String,
        completion: DataCompletionHandler<GoogleSignInAccessTokenDataClass?>
    ) {
        val retrofit = Retrofit.Builder().addConverterFactory(MoshiConverterFactory.create())
            .baseUrl("https://www.googleapis.com/oauth2/v4/").build().create(AuthService::class.java)
        val j = scope.launch {
            try {
                val response = retrofit.getAccessTokenGoogle(
                    grant_type = "authorization_code",
                    client_id = getString(R.string.server_client_id),
                    client_secret = getString(R.string.client_secret),
                    redirect_uri = "",
                    authCode = authCode,
                    id_token = idToken
                )
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        Log.e(this@ServicesAuthRepository.javaClass.simpleName, "Success\n${response.body()}")
                        completion(response.body(), null)
                    } else
                        completion(null, ErrType(response.code()))

                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GOOGLE_ACCESS_TOKEN)
            }
        }
        activeJobs[AUTH_GOOGLE] = j
    }

    override fun authGoogle(token: String?, id: String?, completion: DataCompletionHandler<ResponseData?>) {
        val j = scope.launch {
            try {
                val response = service.authGoogle(token = token, id = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(AUTH_GOOGLE)
            }
        }
        activeJobs[AUTH_GOOGLE] = j
    }

    override fun authVK(res: VKAccessToken, completion: DataCompletionHandler<ResponseData?>) {
        val j = scope.launch {
            try {
                val response = service.authVK(accessToken = res.accessToken.toString())
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                        Log.e("RequestVKAuth", "${data?.token}")
                    } else {
                        completion(null, ErrType(response.code()))
                        Log.e("RequestVKAuth", response.code().toString())
                    }
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(AUTH_VK)
            }
        }
        activeJobs[AUTH_VK] = j
    }
}