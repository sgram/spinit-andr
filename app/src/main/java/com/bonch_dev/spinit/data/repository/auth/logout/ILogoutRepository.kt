package com.bonch_dev.spinit.data.repository.auth.logout

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface ILogoutRepository {
    fun logout(completion: DataCompletionHandler<Boolean>)
}