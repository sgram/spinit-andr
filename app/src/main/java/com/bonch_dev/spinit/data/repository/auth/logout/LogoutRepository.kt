package com.bonch_dev.spinit.data.repository.auth.logout

import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.AuthService
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.DEV_TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.LOGOUT
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class LogoutRepository @Inject constructor(private val service: AuthService) :
    ILogoutRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String

    @Inject
    @Named(DEV_TOKEN)
    lateinit var deviceToken: String

    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun logout(completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            Log.e("testToken", "logout:$token")
            try {
                val response = service.logout(token = token, deviceToken = deviceToken)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(LOGOUT)
            }
        }
        activeJobs[LOGOUT] = j
    }
}