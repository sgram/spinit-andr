package com.bonch_dev.spinit.data.repository.broadcasts

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.BroadcastService
import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.GET_BROADCASTS
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class BroadcastsRepository @Inject constructor(private val service: BroadcastService) : IBroadcastRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun getBroadcast(completion: DataCompletionHandler<List<Broadcast>?>) {
        val j = scope.launch {
            try {
                val response = service.getBroadCasts(token = token)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_BROADCASTS)
            }
        }
        activeJobs[GET_BROADCASTS] = j
    }
}