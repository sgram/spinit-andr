package com.bonch_dev.spinit.data.repository.broadcasts

import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface IBroadcastRepository {
    fun getBroadcast(completion: DataCompletionHandler<List<Broadcast>?>)
}