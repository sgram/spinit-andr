package com.bonch_dev.spinit.data.repository.comments

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.CommentsService
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.entity.response.article.CommentData
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.DELETE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_ARTICLE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_LESSON_TEXT_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_LESSON_VIDEO_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_REPLIES
import com.bonch_dev.spinit.domain.utils.Keys.LIKE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.REPLY_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.SEND_ARTICLE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.SEND_LESSON_TEXT_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.SEND_LESSON_VIDEO_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.UNLIKE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.UPDATE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.RequestBody
import javax.inject.Inject
import javax.inject.Named

class CommentsRepository @Inject constructor(private val service: CommentsService) :
    ICommentsRepository {

    @Inject
    @Named(TOKEN)
    lateinit var token: String

    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun getArticleComments(postId: Int, completion: DataCompletionHandler<CommentData?>) {
        val j = scope.launch {
            try {
                val response = service.getComments(token = token, ID = postId)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_ARTICLE_COMMENT)
            }
        }
        activeJobs[GET_ARTICLE_COMMENT] = j
    }

    override fun sendArticleComment(postID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.sendComment(token = token, ID = postID, text = text)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(SEND_ARTICLE_COMMENT)
            }
        }
        activeJobs[SEND_ARTICLE_COMMENT] = j
    }

    override fun likeComment(commentID: Int?, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.likeComment(token = token, id = commentID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(LIKE_COMMENT)
            }
        }
        activeJobs[LIKE_COMMENT] = j
    }

    override fun unLikeComment(commentID: Int?, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.unLikeComment(token = token, id = commentID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(UNLIKE_COMMENT)
            }
        }
        activeJobs[UNLIKE_COMMENT] = j
    }

    override fun replyComment(commentID: Int?, text: RequestBody, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.replyComment(token = token, id = commentID, text = text)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(true, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(REPLY_COMMENT)
            }
        }
        activeJobs[REPLY_COMMENT] = j
    }

    override fun sendLessonTextComment(lessonID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.sendLessonTextComment(token = token, id = lessonID, text = text)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(SEND_LESSON_TEXT_COMMENT)
            }
        }
        activeJobs[SEND_LESSON_TEXT_COMMENT] = j
    }

    override fun sendLessonVideoComment(lessonID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.sendLessonVideoComment(token = token, id = lessonID, text = text)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(SEND_LESSON_VIDEO_COMMENT)
            }
        }
        activeJobs[SEND_LESSON_VIDEO_COMMENT] = j
    }

    override fun getLessonTextComments(lessonID: Int, completion: DataCompletionHandler<CommentData?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonTextComments(token = token, id = lessonID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_LESSON_TEXT_COMMENT)
            }
        }
        activeJobs[GET_LESSON_TEXT_COMMENT] = j
    }

    override fun getLessonVideoComments(lessonID: Int, completion: DataCompletionHandler<CommentData?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonVideoComments(token = token, id = lessonID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_LESSON_VIDEO_COMMENT)
            }
        }
        activeJobs[GET_LESSON_VIDEO_COMMENT] = j
    }

    override fun getReplies(commentID: Int?, completion: DataCompletionHandler<List<Comment>?>) {
        val j = scope.launch {
            try {
                val response = service.getReplies(token = token, commentID = commentID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        completion(data?.data, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: java.lang.Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_REPLIES)
            }
        }
        activeJobs[GET_REPLIES] = j
    }

    override fun updateComment(commentID: Int, text: String, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.updateComment(token = token, id = commentID, text = text)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: java.lang.Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(UPDATE_COMMENT)
            }
        }
        activeJobs[UPDATE_COMMENT] = j
    }

    override fun deleteComment(commentID: Int, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.deleteComment(token = token, id = commentID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: java.lang.Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(DELETE_COMMENT)
            }
        }
        activeJobs[DELETE_COMMENT] = j
    }
}