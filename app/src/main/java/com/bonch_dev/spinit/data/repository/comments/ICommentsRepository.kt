package com.bonch_dev.spinit.data.repository.comments

import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.entity.response.article.CommentData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import okhttp3.RequestBody

interface ICommentsRepository {
    fun deleteComment(commentID: Int, completion: DataCompletionHandler<Boolean>)
    fun updateComment(commentID: Int, text: String, completion: DataCompletionHandler<Boolean>)
    fun getArticleComments(postId: Int, completion: DataCompletionHandler<CommentData?>)
    fun sendArticleComment(postID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>)
    fun likeComment(commentID: Int?, completion: DataCompletionHandler<Boolean>)
    fun unLikeComment(commentID: Int?, completion: DataCompletionHandler<Boolean>)
    fun replyComment(commentID: Int?, text: RequestBody, completion: DataCompletionHandler<Boolean>)
    fun sendLessonTextComment(lessonID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>)
    fun sendLessonVideoComment(lessonID: Int, text: RequestBody, completion: DataCompletionHandler<Boolean>)
    fun getLessonTextComments(lessonID: Int, completion: DataCompletionHandler<CommentData?>)
    fun getLessonVideoComments(lessonID: Int, completion: DataCompletionHandler<CommentData?>)
    fun getReplies(commentID: Int?, completion: DataCompletionHandler<List<Comment>?>)
}