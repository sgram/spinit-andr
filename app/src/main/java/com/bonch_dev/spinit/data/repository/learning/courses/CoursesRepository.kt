package com.bonch_dev.spinit.data.repository.learning.courses

import android.content.SharedPreferences
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.CoursesService
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.ATTEND
import com.bonch_dev.spinit.domain.utils.Keys.GET_ALL_COURSES
import com.bonch_dev.spinit.domain.utils.Keys.GET_COURSE_INFO
import com.bonch_dev.spinit.domain.utils.Keys.GET_COURSE_STATUS
import com.bonch_dev.spinit.domain.utils.Keys.GET_MY_COURSES
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class CoursesRepository @Inject constructor(private val service: CoursesService) : ICoursesRepository {

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
        token = sp.getString(TOKEN, "") ?: ""
    }

    override fun getAllCourses(completion: DataCompletionHandler<List<Course>?>) {
        val j = scope.launch {
            try {
                val response = service.getCourses()
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_ALL_COURSES)
            }
        }
        activeJobs[GET_ALL_COURSES] = j
    }

    override fun getMyCourses(completion: DataCompletionHandler<List<MyCourseInfo>?>) {
        val j = scope.launch {
            try {
                val response = service.getMyCourses(token = sp.getString(TOKEN, "") ?: "")
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body()?.courses, null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_MY_COURSES)
            }
        }
        activeJobs[GET_MY_COURSES] = j
    }

    override fun getStatus(id: Int, completion: DataCompletionHandler<StatusData?>) {
        val j = scope.launch {
            try {
                val response = service.getCourseStatus(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body()?.data, null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_COURSE_STATUS)
            }
        }
        activeJobs[GET_COURSE_STATUS] = j
    }

    override fun getCourseInfo(courseID: Int, completion: DataCompletionHandler<AboutCourseInfoData?>) {
        val j = scope.launch {
            try {
                val response = service.getCourseInfo(token = token, ID = courseID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_COURSE_INFO)
            }
        }
        activeJobs[GET_COURSE_INFO] = j
    }

    override fun attend(courseID: Int, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.attendCourse(token = token, courseID = courseID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(ATTEND)
            }
        }
        activeJobs[ATTEND] = j
    }
}