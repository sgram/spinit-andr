package com.bonch_dev.spinit.data.repository.learning.courses

import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface ICoursesRepository {
    fun getCourseInfo(courseID: Int, completion: DataCompletionHandler<AboutCourseInfoData?>)
    fun attend(courseID: Int, completion: DataCompletionHandler<Boolean>)
    fun getStatus(id: Int, completion: DataCompletionHandler<StatusData?>)
    fun getMyCourses(completion: DataCompletionHandler<List<MyCourseInfo>?>)
    fun getAllCourses(completion: DataCompletionHandler<List<Course>?>)
}