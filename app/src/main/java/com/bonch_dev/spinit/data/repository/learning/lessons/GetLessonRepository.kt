package com.bonch_dev.spinit.data.repository.learning.lessons

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.LessonService
import com.bonch_dev.spinit.domain.entity.response.lesson.*
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.GET_HOMEWORK
import com.bonch_dev.spinit.domain.utils.Keys.GET_HOMEWORK_STATUS
import com.bonch_dev.spinit.domain.utils.Keys.GET_TEST
import com.bonch_dev.spinit.domain.utils.Keys.GET_TEXT
import com.bonch_dev.spinit.domain.utils.Keys.GET_VIDEO
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class GetLessonRepository @Inject constructor(private val service: LessonService) : IGetLessonRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun getLessonText(id: Int, completion: DataCompletionHandler<LessonText?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonText(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_TEXT)
            }
        }
        activeJobs[GET_TEXT] = j
    }

    override fun getLessonVideo(id: Int, completion: DataCompletionHandler<Video?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonVideo(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_VIDEO)
            }
        }
        activeJobs[GET_VIDEO] = j
    }

    override fun getLessonTest(id: Int, completion: DataCompletionHandler<LessonTest?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonTest(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_TEST)
            }
        }
        activeJobs[GET_TEST] = j
    }

    override fun getLessonHomework(id: Int, completion: DataCompletionHandler<LessonHomework?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonHomework(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_HOMEWORK)
            }
        }
        activeJobs[GET_HOMEWORK] = j
    }

    override fun getLessonStatus(id: Int, completion: DataCompletionHandler<LessonStatusData?>) {
        val j = scope.launch {
            try {
                val response = service.getLessonStatus(token = token, ID = id)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body(), null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_HOMEWORK_STATUS)
            }
        }
        activeJobs[GET_HOMEWORK_STATUS] = j
    }
}