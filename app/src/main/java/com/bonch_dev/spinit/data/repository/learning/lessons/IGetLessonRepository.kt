package com.bonch_dev.spinit.data.repository.learning.lessons

import com.bonch_dev.spinit.domain.entity.response.lesson.*
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface IGetLessonRepository {
    fun getLessonText(id: Int, completion: DataCompletionHandler<LessonText?>)
    fun getLessonVideo(id: Int, completion: DataCompletionHandler<Video?>)
    fun getLessonTest(id: Int, completion: DataCompletionHandler<LessonTest?>)
    fun getLessonHomework(id: Int, completion: DataCompletionHandler<LessonHomework?>)
    fun getLessonStatus(id: Int, completion: DataCompletionHandler<LessonStatusData?>)
}