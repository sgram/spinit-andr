package com.bonch_dev.spinit.data.repository.learning.lessons

import com.bonch_dev.spinit.domain.entity.response.lesson.Check
import com.bonch_dev.spinit.domain.entity.response.lesson.SendTest
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import okhttp3.MultipartBody

interface ISendLessonRepository {
    fun sendHomework(text: String, id: Int, files: ArrayList<MultipartBody.Part>?, completion: DataCompletionHandler<Boolean>)
    fun send(body: SendTest, id: Int, completion: DataCompletionHandler<List<Check>?>)
}