package com.bonch_dev.spinit.data.repository.learning.lessons

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.LessonService
import com.bonch_dev.spinit.domain.entity.response.lesson.Check
import com.bonch_dev.spinit.domain.entity.response.lesson.SendTest
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.SEND_HOMEWORK
import com.bonch_dev.spinit.domain.utils.Keys.SEND_TEST
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.domain.utils.toRequestBody
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import javax.inject.Inject
import javax.inject.Named

class SendLessonRepository @Inject constructor(private val service: LessonService) : ISendLessonRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun send(body: SendTest, id: Int, completion: DataCompletionHandler<List<Check>?>) {
        val j = scope.launch {
            try {
                val response = service.sendTest(token = token, ID = id, tasks = body)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        val data = response.body()
                        val correct = data?.check
                        completion(correct, null)
                    } else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(SEND_TEST)
            }
        }
        activeJobs[SEND_TEST] = j
    }

    override fun sendHomework(
        text: String,
        id: Int,
        files: ArrayList<MultipartBody.Part>?,
        completion: DataCompletionHandler<Boolean>
    ) {
        val j = scope.launch {
            try {
                val response = service.sendHomework(
                    token = token,
                    ID = id,
                    text = text.toRequestBody(),
                    files = files
                )
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(SEND_HOMEWORK)
            }
        }
        activeJobs[SEND_HOMEWORK] = j
    }
}