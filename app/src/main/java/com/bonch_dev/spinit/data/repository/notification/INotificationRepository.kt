package com.bonch_dev.spinit.data.repository.notification

import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface INotificationRepository {
    fun getNotifications(completion: DataCompletionHandler<List<Notification>?>)
    fun readNotification(notificationID: Int, completion: DataCompletionHandler<Boolean>)
    fun deleteNotification(notificationID: String, completion: DataCompletionHandler<Boolean>)
}