package com.bonch_dev.spinit.data.repository.notification

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.NotificationService
import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.DELETE_NOTIFICATION
import com.bonch_dev.spinit.domain.utils.Keys.GET_NOTIFICATIONS
import com.bonch_dev.spinit.domain.utils.Keys.READ_NOTIFICATION
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class NotificationRepository @Inject constructor(private val service: NotificationService) : INotificationRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun getNotifications(completion: DataCompletionHandler<List<Notification>?>) {
        val j = scope.launch {
            try {
                val response = service.getNotifications(token = token)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(response.body()?.data, null)
                    else completion(null, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(GET_NOTIFICATIONS)
            }
        }
        activeJobs[GET_NOTIFICATIONS] = j
    }

    override fun readNotification(notificationID: Int, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.readNotification(token = token, notificationID = notificationID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(READ_NOTIFICATION)
            }
        }
        activeJobs[READ_NOTIFICATION] = j
    }

    override fun deleteNotification(notificationID: String, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.deleteNotification(token = token, notificationID = notificationID)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(DELETE_NOTIFICATION)
            }
        }
        activeJobs[DELETE_NOTIFICATION] = j
    }
}