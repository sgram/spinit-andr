package com.bonch_dev.spinit.data.repository.profile.password

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface IPasswordRepository {

    fun changePassword(
        oldPass: String,
        newPass: String,
        confirmPass: String,
        completion: DataCompletionHandler<Boolean>
    )

    fun forgot(email: String, completion: DataCompletionHandler<Boolean>)
}