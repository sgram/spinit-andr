package com.bonch_dev.spinit.data.repository.profile.password

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.UpdateInfoService
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.CHANGE_PASS
import com.bonch_dev.spinit.domain.utils.Keys.FORGOT
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.domain.utils.toRequestBody
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class PasswordRepository @Inject constructor(private val service: UpdateInfoService) : IPasswordRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String
    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun changePassword(
        oldPass: String,
        newPass: String,
        confirmPass: String,
        completion: DataCompletionHandler<Boolean>
    ) {
        val j = scope.launch {
            try {
                val response = service.updatePassword(
                    token = token,
                    password = oldPass.toRequestBody(),
                    newPassword = newPass.toRequestBody(),
                    confirmPassword = confirmPass.toRequestBody()
                )
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                    activeJobs.remove(CHANGE_PASS)
                }
            } catch (err: Exception) {
                activeJobs.remove(CHANGE_PASS)
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            }
        }
        activeJobs[CHANGE_PASS] = j
    }

    override fun forgot(email: String, completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.resetPass(
                    email = email.toRequestBody()
                )
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(FORGOT)
            }
        }
        activeJobs[FORGOT] = j
    }
}