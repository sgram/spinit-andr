package com.bonch_dev.spinit.data.repository.profile.update_info

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import okhttp3.MultipartBody

interface IUpdateInfoRepository {
    fun updateData(
        body: MultipartBody.Part?,
        gender: String?,
        name: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        completion: DataCompletionHandler<Boolean>
    )
    fun reset(completion: DataCompletionHandler<Boolean>)
}