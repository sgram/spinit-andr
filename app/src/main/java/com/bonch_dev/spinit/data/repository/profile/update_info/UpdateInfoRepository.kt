package com.bonch_dev.spinit.data.repository.profile.update_info

import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.UpdateInfoService
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.RESET_AVATAR
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.UPDATE_AVATAR
import com.bonch_dev.spinit.domain.utils.Keys.UPDATE_DATA
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MultipartBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Named

class UpdateInfoRepository @Inject constructor(private val service: UpdateInfoService) : IUpdateInfoRepository {
    @Inject
    @Named(TOKEN)
    lateinit var token: String

    private val scope = CoroutineScope(Dispatchers.IO)
    private val deferredList: HashMap<String, Response<*>> = hashMapOf()

    init {
        App.appComponent.inject(this)
    }

    override fun updateData(
        body: MultipartBody.Part?,
        gender: String?,
        name: String?,
        lastName: String?,
        email: String?,
        phone: String?,
        completion: DataCompletionHandler<Boolean>
    ) {
        val j = scope.launch {
            try {
                scope.launch {
                    launch {
                        try {
                            Log.e(this@UpdateInfoRepository.javaClass.simpleName, "updateData: started")
                            val response = service.updateUserInfo(
                                token = token,
                                firstName = name,
                                lastName = lastName,
                                email = email,
                                phoneNumber = phone,
                                gender = gender
                            )
                            deferredList[UPDATE_DATA] = response
                            Log.e(this@UpdateInfoRepository.javaClass.simpleName, "updateData: finished")
                        } catch (err: Exception) {
                            withContext(Dispatchers.Main) {
                                val errRes = ExceptionHandler.handle(err)
                                completion(false, errRes)
                            }
                        }
                    }
                    if (body != null) {
                        launch {
                            try {
                                Log.e(this@UpdateInfoRepository.javaClass.simpleName, "updateAvatar: started")
                                val response = service.changeAvatar(file = body, token = token)
                                deferredList[UPDATE_AVATAR] = response
                                Log.e(this@UpdateInfoRepository.javaClass.simpleName, "updateAvatar: finished")
                            } catch (err: Exception) {
                                withContext(Dispatchers.Main) {
                                    val errRes = ExceptionHandler.handle(err)
                                    completion(false, errRes)
                                }
                            }
                        }
                    }
                }.join()
                withContext(Dispatchers.Main) {
                    deferredList.let {
                        when {
                            it.all { value -> value.value.isSuccessful } -> completion(true, null)
                            it[UPDATE_DATA]?.isSuccessful != true -> completion(false, ErrType(it[UPDATE_DATA]?.code()))
                            it[UPDATE_AVATAR]?.isSuccessful != true -> completion(false, ErrType(it[UPDATE_DATA]?.code()))
                        }
                    }
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(UPDATE_DATA)
            }
        }
        activeJobs[UPDATE_DATA] = j
    }

    override fun reset(completion: DataCompletionHandler<Boolean>) {
        val j = scope.launch {
            try {
                val response = service.resetAvatar(token = token)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful)
                        completion(true, null)
                    else completion(false, ErrType(response.code()))
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(false, errRes)
                }
            } finally {
                activeJobs.remove(RESET_AVATAR)
            }
        }
        activeJobs[RESET_AVATAR] = j
    }
}