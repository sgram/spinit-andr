package com.bonch_dev.spinit.data.repository.profile.userinfo

import com.bonch_dev.spinit.domain.entity.response.userinfo.UserInfoData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler

interface IUserInfoRepository {
    fun getUserInfo(needToSendToken: Boolean, completion: DataCompletionHandler<UserInfoData?>)
}