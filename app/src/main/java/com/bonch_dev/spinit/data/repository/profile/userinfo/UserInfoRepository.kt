package com.bonch_dev.spinit.data.repository.profile.userinfo

import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.network.user.UserInfoService
import com.bonch_dev.spinit.domain.entity.response.userinfo.UserInfoData
import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.domain.utils.ExceptionHandler
import com.bonch_dev.spinit.domain.utils.Keys.DEV_TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.USER_INFO
import com.bonch_dev.spinit.domain.utils.Keys.activeJobs
import com.bonch_dev.spinit.domain.utils.toRequestBody
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataCompletionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Named

class UserInfoRepository @Inject constructor(private val service: UserInfoService) : IUserInfoRepository {

    @Inject
    @Named(TOKEN)
    lateinit var token: String

    @Inject
    @Named(DEV_TOKEN)
    lateinit var deviceToken: String

    private val scope = CoroutineScope(Dispatchers.IO)

    init {
        App.appComponent.inject(this)
    }

    override fun getUserInfo(needToSendToken: Boolean, completion: DataCompletionHandler<UserInfoData?>) {
        val j = scope.launch {
            Log.e("testToken", "userInfo: $token")
            Log.e("testToken", needToSendToken.toString())
            try {
                val deferredList: HashMap<String, Response<*>> = hashMapOf()
                scope.launch {
                    launch {
                        try {
                            val userInfo = service.getUserInfo(token = token)
                            deferredList[USER_INFO] = userInfo
                        } catch (ex: Exception) {
                            withContext(Dispatchers.Main) {
                                val errRes = ExceptionHandler.handle(ex)
                                completion(null, errRes)
                            }
                        }
                    }
                    if (needToSendToken) {
                        launch {
                            try {
                                val devToken =
                                    service.sendDeviceToken(deviceToken = deviceToken.toRequestBody(), token = token)
                                deferredList[DEV_TOKEN] = devToken
                            } catch (ex: Exception) {
                                withContext(Dispatchers.Main) {
                                    val errRes = ExceptionHandler.handle(ex)
                                    completion(null, errRes)
                                }
                            }
                        }
                    }
                }.join()
                withContext(Dispatchers.Main) {
                    deferredList.let {
                        when {
                            it.all { value -> value.value.isSuccessful } -> completion(it[USER_INFO]?.body() as UserInfoData?, null)
                            it[USER_INFO]?.isSuccessful != true -> completion(null, ErrType(it[USER_INFO]?.code()))
                            it[DEV_TOKEN]?.isSuccessful != true -> completion(null, ErrType(it[DEV_TOKEN]?.code()))
                        }
                    }
                }
            } catch (err: Exception) {
                withContext(Dispatchers.Main) {
                    val errRes = ExceptionHandler.handle(err)
                    completion(null, errRes)
                }
            } finally {
                activeJobs.remove(USER_INFO)
            }
        }
        activeJobs[USER_INFO] = j
    }
}