package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.presentation.modules.about_app.presenter.AboutPresenter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.ContactPresenter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.IAboutPresenter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.IContactPresenter
import dagger.Module
import dagger.Provides

@Module
class AboutAppModule {
    @Provides
    fun provideContactPresenter(): IContactPresenter = ContactPresenter()

    @Provides
    fun provideAboutPresenter(): IAboutPresenter = AboutPresenter()
}