package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.repository.article.ArticleRepository
import com.bonch_dev.spinit.data.repository.auth.logout.LogoutRepository
import com.bonch_dev.spinit.data.repository.broadcasts.BroadcastsRepository
import com.bonch_dev.spinit.data.repository.comments.CommentsRepository
import com.bonch_dev.spinit.data.repository.learning.courses.CoursesRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.GetLessonRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.SendLessonRepository
import com.bonch_dev.spinit.data.repository.notification.NotificationRepository
import com.bonch_dev.spinit.data.repository.profile.password.PasswordRepository
import com.bonch_dev.spinit.data.repository.profile.update_info.UpdateInfoRepository
import com.bonch_dev.spinit.data.repository.profile.userinfo.UserInfoRepository
import com.bonch_dev.spinit.domain.interactors.auth.signin.SignInInteractor
import com.bonch_dev.spinit.domain.interactors.update_info.UpdateInfoInteractor
import com.bonch_dev.spinit.presentation.base.presenters.BottomNavigationPresenter
import com.bonch_dev.spinit.presentation.base.view.Base
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.about_app.view.AboutFragment
import com.bonch_dev.spinit.presentation.modules.about_app.view.ContactFragment
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticlesAdapter
import com.bonch_dev.spinit.presentation.modules.article.presenter.FullArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.view.FullArticleFragment
import com.bonch_dev.spinit.presentation.modules.article.view.SavedArticlesFragment
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.SignInPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.view.SignInFragment
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.SignUpStepTwoPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.SignUpStepOneFragment
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.SignUpStepTwoFragment
import com.bonch_dev.spinit.presentation.modules.blog.presenter.BlogPresenter
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.broadcast.adapters.BroadcastsAdapter
import com.bonch_dev.spinit.presentation.modules.broadcast.view.BroadcastFragment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.CommentsAdapter
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.CommentsPresenter
import com.bonch_dev.spinit.presentation.modules.comments.view.CommentETDialog
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter.AboutCoursePresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view.AboutCourseFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.adapter.AllCoursesAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter.AllCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.AllCoursesFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.adapter.MyCoursesAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.presenter.MyCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view.MyCoursesFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.HomeworkPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter.LectionPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.view.LectionFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.adapters.LessonCategoryAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter.LessonCategoryPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.view.LessonCategoryFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.adapters.LessonsAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter.LessonsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.view.LessonsFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter.MaterialsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.view.MaterialsFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter.TestPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.view.TestFragment
import com.bonch_dev.spinit.presentation.modules.notification.adapters.PushNotificationsAdapter
import com.bonch_dev.spinit.presentation.modules.notification.presenter.NotificationPresenter
import com.bonch_dev.spinit.presentation.modules.notification.view.NotificationFragment
import com.bonch_dev.spinit.presentation.modules.onboarding.presenter.OnBoardingPresenter
import com.bonch_dev.spinit.presentation.modules.onboarding.view.OnBoardingFragment
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.MainFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.ChangePasswordPresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.EditProfilePresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.ChangePasswordFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.EditProfileFragment
import com.bonch_dev.spinit.presentation.modules.settings.adapters.SettingsAdapter
import com.bonch_dev.spinit.presentation.modules.settings.presenter.settings.SettingsPresenter
import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment
import com.bonch_dev.spinit.presentation.modules.teachermsg.view.TeacherMessagesFragment
import com.bonch_dev.spinit.services.NotifService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        RouterModule::class,
        AppModule::class,
        NetworkModule::class,
        LessonModule::class,
        ArticleModule::class,
        AboutAppModule::class,
        CourseModule::class,
        BroadcastModule::class,
        LogoutModule::class,
        NotificationModule::class,
        UserInfoModule::class,
        UpdateInfoModule::class,
        AuthModule::class,
        SettingsModule::class,
        BlogModule::class,
        OnBoardingModule::class
    ]
)
interface AppComponent {
    // Application
    fun inject(app: App)
    fun inject(base: Base)

    // Interactors
    fun inject(signInInteractor: SignInInteractor)
    fun inject(updateInfoInteractor: UpdateInfoInteractor)

    // Activity
    fun inject(mainActivity: MainActivity)
    fun inject(lessonActivity: LessonActivity)

    // View
    fun inject(savedArticlesFragment: SavedArticlesFragment)
    fun inject(aboutFragment: AboutFragment)
    fun inject(contactFragment: ContactFragment)
    fun inject(notificationFragment: NotificationFragment)
    fun inject(signUpStepOneFragment: SignUpStepOneFragment)
    fun inject(onBoardingFragment: OnBoardingFragment)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(signInFragment: SignInFragment)
    fun inject(bottomNavigationFragment: BottomNavigationFragment)
    fun inject(signUpStepTwoFragment: SignUpStepTwoFragment)
    fun inject(testFragment: TestFragment)
    fun inject(lessonCategoryFragment: LessonCategoryFragment)
    fun inject(lessonsFragment: LessonsFragment)
    fun inject(lectionFragment: LectionFragment)
    fun inject(materialsFragment: MaterialsFragment)
    fun inject(homeworkFragment: HomeworkFragment)
    fun inject(changePasswordFragment: ChangePasswordFragment)
    fun inject(fullArticleFragment: FullArticleFragment)
    fun inject(blogFragment: BlogFragment)
    fun inject(myCoursesFragment: MyCoursesFragment)
    fun inject(aboutCourseActivity: AboutCourseFragment)
    fun inject(allCoursesFragment: AllCoursesFragment)
    fun inject(editProfileFragment: EditProfileFragment)
    fun inject(commentETDialog: CommentETDialog)
    fun inject(mainFragment: MainFragment)
    fun inject(teacherMessagesFragment: TeacherMessagesFragment)
    fun inject(broadcastFragment: BroadcastFragment)

    // Adapters
    fun inject(myCoursesAdapter: MyCoursesAdapter)
    fun inject(pushNotificationsAdapter: PushNotificationsAdapter)
    fun inject(lessonsAdapter: LessonsAdapter)
    fun inject(allCoursesAdapter: AllCoursesAdapter)
    fun inject(broadcastsAdapter: BroadcastsAdapter)
    fun inject(lessonCategoryAdapter: LessonCategoryAdapter)
    fun inject(settingsAdapter: SettingsAdapter)
    fun inject(commentsAdapter: CommentsAdapter)
    fun inject(articlesAdapter: ArticlesAdapter)
    fun inject(repliesAdapter: RepliesAdapter)

    // Repository
    fun inject(iCommentsRepository: CommentsRepository)
    fun inject(iUserInfoRepository: UserInfoRepository)
    fun inject(iUpdateInfoRepository: UpdateInfoRepository)
    fun inject(iPasswordRepository: PasswordRepository)
    fun inject(iNotificationRepository: NotificationRepository)
    fun inject(iSendLessonRepository: SendLessonRepository)
    fun inject(iGetLessonRepository: GetLessonRepository)
    fun inject(iCoursesRepository: CoursesRepository)
    fun inject(iBroadcastRepository: BroadcastsRepository)
    fun inject(iLogoutRepository: LogoutRepository)
    fun inject(iPostsRepository: ArticleRepository)

    // Presenters
    fun inject(onBoardingPresenter: OnBoardingPresenter)
    fun inject(signUpStepTwoPresenter: SignUpStepTwoPresenter)
    fun inject(myCoursesPresenter: MyCoursesPresenter)
    fun inject(allCoursesPresenter: AllCoursesPresenter)
    fun inject(lessonCategoryPresenter: LessonCategoryPresenter)
    fun inject(lessonsPresenter: LessonsPresenter)
    fun inject(materialsPresenter: MaterialsPresenter)
    fun inject(lectionPresenter: LectionPresenter)
    fun inject(homeworkPresenter: HomeworkPresenter)
    fun inject(testPresenter: TestPresenter)
    fun inject(notificationPresenter: NotificationPresenter)
    fun inject(editProfilePresenter: EditProfilePresenter)
    fun inject(commentsPresenter: CommentsPresenter)
    fun inject(changePasswordPresenter: ChangePasswordPresenter)
    fun inject(blogPresenter: BlogPresenter)
    fun inject(fullArticlePresenter: FullArticlePresenter)
    fun inject(aboutCoursePresenter: AboutCoursePresenter)
    fun inject(bottomNavigationPresenter: BottomNavigationPresenter)
    fun inject(signInPresenter: SignInPresenter)
    fun inject(settingsPresenter: SettingsPresenter)

    fun inject(notifService: NotifService)
}