package com.bonch_dev.spinit.di

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.bonch_dev.spinit.domain.interactors.userinfo.IUserInfoInteractor
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_THEME
import com.bonch_dev.spinit.domain.utils.Keys.DEV_TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.NOTIFICATION
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.USER
import com.bonch_dev.spinit.presentation.base.presenters.BottomNavigationPresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBottomNavigationPresenter
import com.bonch_dev.spinit.router.MainRouter
import com.bonch_dev.spinit.router.SettingsRouter
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule constructor(val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    fun provideMainPresenter(
        iUserInfoInteractor: IUserInfoInteractor,
        mainRouter: MainRouter,
        settingsRouter: SettingsRouter
    ): IBottomNavigationPresenter =
        BottomNavigationPresenter(iUserInfoInteractor, mainRouter, settingsRouter)

    @Provides
    @Named(NOTIFICATION)
    fun provideNotificationSP(context: Context): SharedPreferences =
        context.getSharedPreferences("pushNotifications", MODE_PRIVATE)

    @Provides
    @Named(NOTIFICATION)
    fun provideNotificationSPEditor(@Named(NOTIFICATION) sp: SharedPreferences): SharedPreferences.Editor = sp.edit()

    @Provides
    @Named(MAIN)
    fun provideSP(context: Context): SharedPreferences = context.getSharedPreferences("serverToken", MODE_PRIVATE)

    @Provides
    @Named(MAIN)
    fun provideSPEditor(@Named(MAIN) sp: SharedPreferences): SharedPreferences.Editor = sp.edit()

    @Provides
    @Named(TOKEN)
    fun provideTokenSP(@Named(MAIN) sp: SharedPreferences): String = sp.getString(TOKEN, "") ?: ""

    @Provides
    @Named(DEV_TOKEN)
    fun provideDeviceToken(@Named(MAIN) sp: SharedPreferences): String = sp.getString(DEV_TOKEN, "") ?: ""

    @Provides
    @Named(COURSE_THEME)
    fun provideCourseTheme(@Named(MAIN) sp: SharedPreferences): String = sp.getString(COURSE_THEME, "") ?: ""

    @Provides
    @Named(USER)
    fun provideUserSP(context: Context): SharedPreferences = context.getSharedPreferences("login", MODE_PRIVATE)

    @Provides
    @Named(USER)
    fun provideUserSPEditor(@Named(USER) sp: SharedPreferences): SharedPreferences.Editor = sp.edit()

    @Provides
    @Named("Email")
    fun provideEmail(@Named(USER) sp: SharedPreferences): String = sp.getString("email", "") ?: ""

    @Provides
    @Named("Password")
    fun providePassword(@Named(USER) sp: SharedPreferences): String = sp.getString("password", "") ?: ""
}