package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.ArticleService
import com.bonch_dev.spinit.data.network.user.CommentsService
import com.bonch_dev.spinit.data.repository.article.ArticleRepository
import com.bonch_dev.spinit.data.repository.article.IArticleRepository
import com.bonch_dev.spinit.data.repository.comments.CommentsRepository
import com.bonch_dev.spinit.data.repository.comments.ICommentsRepository
import com.bonch_dev.spinit.domain.interactors.article.ArticleInteractor
import com.bonch_dev.spinit.domain.interactors.article.IArticleInteractor
import com.bonch_dev.spinit.domain.interactors.comments.CommentsInteractor
import com.bonch_dev.spinit.domain.interactors.comments.ICommentsInteractor
import com.bonch_dev.spinit.presentation.modules.article.presenter.FullArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.presenter.IFullArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.presenter.ISavedArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.presenter.SavedArticlesPresenter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.CommentsPresenter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class ArticleModule {
    @Provides
    fun getPostsRepository(service: ArticleService): IArticleRepository = ArticleRepository(service)

    @Provides
    fun getCommentsRepository(service: CommentsService): ICommentsRepository =
        CommentsRepository(service)

    @Provides
    fun getCommentsInteractor(iCommentsRepository: ICommentsRepository): ICommentsInteractor =
        CommentsInteractor(iCommentsRepository)

    @Provides
    fun provideArticleInteractor(iArticleRepository: IArticleRepository): IArticleInteractor =
        ArticleInteractor(iArticleRepository)

    @Provides
    fun provideFullArticlePresenter(iArticleInteractor: IArticleInteractor): IFullArticlePresenter =
        FullArticlePresenter(iArticleInteractor)

    @Provides
    fun provideCommentsPresenter(iCommentsInteractor: ICommentsInteractor): ICommentsPresenter =
        CommentsPresenter(iCommentsInteractor)

    @Provides
    fun provideSavedArticlePresenter(router: MainRouter, articleInteractor: IArticleInteractor): ISavedArticlePresenter =
        SavedArticlesPresenter(router, articleInteractor)
}