package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.AuthService
import com.bonch_dev.spinit.data.repository.auth.CasualAuthRepository
import com.bonch_dev.spinit.data.repository.auth.ICasualAuthRepository
import com.bonch_dev.spinit.data.repository.auth.IServicesAuthRepository
import com.bonch_dev.spinit.data.repository.auth.ServicesAuthRepository
import com.bonch_dev.spinit.data.repository.profile.password.IPasswordRepository
import com.bonch_dev.spinit.domain.interactors.auth.signin.ISignInInteractor
import com.bonch_dev.spinit.domain.interactors.auth.signin.SignInInteractor
import com.bonch_dev.spinit.domain.interactors.auth.signup.ISignUpInteractor
import com.bonch_dev.spinit.domain.interactors.auth.signup.SignUpInteractor
import com.bonch_dev.spinit.presentation.base.presenters.MainPresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IMainPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.ISignInPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.SignInPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.ISignUpStepOnePresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.ISignUpStepTwoPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.SignUpStepOnePresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.SignUpStepTwoPresenter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class AuthModule() {
    @Provides
    fun provideSignInInteractor(
        iCasualAuthRepository: ICasualAuthRepository,
        iServicesAuthRepository: IServicesAuthRepository,
        iPasswordRepository: IPasswordRepository
    ): ISignInInteractor = SignInInteractor(iCasualAuthRepository, iServicesAuthRepository, iPasswordRepository)

    @Provides
    fun provideSignUpInteractor(iCasualAuthRepository: ICasualAuthRepository): ISignUpInteractor =
        SignUpInteractor(iCasualAuthRepository)

    @Provides
    fun provideSignInPresenter(iSignInInteractor: ISignInInteractor, mainRouter: MainRouter): ISignInPresenter =
        SignInPresenter(iSignInInteractor, mainRouter)

    @Provides
    fun provideSignUpStepOnePresenter(mainRouter: MainRouter): ISignUpStepOnePresenter =
        SignUpStepOnePresenter(mainRouter)

    @Provides
    fun provideSignUpStepTwoPresenter(iSignUpInteractor: ISignUpInteractor, mainRouter: MainRouter): ISignUpStepTwoPresenter =
        SignUpStepTwoPresenter(iSignUpInteractor, mainRouter)

    @Provides
    fun provideAuthPresenter(iSignInInteractor: ISignInInteractor, mainRouter: MainRouter): IMainPresenter =
        MainPresenter(mainRouter, iSignInInteractor)

    @Provides
    fun getServicesRepository(service: AuthService): IServicesAuthRepository = ServicesAuthRepository(service)

    @Provides
    fun getCasualAuthRepository(service: AuthService): ICasualAuthRepository = CasualAuthRepository(service)
}