package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.domain.interactors.article.IArticleInteractor
import com.bonch_dev.spinit.domain.interactors.broadcast.IBroadcastInteractor
import com.bonch_dev.spinit.presentation.modules.blog.presenter.BlogPresenter
import com.bonch_dev.spinit.presentation.modules.blog.presenter.IBlogPresenter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module
class BlogModule {

    @Provides
    fun provideBlogPresenter(
        iArticleInteractor: IArticleInteractor,
        iBroadcastInteractor: IBroadcastInteractor,
        router: MainRouter
    ): IBlogPresenter = BlogPresenter(iArticleInteractor, iBroadcastInteractor, router)
}