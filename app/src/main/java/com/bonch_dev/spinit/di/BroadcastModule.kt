package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.BroadcastService
import com.bonch_dev.spinit.data.repository.broadcasts.BroadcastsRepository
import com.bonch_dev.spinit.data.repository.broadcasts.IBroadcastRepository
import com.bonch_dev.spinit.domain.interactors.broadcast.BroadcastInteractor
import com.bonch_dev.spinit.domain.interactors.broadcast.IBroadcastInteractor
import com.bonch_dev.spinit.presentation.modules.broadcast.presenter.BroadcastPresenter
import com.bonch_dev.spinit.presentation.modules.broadcast.presenter.IBroadcastPresenter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class BroadcastModule {
    @Provides
    fun getBroadCastRepository(service: BroadcastService): IBroadcastRepository = BroadcastsRepository(service)

    @Provides
    fun provideBroadCastInteractor(iBroadcastRepository: IBroadcastRepository): IBroadcastInteractor =
        BroadcastInteractor(iBroadcastRepository)

    @Provides
    fun provideBroadcastPresenter(): IBroadcastPresenter = BroadcastPresenter()
}