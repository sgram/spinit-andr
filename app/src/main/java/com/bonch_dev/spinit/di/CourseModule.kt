package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.CoursesService
import com.bonch_dev.spinit.data.repository.learning.courses.CoursesRepository
import com.bonch_dev.spinit.data.repository.learning.courses.ICoursesRepository
import com.bonch_dev.spinit.domain.interactors.course.CourseInteractor
import com.bonch_dev.spinit.domain.interactors.course.ICourseInteractor
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter.AboutCoursePresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter.IAboutCoursePresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter.AllCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter.IAllCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.presenter.IMyCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.presenter.MyCoursesPresenter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class CourseModule {
    @Provides
    fun provideCourseInteractor(iCoursesRepository: ICoursesRepository): ICourseInteractor = CourseInteractor(iCoursesRepository)

    @Provides
    fun provideAllCoursesPresenter(iCourseInteractor: ICourseInteractor, mainRouter: MainRouter): IAllCoursesPresenter =
        AllCoursesPresenter(iCourseInteractor, mainRouter)

    @Provides
    fun provideAboutCoursePresenter(iCourseInteractor: ICourseInteractor): IAboutCoursePresenter =
        AboutCoursePresenter(
            iCourseInteractor
        )

    @Provides
    fun provideMyCoursePresenter(iCourseInteractor: ICourseInteractor): IMyCoursesPresenter =
        MyCoursesPresenter(iCourseInteractor)

    @Provides
    fun getCoursesRepository(service: CoursesService): ICoursesRepository = CoursesRepository(service)
}