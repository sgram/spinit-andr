package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.LessonService
import com.bonch_dev.spinit.data.repository.learning.courses.ICoursesRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.GetLessonRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.IGetLessonRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.ISendLessonRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.SendLessonRepository
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.domain.interactors.lesson.LessonInteractor
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.HomeworkPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.IHomeworkPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter.ILectionPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter.LectionPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter.ILessonCategoryPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter.LessonCategoryPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter.ILessonsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter.LessonsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter.IMaterialsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter.MaterialsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter.ITestPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter.TestPresenter
import com.bonch_dev.spinit.presentation.modules.teachermsg.presenter.ITeacherMsgPresenter
import com.bonch_dev.spinit.presentation.modules.teachermsg.presenter.TeacherMsgPresenter
import com.bonch_dev.spinit.router.LessonRouter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class LessonModule {
    @Provides
    fun provideGetLessonRepository(service: LessonService): IGetLessonRepository = GetLessonRepository(service)

    @Provides
    fun provideSendLessonRepository(service: LessonService): ISendLessonRepository = SendLessonRepository(service)

    @Provides
    fun provideLessonInteractor(
        iCoursesRepository: ICoursesRepository,
        iGetLessonRepository: IGetLessonRepository,
        iSendLessonRepository: ISendLessonRepository
    ): ILessonInteractor = LessonInteractor(iCoursesRepository, iGetLessonRepository, iSendLessonRepository)

    @Provides
    fun provideLessonCategoryPresenter(iLessonInteractor: ILessonInteractor, router: LessonRouter): ILessonCategoryPresenter =
        LessonCategoryPresenter(iLessonInteractor, router)

    @Provides
    fun provideLessonsPresenter(iLessonInteractor: ILessonInteractor, lessonRouter: LessonRouter): ILessonsPresenter =
        LessonsPresenter(iLessonInteractor, lessonRouter)

    @Provides
    fun provideMaterialsPresenter(iLessonInteractor: ILessonInteractor, lessonRouter: LessonRouter): IMaterialsPresenter =
        MaterialsPresenter(iLessonInteractor, lessonRouter)

    @Provides
    fun provideLectionPresenter(iLessonInteractor: ILessonInteractor, lessonRouter: LessonRouter): ILectionPresenter =
        LectionPresenter(iLessonInteractor, lessonRouter)

    @Provides
    fun provideTestPresenter(iLessonInteractor: ILessonInteractor, lessonRouter: LessonRouter): ITestPresenter =
        TestPresenter(iLessonInteractor, lessonRouter)

    @Provides
    fun provideHomeworkPresenter(iLessonInteractor: ILessonInteractor, lessonRouter: LessonRouter): IHomeworkPresenter =
        HomeworkPresenter(iLessonInteractor, lessonRouter)

    @Provides
    fun provideTeacherMsgPresenter(mainRouter: MainRouter): ITeacherMsgPresenter = TeacherMsgPresenter(mainRouter)
}