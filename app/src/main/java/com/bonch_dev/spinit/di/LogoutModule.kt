package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.AuthService
import com.bonch_dev.spinit.data.repository.auth.logout.ILogoutRepository
import com.bonch_dev.spinit.data.repository.auth.logout.LogoutRepository
import com.bonch_dev.spinit.domain.interactors.auth.logout.ILogoutInteractor
import com.bonch_dev.spinit.domain.interactors.auth.logout.LogoutInteractor
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class LogoutModule {

    @Provides
    fun provideLogoutInteractor(iLogoutRepository: ILogoutRepository): ILogoutInteractor =
        LogoutInteractor(iLogoutRepository)

    @Provides
    fun getLogoutRepository(service: AuthService): ILogoutRepository =
        LogoutRepository(service)
}