package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.*
import com.bonch_dev.spinit.domain.utils.Keys.BASE_URL
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    @Provides
    fun provideUserInfoService(retrofit: Retrofit): UserInfoService = retrofit.create(UserInfoService::class.java)

    @Provides
    fun provideCoursesService(retrofit: Retrofit): CoursesService = retrofit.create(CoursesService::class.java)

    @Provides
    fun provideLessonsService(retrofit: Retrofit): LessonService =
        retrofit.create(LessonService::class.java)

    @Provides
    fun provideAuthService(retrofit: Retrofit): AuthService =
        retrofit.create(AuthService::class.java)

    @Provides
    fun provideCommentsService(retrofit: Retrofit): CommentsService =
        retrofit.create(CommentsService::class.java)

    @Provides
    fun provideArticleService(retrofit: Retrofit): ArticleService =
        retrofit.create(ArticleService::class.java)

    @Provides
    fun provideBroadcastService(retrofit: Retrofit): BroadcastService =
        retrofit.create(BroadcastService::class.java)

    @Provides
    fun provideUpdateInfoService(retrofit: Retrofit): UpdateInfoService =
        retrofit.create(UpdateInfoService::class.java)

    @Provides
    fun provideNotificationService(retrofit: Retrofit): NotificationService =
        retrofit.create(NotificationService::class.java)
}