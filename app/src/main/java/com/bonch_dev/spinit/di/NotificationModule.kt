package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.NotificationService
import com.bonch_dev.spinit.data.repository.notification.INotificationRepository
import com.bonch_dev.spinit.data.repository.notification.NotificationRepository
import com.bonch_dev.spinit.domain.interactors.notification.INotificationInteractor
import com.bonch_dev.spinit.domain.interactors.notification.NotificationInteractor
import com.bonch_dev.spinit.presentation.modules.notification.presenter.INotificationPresenter
import com.bonch_dev.spinit.presentation.modules.notification.presenter.NotificationPresenter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class NotificationModule {
    @Provides
    fun provideNotificationRepository(service: NotificationService): INotificationRepository =
        NotificationRepository(service)

    @Provides
    fun provideNotificationInteractor(iNotificationRepository: INotificationRepository): INotificationInteractor =
        NotificationInteractor(iNotificationRepository)

    @Provides
    fun provideNotificationPresenter(iNotificationInteractor: INotificationInteractor): INotificationPresenter =
        NotificationPresenter(iNotificationInteractor)
}