package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.presentation.modules.onboarding.presenter.IOnBoardingPresenter
import com.bonch_dev.spinit.presentation.modules.onboarding.presenter.OnBoardingPresenter
import com.bonch_dev.spinit.router.MainRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class OnBoardingModule {

    @Provides
    fun provideOnBoardingPresenter(mainRouter: MainRouter): IOnBoardingPresenter =
        OnBoardingPresenter(mainRouter)
}