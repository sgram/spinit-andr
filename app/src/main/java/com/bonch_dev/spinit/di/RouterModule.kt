package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.router.LessonRouter
import com.bonch_dev.spinit.router.MainRouter
import com.bonch_dev.spinit.router.SettingsRouter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RouterModule {
    @Provides
    @Singleton
    fun provideAuthRouter(): MainRouter = MainRouter()

    @Provides
    @Singleton
    fun provideLessonRouter(): LessonRouter = LessonRouter()

    @Provides
    @Singleton
    fun provideSettingsRouter(): SettingsRouter = SettingsRouter()
}