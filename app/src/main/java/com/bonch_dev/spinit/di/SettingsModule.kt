package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.domain.interactors.auth.logout.ILogoutInteractor
import com.bonch_dev.spinit.presentation.modules.settings.presenter.settings.ISettingsPresenter
import com.bonch_dev.spinit.presentation.modules.settings.presenter.settings.SettingsPresenter
import com.bonch_dev.spinit.router.SettingsRouter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class])
class SettingsModule {

    @Provides
    fun provideSettingsPresenter(iLogoutInteractor: ILogoutInteractor, settingsRouter: SettingsRouter): ISettingsPresenter =
        SettingsPresenter(iLogoutInteractor, settingsRouter)
}