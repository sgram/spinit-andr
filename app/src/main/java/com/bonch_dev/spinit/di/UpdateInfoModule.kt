package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.UpdateInfoService
import com.bonch_dev.spinit.data.repository.profile.password.IPasswordRepository
import com.bonch_dev.spinit.data.repository.profile.password.PasswordRepository
import com.bonch_dev.spinit.data.repository.profile.update_info.IUpdateInfoRepository
import com.bonch_dev.spinit.data.repository.profile.update_info.UpdateInfoRepository
import com.bonch_dev.spinit.domain.interactors.update_info.IUpdateInfoInteractor
import com.bonch_dev.spinit.domain.interactors.update_info.UpdateInfoInteractor
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.ChangePasswordPresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.EditProfilePresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.IChangePasswordPresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.IEditProfilePresenter
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class])
class UpdateInfoModule {
    @Provides
    fun provideUpdateInfoInteractor(
        iPasswordRepository: IPasswordRepository,
        iUpdateInfoRepository: IUpdateInfoRepository
    ): IUpdateInfoInteractor = UpdateInfoInteractor(iPasswordRepository, iUpdateInfoRepository)

    @Provides
    fun provideEditProfilePresenter(iUpdateInfoInteractor: IUpdateInfoInteractor): IEditProfilePresenter = EditProfilePresenter(iUpdateInfoInteractor)

    @Provides
    fun provideChangePasswordPresenter(iUpdateInfoInteractor: IUpdateInfoInteractor): IChangePasswordPresenter =
        ChangePasswordPresenter(iUpdateInfoInteractor)

    @Provides
    fun provideUpdateInfoRepository(service: UpdateInfoService): IUpdateInfoRepository = UpdateInfoRepository(service)

    @Provides
    fun providePasswordRepository(service: UpdateInfoService): IPasswordRepository = PasswordRepository(service)
}