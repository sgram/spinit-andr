package com.bonch_dev.spinit.di

import com.bonch_dev.spinit.data.network.user.UserInfoService
import com.bonch_dev.spinit.data.repository.profile.userinfo.IUserInfoRepository
import com.bonch_dev.spinit.data.repository.profile.userinfo.UserInfoRepository
import com.bonch_dev.spinit.domain.interactors.userinfo.IUserInfoInteractor
import com.bonch_dev.spinit.domain.interactors.userinfo.UserInfoInteractor
import dagger.Module
import dagger.Provides

@Module(includes = [AppModule::class, NetworkModule::class])
class UserInfoModule {
    @Provides
    fun provideUserInfoRepository(service: UserInfoService): IUserInfoRepository = UserInfoRepository(service)

    @Provides
    fun provideUserInfoInteractor(iUserInfoRepository: IUserInfoRepository): IUserInfoInteractor =
        UserInfoInteractor(iUserInfoRepository)
}