package com.bonch_dev.spinit.domain.entity

import android.net.Uri

data class Document(
    val name: String?,
    val ext: String?,
    val sizeFormatted: String?,
    val sizeInBytes: Long?,
    val uri: Uri?
)