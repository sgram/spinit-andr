package com.bonch_dev.spinit.domain.entity

data class EmailPass(
    val email: String,
    val password: String
)