package com.bonch_dev.spinit.domain.entity

data class HowWeLearn(val title: String, val desc: String)