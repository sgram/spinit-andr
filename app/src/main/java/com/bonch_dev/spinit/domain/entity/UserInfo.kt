package com.bonch_dev.spinit.domain.entity

import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.userinfo.Achievement
import com.bonch_dev.spinit.domain.entity.response.userinfo.HomeworkAnswer
import com.bonch_dev.spinit.domain.entity.response.userinfo.HomeworkLesson

data class UserInfo(
    val id: Double,
    val first_name: String?,
    val last_name: String?,
    val email: String?,
    val created_at: String?,
    val updated_at: String?,
    val birth_date: String?,
    val gender: String?,
    val phone_number: String?,
    val avatar_url: String?,
    val courses: List<MyCourseInfo>?,
    val achievements: List<Achievement>?,
    val liked: List<Post?>?,
    val have_password: Boolean?,
    val homeworks: List<UserHomework>?,
    val admin_access: Boolean?,
    val notifications_count: Int?
)

data class UserHomework(
    val id: Int,
    val lesson: HomeworkLesson?,
    val course: Course?,
    var status: HomeworkCompletionStatus?,
    val created_at: String?,
    val answers: List<HomeworkAnswer>?
)


