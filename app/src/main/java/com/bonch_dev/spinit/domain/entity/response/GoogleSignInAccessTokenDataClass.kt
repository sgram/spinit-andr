package com.bonch_dev.spinit.domain.entity.response

data class GoogleSignInAccessTokenDataClass(
    val access_token: String?,
    val expires_in: Int?,
    val id_token: String?,
    val token_type: String?,
    val refresh_token: String?
)