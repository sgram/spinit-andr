package com.bonch_dev.spinit.domain.entity.response

data class Message(val message: String)