package com.bonch_dev.spinit.domain.entity.response

data class ResponseData(val message: String, val token: String)