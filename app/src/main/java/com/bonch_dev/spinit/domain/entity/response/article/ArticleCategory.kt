package com.bonch_dev.spinit.domain.entity.response.article

data class ArticleCategory(
    val id: Int?,
    val title: String?,
    val slug: String?
)