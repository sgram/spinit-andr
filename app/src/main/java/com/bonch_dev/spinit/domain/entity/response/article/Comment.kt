package com.bonch_dev.spinit.domain.entity.response.article

data class CommentData(val data: List<Comment>)

data class Comment(
    val id: Int?,
    val text: String?,
    val created_at: String?,
    val author: CommentAuthor?,
    val rates: CommentRate?,
    val replies: Int?,
    val functions: CommentFunction?,
    val moderator: Boolean?
)

data class CommentFunction(
    val edit: Boolean?,
    val delete: Boolean?
)

data class CommentAuthor(
    val id: Int?,
    val vk_id: Int?,
    val google_id: String?,
    val first_name: String?,
    val last_name: String?,
    val avatar_url: String?,
    val gender: String?
)

data class CommentRate(
    val likes: Int?,
    val dislikes: Int?,
    val liked: Int?
)