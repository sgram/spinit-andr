package com.bonch_dev.spinit.domain.entity.response.article

data class FullArticle(
    val data: FullArticleData
)

data class FullArticleData(
    val id: Int,
    val title: String,
    val description: String,
    val text: String,
    val created_at: String,
    val author_id: ArticleAuthor,
    val category: String
)

data class ArticleAuthor(
    val id: Int,
    val vk_id: Int?,
    val google_id: Int?,
    val first_name: String,
    val last_name: String,
    val avatar_url: String?,
    val gender: String?
)
