package com.bonch_dev.spinit.domain.entity.response.article

data class LikedPost(
    val id: Int,
    val title: String?,
    val img_url: String?,
    val description: String?,
    val created_at: String?,
    val author_id: Int?
)