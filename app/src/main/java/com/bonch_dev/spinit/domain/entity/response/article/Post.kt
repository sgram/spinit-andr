package com.bonch_dev.spinit.domain.entity.response.article

data class PostData(val data: List<Post>)
data class Post(
    val id: Int?,
    val title: String?,
    val img_url: String?,
    val description: String?,
    val created_at: String?,
    val author_id: Int?,
    val category: String?,
    var liked: Boolean?
)