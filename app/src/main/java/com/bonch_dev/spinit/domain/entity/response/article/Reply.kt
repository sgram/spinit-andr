package com.bonch_dev.spinit.domain.entity.response.article

data class Reply(
    val id: Int?,
    val text: String?,
    val created_at: String?,
    val author: ReplyAuthor?,
    val rates: ReplyRate?
)

data class ReplyAuthor(
    val id: Int?,
    val vk_id: Int?,
    val google_id: String?,
    val first_name: String?,
    val last_name: String?,
    val avatar_url: String?,
    val gender: String?
)

data class ReplyRate(
    val likes: Int?,
    val dislikes: Int?,
    val user_rate: String?
)