package com.bonch_dev.spinit.domain.entity.response.broadcast

data class Broadcast(
    val id: Int?,
    val title: String?,
    val description: String?,
    val yt_video: String?,
    val live: Int?,
    val created_at: String?,
    val updated_at: String?,
    val category: String?
)