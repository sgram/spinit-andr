package com.bonch_dev.spinit.domain.entity.response.course

data class AboutCourseInfoData(
    val data: AboutCourseInfo?
)

data class AboutCourseInfo(
    val id: Int?,
    val title: String?,
    val description: String?,
    val type: String?,
    val yt_video: String?,
    val slug: String?,
    val category: List<AboutCourseInfoCategory>?,
    val lessons: List<AboutCourseInfoLessons>?,
    val teachers: List<Teachers>?,
    val attendance: Boolean?
)

data class AboutCourseInfoCategory(
    val id: Int?,
    val title: String?,
    val slug: String?
)

data class AboutCourseInfoLessons(
    val title: String?,
    val themes: List<String>?
)

data class Teachers(
    val id: Int?,
    val name: String?,
    val position: String?,
    val avatar_url: String?,
    val localAvatar: Int?
)
