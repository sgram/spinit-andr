package com.bonch_dev.spinit.domain.entity.response.course

data class Course(
    val id: Int?,
    val title: String?,
    val description: String?,
    val type: String?,
    val icon_url: String?,
    val slug: String?,
    val created_at: String?,
    val updated_at: String?,
    val category: String?
)