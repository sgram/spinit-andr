package com.bonch_dev.spinit.domain.entity.response.course

data class MyCourse(val courses: List<MyCourseInfo>)
data class MyCourseInfo(
    val id: Int?,
    val title: String?,
    val description: String?,
    val type: String?,
    val icon_url: String?,
    val slug: String?,
    val created_at: String?,
    val updated_at: String?,
    val category: String?,
    val pivot: Pivot?
)

data class Pivot(val user_id: Int, val course_id: Int)