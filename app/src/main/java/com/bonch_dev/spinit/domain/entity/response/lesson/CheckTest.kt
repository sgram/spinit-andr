package com.bonch_dev.spinit.domain.entity.response.lesson

data class CheckTest(
    val check: List<Check>
)

data class Check(
    val id: Int,
    val correct: Boolean
)
