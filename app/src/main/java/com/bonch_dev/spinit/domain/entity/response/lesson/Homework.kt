package com.bonch_dev.spinit.domain.entity.response.lesson

data class Homework(val message: String, val homework: SendHomeworkData)
data class SendHomeworkData(val text: String, val user_id: Int, val lesson_id: Int, val id: Int)