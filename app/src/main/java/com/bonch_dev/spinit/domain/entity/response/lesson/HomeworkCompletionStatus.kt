package com.bonch_dev.spinit.domain.entity.response.lesson

enum class HomeworkCompletionStatus {
    WAITING, ACCEPTED, NOT_ACCEPTED, NONE
}