package com.bonch_dev.spinit.domain.entity.response.lesson

data class LessonHomework(val homework: String)