package com.bonch_dev.spinit.domain.entity.response.lesson

data class LessonStatusData(val status: Int?, val progress: Int = 0, val test: Int?, val time:String?)
data class HomeworkStatus(val status: HomeworkCompletionStatus, var progress: Int = 0, val test: TestCompletionStatus, var time:String?)