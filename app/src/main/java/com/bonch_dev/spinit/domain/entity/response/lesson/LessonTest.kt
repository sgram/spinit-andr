package com.bonch_dev.spinit.domain.entity.response.lesson

data class LessonTest(
    val test: List<TestData>,
    val time: String?,
    val next: Int?
)

data class TestData(
    val id: Int?,
    val title: String?,
    val type: Int?,
    val lesson_id: Int?,
    val boxes: List<Boxes>?
)

data class Boxes(
    val id: Int?,
    val title: String?,
    val task_id: Int?
)
