package com.bonch_dev.spinit.domain.entity.response.lesson

data class LessonText(val text: String?)