package com.bonch_dev.spinit.domain.entity.response.lesson

data class SendTest(
    val tasks: List<Task>
)

data class Task(
    val id: Int,
    val answer: Any
)
