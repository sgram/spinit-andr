package com.bonch_dev.spinit.domain.entity.response.lesson

data class Status(val data: StatusData?)
data class StatusData(
    val id: Int?,
    val title: String?,
    val type: String?,
    val lessons: MutableList<Lesson>?,
    val current_lesson: CurrentLesson?
)

data class Lesson(
    val id: Int?,
    val number: Int?,
    val title: String?,
    val mobile_img: String?,
    val status: LessonStatus?
)

data class LessonStatus(
    val completed: Boolean?
)

data class CurrentLesson(
    val lesson_number: Int?,
    val progress: Int?
)