package com.bonch_dev.spinit.domain.entity.response.lesson

enum class TestCompletionStatus {
    COMPLETED, FAILED, NONE
}