package com.bonch_dev.spinit.domain.entity.response.notification

data class NotificationObject(
    val data: List<Notification>?
)

data class Notification(
    val id: String?,
    val type: String?,
    val data: NotificationData?,
    val created_at: String?
)

data class NotificationData(
    val title: String?,
    val icon_url: String?
)