package com.bonch_dev.spinit.domain.entity.response.teachermsg

data class TeacherMSG(val name: String, val date: String, val image: Int, val content: String)