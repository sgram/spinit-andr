package com.bonch_dev.spinit.domain.entity.response.userinfo

import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo

data class UserInfoData(val data: UserInfo)
data class UserInfo(
    val id: Double,
    val first_name: String?,
    val last_name: String?,
    val email: String?,
    val created_at: String?,
    val updated_at: String?,
    val birth_date: String?,
    val gender: String?,
    val phone_number: String?,
    val avatar_url: String?,
    val courses: List<MyCourseInfo>?,
    val achievements: List<Achievement>?,
    val liked: List<Post?>?,
    val have_password: Boolean?,
    val homeworks: List<UserHomework>?,
    val admin_access: Boolean?,
    val notifications_count: Int?
)

data class UserHomework(
    val id: Int,
    val lesson: HomeworkLesson?,
    val course: Course?,
    val status: Int?,
    val created_at: String?,
    val answers: List<HomeworkAnswer>?
)

data class HomeworkLesson(
    val id: Int?,
    val title: String?
)

data class HomeworkAnswer(
    val id: Int,
    val moder: Moder,
    val homework_id: Int,
    val text: String,
    val created_at: String
)

data class Moder(
    val id: Int,
    val vk_id: Int?,
    val google_id: Int?,
    val first_name: String,
    val last_name: String,
    val avatar_url: String?,
    val gender: String?
)

data class Achievement(
    val id: Int,
    val title: String,
    val description: String,
    val icon_url: String,
    val pivot: AchievementPivot
)

data class AchievementPivot(
    val user_id: Int,
    val achievement_id: Int
)

data class RegistrationProfileSend(
    val first_name: String,
    val last_name: String,
    val email: String,
    val password: String,
    val password_confirmation: String,
    var phone_number: String,
    var birth_date: String,
    var gender: String
)
