package com.bonch_dev.spinit.domain.interactors.article

import com.bonch_dev.spinit.data.repository.article.IArticleRepository
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import javax.inject.Inject

class ArticleInteractor @Inject constructor(private val repository: IArticleRepository) :
    IArticleInteractor {

    override fun getCategories(result: DataHandler<List<ArticleCategory>>, err: ErrorHandler) {
        repository.getCategories() { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getArticle(postID: Int, result: DataHandler<FullArticle>, err: ErrorHandler) {
        repository.getFullArticle(postID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getPosts(category: String?, result: DataHandler<List<Post?>>, err: ErrorHandler) {
        repository.getPosts(category) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun likePost(postID: Int, result: SuccessHandler, err: ErrorHandler) {
        repository.likePost(postID) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun unlikePost(postID: Int, result: SuccessHandler, err: ErrorHandler) {
        repository.unlikePost(postID) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }
}