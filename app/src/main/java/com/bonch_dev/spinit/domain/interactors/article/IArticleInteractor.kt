package com.bonch_dev.spinit.domain.interactors.article

import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface IArticleInteractor {
    fun getArticle(postID: Int, result: DataHandler<FullArticle>, err: ErrorHandler)
    fun getPosts(category: String?, result: DataHandler<List<Post?>>, err: ErrorHandler)
    fun likePost(postID: Int, result: SuccessHandler, err: ErrorHandler)
    fun unlikePost(postID: Int, result: SuccessHandler, err: ErrorHandler)
    fun getCategories(result: DataHandler<List<ArticleCategory>>, err: ErrorHandler)
}