package com.bonch_dev.spinit.domain.interactors.auth.logout

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface ILogoutInteractor {
    fun logout(result: SuccessHandler, err: ErrorHandler)
}