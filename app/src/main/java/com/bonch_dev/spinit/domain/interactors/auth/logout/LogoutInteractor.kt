package com.bonch_dev.spinit.domain.interactors.auth.logout

import com.bonch_dev.spinit.data.repository.auth.logout.ILogoutRepository
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import javax.inject.Inject

class LogoutInteractor @Inject constructor(private val repository: ILogoutRepository) :
    ILogoutInteractor {

    override fun logout(result: SuccessHandler, err: ErrorHandler) {
        repository.logout { isSuccess, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }
}