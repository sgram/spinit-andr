package com.bonch_dev.spinit.domain.interactors.auth.signin

import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.entity.response.GoogleSignInAccessTokenDataClass
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.vk.sdk.VKAccessToken

interface ISignInInteractor {
    fun getAccessToken(
        authCode: String,
        idToken: String,
        callback: DataHandler<GoogleSignInAccessTokenDataClass>,
        err: ErrorHandler
    )

    fun signIn(loginData: EmailPass, callback: SuccessHandler, err: ErrorHandler)
    fun forgotPass(email: String, callback: SuccessHandler, err: ErrorHandler)
    fun googleSignIn(task: Task<GoogleSignInAccount>, callback: SuccessHandler, err: ErrorHandler)
    fun vkSignIn(res: VKAccessToken?, callback: SuccessHandler, err: ErrorHandler)
    fun checkAuth(isLogin: () -> Unit, err: ErrorHandler, notLogin: SuccessHandler)
    fun checkIsEmailExist(email: String, onEmailExist: () -> Unit, onEmailNotExist: () -> Unit, onError: (msg: String) -> Unit)
}