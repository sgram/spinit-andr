package com.bonch_dev.spinit.domain.interactors.auth.signin

import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.repository.auth.ICasualAuthRepository
import com.bonch_dev.spinit.data.repository.auth.IServicesAuthRepository
import com.bonch_dev.spinit.data.repository.profile.password.IPasswordRepository
import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.entity.response.GoogleSignInAccessTokenDataClass
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.USER
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Task
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKSdk
import javax.inject.Inject
import javax.inject.Named

class SignInInteractor @Inject constructor(
    private val casualAuthRepository: ICasualAuthRepository,
    private val servicesAuthRepository: IServicesAuthRepository,
    private val passwordRepository: IPasswordRepository
) : ISignInInteractor, GoogleApiClient.ConnectionCallbacks {

    @Inject
    @Named(MAIN)
    lateinit var tokenSP: SharedPreferences.Editor

    @Inject
    @Named(USER)
    lateinit var userSP: SharedPreferences

    @Inject
    @Named(USER)
    lateinit var userSPEditor: SharedPreferences.Editor

    @Inject
    @Named("Email")
    lateinit var email: String

    @Inject
    @Named("Password")
    lateinit var pass: String

    init {
        App.appComponent.inject(this)
    }

    override fun getAccessToken(
        authCode: String,
        idToken: String,
        callback: DataHandler<GoogleSignInAccessTokenDataClass>,
        err: ErrorHandler
    ) {
        servicesAuthRepository.getAccessToken(authCode, idToken) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) callback(data)
        }
    }

    override fun signIn(loginData: EmailPass, callback: SuccessHandler, err: ErrorHandler) {
        casualAuthRepository.signIn(loginData) { data, error ->
            if (error != null)
                err(SignInHandler.handleError(error))
            if (data != null) {
                tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                userSPEditor.putString("email", loginData.email).putString("password", loginData.password).apply()
                callback()
            }
        }
    }

    override fun forgotPass(email: String, callback: SuccessHandler, err: ErrorHandler) {
        passwordRepository.forgot(email) { success, error ->
            if (error != null)
                err(LoginHandler.handleError(error))
            if (success) callback()
        }
    }

    override fun checkIsEmailExist(
        email: String,
        onEmailExist: () -> Unit,
        onEmailNotExist: () -> Unit,
        onError: (msg: String) -> Unit
    ) {
        onEmailNotExist()
    }

    override fun googleSignIn(task: Task<GoogleSignInAccount>, callback: SuccessHandler, err: ErrorHandler) {
        try {
            val account = task.getResult(ApiException::class.java)
            if (account != null) {
                Log.e(javaClass.simpleName, "token: ${account.idToken}\nisExpired: ${account.isExpired}")
                val idToken = account.idToken
                val id = account.id
                if (idToken != null && id != null) {
                    servicesAuthRepository.authGoogle(idToken, id) { data, error ->
                        if (error != null)
                            err(LoginHandler.handleError(error))
                        if (data != null) {
                            tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                            callback()
                        }
                    }
                }
            }
        } catch (e: ApiException) {
            err(e.statusCode.toString())
        }
    }

    override fun vkSignIn(res: VKAccessToken?, callback: SuccessHandler, err: ErrorHandler) {
        if (res != null) {
            servicesAuthRepository.authVK(res) { data, error ->
                if (error != null)
                    err(LoginHandler.handleError(error))
                if (data != null) {
                    tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                    callback()
                }
            }
        } else Log.e("vkSignIn", "Null")
    }

    override fun checkAuth(isLogin: () -> Unit, err: ErrorHandler, notLogin: SuccessHandler) {
        val authCheck = AuthCheck()
        when {
            authCheck.checkVKAuth() -> {
                servicesAuthRepository.authVK(VKSdk.getAccessToken()) { data, error ->
                    if (error != null)
                        err(LoginHandler.handleError(error))
                    if (data != null) {
                        tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                        isLogin()
                    }
                }
            }
            authCheck.checkGoogleAuth() != null -> {
                var idToken: String?
                var id: String?
                Keys.GAC.connect()
                val acct = authCheck.checkGoogleAuth()
                if (acct != null) {
                    val optionalPendingResult = Auth.GoogleSignInApi.silentSignIn(Keys.GAC)
                    if (optionalPendingResult.isDone) {
                        idToken = optionalPendingResult.get().signInAccount?.idToken
                        id = optionalPendingResult.get().signInAccount?.id
                        if (idToken != null && id != null) {
                            servicesAuthRepository.authGoogle(idToken, id) { data, error ->
                                if (error != null)
                                    err(LoginHandler.handleError(error))
                                if (data != null) {
                                    tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                                    isLogin()
                                }
                            }
                        } else err(LoginHandler.handleError(ErrType(1)))
                    } else {
                        optionalPendingResult.setResultCallback {
                            id = it.signInAccount?.id
                            idToken = it.signInAccount?.idToken
                            if (idToken != null && id != null) {
                                servicesAuthRepository.authGoogle(idToken, id) { data, error ->
                                    if (data != null) {
                                        tokenSP.putString(TOKEN, "Bearer " + data.token).apply()
                                        isLogin()
                                    }
                                    if (error != null)
                                        err(LoginHandler.handleError(error))
                                }
                            } else err(LoginHandler.handleError(ErrType(1)))
                        }
                    }
                }
            }
            authCheck.checkEmail() -> {
                if (email.isNotEmpty() && pass.isNotEmpty()) {
                    val loginData = EmailPass(email, pass)
                    casualAuthRepository.signIn(loginData) { result, error ->
                        if (error != null) err(LoginHandler.handleError(error))
                        if (result != null) {
                            tokenSP.putString(TOKEN, "Bearer " + result.token).apply()
                            isLogin()
                        }
                    }
                }
            }
            else -> notLogin()
        }
    }

    override fun onConnected(p0: Bundle?) {}

    override fun onConnectionSuspended(p0: Int) = Keys.GAC.connect()
}