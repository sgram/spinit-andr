package com.bonch_dev.spinit.domain.interactors.auth.signup

import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface ISignUpInteractor {
    fun signUp(
        profile: RegistrationProfileSend,
        callback: SuccessHandler,
        err: ErrorHandler
    )
}