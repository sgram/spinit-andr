package com.bonch_dev.spinit.domain.interactors.auth.signup

import com.bonch_dev.spinit.data.repository.auth.ICasualAuthRepository
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.domain.utils.RegistrationHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import javax.inject.Inject

class SignUpInteractor @Inject constructor(private val repository: ICasualAuthRepository) :
    ISignUpInteractor {

    override fun signUp(profile: RegistrationProfileSend, callback: SuccessHandler, err: ErrorHandler) {
        repository.signUp(profile) { isSuccess, error ->
            if (error != null)
                err(RegistrationHandler.handleError(error))
            if (isSuccess) callback()
        }
    }
}