package com.bonch_dev.spinit.domain.interactors.broadcast

import com.bonch_dev.spinit.data.repository.broadcasts.IBroadcastRepository
import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import javax.inject.Inject

class BroadcastInteractor @Inject constructor(private val repository: IBroadcastRepository) : IBroadcastInteractor {
    override fun getBroadCast(result: DataHandler<List<Broadcast>>, err: ErrorHandler) {
        repository.getBroadcast { data, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }
}