package com.bonch_dev.spinit.domain.interactors.broadcast

import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler

interface IBroadcastInteractor {
    fun getBroadCast(result: DataHandler<List<Broadcast>>, err: ErrorHandler)
}