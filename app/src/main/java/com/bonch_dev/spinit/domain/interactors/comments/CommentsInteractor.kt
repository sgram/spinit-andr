package com.bonch_dev.spinit.domain.interactors.comments

import com.bonch_dev.spinit.data.repository.comments.ICommentsRepository
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.entity.response.article.CommentData
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import okhttp3.RequestBody
import javax.inject.Inject

class CommentsInteractor @Inject constructor(private val repository: ICommentsRepository) :ICommentsInteractor {

    override fun getReplies(commentID: Int?, result: DataHandler<List<Comment>>, err: ErrorHandler) {
        repository.getReplies(commentID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun updateComment(commentID: Int, text: String, result: SuccessHandler, err: ErrorHandler) {
        repository.updateComment(commentID, text) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun deleteComment(commentID: Int, result: SuccessHandler, err: ErrorHandler) {
        repository.deleteComment(commentID) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun sendLessonTextComment(lessonID: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler) {
        repository.sendLessonTextComment(lessonID, text) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun sendLessonVideoComment(lessonID: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler) {
        repository.sendLessonVideoComment(lessonID, text) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun sendArticleComment(postId: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler) {
        repository.sendArticleComment(postId, text) { isSuccess: Boolean, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun likeComment(commentID: Int?, result: SuccessHandler, err: ErrorHandler) {
        repository.likeComment(commentID) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun dislikeComment(commentID: Int?, result: SuccessHandler, err: ErrorHandler) {
        repository.unLikeComment(commentID) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun replyComment(commentID: Int?, text: RequestBody, result: SuccessHandler, err: ErrorHandler) {
        repository.replyComment(commentID, text) { isSuccess, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun getLessonTextComments(lessonID: Int, result: DataHandler<CommentData>, err: ErrorHandler) {
        repository.getLessonTextComments(lessonID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getLessonVideoComments(lessonID: Int, result: DataHandler<CommentData>, err: ErrorHandler) {
        repository.getLessonVideoComments(lessonID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getArticleComments(postID: Int, result: DataHandler<CommentData>, err: ErrorHandler) {
        repository.getArticleComments(postID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }
}