package com.bonch_dev.spinit.domain.interactors.comments


import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.entity.response.article.CommentData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

import okhttp3.RequestBody

interface ICommentsInteractor {
    fun deleteComment(commentID: Int, result: SuccessHandler, err: ErrorHandler)
    fun updateComment(commentID: Int, text: String, result: SuccessHandler, err: ErrorHandler)
    fun getReplies(commentID: Int?, result: DataHandler<List<Comment>>, err: ErrorHandler)
    fun sendArticleComment(postId: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler)
    fun likeComment(commentID: Int?, result: SuccessHandler, err: ErrorHandler)
    fun dislikeComment(commentID: Int?, result: SuccessHandler, err: ErrorHandler)
    fun replyComment(commentID: Int?, text: RequestBody, result: SuccessHandler, err: ErrorHandler)
    fun getArticleComments(postID: Int, result: DataHandler<CommentData>, err: ErrorHandler)
    fun getLessonTextComments(lessonID: Int, result: DataHandler<CommentData>, err: ErrorHandler)
    fun getLessonVideoComments(lessonID: Int, result: DataHandler<CommentData>, err: ErrorHandler)
    fun sendLessonTextComment(lessonID: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler)
    fun sendLessonVideoComment(lessonID: Int, text: RequestBody, result: SuccessHandler, err: ErrorHandler)
}