package com.bonch_dev.spinit.domain.interactors.course

import com.bonch_dev.spinit.data.repository.learning.courses.ICoursesRepository
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.domain.utils.CourseHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import javax.inject.Inject

class CourseInteractor @Inject constructor(private val repository: ICoursesRepository) : ICourseInteractor {

    override fun getCourseInfo(courseID: Int, result: DataHandler<AboutCourseInfoData>, err: ErrorHandler) {
        repository.getCourseInfo(courseID) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getAllcourses(result: DataHandler<MutableList<Course>>, err: ErrorHandler) {
        repository.getAllCourses { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data as MutableList<Course>)
        }
    }

    override fun getMyCourses(result: DataHandler<List<MyCourseInfo>>, err: ErrorHandler) {
        repository.getMyCourses { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun attend(courseID: Int, result: SuccessHandler, err: ErrorHandler) {
        repository.attend(courseID) { isSuccess, error ->
            if (error != null) err(CourseHandler.handleError(error))
            if (isSuccess) result()
        }
    }
}