package com.bonch_dev.spinit.domain.interactors.course

import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface ICourseInteractor {
    fun getCourseInfo(courseID: Int, result: DataHandler<AboutCourseInfoData>, err: ErrorHandler)
    fun getAllcourses(result: DataHandler<MutableList<Course>>, err: ErrorHandler)
    fun getMyCourses(result: DataHandler<List<MyCourseInfo>>, err: ErrorHandler)
    fun attend(courseID: Int, result: SuccessHandler, err: ErrorHandler)
}