package com.bonch_dev.spinit.domain.interactors.lesson

import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.domain.entity.response.lesson.*
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface ILessonInteractor {
    fun getLessonStatus(id: Int, result: DataHandler<HomeworkStatus>, err: ErrorHandler)
    fun sendHomework(text: String, id: Int, files: MutableList<Document>?, result: SuccessHandler, err: ErrorHandler)
    fun sendTest(test: SendTest, id: Int, result: (data: Int, correctCount: Int) -> Unit, err: ErrorHandler)
    fun getLessonHomework(id: Int, result: DataHandler<LessonHomework>, err: ErrorHandler)
    fun getLessonVideo(id: Int, result: DataHandler<Video>, err: ErrorHandler)
    fun getLessonMaterials(id: Int, result: DataHandler<LessonText>, err: ErrorHandler)
    fun getLessonTest(
        id: Int,
        result: DataHandler<LessonTest>,
        skip: () -> Unit,
        notComplete: (String) -> Unit,
        err: ErrorHandler
    )

    fun getStatus(id: Int, result: DataHandler<StatusData>, err: ErrorHandler)
}