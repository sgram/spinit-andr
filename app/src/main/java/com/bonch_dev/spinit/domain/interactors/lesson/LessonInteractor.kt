package com.bonch_dev.spinit.domain.interactors.lesson

import android.util.Log
import com.bonch_dev.spinit.data.repository.learning.courses.ICoursesRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.IGetLessonRepository
import com.bonch_dev.spinit.data.repository.learning.lessons.ISendLessonRepository
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.domain.entity.response.lesson.*
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.domain.utils.UriToFileConverter
import com.bonch_dev.spinit.domain.utils.toFileRequestBody
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import okhttp3.MultipartBody
import javax.inject.Inject

class LessonInteractor @Inject constructor(
    private val coursesRepository: ICoursesRepository,
    private val getLessonRepository: IGetLessonRepository,
    private val sendLessonRepository: ISendLessonRepository
) : ILessonInteractor {

    override fun getLessonStatus(id: Int, result: DataHandler<HomeworkStatus>, err: ErrorHandler) {
        getLessonRepository.getLessonStatus(id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) {

                val homeworkStatus = when (data.status) {
                    0 -> HomeworkCompletionStatus.WAITING
                    1 -> HomeworkCompletionStatus.ACCEPTED
                    2 -> HomeworkCompletionStatus.NOT_ACCEPTED
                    null -> HomeworkCompletionStatus.NONE
                    else -> throw IllegalArgumentException("No such homework status value")
                }
                val testCompletionStatus = when (data.test) {
                    0 -> TestCompletionStatus.NONE
                    1 -> TestCompletionStatus.COMPLETED
                    2 -> TestCompletionStatus.FAILED
                    else -> throw IllegalArgumentException("No such test status value")
                }
                result(HomeworkStatus(homeworkStatus, data.progress, testCompletionStatus, data.time))
            }
        }
    }

    override fun sendHomework(text: String, id: Int, files: MutableList<Document>?, result: SuccessHandler, err: ErrorHandler) {
        val multipartList = arrayListOf<MultipartBody.Part>()
        files?.forEach {
            val uri = it.uri
            if (uri != null) {
                val file = UriToFileConverter().createDocFileFromUri(uri)
                val body = MultipartBody.Part.createFormData("files[]", file.name, file.toFileRequestBody())
                multipartList.add(body)
            }
        }
        Log.e(this.javaClass.simpleName, "multipartList: ${multipartList.size}")
        sendLessonRepository.sendHomework(text, id, multipartList) { isSuccess, error ->
            Log.e(this.javaClass.simpleName, "error: ${error?.name}")
            if (error != null) err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun sendTest(test: SendTest, id: Int, result: (data: Int, correctCount: Int) -> Unit, err: ErrorHandler) {
        sendLessonRepository.send(test, id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) {
                val corrects = data.filter { it.correct }
                result(data.size, corrects.size)
            }
        }
    }

    override fun getLessonHomework(id: Int, result: DataHandler<LessonHomework>, err: ErrorHandler) {
        getLessonRepository.getLessonHomework(id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getLessonVideo(id: Int, result: DataHandler<Video>, err: ErrorHandler) {
        getLessonRepository.getLessonVideo(id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getLessonMaterials(id: Int, result: DataHandler<LessonText>, err: ErrorHandler) {
        getLessonRepository.getLessonText(id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun getLessonTest(
        id: Int,
        result: DataHandler<LessonTest>,
        skip: () -> Unit,
        notComplete: (String) -> Unit,
        err: ErrorHandler
    ) {
        getLessonRepository.getLessonTest(id) { data, error ->
            when {
                error != null -> err(BasicHandler.handleError(error))
                data?.next == 1 -> skip()
                data?.next == 0 && data.time != null -> notComplete(data.time)
                data?.test != null -> result(data)
            }
        }
    }

    override fun getStatus(id: Int, result: DataHandler<StatusData>, err: ErrorHandler) {
        coursesRepository.getStatus(id) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }
}