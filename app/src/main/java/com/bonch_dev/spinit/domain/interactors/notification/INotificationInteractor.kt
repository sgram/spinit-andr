package com.bonch_dev.spinit.domain.interactors.notification

import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler

interface INotificationInteractor {
    fun getNotifications(result: DataHandler<List<Notification>>, err: ErrorHandler)
    fun deleteNotification(notificationID: String, result: SuccessHandler, err: ErrorHandler)
}