package com.bonch_dev.spinit.domain.interactors.notification

import com.bonch_dev.spinit.data.repository.notification.INotificationRepository
import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import javax.inject.Inject

class NotificationInteractor @Inject constructor(private val repository: INotificationRepository) :
    INotificationInteractor {
    override fun getNotifications(result: DataHandler<List<Notification>>, err: ErrorHandler) {
        repository.getNotifications { data, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (data != null) result(data)
        }
    }

    override fun deleteNotification(notificationID: String, result: SuccessHandler, err: ErrorHandler) {
        repository.deleteNotification(notificationID) { isSuccess, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }
}