package com.bonch_dev.spinit.domain.interactors.update_info

import android.net.Uri
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData
import okhttp3.MultipartBody

interface IUpdateInfoInteractor {
    fun prepareEventPicture(data: Uri?, result: DataHandler<MultipartBody.Part?>)
    fun changePassword(oldPass: String, newPass: String, confirmPass: String, result: SuccessHandler, err: ErrorHandler)
    fun resetAvatar(result: SuccessHandler, err: ErrorHandler)
    fun updateData(userData: UserData, uri: Uri?, result: SuccessHandler, err: ErrorHandler)
}