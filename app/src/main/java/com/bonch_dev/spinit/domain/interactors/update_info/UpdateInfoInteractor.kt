package com.bonch_dev.spinit.domain.interactors.update_info

import android.content.SharedPreferences
import android.net.Uri
import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.data.repository.profile.password.IPasswordRepository
import com.bonch_dev.spinit.data.repository.profile.update_info.IUpdateInfoRepository
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.USER
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.SuccessHandler
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData
import id.zelory.compressor.Compressor
import okhttp3.MultipartBody
import javax.inject.Inject
import javax.inject.Named

class UpdateInfoInteractor @Inject constructor(
    private val passwordRepository: IPasswordRepository,
    private val updateInfoRepository: IUpdateInfoRepository
) : IUpdateInfoInteractor {

    @Inject
    @Named(USER)
    lateinit var spEditor: SharedPreferences.Editor

    init {
        App.appComponent.inject(this)
    }

    override fun prepareEventPicture(data: Uri?, result: DataHandler<MultipartBody.Part?>) {
        val thread = Thread {
            val ctx = App.applicationContext()
            var body: MultipartBody.Part? = null
            if (data != null) {
                val file = UriToFileConverter().createImageFileFromUri(data)
                val compressed = Compressor.getDefault(ctx).compressToFile(file)
                Log.e("compressed", compressed.absolutePath)
                val reqFile = compressed.toImageRequestBody()
                body = MultipartBody.Part.createFormData("file", compressed.name, reqFile)
            }
            result(body)
        }
        thread.start()
    }

    override fun changePassword(
        oldPass: String,
        newPass: String,
        confirmPass: String,
        result: SuccessHandler,
        err: ErrorHandler
    ) {
        passwordRepository.changePassword(oldPass, newPass, confirmPass) { isSuccess, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (isSuccess) {
                spEditor.putString("password", confirmPass)?.apply()
                result()
            }
        }
    }

    override fun resetAvatar(result: SuccessHandler, err: ErrorHandler) {
        updateInfoRepository.reset { isSuccess, error ->
            if (error != null)
                err(BasicHandler.handleError(error))
            if (isSuccess) result()
        }
    }

    override fun updateData(userData: UserData, uri: Uri?, result: SuccessHandler, err: ErrorHandler) {
        prepareEventPicture(uri,
            result = {
                updateInfoRepository.updateData(
                    it,
                    gender = userData.gender,
                    name = userData.name,
                    lastName = userData.lastName,
                    email = userData.email,
                    phone = userData.phone
                ) { isSuccess, error ->
                    if (error != null) err(RegistrationHandler.handleError(error))
                    if (isSuccess) {
                        userData.run {
                            name?.let { CurrentUser.name = it }
                            lastName?.let { CurrentUser.lastName = it }
                            phone?.let { CurrentUser.phone = it }
                            gender?.let { CurrentUser.gender = it }
                            email?.let { CurrentUser.email = it }
                        }
                        result()
                    }
                }
            }
        )
    }
}