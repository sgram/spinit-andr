package com.bonch_dev.spinit.domain.interactors.userinfo

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler

interface IUserInfoInteractor {
    fun getUserInfo(
        needSendToken: Boolean,
        result: DataHandler<com.bonch_dev.spinit.domain.entity.UserInfo>,
        err: ErrorHandler
    )
}