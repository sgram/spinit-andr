package com.bonch_dev.spinit.domain.interactors.userinfo

import com.bonch_dev.spinit.data.repository.profile.userinfo.IUserInfoRepository
import com.bonch_dev.spinit.domain.entity.UserInfo
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.utils.BasicHandler
import com.bonch_dev.spinit.domain.utils.CurrentUser
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.ErrorHandler
import javax.inject.Inject

class UserInfoInteractor @Inject constructor(private val repository: IUserInfoRepository) :
    IUserInfoInteractor {

    override fun getUserInfo(needSendToken: Boolean, result: DataHandler<UserInfo>, err: ErrorHandler) {
        repository.getUserInfo(needSendToken) { data, error ->
            if (error != null) err(BasicHandler.handleError(error))
            if (data != null)
                data.data.apply {
                    val homeworkList = mutableListOf<com.bonch_dev.spinit.domain.entity.UserHomework>()
                    homeworks?.forEach {
                        val homeworkItem = com.bonch_dev.spinit.domain.entity.UserHomework(
                            it.id,
                            it.lesson,
                            it.course,
                            null,
                            it.created_at,
                            it.answers
                        )
                        when (it.status) {
                            0 -> homeworkItem.status = HomeworkCompletionStatus.WAITING
                            1 -> homeworkItem.status = HomeworkCompletionStatus.ACCEPTED
                            2 -> homeworkItem.status = HomeworkCompletionStatus.NOT_ACCEPTED
                            null -> homeworkItem.status = HomeworkCompletionStatus.NONE
                        }
                        homeworkList.add(homeworkItem)
                    }
                    CurrentUser.bindData(
                        _id = id,
                        _name = first_name,
                        _lastName = last_name,
                        _email = email,
                        _phone = phone_number,
                        _gender = gender,
                        _birthdayDate = birth_date
                    )
                    val userInfo = UserInfo(
                        id,
                        first_name,
                        last_name,
                        email,
                        created_at,
                        updated_at,
                        birth_date,
                        gender,
                        phone_number,
                        avatar_url,
                        courses,
                        achievements,
                        liked,
                        have_password,
                        homeworkList,
                        admin_access,
                        notifications_count
                    )
                    result(userInfo)
                }
        }
    }
}