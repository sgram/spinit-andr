package com.bonch_dev.spinit.domain.utils

import android.content.Context
import com.bonch_dev.spinit.App
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.vk.sdk.VKSdk

class AuthCheck() {

    private val ctx = App.applicationContext()

    fun checkVKAuth(): Boolean = VKSdk.isLoggedIn()

    fun checkEmail(): Boolean {
        val email = ctx.getSharedPreferences("login", Context.MODE_PRIVATE).getString("email", "")
        val password = ctx.getSharedPreferences("login", Context.MODE_PRIVATE).getString("password", "")
        return !(email.isNullOrEmpty() && password.isNullOrEmpty())
    }

    fun checkGoogleAuth(): GoogleSignInAccount? = GoogleSignIn.getLastSignedInAccount(ctx)
}