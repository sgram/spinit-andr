package com.bonch_dev.spinit.domain.utils

import com.bonch_dev.spinit.App
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

class ConditionsOfUseReader {
    fun getConditionsOfUseString(): String? {
        val ConditionsOfUseString = StringBuilder()
        try {
            val reader = BufferedReader(InputStreamReader(App.applicationContext().assets.open("conditions_of_use.txt")))
            var str: String? = reader.readText()
            while (str != null) {
                ConditionsOfUseString.append(str)
                str = reader.readLine()
            }
            reader.close()
            return ConditionsOfUseString.toString()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }
}