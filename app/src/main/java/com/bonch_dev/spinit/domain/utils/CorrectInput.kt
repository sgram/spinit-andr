package com.bonch_dev.spinit.domain.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.regex.Pattern

object CorrectInput {
    fun isCorrectEmail(email: String): Boolean {
        val EMAIL_ADDRESS_PATTERN: Pattern = Pattern
            .compile("[a-zA-Z0-9+._%-+]{1,256}" + "@" + "[a-zA-Z0-9][a-zA-Z0-9-]{0,64}" + "(" + "." + "[a-zA-Z0-9][a-zA-Z0-9-]{0,25}" + ")+")
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches()
    }

    @SuppressLint("SimpleDateFormat")
    fun isCorrectDate(date: String): Boolean {
        val format = SimpleDateFormat("dd.MM.yyyy")
        format.isLenient = false
        return if (date.contains(Regex("[DMY]"))) false else {
            try {
                format.parse(date)
                true
            } catch (err: ParseException) {
                false
            }
        }
    }
}