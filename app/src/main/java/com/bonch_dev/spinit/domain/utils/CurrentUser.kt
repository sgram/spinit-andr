package com.bonch_dev.spinit.domain.utils

object CurrentUser {
    var id: Double? = null
    var name: String? = null
    var lastName: String? = null
    var email: String? = null
    private var birthdayDate: String? = null
    var phone: String? = null
    var gender: String? = null

    fun bindData(
        _id: Double,
        _name: String?,
        _lastName: String?,
        _email: String?,
        _birthdayDate: String?,
        _phone: String?,
        _gender: String?
    ) {
        id = _id
        name = _name
        lastName = _lastName
        email = _email
        phone = _phone
        gender = _gender
        birthdayDate = _birthdayDate?.split('-')?.run {
            "${component3()}.${component2()}.${component1()}"
        }
    }
}