package com.bonch_dev.spinit.domain.utils

import com.bonch_dev.spinit.R

abstract class RetrofitExHandler {
    open val errors = hashMapOf(
        Pair(1003, R.string.job_cancel),
        Pair(1002, R.string.io_exception),
        Pair(1001, R.string.http_exception),
        Pair(1000, R.string.socket_timeout_exception),
        Pair(401, R.string.unauthorized),
        Pair(404, R.string.not_found),
        Pair(429, R.string.too_many_reqs),
        Pair(500, R.string.int_server_err),
        Pair(503, R.string.service_unavailable),
        Pair(403, R.string.forbidden),
        Pair(400, R.string.bad_req)
    )

    open fun handleError(err: ErrType): String {
        errors[err.code]?.let {
            if (err.code != Keys.CANCELLATION_EXCEPTION) return ResourceManager.getString(it)
        }
        return err.name
    }
}

object BasicHandler : RetrofitExHandler() {
    override val errors: HashMap<Int, Int> = super.errors
}

object RegistrationHandler : RetrofitExHandler() {
    override val errors: HashMap<Int, Int> = super.errors.apply {
        this[422] = R.string.reg_code_422
    }
}

object SignInHandler : RetrofitExHandler() {
    override val errors: HashMap<Int, Int> = super.errors.apply {
        this[422] = R.string.login_code_422
        this[403] = R.string.login_code_403
    }
}

object CourseHandler : RetrofitExHandler() {
    override val errors: HashMap<Int, Int> = super.errors.apply {
        this[403] = R.string.already_attended
    }
}

object LoginHandler : RetrofitExHandler() {
    override val errors: HashMap<Int, Int> = super.errors.apply {
        this[403] = R.string.forgot_pass_code_403
    }
}

class ErrType(var code: Int?) {
    var name: String = ""

    constructor(err: Int, name: String) : this(err) {
        this.name = name
    }
}