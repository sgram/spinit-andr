package com.bonch_dev.spinit.domain.utils

import com.bonch_dev.spinit.domain.utils.Keys.CANCELLATION_EXCEPTION
import com.bonch_dev.spinit.domain.utils.Keys.HTTP_EXCEPTION
import com.bonch_dev.spinit.domain.utils.Keys.IO_EXCEPTION
import com.bonch_dev.spinit.domain.utils.Keys.SOCKET_TIMEOUT_EXCEPTION
import com.bonch_dev.spinit.domain.utils.Keys.UNKNOWN_EXCEPTION
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.concurrent.CancellationException

object ExceptionHandler {
    fun handle(err: Exception): ErrType {
        return when (err) {
            is CancellationException -> ErrType(CANCELLATION_EXCEPTION)
            is SocketTimeoutException -> ErrType(SOCKET_TIMEOUT_EXCEPTION)
            is HttpException -> ErrType(HTTP_EXCEPTION)
            is IOException -> ErrType(IO_EXCEPTION)
            else -> ErrType(UNKNOWN_EXCEPTION, err.message ?: "")
        }
    }
}