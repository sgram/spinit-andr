package com.bonch_dev.spinit.domain.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

fun Fragment.showToast(msg: String?) {
    if (msg != null)
        Toast.makeText(App.applicationContext(), msg, Toast.LENGTH_LONG).show()
}

fun AppCompatActivity.showToast(msg: String) {
    Toast.makeText(App.applicationContext(), msg, Toast.LENGTH_LONG).show()
}

fun String.toRequestBody(): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), this)
}

fun File.toFileRequestBody(): RequestBody {
    val mediaType = MediaType.parse("multipart/form-data")
    return RequestBody.create(mediaType, this)
}

fun File.toImageRequestBody(): RequestBody {
    return RequestBody.create(MediaType.parse("image/*"), this)
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun Toolbar.initBackArrow(activity: AppCompatActivity) {

    activity.run {
        setSupportActionBar(this@initBackArrow)
        supportActionBar?.run {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }
    this.setNavigationOnClickListener {
        activity.run {
            onBackPressed()
            val view = window?.decorView?.rootView?.windowToken
            Log.e("keyboard", "hide" + view.toString())
            val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (imm.isAcceptingText)
                imm.hideSoftInputFromWindow(view, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}