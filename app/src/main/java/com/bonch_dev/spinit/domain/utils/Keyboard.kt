package com.bonch_dev.spinit.domain.utils

import android.content.Context
import android.util.Log
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment

object Keyboard {
    fun hide(fragment: Fragment) {
        val view = fragment.activity?.window?.decorView?.rootView?.windowToken
        Log.e("keyboard", "hide" + view.toString())
        fragment.activity?.run {
            val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (imm.isAcceptingText)
                imm.hideSoftInputFromWindow(view, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}