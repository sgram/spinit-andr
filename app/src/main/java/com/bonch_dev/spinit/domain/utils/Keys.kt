package com.bonch_dev.spinit.domain.utils

import android.util.Log
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Scope
import kotlinx.coroutines.Job

object Keys {

    const val BASE_URL = "https://spinit.advanture.website/"
    const val VK_LINK = "https://vk.com/bonch_dev"
    const val INST_LINK = "https://www.instagram.com/bonch.dev/"
    const val SITE_LINK = "https://spinit-frontend-staging.server.bonch.dev/"
    const val OFFER_LINK = "http://spinit.advanture.website/storage/docs/public_offer_self-employed.pdf"

    // registration bundle keys
    const val BUNDLE_EMAIL = "email"
    const val BUNDLE_PASS = "pass"
    const val BUNDLE_CONFIRM_PASS = "confirmPass"

    // reqs
    const val AUTHORIZATION = "Authorization"
    const val ACCEPT = "Accept"
    const val CONTENT_TYPE = "Content-Type"
    const val APP_JSON = "application/json"

    const val BOTTOM_NAVIGATION = "BottomNavigation"
    const val BLOG_FRAG = "BlogFrag"
    const val ALL_COURSES_FRAG = "AllCourses"
    const val MAIN_FRAG = "MainFrag"
    const val TOKEN = "token"
    const val USER = "User"
    const val MAIN = "Main"
    const val THEME = "theme"
    const val COURSE_ID = "course_id"
    const val COURSE_TITLE = "courseTitle"
    const val ID = "id"
    const val LESSON_ID = "lessonId"
    const val COURSE_THEME = "courseTheme"
    const val MALE = "male"
    const val FEMALE = "female"

    // google consts
    val GSO: GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(ResourceManager.getString(R.string.server_client_id))
        .requestServerAuthCode(ResourceManager.getString(R.string.server_client_id))
        .requestId()
        .requestScopes(Scope(Scopes.PROFILE))
        .requestEmail()
        .build()
    val GAC: GoogleApiClient = GoogleApiClient.Builder(App.applicationContext())
        .addApi(Auth.GOOGLE_SIGN_IN_API, GSO)
        .build()

    const val SOCKET_TIMEOUT_EXCEPTION = 1000
    const val HTTP_EXCEPTION = 1001
    const val IO_EXCEPTION = 1002
    const val CANCELLATION_EXCEPTION = 1003
    const val UNKNOWN_EXCEPTION = 9999

    const val MAX_HOMEWORK_DOCS_COUNT = 10

    // User info
    const val USER_INFO = "userInfo"
    const val DEV_TOKEN = "deviceToken"

    // Update info
    const val UPDATE_DATA = "updateData"
    const val RESET_AVATAR = "resetAvatar"
    const val UPDATE_AVATAR = "updateAvatar"

    // Password
    const val CHANGE_PASS = "changePass"
    const val FORGOT = "forgot"

    // Notifications
    const val GET_NOTIFICATIONS = "getNotifications"
    const val READ_NOTIFICATION = "readNotification"
    const val DELETE_NOTIFICATION = "deleteNotification"
    const val NOTIFICATION = "Notification"

    // Lessons
    const val SEND_HOMEWORK_FILES = "homeworkFiles"
    const val SEND_TEST = "sendTest"
    const val SEND_HOMEWORK = "sendHomework"
    const val GET_TEXT = "getText"
    const val GET_VIDEO = "getVideo"
    const val GET_TEST = "getTest"
    const val GET_HOMEWORK = "getHomework"
    const val GET_HOMEWORK_STATUS = "getStatus"

    // Courses
    const val GET_ALL_COURSES = "getAllCourses"
    const val GET_MY_COURSES = "getMyCourses"
    const val GET_COURSE_INFO = "getCourseInfo"
    const val GET_COURSE_STATUS = "getCourseStatus"
    const val ATTEND = "attend"

    // Comments
    const val GET_ARTICLE_COMMENT = "getArticleComment"
    const val SEND_ARTICLE_COMMENT = "sendArticleComment"
    const val LIKE_COMMENT = "likeComment"
    const val UNLIKE_COMMENT = "unlikeComment"
    const val REPLY_COMMENT = "replyComment"
    const val SEND_LESSON_TEXT_COMMENT = "sendLessonTextComment"
    const val GET_LESSON_TEXT_COMMENT = "getLessonTextComment"
    const val SEND_LESSON_VIDEO_COMMENT = "sendLessonVideoComment"
    const val GET_LESSON_VIDEO_COMMENT = "getLessonVideoComment"
    const val GET_REPLIES = "getReplies"
    const val DELETE_COMMENT = "deleteComment"
    const val UPDATE_COMMENT = "updateComment"

    // Broadcasts
    const val GET_BROADCASTS = "getBroadcasts"

    // Services auth
    const val AUTH_GOOGLE = "authGoogle"
    const val GOOGLE_ACCESS_TOKEN="googleAccessToken"
    const val AUTH_VK = "authVK"

    // Casual auth
    const val SIGN_IN = "signIn"
    const val SIGN_UP = "signUp"

    // logout
    const val LOGOUT = "logout"

    // Article
    const val LIKE_POST = "likePost"
    const val UNLIKE_POST = "unlikePost"
    const val GET_POSTS = "getPosts"
    const val GET_FULL_ARTICLE = "getFullArticle"
    const val GET_CATEGORIES = "getCategories"

    fun cancelJobs(vararg active: String) {
        active.forEach {
            if (activeJobs[it]?.isActive == true)
                Log.e("cancelJobs", it)
            activeJobs[it]?.cancel()
            activeJobs.remove(it)
        }
    }

    const val CHANNEL_ID = "SpinITMain"

    // Avatar
    var avatarURL = ""
    var gender = ""

    // Admin
    var adminAccess = false
    const val CONNECTIVITY_ACTION = "android.net.conn.CONNECTIVITY_CHANGE"

    val activeJobs = hashMapOf<String, Job>()

    var havePassword: Boolean = false
}