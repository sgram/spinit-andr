@file:Suppress("DEPRECATION")

package com.bonch_dev.spinit.domain.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.bonch_dev.spinit.App

object NetworkConnection {
    fun isInternetAvailable(): Boolean {
        val cm = App.applicationContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        return activeNetwork?.isConnectedOrConnecting == true
    }

    fun getConnectivityStatus(ctx: Context?): Boolean {
        val cm = ctx?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }
}