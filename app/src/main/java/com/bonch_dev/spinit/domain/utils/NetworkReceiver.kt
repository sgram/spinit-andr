package com.bonch_dev.spinit.domain.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keys.CONNECTIVITY_ACTION

class NetworkReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == CONNECTIVITY_ACTION) {
            val status = NetworkConnection.getConnectivityStatus(context)
            if (!status) Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_LONG).show()
        }
    }
}