package com.bonch_dev.spinit.domain.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.bonch_dev.spinit.App
import java.security.InvalidParameterException

object ResourceManager {

    var context: Context = App.applicationContext()

    fun getColor(@ColorRes id: Int) = ContextCompat.getColor(context, id)

    fun getString(@StringRes id: Int): String = context.getString(id)

    fun getStringArray(@ArrayRes id: Int): Array<out String> = context.resources.getStringArray(id)

    fun getAnimation(id: Int): Animation = AnimationUtils.loadAnimation(context, id)

    fun getDrawable(id: Int): Drawable =
        ContextCompat.getDrawable(context, id) ?: throw InvalidParameterException("Invalid resource id")
}