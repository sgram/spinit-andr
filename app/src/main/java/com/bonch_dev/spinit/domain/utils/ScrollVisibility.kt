package com.bonch_dev.spinit.domain.utils

import android.graphics.Rect
import android.view.View
import androidx.core.widget.NestedScrollView

object ScrollVisibility {
    fun isViewVisibleOnScreen(sv: NestedScrollView?, v: View): Boolean {
        val scrollBounds = Rect()
        sv?.getDrawingRect(scrollBounds)
        val bounds = Rect()
        v.getHitRect(bounds)
        return Rect.intersects(scrollBounds, bounds)
    }
}