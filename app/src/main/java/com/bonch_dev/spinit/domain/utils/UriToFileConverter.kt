package com.bonch_dev.spinit.domain.utils

import android.content.ContentUris
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import com.bonch_dev.spinit.App
import java.io.File

class UriToFileConverter {

    fun createImageFileFromUri(uri: Uri): File {
        var filePath = ""
        Log.e("uri", uri.toString())
        val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = App.applicationContext().contentResolver.query(uri, filePathColumn, null, null, null)
        cursor?.moveToFirst()
        val columnIndex = cursor?.getColumnIndex(filePathColumn[0])
        if (cursor != null && columnIndex != null)
            filePath = cursor.getString(columnIndex)
        Log.e("filepath", filePath)
        cursor?.close()
        return File(filePath)
    }

    fun createDocFileFromUri(uri: Uri): File {
        val path = getRealPath(uri)
        return File(path)
    }

    @Suppress("DEPRECATION")
    private fun getRealPath(uri: Uri): String? {
        when {
            DocumentsContract.isDocumentUri(App.applicationContext(), uri) -> {
                when {
                    isExternalStorageDocument(uri) -> {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        // This is for checking Main Memory
                        return if ("primary".equals(type, ignoreCase = true)) {
                            if (split.size > 1) Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                            else Environment.getExternalStorageDirectory().toString() + "/"
                            // This is for checking SD Card
                        } else "storage" + "/" + docId.replace(":", "/")
                    }
                    isDownloadsDocument(uri) -> {
                        val fileName = getFilePath(uri)
                        if (fileName != null) return Environment.getExternalStorageDirectory()
                            .toString() + "/Download/" + fileName
                        val id = DocumentsContract.getDocumentId(uri)
                        val contentUri: Uri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id)
                        )
                        return getDataColumn(contentUri, null, null)
                    }
                    isMediaDocument(uri) -> {
                        val docId = DocumentsContract.getDocumentId(uri)
                        val split = docId.split(":").toTypedArray()
                        val type = split[0]
                        var contentUri: Uri? = null
                        contentUri = when (type) {
                            "image" -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                            "video" -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                            "audio" -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                            else -> null
                        }
                        val selection = "_id=?"
                        val selectionArgs = arrayOf(split[1])
                        if (contentUri != null) return getDataColumn(contentUri, selection, selectionArgs)
                    }
                }
            }
            "content".equals(
                uri.scheme,
                true
            ) -> return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(uri, null, null)
            "file".equals(uri.scheme, ignoreCase = true) -> return uri.path
        }
        return null
    }

    private fun getDataColumn(uri: Uri, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = App.applicationContext().contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    private fun getFilePath(uri: Uri): String? {
        var cursor: Cursor? = null
        val projection = arrayOf(
            MediaStore.MediaColumns.DISPLAY_NAME
        )
        try {
            cursor = App.applicationContext().contentResolver.query(
                uri, projection, null, null,
                null
            )
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    private fun isExternalStorageDocument(uri: Uri): Boolean = "com.android.externalstorage.documents" == uri.authority

    private fun isDownloadsDocument(uri: Uri): Boolean = "com.android.providers.downloads.documents" == uri.authority

    private fun isMediaDocument(uri: Uri): Boolean = "com.android.providers.media.documents" == uri.authority

    private fun isGooglePhotosUri(uri: Uri): Boolean = "com.google.android.apps.photos.content" == uri.authority
}