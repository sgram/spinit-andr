package com.bonch_dev.spinit.domain.utils

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.Transformation

object ViewAnimation {
    fun expand(v: View) {
        val a = expandAction(v)
        a.setAnimationListener(object : AnimationListener {
            override fun onAnimationStart(animation: Animation) {}
            override fun onAnimationEnd(animation: Animation) {}
            override fun onAnimationRepeat(animation: Animation) {}
        })
        v.startAnimation(a)
    }

    private fun expandAction(v: View): Animation {
        v.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        val targetHeight = v.measuredHeight
        v.layoutParams.height = 0
        v.visible()
        val a: Animation = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                v.layoutParams.height =
                    if (interpolatedTime == 1f) ViewGroup.LayoutParams.WRAP_CONTENT else (targetHeight * interpolatedTime).toInt()
                v.requestLayout()
            }

            override fun willChangeBounds(): Boolean = true

        }
        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toLong()
        v.startAnimation(a)
        return a
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation
            ) {
                if (interpolatedTime == 1f)
                    v.gone()
                else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean = true
        }
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toLong()
        v.startAnimation(a)
    }

    fun initScaleDownAnim(v: View, xScale: Float, yScale: Float, delay: Long): AnimatorSet {
        val xAnim = ObjectAnimator.ofFloat(v, "scaleX", xScale)
        val yAnim = ObjectAnimator.ofFloat(v, "scaleY", yScale)
        return AnimatorSet().apply {
            startDelay = delay
            duration = 150
            playTogether(xAnim, yAnim)
        }
    }

    fun initScaleUpAnim(v: View, xScale: Float, yScale: Float, delay: Long): AnimatorSet {
        val xAnim = ObjectAnimator.ofFloat(v, "scaleX", xScale)
        val yAnim = ObjectAnimator.ofFloat(v, "scaleY", yScale)
        return AnimatorSet().apply {
            startDelay = delay
            duration = 150
            playTogether(xAnim, yAnim)
        }
    }

    fun clearAnim(anim: AnimatorSet) = anim.run {
        removeAllListeners()
        end()
        cancel()
    }
}