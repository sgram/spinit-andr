package com.bonch_dev.spinit.presentation.base.interfaces

interface IDataCompletion<in T> {
    fun onResult(data: T)
    fun onError(msg: String? = null)
}