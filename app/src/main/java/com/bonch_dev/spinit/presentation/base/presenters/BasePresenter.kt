package com.bonch_dev.spinit.presentation.base.presenters

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBasePresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

open class BasePresenter<V : IBaseView> : IBasePresenter<V> {
    private var fragment: V? = null

    override fun attachView(view: V) {
        fragment = view
    }

    override fun detachView() {
        if (fragment != null) fragment = null
    }

    fun getView(): V? = fragment
}