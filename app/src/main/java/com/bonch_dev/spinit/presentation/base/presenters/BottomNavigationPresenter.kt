package com.bonch_dev.spinit.presentation.base.presenters

import android.util.Log
import android.view.MenuItem
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.interactors.userinfo.IUserInfoInteractor
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.FEMALE
import com.bonch_dev.spinit.domain.utils.Keys.MALE
import com.bonch_dev.spinit.domain.utils.Keys.adminAccess
import com.bonch_dev.spinit.domain.utils.Keys.avatarURL
import com.bonch_dev.spinit.domain.utils.Keys.havePassword
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBottomNavigationPresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.base.view.interfaces.IBottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.achievements.view.AchievementsFragment
import com.bonch_dev.spinit.presentation.modules.article.view.SavedArticlesFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view.MyCoursesFragment
import com.bonch_dev.spinit.presentation.modules.teachermsg.view.TeacherMessagesFragment
import com.bonch_dev.spinit.router.MainRouter
import com.bonch_dev.spinit.router.SettingsRouter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import javax.inject.Inject

class BottomNavigationPresenter @Inject constructor(
    private val interactor: IUserInfoInteractor,
    private val router: MainRouter,
    private val settingsRouter: SettingsRouter
) :
    IBottomNavigationPresenter, BasePresenter<IBottomNavigationFragment>(),
    IDataCompletion<com.bonch_dev.spinit.domain.entity.UserInfo> {

    override fun onResume() {
        getUserInfo()
    }

    override fun onStart() {
        getView()?.run {
            lockDrawer()
            (getView() as InternetFragment).hideConnErr()
        }
    }

    override fun onNotificationIconClicked(bottomNavigationFragment: BottomNavigationFragment) {
        router.toNotificationFragment(bottomNavigationFragment)
    }

    override fun onHeaderIVClick(bottomNavigationFragment: BottomNavigationFragment) {
        settingsRouter.toEditProfile(bottomNavigationFragment)
        closeDrawer()
    }

    override fun getUserInfo() {
        if (NetworkConnection.isInternetAvailable())
            interactor.getUserInfo(
                getView()?.needToSendToken ?: true,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onError(msg: String?) {
        Log.e(javaClass.simpleName, "error")
        getView()?.run {
            getMainAct().removeSplash()
            unlockDrawer()
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    private fun bindKeys(userInfo: com.bonch_dev.spinit.domain.entity.UserInfo) {
        Log.e(javaClass.simpleName, "result")
        userInfo.apply {
            admin_access?.let { adminAccess = false }
            avatar_url?.let { avatarURL = it }
            gender?.let { Keys.gender = it }
            have_password?.let { havePassword = it }
        }
    }

    private fun bindNotificationCountBadge(count: Int?) {
        count?.let {
            if (it > 0) getView()?.showNotifBadge(it.toString())
            else getView()?.hideNotifBadge()
        }
    }

    private fun setProfileBackground() {
        getView()?.run {
            when (Keys.gender) {
                MALE -> {
                    setHeaderImage(R.drawable.avatar)
                    getMainFrag()?.setProfileBackground(R.drawable.avatar)
                }
                FEMALE -> {
                    setHeaderImage(R.drawable.avatar_women)
                    getMainFrag()?.setProfileBackground(R.drawable.avatar_women)
                }
                else -> {
                }
            }
        }
    }

    private fun bindHeaderData(userInfo: com.bonch_dev.spinit.domain.entity.UserInfo) {
        userInfo.run {
            if (email != null) getView()?.setEmail(email)
            else if (phone_number != null) getView()?.setEmail(phone_number)
            getView()?.setProfileName("$first_name $last_name")
        }
    }

    override fun onResult(data: com.bonch_dev.spinit.domain.entity.UserInfo) {
        getView()?.run {
            if (needToSendToken) needToSendToken = false
            unlockDrawer()
        }
        data.run {
            bindKeys(this)
            bindNotificationCountBadge(notifications_count)
            setProfileBackground()
            bindHeaderData(this)
        }
        Keys.run {
            if (!adminAccess) {
                val viewpagerAdapter = getView()?.getViewPagerAdapter()
                viewpagerAdapter?.let {
                    val myCoursesFragment = it.getItem(0) as MyCoursesFragment
                    val savedArticlesFragment = it.getItem(1) as SavedArticlesFragment
                    val achievementsFragment = it.getItem(2) as AchievementsFragment
                    val teacherMessagesFragment = it.getItem(3) as TeacherMessagesFragment
                    myCoursesFragment.bindData(data.courses)
                    achievementsFragment.bindAchievements(data.achievements)
                    savedArticlesFragment.bindSavedArticles(data.liked)
                    teacherMessagesFragment.onHomeworkGot(data.homeworks as MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>)
                }
                getView()?.getMainFrag()?.bindData(data)
            }
        }
        loadPhotos(data)
    }

    override fun loadPhotos(data: com.bonch_dev.spinit.domain.entity.UserInfo) {
        getView()?.let {
            Glide.with(it.getCtx()).load(data.avatar_url).apply(
                RequestOptions().signature(ObjectKey(System.currentTimeMillis().toString())).circleCrop()
            ).into(it.getHeaderImageView())
        }
    }

    override fun onBottomNavItemSelected(menuItem: MenuItem, bottomNavigationFragment: BottomNavigationFragment) {
        when (menuItem.itemId) {
            R.id.nav_main -> toMainFrag(bottomNavigationFragment, menuItem)
            R.id.nav_courses -> toAllCoursesFrag(bottomNavigationFragment, menuItem)
            R.id.nav_blog -> toBlogFrag(bottomNavigationFragment, menuItem)
        }
    }

    private fun hideSearch() {
        getView()?.hideSearch()
    }

    private fun showSearch() {
        getView()?.showSearch()

    }

    override fun onDrawerItemSelected(menuItem: MenuItem, bottomNavigationFragment: BottomNavigationFragment) {
        when (menuItem.itemId) {
            R.id.about -> router.toAboutAppFragment(bottomNavigationFragment)
            R.id.contact -> router.toContactFragment(bottomNavigationFragment)
            R.id.settings -> router.toSettingsFragment(bottomNavigationFragment)
        }
        closeDrawer()
    }

    private fun closeDrawer() {
        val drawer = getView()?.getDrawer()
        drawer?.postDelayed({
            drawer.closeDrawer(GravityCompat.START)
            getView()?.getMainAct()?.isDrawerOpen = false
        }, 200)
    }

    private fun toMainFrag(bottomNavigationFragment: BottomNavigationFragment, menuItem: MenuItem) {
        hideSearch()
        bottomNavigationFragment.run {
            if (currentFrag.isVisible && !mainFragment.isVisible) {
                fm.beginTransaction().hide(currentFrag).show(mainFragment).commitNow()
                menuItem.isChecked = true
                currentFrag = mainFragment
            }
        }
    }

    private fun toAllCoursesFrag(bottomNavigationFragment: BottomNavigationFragment, menuItem: MenuItem) {
        showSearch()
        bottomNavigationFragment.run {
            if (currentFrag.isVisible && !allCoursesFragment.isVisible) {
                fm.beginTransaction().hide(currentFrag).show(allCoursesFragment).commitNow()
                menuItem.isChecked = true
                currentFrag = allCoursesFragment
            }
        }
    }

    private fun toBlogFrag(bottomNavigationFragment: BottomNavigationFragment, menuItem: MenuItem) {
        hideSearch()
        bottomNavigationFragment.run {
            if (currentFrag.isVisible && !blogFragment.isVisible) {
                fm.beginTransaction().hide(currentFrag).show(blogFragment).commitNow()
                menuItem.isChecked = true
                currentFrag = blogFragment
            }
        }
    }
}
