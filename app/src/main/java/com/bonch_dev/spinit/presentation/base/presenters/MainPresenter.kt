package com.bonch_dev.spinit.presentation.base.presenters

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.animation.ValueAnimator
import android.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.ImageView
import androidx.core.animation.doOnCancel
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.interactors.auth.signin.ISignInInteractor
import com.bonch_dev.spinit.domain.utils.ConditionsOfUseReader
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.ResourceManager.getString
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IMainPresenter
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.base.view.interfaces.IMainActivity
import com.bonch_dev.spinit.router.MainRouter
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import javax.inject.Inject

class MainPresenter @Inject constructor(private val router: MainRouter, private val interactor: ISignInInteractor) :
    IMainPresenter {

    override fun onStart(act: IMainActivity) {
        interactor.checkAuth(
            isLogin = act::onLogin,
            err = {
                AlertDialog.Builder(act.ctx)
                    .setTitle(getString(R.string.login_error))
                    .setCancelable(false)
                    .setMessage(getString(R.string.have_problems_with_connection_to_account))
                    .setPositiveButton(getString(R.string.try_again)) { dialog, _ ->
                        onStart(act)
                        dialog.dismiss()
                    }
                    .setNeutralButton(getString(R.string.to_login_screen)) { dialog, _ ->
                        act.onNotLogin()
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            },
            notLogin = act::onNotLogin
        )
    }

    override fun animateSplash(act: IMainActivity) {
        val splashIV = act.getSplashIV().apply { scaleType = ImageView.ScaleType.FIT_XY }
        val translate = ObjectAnimator.ofFloat(splashIV, View.Y, -30f)
        val xAnim = initXAnim(act)
        val yAnim = initYAnim(act)
        initAnimatorSet(translate, xAnim, yAnim, act).start()
    }

    private fun initAnimatorSet(trans: ObjectAnimator, x: ValueAnimator, y: ValueAnimator, act: IMainActivity): AnimatorSet {
        val log: (String) -> Unit = { Log.e(this@MainPresenter.javaClass.simpleName, it) }
        return AnimatorSet().apply {
            playTogether(trans, x, y)
            duration = 300
            interpolator = AccelerateDecelerateInterpolator()
            doOnStart { log("Anim started") }
            doOnCancel {
                act.setAnimStatus(true)
                log("Anim cancelled")
            }
            doOnEnd {
                act.getSplashPB().alpha = 1f
                act.setAnimStatus(true)
                log("Anim finished")
            }
        }
    }

    private fun initXAnim(v: IMainActivity): ValueAnimator {
        val defIVSize = v.getDefaultWH()
        val splashIV = v.getSplashIV()
        val xAnim = ValueAnimator.ofInt(defIVSize.first, defIVSize.first + 150)
        xAnim.addUpdateListener {
            val animatedValue = xAnim.animatedValue as Int
            val layoutParams = splashIV.layoutParams.apply { width = animatedValue }
            splashIV.layoutParams = layoutParams
            splashIV.requestLayout()
        }
        return xAnim
    }

    private fun initYAnim(v: IMainActivity): ValueAnimator {
        val defIVSize = v.getDefaultWH()
        val splashIV = v.getSplashIV()
        val yAnim = ValueAnimator.ofInt(defIVSize.second, defIVSize.second + 150)
        yAnim.addUpdateListener {
            val animatedValue = yAnim.animatedValue as Int
            val layoutParams = splashIV.layoutParams.apply { height = animatedValue }
            splashIV.layoutParams = layoutParams
            splashIV.requestLayout()
        }
        return yAnim
    }

    override fun onVkActivityResultSuccess(res: VKAccessToken?, activity: MainActivity) {
        res?.email?.let {
            interactor.checkIsEmailExist(it,
                onEmailNotExist = {
                    activity.showConditionOfUse(
                        conditionsOfUse = getTextConditionsOfUse(),

                        onAccept = {
                            activity.measureIV()
                            interactor.vkSignIn(
                                res,
                                callback = router::navigateToMainActivity,
                                err = activity::showToast
                            )
                        },

                        onCancel = {
                            VKSdk.logout()
                        }
                    )
                },

                onEmailExist = {
                    activity.measureIV()
                    interactor.vkSignIn(
                        res,
                        callback = router::navigateToMainActivity,
                        err = activity::showToast
                    )
                },

                onError = { msg ->
                    activity.showToast(msg)
                })
        }
    }

    private fun getTextConditionsOfUse(): String {
        ConditionsOfUseReader().getConditionsOfUseString()?.let {
            return it
        } ?: return ""
    }

    override fun onVkActivityResultError(err: VKError?) {
        if (err != null)
            Log.e("VkActivityResultError", err.toString())
    }

    override fun onGoogleActivityResult(task: Task<GoogleSignInAccount>, activity: MainActivity) {
        task.result?.email?.let {
            interactor.checkIsEmailExist(it,
                onEmailExist = {
                    activity.measureIV()
                    interactor.googleSignIn(
                        task,
                        callback = router::navigateToMainActivity,
                        err = activity::showToast
                    )
                },

                onEmailNotExist = {
                    activity.showConditionOfUse(
                        conditionsOfUse = getTextConditionsOfUse(),

                        onAccept = {
                            activity.measureIV()
                            interactor.googleSignIn(
                                task,
                                callback = router::navigateToMainActivity,
                                err = activity::showToast
                            )
                        },

                        onCancel = {
                            Auth.GoogleSignInApi?.signOut(Keys.GAC)
                            Keys.GAC.disconnect()

                        }
                    )
                },

                onError = {
                    activity.showToast(it)
                })
        }
    }
}
