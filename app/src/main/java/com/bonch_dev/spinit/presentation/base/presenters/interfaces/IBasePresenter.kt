package com.bonch_dev.spinit.presentation.base.presenters.interfaces

interface IBasePresenter<V : IBaseView> {
    fun attachView(view: V)
    fun detachView()
}