package com.bonch_dev.spinit.presentation.base.presenters.interfaces

interface IBaseView {
    fun setListeners()
}