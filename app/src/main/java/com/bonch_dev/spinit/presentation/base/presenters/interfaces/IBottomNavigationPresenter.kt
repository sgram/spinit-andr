package com.bonch_dev.spinit.presentation.base.presenters.interfaces

import android.view.MenuItem
import com.bonch_dev.spinit.domain.entity.UserInfo
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment

interface IBottomNavigationPresenter {
    fun onResume()
    fun onStart()
    fun onHeaderIVClick(bottomNavigationFragment: BottomNavigationFragment)
    fun onNotificationIconClicked(bottomNavigationFragment: BottomNavigationFragment)
    fun getUserInfo()
    fun loadPhotos(data: UserInfo)
    fun onBottomNavItemSelected(menuItem: MenuItem, bottomNavigationFragment: BottomNavigationFragment)
    fun onDrawerItemSelected(menuItem: MenuItem, bottomNavigationFragment: BottomNavigationFragment)
}