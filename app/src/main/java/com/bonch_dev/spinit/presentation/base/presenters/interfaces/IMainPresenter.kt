package com.bonch_dev.spinit.presentation.base.presenters.interfaces

import com.bonch_dev.spinit.domain.utils.ErrType
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.base.view.interfaces.IMainActivity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.vk.sdk.VKAccessToken
import com.vk.sdk.api.VKError

typealias DataCompletionHandler<T> = (data: T, error: ErrType?) -> Unit
typealias ErrorHandler = (error: String) -> Unit
typealias DataHandler<T> = (data: T) -> Unit
typealias SuccessHandler = () -> Unit

interface IMainPresenter {
    fun animateSplash(act: IMainActivity)
    fun onStart(act: IMainActivity)
    fun onVkActivityResultSuccess(res: VKAccessToken?, activity: MainActivity)
    fun onVkActivityResultError(err: VKError?)
    fun onGoogleActivityResult(task: Task<GoogleSignInAccount>, activity: MainActivity)
}