package com.bonch_dev.spinit.presentation.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keys.BOTTOM_NAVIGATION
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class Base : Fragment() {
    @Inject
    lateinit var router: MainRouter

    init {
        App.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.base, container, false)
        router.hostFrag?.childFragmentManager?.beginTransaction()
            ?.replace(R.id.base_frame_container, BottomNavigationFragment(), BOTTOM_NAVIGATION)?.commit()
        return view
    }
}