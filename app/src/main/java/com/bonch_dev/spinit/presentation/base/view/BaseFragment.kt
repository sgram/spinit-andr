package com.bonch_dev.spinit.presentation.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keyboard
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.initBackArrow
import com.bonch_dev.spinit.domain.utils.visible

abstract class BaseFragment : Fragment() {

    var toolbar: Toolbar? = null

    fun hideKB() {
        Keyboard.hide(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = provideView(inflater, container)
        initTbParams()
        return view
    }

    open fun initTbParams() {
        toolbar?.initBackArrow(activity as AppCompatActivity)
    }

    open fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        return super.onCreateView(inflater, container, null)
    }

}

abstract class InternetFragment : BaseFragment() {

    private var splash: LinearLayout? = null
    private var noInternet: LinearLayout? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = provideView(inflater, container)
        splash = view?.findViewById(R.id.splash)
        noInternet = view?.findViewById(R.id.no_internet)
        repeatConnBtnListener()
        initTbParams()
        return view
    }

    override fun initTbParams(){
        super.initTbParams()
    }

    abstract fun repeatConnBtnListener()

    fun showSplash() {
        splash?.visible()
    }

    fun hideSplash() {
        splash?.gone()
    }

    fun showConnErr() {
        noInternet?.visible()
    }

    fun hideConnErr() {
        noInternet?.gone()
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        return super.provideView(inflater, container)
    }
}
