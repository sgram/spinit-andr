package com.bonch_dev.spinit.presentation.base.view


import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentBottomNavigationBinding
import com.bonch_dev.spinit.domain.utils.Keys.ALL_COURSES_FRAG
import com.bonch_dev.spinit.domain.utils.Keys.BLOG_FRAG
import com.bonch_dev.spinit.domain.utils.Keys.MAIN_FRAG
import com.bonch_dev.spinit.domain.utils.Keys.USER_INFO
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.invisible
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.presenters.BottomNavigationPresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBottomNavigationPresenter
import com.bonch_dev.spinit.presentation.base.view.interfaces.IBottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.AllCoursesFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.adapter.MainFragPagerAdapter
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.IMainFragment
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.MainFragment
import javax.inject.Inject


class BottomNavigationFragment : InternetFragment(),
    IBottomNavigationFragment {

    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    lateinit var fm: FragmentManager
    var searchView: androidx.appcompat.widget.SearchView? = null
    lateinit var notificationsBTN: MenuItem
    var searchItem: MenuItem? = null
    val mainFragment: MainFragment = MainFragment()
    val blogFragment: BlogFragment = BlogFragment()
    val allCoursesFragment: AllCoursesFragment = AllCoursesFragment()
    var currentFrag: Fragment = mainFragment
    private lateinit var act: MainActivity

    private lateinit var toolbarMenu: Menu

    lateinit var binding: FragmentBottomNavigationBinding
    override var needToSendToken: Boolean = true

    private var isMenuInflated = false

    @Inject
    lateinit var presenter: IBottomNavigationPresenter

    private val TAG = this.javaClass.simpleName

    init {
        App.appComponent.inject(this)
        pres = presenter
        Log.e(TAG, "Init")
    }

    companion object {
        var pres: IBottomNavigationPresenter? = null
        fun refresh() = pres?.getUserInfo()
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentBottomNavigationBinding.inflate(inflater, container, false)
        toolbar = binding.toolbar
        act = activity as MainActivity

        act.setSupportActionBar(toolbar)
        fm = childFragmentManager

        act.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        actionBarDrawerToggle = getDrawerListener()


        (presenter as BottomNavigationPresenter).attachView(this)
        presenter.onStart()

        firstFragmentInit()
        setListeners()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
    }

    override fun showErr() {
        getMainFrag()?.showConnErr()
    }

    override fun hideErr() {
        getMainFrag()?.hideConnErr()
    }

    override fun showSearch() {
        searchItem?.isVisible = true
        searchView?.setQuery("", false)
        searchView?.onActionViewCollapsed()
    }

    override fun hideSearch() {
        searchItem?.isVisible = false
        searchView?.onActionViewCollapsed()
    }

    override fun repeatConnBtnListener() {

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (!isMenuInflated) {
            act.menuInflater.inflate(R.menu.toolbar_menu, menu)
            toolbarMenu = menu
            toolbarMenu.findItem(R.id.menu_search).isVisible = false
            isMenuInflated = true
        }

    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)
        notificationsBTN = toolbarMenu.findItem(R.id.notification)
        searchItem = toolbarMenu.findItem(R.id.menu_search)
        searchView = searchItem?.actionView as androidx.appcompat.widget.SearchView?

        searchView?.setOnSearchClickListener {
            showBackIcon()
            searchView?.requestFocus()
            notificationsBTN.isVisible = false
        }

        searchView?.setOnCloseListener {
            searchView?.setQuery("", false)
            true
        }

    }

    private fun showBackIcon() {
        actionBarDrawerToggle.isDrawerIndicatorEnabled = false
        act.supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        act.supportActionBar?.setDisplayHomeAsUpEnabled(true)

        lockDrawer()

        binding.toolbar.setNavigationOnClickListener {
            collapseSearchView()
        }
    }

    private fun showDrawerIcon() {

        unlockDrawer()

        binding.toolbar.setNavigationOnClickListener {
            getDrawer().openDrawer(GravityCompat.START)
        }

        // act.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        actionBarDrawerToggle.isDrawerIndicatorEnabled = true
        actionBarDrawerToggle.syncState()

    }

    override fun isSearchViewVisible(): Boolean = searchView?.isIconified == false

    override fun collapseSearchView() {
        hideKB()
        searchView?.setQuery("", false)
        notificationsBTN.isVisible = true
        showDrawerIcon()
        searchView?.onActionViewCollapsed()
    }

    override fun getMainAct(): MainActivity = act

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden)
            setCurrentFrag()
    }

    private fun setCurrentFrag() {
        val backHandle: CurrentFragListener = act
        backHandle.selectedFragment(this)
    }


    override fun getDrawer(): DrawerLayout = binding.drawerLayout

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.notification -> presenter.onNotificationIconClicked(this)
        }
        return true
    }

    override fun setHeaderImage(res: Int) =
        binding.navigationView.getHeaderView(0).findViewById<ImageView>(R.id.profile_iv).setBackgroundResource(res)

    override fun getViewPagerAdapter(): MainFragPagerAdapter? = getMainFrag()?.getViewPagerAdapter()

    override fun getMainFrag(): IMainFragment? = fm.findFragmentByTag(MAIN_FRAG) as IMainFragment?

    override fun lockDrawer() = binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

    override fun unlockDrawer() = binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

    override fun setEmail(email: String) {
        binding.navigationView.getHeaderView(0).findViewById<TextView>(R.id.email_tv).text = email
    }

    override fun setProfileName(name: String) {
        binding.navigationView.getHeaderView(0).findViewById<TextView>(R.id.name_tv).text = name
    }

    private fun getDrawerListener(): ActionBarDrawerToggle {
        return object : ActionBarDrawerToggle(
            act,
            binding.drawerLayout,
            binding.toolbar,
            R.string.open_nav_drawer,
            R.string.close_nav_drawer
        ) {
            override fun onDrawerOpened(drawerView: View) {
                Log.e(javaClass.simpleName, "drawer opened")
                act.isDrawerOpen = true
            }

            override fun onDrawerClosed(drawerView: View) {
                Log.e(javaClass.simpleName, "drawer closed")
                act.isDrawerOpen = false
            }

        }
    }

    override fun initTbParams() {
        getDrawer().addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.apply {
            drawerArrowDrawable.color = Color.WHITE
            syncState()
        }

    }

    override fun showNotifBadge(text: String) =
        binding.badge.run {
            this.text = text
            visible()
        }

    override fun hideNotifBadge() = binding.badge.invisible()

    override fun getHeaderImageView(): ImageView =
        binding.navigationView.getHeaderView(0).findViewById(R.id.profile_iv)

    override fun getCtx(): Context = requireContext()

    override fun firstFragmentInit() {
        val ft = fm.beginTransaction()
        @IdRes val container: Int = R.id.fragment_container
        ft.run {
            if (!blogFragment.isAdded)
                add(container, blogFragment, BLOG_FRAG).hide(blogFragment)
            if (!allCoursesFragment.isAdded)
                add(container, allCoursesFragment, ALL_COURSES_FRAG).hide(allCoursesFragment)
            if (!mainFragment.isAdded)
                add(container, mainFragment, MAIN_FRAG).commit()
        }
    }

    override fun setListeners() {
        binding.run {
            navigationView.getHeaderView(0).findViewById<ImageView>(R.id.profile_iv).setOnClickListener {
                presenter.onHeaderIVClick(this@BottomNavigationFragment)
            }
            navigationView.setNavigationItemSelectedListener { p0 ->
                presenter.onDrawerItemSelected(p0, this@BottomNavigationFragment)
                true
            }
            bottomNavigationBar.setOnNavigationItemSelectedListener { p0 ->
                presenter.onBottomNavItemSelected(p0, this@BottomNavigationFragment)
                true
            }
        }

    }


    override fun onDestroyView() {
        cancelJobs(USER_INFO)
        Log.e(javaClass.simpleName, "onDestroyView")
        pres = null
        (presenter as BottomNavigationPresenter).detachView()
        super.onDestroyView()
    }

    override fun onResume() {
        super.onResume()
        setCurrentFrag()
        presenter.onResume()
    }
}