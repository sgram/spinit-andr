package com.bonch_dev.spinit.presentation.base.view

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.NavInflater
import androidx.navigation.fragment.NavHostFragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.ActivityMainBinding
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.THEME
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IMainPresenter
import com.bonch_dev.spinit.presentation.base.view.interfaces.IBottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.interfaces.IMainActivity
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.ISignInPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.view.ISignInFragment
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.ConditionsOfUseDialog
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.ISignUpStepOneFragment
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.ISignUpStepTwoFragment
import com.bonch_dev.spinit.presentation.modules.broadcast.view.BroadcastFragment
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.notification.view.INotificationFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.EditProfileFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.EditProfileFragment.Companion.GALLERY_CODE
import com.bonch_dev.spinit.router.MainRouter
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity(), IMainActivity, CurrentFragListener, Animation.AnimationListener {
    private var defaultLogoWidth: Int = 0
    private var defaultLogoHeight: Int = 0
    private var selectedFrag: Fragment? = null
    private var animationHasBeenPlayed: Boolean = false
    private lateinit var controller: NavController
    private lateinit var inflater: NavInflater
    private lateinit var navGraph: NavGraph
    private var destListener: NavController.OnDestinationChangedListener? = null
    private lateinit var hostFrag: NavHostFragment
    private var splashIsRemoved: Boolean = false
    override val ctx: Context = this
    override var isAuthChecked: Boolean = false
    private var isIVSizeMeasured: Boolean = false
    var isDrawerOpen: Boolean = false

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var presenter: IMainPresenter

    @Inject
    lateinit var router: MainRouter

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    @Inject
    lateinit var authPresenter: ISignInPresenter

    init {
        App.appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initHostFrag()

        initNavController()

        binding.splash.root.visible()
    }

    override fun measureIV() {
        binding.splash.run {
            if (root.isGone)
                root.visible()
            splashIv.post {
                defaultLogoHeight = splashIv.measuredHeight
                defaultLogoWidth = splashIv.measuredWidth
                Log.e(javaClass.simpleName, "width: $defaultLogoWidth\nheight: $defaultLogoHeight")
                isIVSizeMeasured = true
                presenter.animateSplash(this@MainActivity)
            }
        }

    }

    override fun onStart() {
        super.onStart()
        Log.e(javaClass.simpleName, "onStart")
        if (!isAuthChecked)
            presenter.onStart(this)
    }

    private fun initNavController() {
        controller = hostFrag.navController
        inflater = controller.navInflater
        navGraph = inflater.inflate(R.navigation.navigation_main)
    }

    private fun initHostFrag() {
        hostFrag = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        router.hostFrag = hostFrag
    }

    override fun getSplashIV(): ImageView = binding.splash.splashIv

    override fun getSplashPB(): ProgressBar = binding.splash.splashProgress

    override fun setAnimStatus(status: Boolean) {
        animationHasBeenPlayed = status
    }

    override fun getDefaultWH(): Pair<Int, Int> = Pair(defaultLogoWidth, defaultLogoHeight)

    override fun onAnimationEnd(animation: Animation?) {
        binding.splash.run {
            splashProgress.alpha = 0f
            splashIv.scaleType = ImageView.ScaleType.FIT_XY
            splashIv.layoutParams = LinearLayout.LayoutParams(defaultLogoWidth, defaultLogoHeight)
            splashIv.requestLayout()
        }

        hideSplash()
        splashIsRemoved = true
    }

    override fun onAnimationRepeat(animation: Animation?) {}
    override fun onAnimationStart(animation: Animation?) = window.decorView.setBackgroundColor(Color.WHITE)

    override fun removeSplash() {
        if (!splashIsRemoved) {
            val anim = AnimationUtils.loadAnimation(this, R.anim.activity_exit_anim).apply {
                duration = 200
                setAnimationListener(this@MainActivity)
            }
            binding.splash.root.startAnimation(anim)
        }
    }

    fun hideSplash() = binding.splash.root.gone()

    override fun onLogin() {
        navGraph.startDestination = R.id.base
        controller.graph = navGraph

        router.run {
            navController = controller
        }

        destListener = NavController.OnDestinationChangedListener { _, destination, _ ->
            Log.e("destination", destination.label.toString())
        }

        destListener?.let {
            router.navController?.addOnDestinationChangedListener(it)
        }

        measureIV()
        isAuthChecked = true

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.e(javaClass.simpleName, "requestCode:$requestCode\ngalleryCode:${GALLERY_CODE}\ngrant:${grantResults[0]}")
        if (requestCode == GALLERY_CODE && selectedFrag is EditProfileFragment && grantResults[0] == PackageManager.PERMISSION_GRANTED) (selectedFrag as EditProfileFragment).openGallery()
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onNotLogin() {
        hideSplash()
        window.decorView.setBackgroundColor(Color.WHITE)
        navGraph.startDestination = R.id.startFragment
        controller.graph = navGraph

        router.run {
            navController = controller
        }

        destListener = NavController.OnDestinationChangedListener { _, destination, _ ->
            Log.e("destination", destination.label.toString())
        }

        destListener?.let {
            router.navController?.addOnDestinationChangedListener(it)
        }

        isAuthChecked = true
    }

    override fun setTheme() {
        when (sp.getInt(THEME, R.style.LightTheme)) {
            R.style.DarkTheme -> setTheme(R.style.DarkTheme)
            R.style.LightTheme -> setTheme(R.style.LightTheme)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 10) {
            Keys.GAC.connect()
            val res = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (res?.isSuccess == true) {
                val acct = res.signInAccount
                val idToken = acct?.idToken
                val authCode = acct?.serverAuthCode
                Log.e(javaClass.simpleName, "authCode: ${authCode ?: "No server code"}\nidToken: $idToken")
                presenter.onGoogleActivityResult(GoogleSignIn.getSignedInAccountFromIntent(data), this)
                // authPresenter.getAccessToken(idToken, authCode)
            }
            return
        }
        if (!VKSdk.onActivityResult(requestCode, resultCode, data,
                object : VKCallback<VKAccessToken> {
                    override fun onResult(res: VKAccessToken?) {
                        presenter.onVkActivityResultSuccess(res, this@MainActivity)
                    }

                    override fun onError(error: VKError?) =
                        presenter.onVkActivityResultError(error)
                })
        )
            super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        Log.e(javaClass.simpleName, selectedFrag?.toString() ?: "selected frag is null")
        if (selectedFrag is IBottomNavigationFragment) {
            val frag = selectedFrag as IBottomNavigationFragment
            Log.e(javaClass.simpleName, "selectedFrag is BottomNav")
            val drawer = frag.getDrawer()
            if (isDrawerOpen) {
                drawer.closeDrawer(GravityCompat.START)
                return
            }
            if (frag.isSearchViewVisible()) {
                frag.collapseSearchView()
                return
            }
        }
        if (router.hostFrag?.childFragmentManager?.backStackEntryCount == 0) {
            super.onBackPressed()
            return
        }
        when (selectedFrag) {
            is ISignInFragment, is ISignUpStepOneFragment, is ISignUpStepTwoFragment -> router.navController?.navigateUp()
            is INotificationFragment -> {
                BottomNavigationFragment.refresh()
                router.hostFrag?.childFragmentManager?.popBackStack()
            }
            is IComment -> {
                val iComment = selectedFrag as IComment
                iComment.run {
                    if (getBSBehaviour().state == BottomSheetBehavior.STATE_EXPANDED)
                        getBSBehaviour().state = BottomSheetBehavior.STATE_HIDDEN
                    else
                        router.hostFrag?.childFragmentManager?.popBackStack()
                }
            }
            is BroadcastFragment -> {
                val broadcastFragment = selectedFrag as BroadcastFragment
                broadcastFragment.run {
                    if (getVideoPlayer().isFullScreen()) {
                        getVideoPlayer().exitFullScreen()
                        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    } else
                        router.hostFrag?.childFragmentManager?.popBackStack()
                }
            }
            else -> router.hostFrag?.childFragmentManager?.popBackStack()
        }
    }

    override fun onDestroy() {
        Log.e("${javaClass.simpleName} lifecycle", "destroy")
        destListener?.let {
            router.navController?.removeOnDestinationChangedListener(it)
        }
        super.onDestroy()
    }

    override fun selectedFragment(fragment: Fragment) {
        selectedFrag = fragment
    }

    fun showConditionOfUse(conditionsOfUse: String, onAccept: () -> Unit, onCancel: () -> Unit) {
        val clickListener = object : ConditionsOfUseDialog.ConditionsOfUseClickListener {
            override fun onConditionsOfUseAccepted() {
                onAccept()
            }

            override fun onConditionsOfUseCancelled() {
                onCancel()
            }
        }

        val dialog = ConditionsOfUseDialog(conditionsOfUse, clickListener)
        dialog.isCancelable = false
        dialog.show(supportFragmentManager, "ConditionsOfUse")
    }

    override fun onStop() {
        Log.e("${javaClass.simpleName} lifecycle", "stop")
        super.onStop()
    }


}
