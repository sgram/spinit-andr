package com.bonch_dev.spinit.presentation.base.view.interfaces

import android.content.Context
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.drawerlayout.widget.DrawerLayout
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.adapter.MainFragPagerAdapter
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.IMainFragment

interface IBottomNavigationFragment : IBaseView {
    fun getDrawer(): DrawerLayout
    fun getMainAct(): MainActivity
    fun showErr()
    fun hideErr()
    var needToSendToken: Boolean
    fun isSearchViewVisible(): Boolean
    fun collapseSearchView()
    fun lockDrawer()
    fun unlockDrawer()
    fun setEmail(email: String)
    fun setProfileName(name: String)
    fun setHeaderImage(@DrawableRes res: Int)
    fun showNotifBadge(text: String)
    fun hideNotifBadge()
    fun getHeaderImageView(): ImageView
    fun getViewPagerAdapter(): MainFragPagerAdapter?
    fun getMainFrag(): IMainFragment?
    fun getCtx(): Context
    fun firstFragmentInit()
    fun showSearch()
    fun hideSearch()
}