package com.bonch_dev.spinit.presentation.base.view.interfaces

import android.content.Context
import android.widget.ImageView
import android.widget.ProgressBar

interface IMainActivity {
    fun measureIV()
    val ctx: Context
    var isAuthChecked: Boolean
    fun setTheme()
    fun onLogin()
    fun onNotLogin()
    fun removeSplash()
    fun getSplashIV(): ImageView
    fun getSplashPB(): ProgressBar
    fun getDefaultWH(): Pair<Int, Int>
    fun setAnimStatus(status: Boolean)
}