package com.bonch_dev.spinit.presentation.modules.about_app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.databinding.ItemHowWeLearnBinding
import com.bonch_dev.spinit.domain.entity.HowWeLearn

class HowWeLearnAdapter(val list: List<HowWeLearn>) : RecyclerView.Adapter<HowWeLearnAdapter.HowWeLearnViewHolder>() {
    inner class HowWeLearnViewHolder(val binding: ItemHowWeLearnBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            val item = list[position]
            binding.run {
                howWeLearnTvNumber.text = (position + 1).toString()
                howWeLearnTvTitle.text = item.title
                howWeLearnTvDesc.text = item.desc
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HowWeLearnViewHolder =
        HowWeLearnViewHolder(ItemHowWeLearnBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: HowWeLearnViewHolder, position: Int) = holder.bind(position)
}