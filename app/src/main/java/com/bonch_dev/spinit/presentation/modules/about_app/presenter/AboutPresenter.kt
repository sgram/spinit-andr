package com.bonch_dev.spinit.presentation.modules.about_app.presenter

import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.about_app.view.IAboutFragment

class AboutPresenter : IAboutPresenter, BasePresenter<IAboutFragment>() {

    override fun initSpanText(): SpannableString {
        val processText =
            SpannableString("Учиться на SpinIT удобно: на сайте или в мобильном приложении")
        val processWords = processText.split(" ")
        for (word in processWords) {
            if (word.startsWith("сайте")) {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(Keys.SITE_LINK)
                        getView()?.getCtx()?.startActivity(intent)
                    }
                }
                processText.setSpan(
                    clickableSpan,
                    processText.indexOf(word),
                    processText.indexOf(word) + word.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        return processText
    }
}