package com.bonch_dev.spinit.presentation.modules.about_app.presenter

import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.about_app.view.IContactFragment

class ContactPresenter : IContactPresenter, BasePresenter<IContactFragment>() {

    override fun initSocialText(): SpannableString {
        val socialText =
            SpannableString("Новости платформы Вы можете найти в социальных сетях: Вконтакте, Instagram")
        val socialWords = socialText.split(" ")
        for (word in socialWords) {
            if (word.startsWith("Вконтакте,")) {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(android.content.Intent.ACTION_VIEW)
                        intent.data = Uri.parse(Keys.VK_LINK)
                        getView()?.getCtx()?.startActivity(intent)
                    }
                }
                socialText.setSpan(
                    clickableSpan,
                    socialText.indexOf(word),
                    socialText.indexOf(word) + word.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            } else if (word.startsWith("Instagram")) {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(android.content.Intent.ACTION_VIEW)
                        intent.data = Uri.parse(Keys.INST_LINK)
                        getView()?.getMainAct()?.startActivity(intent)
                    }
                }
                socialText.setSpan(
                    clickableSpan,
                    socialText.indexOf(word),
                    socialText.indexOf(word) + word.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        return socialText
    }

    override fun initMailText(): SpannableString {
        val mailText =
            SpannableString("Если у вас есть вопросы или предложения по поводу сотрудничества напишите нам на почту info@spinit.org")
        val mailWords = mailText.split(" ")
        for (word in mailWords) {
            if (word.startsWith("info@spinit.org")) {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(Intent.ACTION_SENDTO)
                        intent.data = Uri.parse("mailto:$word")
                        if (getView()?.getCtx()?.packageManager?.let { intent.resolveActivity(it) } != null) {
                            getView()?.getMainAct()?.startActivity(intent)
                        }
                    }
                }
                mailText.setSpan(
                    clickableSpan,
                    mailText.indexOf(word),
                    mailText.indexOf(word) + word.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        return mailText
    }

    override fun onYandexBTNClicked() {
        val act = getView()?.getMainAct()
        val uri = Uri.parse("yandexmaps://maps.yandex.ru/?oid=1023718302")
        var intent = Intent(Intent.ACTION_VIEW, uri)
        val activities =
            act?.packageManager?.queryIntentActivities(intent, 0)

        if (activities != null && activities.size > 0)
            act.startActivity(intent)
        else {
            intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=ru.yandex.yandexmaps")
            act?.startActivity(intent)
        }
    }

    override fun onGoogleBTNClicked() {
        val act = getView()?.getMainAct()
        val uri: Uri = Uri.parse("google.navigation:q=59.9028746,30.4885295+&mode=w")
        var intent = Intent(Intent.ACTION_VIEW, uri)
        intent.setPackage("com.google.android.apps.maps")
        if (act?.packageManager?.let { intent.resolveActivity(it) } != null)
            act.startActivity(intent)
        else {
            intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.maps")
            act?.startActivity(intent)
        }
    }
}