package com.bonch_dev.spinit.presentation.modules.about_app.presenter

import android.text.SpannableString

interface IAboutPresenter {
    fun initSpanText(): SpannableString
}