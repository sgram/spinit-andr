package com.bonch_dev.spinit.presentation.modules.about_app.presenter

import android.text.SpannableString

interface IContactPresenter {
    fun initSocialText(): SpannableString
    fun initMailText(): SpannableString
    fun onYandexBTNClicked()
    fun onGoogleBTNClicked()
}