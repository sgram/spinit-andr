package com.bonch_dev.spinit.presentation.modules.about_app.view

import android.content.Context
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentAboutAppBinding
import com.bonch_dev.spinit.domain.entity.HowWeLearn
import com.bonch_dev.spinit.domain.entity.response.course.Teachers
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.modules.about_app.adapter.HowWeLearnAdapter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.AboutPresenter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.IAboutPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter.TeacherPhotosAdapter
import javax.inject.Inject

class AboutFragment : BaseFragment(), IAboutFragment {

    @Inject
    lateinit var presenter: IAboutPresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentAboutAppBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentAboutAppBinding.inflate(inflater, container, false)
        toolbar = binding.aboutToolbar
        (presenter as AboutPresenter).attachView(this)
        return binding.root
    }

    private fun animateLogo() {
        val anim = ViewAnimation.initScaleUpAnim(binding.logoIv, 1.0f, 1.0f, 600)
        anim.start()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.run {
            aboutHelloTv.text = ResourceManager.getString(R.string.about_us_hello)
            aboutSystemTv.text = ResourceManager.getString(R.string.about_us_system)
            aboutScrumTv.text = ResourceManager.getString(R.string.about_ut_scrum)
            aboutProcessTv.text = presenter.initSpanText()
            aboutProcessTv.movementMethod = LinkMovementMethod.getInstance()
            aboutHowWeLearn.text = ResourceManager.getString(R.string.about_us_how_we_learn)
        }

        bindRecyclers()
        animateLogo()

    }

    override fun getCtx(): Context = requireContext()

    override fun setListeners() {}

    private fun bindHowWeLearnRecycler() {
        binding.howWeLearnRecycler.run {
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = false
            adapter = HowWeLearnAdapter(
                listOf(
                    HowWeLearn(
                        "Оффлайн лекции",
                        "В период офлайн обучения общаетесь с преподавателями и другими студентами — быстро получаете ответы на свои вопросы и обмениваетесь опытом."
                    ),
                    HowWeLearn(
                        "Домашние задания",
                        "Новый материал закрепляется с помощью теста и практического задания, которое будет проверено преподователями."
                    ),
                    HowWeLearn(
                        "Трансляции",
                        "В зависимости от выбранного курса будут проводиться закрытые видеотрансляции, а также общие стримы."
                    ),
                    HowWeLearn(
                        "Онлайн лекции",
                        "На период онлайн обучения изучайте видеоматериалы, размещенные в личном кабинете вместе с учебными материалами и заданием."
                    ),
                    HowWeLearn(
                        "Итоговый проект",
                        "В конце курса вас ждет заключительный проект. После успешной защиты вы получаете не просто сертификат об окончании, но и кейс в портфолио."
                    )
                )
            )
        }
    }

    private fun bindTeacherRV() {
        binding.aboutTeacherRecycler.run {
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = false
            adapter = TeacherPhotosAdapter(
                listOf(
                    Teachers(null, "Александра Горшенина", "Руководитель направления Design", null, R.drawable.gorshenina),
                    Teachers(null, "Александра Горшенина", "Руководитель направления Design", null, R.drawable.gorshenina),
                    Teachers(null, "Александра Горшенина", "Руководитель направления Design", null, R.drawable.gorshenina),
                    Teachers(null, "Александра Горшенина", "Руководитель направления Design", null, R.drawable.gorshenina)
                )
            )
        }
    }

    private fun bindRecyclers() {
        binding.run {
            when {
                howWeLearnRecycler.isShown -> {
                    bindHowWeLearnRecycler()
                    Log.e(javaClass.simpleName, "bind how we learn recycler view adapter")
                }
                aboutTeacherRecycler.isShown -> {
                    bindTeacherRV()
                    Log.e(javaClass.simpleName, "bind teacher recycler view adapter")
                }
            }
            aboutNestedSv.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
                override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
                    when {
                        ScrollVisibility.isViewVisibleOnScreen(v, howWeLearnRecycler) && howWeLearnRecycler.adapter == null -> {
                            bindHowWeLearnRecycler()
                            Log.e(javaClass.simpleName, "bind how we learn recycler view adapter")
                        }
                        ScrollVisibility.isViewVisibleOnScreen(
                            v,
                            aboutTeacherRecycler
                        ) && aboutTeacherRecycler.adapter == null -> {
                            bindTeacherRV()
                            Log.e(javaClass.simpleName, "bind teacher recycler view adapter")
                        }
                    }
                }
            })
        }

    }

    override fun onDestroyView() {
        (presenter as AboutPresenter).detachView()
        super.onDestroyView()
    }
}
