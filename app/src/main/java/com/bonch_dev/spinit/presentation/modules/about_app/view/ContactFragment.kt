package com.bonch_dev.spinit.presentation.modules.about_app.view

import android.content.Context
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentContactBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.ViewAnimation
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.ContactPresenter
import com.bonch_dev.spinit.presentation.modules.about_app.presenter.IContactPresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import javax.inject.Inject

class ContactFragment : BaseFragment(), IContactFragment, OnMapReadyCallback {
    @Inject
    lateinit var presenter: IContactPresenter

    private lateinit var mapFragment: SupportMapFragment
    private var mMap: GoogleMap? = null

    private lateinit var binding: FragmentContactBinding

    private val bonchCoordinates = LatLng(59.90280405136602, 30.488068757747293)

    init {
        App.appComponent.inject(this)
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentContactBinding.inflate(inflater, container, false)
        toolbar = binding.contactToolbar
        (presenter as ContactPresenter).attachView(this)

        initMaps()

        binding.run {
            socialTv.text = presenter.initSocialText()
            socialTv.movementMethod = LinkMovementMethod.getInstance()
            mailTv.text = presenter.initMailText()
            mailTv.movementMethod = LinkMovementMethod.getInstance()
            spbsutAddressTv.text = ResourceManager.getString(R.string.office_address)
        }

        animateLogo()
        setListeners()

        return binding.root
    }

    private fun animateLogo() {
        val anim = ViewAnimation.initScaleUpAnim(binding.contactLogoIv, 1.0f, 1.0f, 600)
        anim.start()
    }

    private fun initMaps() {
        if (mMap == null) {
            mapFragment = childFragmentManager.findFragmentById(R.id.map) as WorkaroundMapFragment
            mapFragment.getMapAsync(this)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap?.apply {
            uiSettings.isZoomControlsEnabled = true
        }

        setMarker(mMap)

        val mScrollView = binding.scrollView
        (mapFragment as WorkaroundMapFragment).setListener(object : WorkaroundMapFragment.OnTouchListener {
            override fun onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true)
            }
        })
    }

    private fun setMarker(map: GoogleMap?) {
        map?.let {
            it.addMarker(MarkerOptions().position(bonchCoordinates))
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(bonchCoordinates, 14.0F))
        }
    }


    override fun getCtx(): Context = requireContext()

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun setListeners() {
        binding.run {
            yandexBtn.setOnClickListener {
                presenter.onYandexBTNClicked()
            }
            googleBtn.setOnClickListener {
                presenter.onGoogleBTNClicked()
            }
            openLayout.setOnClickListener {
                if (hidingLayout.isGone) {
                    ViewAnimation.expand(hidingLayout)
                    arrow.animate().setDuration(200).rotation(180F)
                } else {
                    ViewAnimation.collapse(hidingLayout)
                    arrow.animate().setDuration(200).rotation(0F)
                }
            }
        }

    }

    override fun onDestroy() {
        (presenter as ContactPresenter).detachView()
        super.onDestroy()
    }
}