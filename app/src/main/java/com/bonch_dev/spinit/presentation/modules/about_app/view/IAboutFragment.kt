package com.bonch_dev.spinit.presentation.modules.about_app.view

import android.content.Context
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

interface IAboutFragment : IBaseView {
    fun getCtx(): Context
}