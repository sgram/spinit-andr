package com.bonch_dev.spinit.presentation.modules.about_app.view

import android.content.Context
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface IContactFragment : IBaseView {
    fun getCtx(): Context
    fun getMainAct(): MainActivity
}