package com.bonch_dev.spinit.presentation.modules.achievements.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.databinding.ItemAchievementBinding
import com.bonch_dev.spinit.domain.entity.response.userinfo.Achievement
import com.bonch_dev.spinit.presentation.modules.achievements.view.AchievementsFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class AchievementAdapter(val list: List<Achievement>?, val achievementsFragment: AchievementsFragment) :
    RecyclerView.Adapter<AchievementAdapter.AchievementViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AchievementViewHolder =
        AchievementViewHolder(ItemAchievementBinding.inflate(LayoutInflater.from(parent.context), parent, false))


    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: AchievementViewHolder, position: Int) = holder.bind(position)

    inner class AchievementViewHolder(val binding: ItemAchievementBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            binding.run {
                Glide.with(achievementsFragment).load(list?.get(position)?.icon_url)
                    .apply(RequestOptions().circleCrop().centerCrop()).into(achievementIv)
                achievementTitle.text = list?.get(position)?.title
                achievementContent.text = list?.get(position)?.description
            }

        }
    }
}
