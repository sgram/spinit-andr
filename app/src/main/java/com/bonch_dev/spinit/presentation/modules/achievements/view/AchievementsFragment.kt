package com.bonch_dev.spinit.presentation.modules.achievements.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.databinding.FragmentAchievementsBinding
import com.bonch_dev.spinit.domain.entity.response.userinfo.Achievement
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.achievements.adapters.AchievementAdapter

class AchievementsFragment : BaseFragment(), IAchievementsFragment {

    private lateinit var binding: FragmentAchievementsBinding

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentAchievementsBinding.inflate(inflater, container, false)
        initRecyclerParams()
        return binding.root
    }

    private fun initRecyclerParams() {
        binding.achievementRecycler.run {
            isNestedScrollingEnabled = false
        }
    }

    override fun bindAchievements(ach: List<Achievement>?) {
        binding.run {
            if (ach.isNullOrEmpty()) achievementsText.gone()
            else achievementsText.visible()
            achievementRecycler.adapter = AchievementAdapter(ach, this@AchievementsFragment)
        }

    }
}
