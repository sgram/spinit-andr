package com.bonch_dev.spinit.presentation.modules.achievements.view

import com.bonch_dev.spinit.domain.entity.response.userinfo.Achievement

interface IAchievementsFragment {
    fun bindAchievements(ach: List<Achievement>?)
}