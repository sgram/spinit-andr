package com.bonch_dev.spinit.presentation.modules.article.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory

class ArticleCategoryAdapter(
    ctx: Context,
    @LayoutRes private val parentRes: Int,
    private val items: ArrayList<ArticleCategory>
) :
    ArrayAdapter<ArticleCategory>(ctx, parentRes) {

    init {
        Log.e(javaClass.simpleName, items.size.toString())
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        return if (view == null) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.spinner_item, null)
            view = v
            val tv: TextView = view.findViewById(R.id.tv)
            tv.text = items[position].title
            view
        } else {
            val tv: TextView = view.findViewById(R.id.tv)
            tv.text = items[position].title
            view
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        return if (view == null) {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.spinner_item_parent, null)
            view = v
            val tv: TextView = view.findViewById(R.id.tv)
            tv.text = items[position].title
            view
        } else {
            val tv: TextView = view.findViewById(R.id.tv)
            tv.text = items[position].title
            view
        }
    }

    fun addItems(items: List<ArticleCategory>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): ArticleCategory? = items[position]

    override fun getCount(): Int = items.size
}