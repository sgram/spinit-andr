package com.bonch_dev.spinit.presentation.modules.article.adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.ItemArticleBinding
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.utils.ViewAnimation
import com.bonch_dev.spinit.router.MainRouter
import com.bumptech.glide.Glide
import javax.inject.Inject

class ArticlesAdapter(var list: ArrayList<Post?>?, private val articleListener: ArticleListener) :
    RecyclerView.Adapter<ArticlesAdapter.ArticleViewHolder>() {

    @Inject
    lateinit var router: MainRouter

    interface ArticleListener {
        fun onArticleClick(id: Int, liked: Boolean)
        fun onLikeButtonClick(id: Int, liked: Boolean, likeBTN: ImageView)
    }

    init {
        App.appComponent.inject(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = ItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        view.root.viewTreeObserver.addOnGlobalLayoutListener {
            val layoutParams: ViewGroup.LayoutParams = view.root.layoutParams
            layoutParams.height = parent.width
            view.root.layoutParams = layoutParams
        }

        return ArticleViewHolder(view)
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) = holder.bind(position)

    inner class ArticleViewHolder(val binding: ItemArticleBinding) : RecyclerView.ViewHolder(binding.root), View.OnTouchListener {

        @SuppressLint("ClickableViewAccessibility")
        fun bind(position: Int) {
            val item = list?.get(position)

            binding.run {
                articleCv.setOnTouchListener(this@ArticleViewHolder)
                likeBtn.setOnTouchListener(this@ArticleViewHolder)

                item?.run {
                    Glide.with(App.applicationContext()).load(img_url).into(articleIv)
                    articleNameTv.text = title
                    articleDescTv.text = description
                    articleDateTv.text = created_at
                }
                val liked = list?.get(position)?.liked ?: true
                likeBtn.setImageResource(if (liked) R.drawable.ic_heart_filled else R.drawable.ic_heart_unfilled)
            }


        }

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            if (v?.id == R.id.like_btn) {
                val id = list?.get(adapterPosition)?.id ?: -1
                val liked = list?.get(adapterPosition)?.liked ?: true
                val scaleDown = ViewAnimation.initScaleDownAnim(v, 0.8f, 0.8f, 0)
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> scaleDown.start()
                    MotionEvent.ACTION_MOVE -> {
                        ViewAnimation.clearAnim(scaleDown)
                        ViewAnimation.initScaleUpAnim(v, 1.0f, 1.0f, 0).start()
                        return true
                    }
                    MotionEvent.ACTION_UP -> {
                        ViewAnimation.clearAnim(scaleDown)
                        ViewAnimation.initScaleUpAnim(v, 1.0f, 1.0f, 0).start()
                        articleListener.onLikeButtonClick(id, liked, binding.likeBtn)
                    }
                }
            } else if (v?.id == R.id.article_cv) {
                val scaleUp = ViewAnimation.initScaleUpAnim(v, 1.03f, 1.03f, 0)
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> scaleUp.start()
                    MotionEvent.ACTION_MOVE -> {
                        ViewAnimation.clearAnim(scaleUp)
                        ViewAnimation.initScaleDownAnim(v, 1.0f, 1.0f, 0).start()
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        ViewAnimation.clearAnim(scaleUp)
                        ViewAnimation.initScaleDownAnim(v, 1.0f, 1.0f, 0).start()
                        val id = list?.get(adapterPosition)?.id ?: -1
                        val liked = list?.get(adapterPosition)?.liked ?: true
                        articleListener.onArticleClick(id, liked)
                    }
                }
            }

            return false
        }
    }

    fun addItem(posts: List<Post?>) {
        list?.clear()
        list?.addAll(posts)
        notifyDataSetChanged()
        Log.e(javaClass.simpleName, "new data: ${posts.size}")
    }
}
