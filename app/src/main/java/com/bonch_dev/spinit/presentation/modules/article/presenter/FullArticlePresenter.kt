package com.bonch_dev.spinit.presentation.modules.article.presenter

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.interactors.article.IArticleInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.view.IFullArticleFragment
import com.bumptech.glide.Glide
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import javax.inject.Inject

class FullArticlePresenter @Inject constructor(private val articleInteractor: IArticleInteractor) : IFullArticlePresenter,
    BasePresenter<IFullArticleFragment>(), IDataCompletion<FullArticle> {

    override fun getArticle(postId: Int) {
        getView()?.run {
            (this as InternetFragment).hideConnErr()
            (this as InternetFragment).showSplash()
        }
        if (NetworkConnection.isInternetAvailable())
            articleInteractor.getArticle(
                postId,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: FullArticle) {
        getView()?.run {
            bindTitle(data)
            createArticle(data.data.text)
            (this as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onLikeBTNClicked(liked: Boolean, postID: Int) {
        if (!liked) {
            getView()?.likeArticle()
            getView()?.setArticleLike(true)
            articleInteractor.likePost(postID,
                result = {
                    BottomNavigationFragment.refresh()
                    getView()?.getBlogFragment()?.presenter?.getPosts()
                },
                err = {
                    getView()?.run {
                        setArticleLike(false)
                        unlikeArticle()
                        (this as Fragment)::showToast
                    }
                }
            )
        } else {
            getView()?.unlikeArticle()
            getView()?.setArticleLike(false)
            articleInteractor.unlikePost(postID,
                result = {
                    BottomNavigationFragment.refresh()
                    getView()?.getBlogFragment()?.presenter?.getPosts()
                },
                err = {
                    getView()?.run {
                        setArticleLike(true)
                        likeArticle()
                        (this as Fragment)::showToast
                    }
                }
            )
        }
    }

    override fun createArticle(text: String) {
        val doc = Jsoup.parse(text)
        for (el in doc.allElements) {
            when (el.tagName()) {
                "h1" -> bindH1(el)
                "h2" -> bindH2(el)
                "h3" -> bindH3(el)
                "h4" -> bindH4(el)
                "h5" -> bindH5(el)
                "h6" -> bindH6(el)
                "img" -> bindIMG(el)
                "p" -> bindP(el)
                "ul" -> bindUL(el)
                "div" -> bindDiv(el)
            }
        }
    }

    private fun initLP(top: Int, bottom: Int, left: Int, right: Int): LinearLayout.LayoutParams? {
        return LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            topMargin = top
            bottomMargin = bottom
            leftMargin = left
            rightMargin = right
        }
    }

    private fun bindH1(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h1TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h1TextView)
    }

    private fun bindH2(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h2TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h2TextView)
    }

    private fun bindH3(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h3TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h3TextView)
    }

    private fun bindH4(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h4TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h4TextView)
    }

    private fun bindH5(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h5TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h5TextView)
    }

    private fun bindH6(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h6TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h6TextView)
    }

    private fun bindIMG(el: Element) {
        val image = el.getElementsByTag("img")
        val src = image.attr("src")
        val imageView = ImageView(getView()?.getCtx())
        val imageLP = initLP(top = 0, bottom = 0, left = 0, right = 0)
        imageView.layoutParams = imageLP
        getView()?.getCtx()?.let { Glide.with(it).load(src).into(imageView) }
        getView()?.addView(imageView)
    }

    private fun bindP(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindUL(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindDiv(el: Element) {
        val divLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val divTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = divLP
            text = getHTML(el.toString())
        }
        getView()?.addView(divTextView)
    }

    @Suppress("DEPRECATION")
    private fun getHTML(text: String): Spanned {
        return if (Build.VERSION.SDK_INT >= 24)
            HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
        else Html.fromHtml(text)
    }
}
