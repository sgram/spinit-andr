package com.bonch_dev.spinit.presentation.modules.article.presenter

interface IFullArticlePresenter {
    fun getArticle(postId: Int)
    fun createArticle(text: String)
    fun onLikeBTNClicked(liked: Boolean, postID: Int)
}