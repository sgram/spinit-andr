package com.bonch_dev.spinit.presentation.modules.article.presenter

import android.widget.ImageView

interface ISavedArticlePresenter {
    fun onLikeBTNClicked(liked: Boolean, id: Int, likeBTN: ImageView)
    fun onArticleClick(id: Int, liked: Boolean)
}