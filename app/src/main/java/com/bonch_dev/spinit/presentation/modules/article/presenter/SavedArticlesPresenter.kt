package com.bonch_dev.spinit.presentation.modules.article.presenter

import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.interactors.article.IArticleInteractor
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.view.ISavedArticlesFragment
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class SavedArticlesPresenter @Inject constructor(private val router: MainRouter, private val interactor: IArticleInteractor) :
    ISavedArticlePresenter, BasePresenter<ISavedArticlesFragment>() {

    override fun onArticleClick(id: Int, liked: Boolean) {
        val bottomNavigationFragment = getView()?.getBottomNavFragment()
        if (bottomNavigationFragment != null)
            router.toFullArticleActivity(bottomNavigationFragment, id, liked)
    }

    override fun onLikeBTNClicked(liked: Boolean, id: Int, likeBTN: ImageView) {
        likeBTN.setImageResource(R.drawable.ic_heart_unfilled)
        val adapter = getView()?.getAdapter()
        var position = 0
        var item: Post? = null
        var list: MutableList<Post?>? = null
        if (adapter != null) {
            list = adapter.list
            list?.run {
                item = find { it?.id == id }
                position = indexOf(item)
                remove(item)
            }
            adapter.notifyItemRemoved(position)
            adapter.notifyItemRangeChanged(position, list?.size ?: 0)
        }
        interactor.unlikePost(id,
            result = {
                BottomNavigationFragment.refresh()
                getView()?.getBlogFragment()?.presenter?.getPosts()
            },
            err = {
                list?.add(position, item)
                adapter?.notifyItemInserted(position)
                adapter?.notifyItemRangeChanged(position, list?.size ?: 0)
                (getView() as Fragment)::showToast
                likeBTN.setImageResource(R.drawable.ic_heart_filled)
            }
        )
    }
}