package com.bonch_dev.spinit.presentation.modules.article.view

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentFullArticleBinding
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.BLOG_FRAG
import com.bonch_dev.spinit.domain.utils.Keys.BOTTOM_NAVIGATION
import com.bonch_dev.spinit.domain.utils.Keys.GET_ARTICLE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_FULL_ARTICLE
import com.bonch_dev.spinit.domain.utils.Keys.LIKE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.LIKE_POST
import com.bonch_dev.spinit.domain.utils.Keys.SEND_ARTICLE_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.UNLIKE_POST
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.ViewAnimation.clearAnim
import com.bonch_dev.spinit.domain.utils.ViewAnimation.initScaleDownAnim
import com.bonch_dev.spinit.domain.utils.ViewAnimation.initScaleUpAnim
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.article.presenter.FullArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.presenter.IFullArticlePresenter
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.CommentsAdapter
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.CommentsPresenter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import javax.inject.Inject

class FullArticleFragment : InternetFragment(), IFullArticleFragment, CommentsAdapter.OnCommentClickListener,
    RepliesAdapter.OnReplyClickListener, IComment {

    @Inject
    lateinit var presenter: IFullArticlePresenter

    @Inject
    lateinit var commentsPresenter: ICommentsPresenter

    var postId = -1
    var liked = false
    private lateinit var act: MainActivity

    private lateinit var currentFragListener: CurrentFragListener

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private var textWatcher: TextWatcher? = null

    private lateinit var binding: FragmentFullArticleBinding

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentFullArticleBinding.inflate(inflater, container, false)
        toolbar = binding.fullArticleToolbar
        act = activity as MainActivity
        currentFragListener = act
        attachViews()
        getDataFromArgs()
        loadAvatar()
        initBehaviour()
        initRecyclerParams()
        if (liked) likeArticle()
        initTextWatcher()
        setListeners()
        presenter.getArticle(postId)
        getComments()
        return binding.root
    }

    private fun attachViews() {
        (commentsPresenter as CommentsPresenter).attachView(this)
        (presenter as FullArticlePresenter).attachView(this)
    }

    override fun getBSBehaviour(): BottomSheetBehavior<View> = bottomSheetBehavior

    private fun getDataFromArgs() {
        arguments?.run {
            liked = getBoolean("liked")
            postId = getInt("post_id")
        }
    }

    override fun onStart() {
        super.onStart()
        currentFragListener.selectedFragment(this)
    }

    override fun getComments() = commentsPresenter.getArticleComments(postId)

    override fun setArticleLike(b: Boolean) {
        liked = b
    }

    override fun getCtx(): Context = requireContext()

    override fun getBlogFragment(): BlogFragment {
        val base = parentFragment as NavHostFragment
        val bottomNavigationFragment = base.childFragmentManager.findFragmentByTag(BOTTOM_NAVIGATION) as BottomNavigationFragment
        return bottomNavigationFragment.childFragmentManager.findFragmentByTag(BLOG_FRAG) as BlogFragment
    }


    override fun getBottomSheetState(): Int = bottomSheetBehavior.state


    override fun likeArticle() = binding.likeIv.setImageResource(R.drawable.ic_heart_filled)

    override fun unlikeArticle() = binding.likeIv.setImageResource(R.drawable.ic_heart_unfilled)

    override fun bindReplies(data: List<Comment>, repliesAdapter: RepliesAdapter?) {
        if (repliesAdapter == null) getRepliesRV().adapter = RepliesAdapter(data, this)
        else {
            repliesAdapter.list = data
            repliesAdapter.notifyDataSetChanged()
        }
    }

    override fun bindTitle(data: FullArticle) {
        binding.titleTv.text = data.data.title
        binding.createdTv.text = data.data.created_at
    }

    private fun getCommentsRv(): RecyclerView = binding.commentContainer.commentRecycler

    override fun getRepliesRV(): RecyclerView = binding.replies.repliesRv

    override fun addView(view: View) = binding.mainLayout.addView(view)

    override fun getAdapter(): CommentsAdapter? = getCommentsRv().adapter as CommentsAdapter?

    override fun bindComments(data: List<Comment>) {
        binding.commentContainer.commentsCount.text = data.size.toString()
        val ad = getAdapter()
        if (ad == null) getCommentsRv().adapter = CommentsAdapter(data, this)
        else {
            ad.list = data
            ad.notifyDataSetChanged()
        }
    }

    override fun openBottomSheet() {
        commentsPresenter.initParentLayout(binding.replies.parentLayout)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {}

            override fun onStateChanged(p0: View, p1: Int) {
                if (p1 == BottomSheetBehavior.STATE_HIDDEN)
                    commentsPresenter.onBottomSheetHidden()
            }
        })
    }

    override fun initTbParams() {
        act.run {
            binding.fullArticleToolbar.initBackArrow(this)
            binding.replies.repliesToolbar.post { binding.replies.repliesToolbar.initBackArrow(this) }
        }
    }

    override fun onCommentClick(position: Int, viewID: Int?, commentItem: Comment?) {
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentItem?.id, null, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentItem?.id, null, false, null)
            R.id.expand_replies_layout -> commentsPresenter.onShowRepliesBtnClick(commentItem)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (it.isNotEmpty()) binding.commentContainer.sendCommentIv.visible()
                    else binding.commentContainer.sendCommentIv.gone()
                }
            }
        }
    }

    override fun getFM(): FragmentManager = childFragmentManager

    private fun initBehaviour() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.replies.root)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun loadAvatar() {
        if (Keys.avatarURL.isNotEmpty())
            Glide.with(this).load(Keys.avatarURL).apply(RequestOptions().circleCrop()).into(binding.commentContainer.userIv)
    }

    private fun initRecyclerParams() {
        getCommentsRv().run {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }
        getRepliesRV().run {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun onReplyClick(viewID: Int?, commentItem: Comment?, repliesAdapter: RepliesAdapter) {
        val commentID: Int? = commentItem?.id
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun clearET() {
        binding.commentContainer.sendCommentEt.run {
            clearFocus()
            setText("")
        }
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.getArticle(postId)
            getComments()
        }
    }

    override fun setListeners() {
        binding.run {
            likeIv.setOnTouchListener { v, event ->
                val scaleDown = initScaleDownAnim(v, 0.8f, 0.8f, 0)
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> scaleDown.start()
                    MotionEvent.ACTION_MOVE -> {
                        clearAnim(scaleDown)
                        initScaleUpAnim(v, 1.0f, 1.0f, 0).start()
                        return@setOnTouchListener true
                    }
                    MotionEvent.ACTION_UP -> {
                        clearAnim(scaleDown)
                        initScaleUpAnim(v, 1.0f, 1.0f, 0).start()
                        v.performClick()
                        presenter.onLikeBTNClicked(liked, postId)
                    }
                }
                true
            }

            commentContainer.run {
                sendCommentEt.addTextChangedListener(textWatcher)

                sendCommentIv.setOnClickListener {
                    commentsPresenter.onSendArticleCommentBTNClicked(postId, sendCommentEt.text.toString())
                }
            }

        }

    }

    override fun onStop() {
        cancelJobs(GET_FULL_ARTICLE, SEND_ARTICLE_COMMENT, GET_ARTICLE_COMMENT, LIKE_COMMENT, UNLIKE_POST, LIKE_POST)
        textWatcher = null
        binding.commentContainer.sendCommentEt.removeTextChangedListener(textWatcher)
        super.onStop()
    }

    override fun onDestroyView() {
        (presenter as FullArticlePresenter).detachView()
        (commentsPresenter as CommentsPresenter).detachView()
        super.onDestroyView()
    }
}