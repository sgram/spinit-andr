package com.bonch_dev.spinit.presentation.modules.article.view

import android.content.Context
import android.view.View
import com.bonch_dev.spinit.domain.entity.response.article.FullArticle
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment

interface IFullArticleFragment : IBaseView {
    fun initTextWatcher()
    fun bindTitle(data: FullArticle)
    fun likeArticle()
    fun unlikeArticle()
    fun getBlogFragment(): BlogFragment
    fun setArticleLike(b: Boolean)
    fun getCtx(): Context
    fun addView(view: View)
}