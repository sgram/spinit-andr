package com.bonch_dev.spinit.presentation.modules.article.view

import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticlesAdapter
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment

interface ISavedArticlesFragment : IBaseView {
    fun getBlogFragment(): BlogFragment
    fun getAdapter(): ArticlesAdapter?
    fun bindSavedArticles(likedPost: List<Post?>?)
    fun getBottomNavFragment(): BottomNavigationFragment
}