package com.bonch_dev.spinit.presentation.modules.article.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentSavedArticlesBinding
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.BLOG_FRAG
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticlesAdapter
import com.bonch_dev.spinit.presentation.modules.article.presenter.ISavedArticlePresenter
import com.bonch_dev.spinit.presentation.modules.article.presenter.SavedArticlesPresenter
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.MainFragment
import javax.inject.Inject

class SavedArticlesFragment : BaseFragment(), ISavedArticlesFragment, ArticlesAdapter.ArticleListener {

    @Inject
    lateinit var presenter: ISavedArticlePresenter

    private lateinit var binding: FragmentSavedArticlesBinding

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentSavedArticlesBinding.inflate(inflater, container, false)
        (presenter as SavedArticlesPresenter).attachView(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerParams()
    }

    private fun initRecyclerParams() {
        binding.savedArticlesRecycler.adapter = ArticlesAdapter(arrayListOf(), this)
    }

    override fun getAdapter() = binding.savedArticlesRecycler.adapter as ArticlesAdapter?

    override fun getBottomNavFragment(): BottomNavigationFragment {
        val mainFragment = parentFragment as MainFragment
        return mainFragment.parentFragment as BottomNavigationFragment
    }

    override fun setListeners() {}

    override fun getBlogFragment(): BlogFragment =
        getBottomNavFragment().childFragmentManager.findFragmentByTag(BLOG_FRAG) as BlogFragment

    override fun bindSavedArticles(likedPost: List<Post?>?) {
        Log.e(javaClass.simpleName, "bindSaveArticles: ${likedPost?.size}")
        binding.run {
            noContent.noContentTv.text = "Вы не сохранили ни одной статьи"
            val adapter = savedArticlesRecycler.adapter as? ArticlesAdapter
            if (likedPost.isNullOrEmpty()) {
                noContent.noContentTv.visible()
                adapter?.addItem(listOf())
                return
            }
            noContent.noContentTv.gone()

            adapter?.addItem(likedPost)

        }


    }

    override fun onArticleClick(id: Int, liked: Boolean) = presenter.onArticleClick(id, liked)

    override fun onLikeButtonClick(id: Int, liked: Boolean, likeBTN: ImageView) = presenter.onLikeBTNClicked(liked, id, likeBTN)

    override fun onDestroyView() {
        (presenter as SavedArticlesPresenter).detachView()
        super.onDestroyView()
    }
}