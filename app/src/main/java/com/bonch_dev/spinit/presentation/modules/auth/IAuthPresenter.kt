package com.bonch_dev.spinit.presentation.modules.auth

interface IAuthPresenter {
    fun conditionsIsAccepted(email: String): Boolean
}