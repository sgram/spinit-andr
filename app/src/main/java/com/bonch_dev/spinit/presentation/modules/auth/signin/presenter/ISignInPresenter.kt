package com.bonch_dev.spinit.presentation.modules.auth.signin.presenter

import com.bonch_dev.spinit.domain.entity.EmailPass

interface ISignInPresenter {
    fun getAccessToken(idToken: String?, authCode: String?)
    fun onError(msg: String? = null)
    fun onResult()
    fun onForgotBtnClicked()
    fun onSignInBtnClicked(loginData: EmailPass)
    fun onVKSignInBtnClicked()
    fun onGoogleSignInClicked()
    fun onBackBtnClicked()
    fun onDialogBtnClicked(email: String)
}