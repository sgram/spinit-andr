package com.bonch_dev.spinit.presentation.modules.auth.signin.presenter

import android.util.Log
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.domain.interactors.auth.signin.ISignInInteractor
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.CorrectInput
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.view.ISignInFragment
import com.bonch_dev.spinit.router.MainRouter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import javax.inject.Inject

class SignInPresenter @Inject constructor(private val interactor: ISignInInteractor, val router: MainRouter) :
    ISignInPresenter, BasePresenter<ISignInFragment>() {

    override fun onForgotBtnClicked() {
        getView()?.showForgotPassDialog()
    }

    override fun onSignInBtnClicked(loginData: EmailPass) {
        val ctx = App.applicationContext()
        when {
            loginData.email.isEmpty() || loginData.password.isEmpty() ->
                (getView() as Fragment).showToast(ctx.getString(R.string.empty_email_field_error))
            !CorrectInput.isCorrectEmail(loginData.email) -> (getView() as Fragment).showToast(ctx.getString(R.string.incorrect_email_error))
            else -> {
                (getView() as BaseFragment).hideKB()
                interactor.signIn(
                    loginData,
                    callback = ::onResult,
                    err = ::onError
                )
            }
        }
    }

    override fun getAccessToken(idToken: String?, authCode: String?) {
        if (!idToken.isNullOrEmpty() && !authCode.isNullOrEmpty())
            interactor.getAccessToken(
                authCode, idToken,
                callback = { Log.e(javaClass.simpleName, "access_token: " + it.access_token) },
                err = { Log.e(javaClass.simpleName, "access_token: $it") }
            )
    }

    override fun onResult() {
        router.navigateToMainActivity()
        getView()?.getMainAct()?.measureIV()
    }

    override fun onError(msg: String?) {
        (getView() as Fragment).showToast(msg)
    }

    override fun onGoogleSignInClicked() {
        val client = GoogleSignIn.getClient(App.applicationContext(), Keys.GSO)
        val intent = client.signInIntent
        getView()?.getMainAct()?.startActivityForResult(intent, 10)
    }

    override fun onBackBtnClicked() {
        router.navController?.navigateUp()
    }

    override fun onDialogBtnClicked(email: String) {
        val toast: (Int) -> Unit = {
            val msg = getView()?.getCtx()?.getString(it)
            (getView() as Fragment).showToast(msg)
        }
        when {
            email.isEmpty() -> toast(R.string.empty_email_field_error)
            !CorrectInput.isCorrectEmail(email) -> toast(R.string.incorrect_email_error)
            else -> interactor.forgotPass(email,
                callback = { getView()?.onSuccessForgotPass() },
                err = { (getView() as Fragment).showToast(it) })
        }
    }

    override fun onVKSignInBtnClicked() {
        getView()?.let {
            VKSdk.login(it.getMainAct(), VKScope.EMAIL)
        }
    }
}