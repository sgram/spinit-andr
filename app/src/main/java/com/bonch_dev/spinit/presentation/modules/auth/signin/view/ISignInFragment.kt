package com.bonch_dev.spinit.presentation.modules.auth.signin.view

import android.content.Context
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface ISignInFragment : IBaseView {
    fun getCtx(): Context
    fun getMainAct(): MainActivity
    fun showForgotPassDialog()
    fun initDialogs()
    fun onSuccessForgotPass()
}