package com.bonch_dev.spinit.presentation.modules.auth.signin.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentSignInBinding
import com.bonch_dev.spinit.domain.entity.EmailPass
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.ISignInPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signin.presenter.SignInPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import javax.inject.Inject

class SignInFragment : BaseFragment(), ISignInFragment {

    @Inject
    lateinit var presenter: ISignInPresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentSignInBinding
    private lateinit var sendEmailDialog: AlertDialog
    private lateinit var emailSentDialog: AlertDialog
    private lateinit var findEmailET: AppCompatEditText


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentSignInBinding.inflate(inflater, container, false)

        toolbar = binding.tb

        (presenter as SignInPresenter).attachView(this)
        val currentFragListener: CurrentFragListener = activity as MainActivity
        currentFragListener.selectedFragment(this)
        setListeners()
        initDialogs()
        return binding.root
    }

    override fun initDialogs() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(requireContext())
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val sendEmailLayout = inflater.inflate(R.layout.dialog_reset_password, null)
        sendEmailDialog = builder.setView(sendEmailLayout).create()
        sendEmailLayout.run {
            findEmailET = findViewById(R.id.find_email_et)
            findViewById<Button>(R.id.cancel_btn).setOnClickListener { sendEmailDialog.dismiss() }
            findViewById<Button>(R.id.send_btn).setOnClickListener {
                presenter.onDialogBtnClicked(findEmailET.text.toString())
            }
        }
        val emailSentLayout = inflater.inflate(R.layout.dialog_email_sent, null)
        emailSentDialog = builder.setView(emailSentLayout).create()
        emailSentLayout.findViewById<Button>(R.id.ok_btn).setOnClickListener {
            emailSentDialog.dismiss()
        }
    }

    override fun getCtx(): Context = requireContext()

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun onSuccessForgotPass() {
        sendEmailDialog.dismiss()
        emailSentDialog.show()
    }

    override fun setListeners() {
        binding.run {
            forgotBtn.setOnClickListener { presenter.onForgotBtnClicked() }
            googleIb.setOnClickListener { presenter.onGoogleSignInClicked() }
            loginBtn.setOnClickListener {
                val email = loginEtEmail.text.toString()
                val pass = loginEtPassword.text.toString()
                val emailPass = EmailPass(email, pass)
                presenter.onSignInBtnClicked(emailPass)
            }
            vkIb.setOnClickListener { presenter.onVKSignInBtnClicked() }
        }
    }

    override fun showForgotPassDialog() = sendEmailDialog.show()

    override fun onDestroyView() {
        (presenter as SignInPresenter).detachView()
        super.onDestroyView()
    }
}
