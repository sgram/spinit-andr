package com.bonch_dev.spinit.presentation.modules.auth.signup.presenter

interface ISignUpStepOnePresenter {
    fun onStepTwoBtnSClicked(email: String, pass: String, confirmPass: String)
    fun onVkRegBtnClicked()
    fun onGoogleRegBtnClicked()
}