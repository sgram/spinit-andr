package com.bonch_dev.spinit.presentation.modules.auth.signup.presenter

import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend

interface ISignUpStepTwoPresenter {
    fun onDialogFragmentBtnClicked()
    fun onFinishBtnClicked(profile: RegistrationProfileSend)
    fun convertDate(date: String): String
    fun convertGender(gender: String): String
    fun checkCorrectInput(profile: RegistrationProfileSend): Boolean
    fun onDetach()
    fun formatPhone(phone: String): String
    fun showConditionsOfUse()
    fun onAcceptedConditionsOfUse()
    fun onCancelledConditionsOfUse()
}