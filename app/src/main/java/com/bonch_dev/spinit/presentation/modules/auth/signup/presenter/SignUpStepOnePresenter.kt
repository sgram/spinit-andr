package com.bonch_dev.spinit.presentation.modules.auth.signup.presenter

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.CorrectInput
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.BUNDLE_CONFIRM_PASS
import com.bonch_dev.spinit.domain.utils.Keys.BUNDLE_EMAIL
import com.bonch_dev.spinit.domain.utils.Keys.BUNDLE_PASS
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.ISignUpStepOneFragment
import com.bonch_dev.spinit.router.MainRouter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import javax.inject.Inject

class SignUpStepOnePresenter @Inject constructor(val router: MainRouter) :
    ISignUpStepOnePresenter, BasePresenter<ISignUpStepOneFragment>() {

    override fun onStepTwoBtnSClicked(email: String, pass: String, confirmPass: String) {
        val toast: (Int) -> Unit = {
            val msg = ResourceManager.getString(it)
            (getView() as Fragment).showToast(msg)
        }
        when {
            email.isEmpty() or pass.isEmpty() or confirmPass.isEmpty() -> toast(R.string.empty_field_error)
            !CorrectInput.isCorrectEmail(email) -> toast(R.string.incorrect_email_error)
            pass != confirmPass -> toast(R.string.passes_dont_match_error)
            pass.length < 6 -> toast(R.string.pass_length_less_6_error)
            else -> {
                val bundle = Bundle().apply {
                    putString(BUNDLE_EMAIL, email)
                    putString(BUNDLE_PASS, pass)
                    putString(BUNDLE_CONFIRM_PASS, confirmPass)
                }
                router.navigateToRegFragment2(bundle)
            }
        }
    }

    override fun onVkRegBtnClicked() {
        getView()?.let {
            VKSdk.login(it.getMainAct(), VKScope.EMAIL)
        }
    }

    override fun onGoogleRegBtnClicked() {
        val client = GoogleSignIn.getClient(App.applicationContext(), Keys.GSO)
        val intent = client.signInIntent
        getView()?.getMainAct()?.startActivityForResult(intent, 10)
    }
}