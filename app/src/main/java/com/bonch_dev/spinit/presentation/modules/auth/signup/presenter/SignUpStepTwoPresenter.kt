package com.bonch_dev.spinit.presentation.modules.auth.signup.presenter

import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.domain.interactors.auth.signup.ISignUpInteractor
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.FEMALE
import com.bonch_dev.spinit.domain.utils.Keys.MALE
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.modules.auth.signup.view.ISignUpStepTwoFragment
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class SignUpStepTwoPresenter @Inject constructor(
    private val interactor: ISignUpInteractor,
    private val router: MainRouter
) :
    ISignUpStepTwoPresenter, BasePresenter<ISignUpStepTwoFragment>() {

    override fun onDetach() = detachView()

    private var profile: RegistrationProfileSend? = null

    override fun onFinishBtnClicked(profile: RegistrationProfileSend) {
        if (checkCorrectInput(profile)) {
            showConditionsOfUse()
            this.profile = profile
        }
    }

    override fun showConditionsOfUse() {
        ConditionsOfUseReader().getConditionsOfUseString()?.let {
            getView()?.showConditionsOfUseDialog(it)
        } ?: {
            // TODO: 16.10.2020 add error handle
        }
    }

    override fun onAcceptedConditionsOfUse() {
        profile?.let { profile ->
            profile.gender = convertGender(profile.gender)
            profile.phone_number = formatPhone(profile.phone_number)
            profile.birth_date = convertDate(profile.birth_date)
            (getView() as BaseFragment).hideKB()
            interactor.signUp(
                profile,
                callback = { getView()?.showAlertDialogFragment() },
                err = { (getView() as Fragment).showToast(it) }
            )
        }
    }

    override fun onCancelledConditionsOfUse() {

    }

    override fun formatPhone(phone: String): String = phone.replace(Regex("[+() -]"), "")

    override fun checkCorrectInput(profile: RegistrationProfileSend): Boolean {
        if (profile.first_name.isEmpty() ||
            profile.last_name.isEmpty() ||
            profile.gender.isEmpty() ||
            profile.birth_date.isEmpty() ||
            profile.phone_number.isEmpty()
        ) {
            (getView() as Fragment).showToast(ResourceManager.getString(R.string.empty_field_error))
            return false
        } else if (!CorrectInput.isCorrectEmail(profile.email)) {
            (getView() as Fragment).showToast(ResourceManager.getString(R.string.incorrect_email_error))
            return false
        } else if (!CorrectInput.isCorrectDate(profile.birth_date)) {
            (getView() as Fragment).showToast(ResourceManager.getString(R.string.incorrect_date_error))
            return false
        } else if (profile.phone_number.length in 1..17) {
            (getView() as Fragment).showToast(ResourceManager.getString(R.string.incorrect_phone_error))
            return false
        }
        return true
    }

    // convert date from "dd.MM.yyyy" to "yyyy-MM-dd"
    override fun convertDate(date: String): String {
        return if (date.isNotEmpty()) {
            val array = date.split(".")
            "${array[2]}-${array[1]}-${array[0]}"
        } else ""
    }

    override fun convertGender(gender: String): String {
        val ctx = App.applicationContext()
        return when (gender) {
            ctx.getString(R.string.radio_male) -> MALE
            ctx.getString(R.string.radio_female) -> FEMALE
            else -> ""
        }
    }

    override fun onDialogFragmentBtnClicked() = router.returnToSignInFragment()
}