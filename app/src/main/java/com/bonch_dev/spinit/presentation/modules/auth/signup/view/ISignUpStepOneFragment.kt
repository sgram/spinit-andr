package com.bonch_dev.spinit.presentation.modules.auth.signup.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface ISignUpStepOneFragment : IBaseView {
    fun getMainAct(): MainActivity
}