package com.bonch_dev.spinit.presentation.modules.auth.signup.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

interface ISignUpStepTwoFragment : IBaseView {
    fun getProfileDataFromFirstFrag()
    fun initMasks()
    fun showAlertDialogFragment()
    fun getGenderText(): String
    fun initDateTextListener()
    fun showConditionsOfUseDialog(textConditionsOfUse: String)
}