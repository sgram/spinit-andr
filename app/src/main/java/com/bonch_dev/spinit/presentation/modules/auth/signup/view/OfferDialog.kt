package com.bonch_dev.spinit.presentation.modules.auth.signup.view

import android.graphics.Color
import android.os.Bundle
import android.text.util.Linkify
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.DialogConditionsOfUseBinding
import com.bonch_dev.spinit.domain.utils.ResourceManager
import java.util.regex.Pattern

class ConditionsOfUseDialog(
    private val textConditionsOfUse: String,
    private val clickListener: ConditionsOfUseClickListener
) : DialogFragment(),
    ViewTreeObserver.OnScrollChangedListener {

    private lateinit var binding: DialogConditionsOfUseBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogConditionsOfUseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.run {
            accept.setOnClickListener {
                dismiss()
                clickListener.onConditionsOfUseAccepted()
            }
            cancel.setOnClickListener {
                dismiss()
                clickListener.onConditionsOfUseCancelled()
            }

            conditionsOfUseText.text = textConditionsOfUse
            addLinks(conditionsOfUseText)

            scrollView.viewTreeObserver.addOnScrollChangedListener(this@ConditionsOfUseDialog)
        }
    }

    override fun onStart() {
        dialog?.window?.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        super.onStart()
    }

    private fun addLinks(textView: TextView) {
        val pattern =
            Pattern.compile("(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})")
        Linkify.addLinks(textView, pattern, null, null, null)
        textView.setLinkTextColor(Color.BLUE)
    }

    interface ConditionsOfUseClickListener {
        fun onConditionsOfUseAccepted()
        fun onConditionsOfUseCancelled()
    }

    override fun onScrollChanged() {
        val sv = binding.scrollView
        val view = sv.getChildAt(0)
        val bottomDetector = view.bottom - (sv.height + sv.scrollY)

        if (bottomDetector == 0) {
            onBottomConditionsOfUseReached()
        }
    }

    private fun onBottomConditionsOfUseReached() {
        binding.accept.run {
            isEnabled = true
            background = ResourceManager.getDrawable(R.drawable.orange_button)
        }
    }
}