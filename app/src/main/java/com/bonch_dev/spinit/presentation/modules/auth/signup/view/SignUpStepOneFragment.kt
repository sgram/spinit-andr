package com.bonch_dev.spinit.presentation.modules.auth.signup.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentRegistrationStepOneBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.ISignUpStepOnePresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.SignUpStepOnePresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import javax.inject.Inject

class SignUpStepOneFragment : BaseFragment(),
    ISignUpStepOneFragment {

    @Inject
    lateinit var presenter: ISignUpStepOnePresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentRegistrationStepOneBinding

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentRegistrationStepOneBinding.inflate(inflater, container, false)
        (presenter as SignUpStepOnePresenter).attachView(this)
        val currentFragListener: CurrentFragListener = activity as MainActivity
        currentFragListener.selectedFragment(this)
        setListeners()
        return binding.root
    }

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun setListeners() {
        binding.run {
            bNextStep.setOnClickListener {
                presenter.onStepTwoBtnSClicked(
                    stepOneEtEmail.text.toString(),
                    stepOneEtPassword.text.toString(),
                    stepOneEtConfirmPassword.text.toString()
                )
            }
            vkIb.setOnClickListener {
                presenter.onVkRegBtnClicked()
            }
            googleIb.setOnClickListener {
                presenter.onGoogleRegBtnClicked()
            }
        }

    }

    override fun onDestroyView() {
        (presenter as SignUpStepOnePresenter).detachView()
        super.onDestroyView()
    }
}