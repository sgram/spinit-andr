package com.bonch_dev.spinit.presentation.modules.auth.signup.view

import android.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentRegistrationStepTwoBinding
import com.bonch_dev.spinit.domain.entity.response.userinfo.RegistrationProfileSend
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.ISignUpStepTwoPresenter
import com.bonch_dev.spinit.presentation.modules.auth.signup.presenter.SignUpStepTwoPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class SignUpStepTwoFragment : BaseFragment(),
    ISignUpStepTwoFragment,
    ConditionsOfUseDialog.ConditionsOfUseClickListener {

    @Inject
    lateinit var presenter: ISignUpStepTwoPresenter

    init {
        App.appComponent.inject(this)
    }

    private var phoneFormatted: String = ""
    private var dateFormatted: String = ""
    private lateinit var email: String
    private lateinit var pass: String
    private lateinit var confirmPass: String

    private lateinit var dateTextListener: TextWatcher

    private lateinit var binding: FragmentRegistrationStepTwoBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentRegistrationStepTwoBinding.inflate(inflater, container, false)
        toolbar = binding.toolbar

        (presenter as SignUpStepTwoPresenter).attachView(this)
        val currentFragListener: CurrentFragListener = activity as MainActivity
        currentFragListener.selectedFragment(this)

        getProfileDataFromFirstFrag()
        return binding.root
    }


    override fun onResume() {
        super.onResume()
        initDateTextListener()
        initMasks()
        setListeners()
    }

    override fun setListeners() {
        binding.run {
            etDate.addTextChangedListener(dateTextListener)
            finishRegBtn.setOnClickListener {
                val name = etName.text.toString()
                val lastName = etLastname.text.toString()
                val date = etDate.text.toString()
                val phone = phoneEt.text.toString()
                val profile = RegistrationProfileSend(name, lastName, email, pass, confirmPass, phone, date, getGenderText())

                presenter.onFinishBtnClicked(profile)
            }
        }

    }


    override fun getProfileDataFromFirstFrag() {
        arguments?.run {
            email = getString(Keys.BUNDLE_EMAIL) ?: ""
            pass = getString(Keys.BUNDLE_PASS) ?: ""
            confirmPass = getString(Keys.BUNDLE_CONFIRM_PASS) ?: ""
        }
    }

    override fun initDateTextListener() {
        dateTextListener = object : TextWatcher {
            private var current = ""
            private val ddmmyyyy = "DDMMYYYY"
            private val cal = Calendar.getInstance()

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString() != current) {
                    var clean = p0.toString().replace("[^\\d.]|\\.".toRegex(), "")
                    val cleanC = current.replace("[^\\d.]|\\.", "")
                    val cl = clean.length
                    var sel = cl
                    var i = 2
                    while (i <= cl && i < 6) {
                        sel++
                        i += 2
                    }
                    if (clean == cleanC) sel--
                    if (clean.length < 8) clean += ddmmyyyy.substring(clean.length)
                    else {
                        var day = Integer.parseInt(clean.substring(0, 2))
                        var mon = Integer.parseInt(clean.substring(2, 4))
                        var year = Integer.parseInt(clean.substring(4, 8))
                        mon = if (mon < 1) 1 else if (mon > 12) 12 else mon
                        cal.set(Calendar.MONTH, mon - 1)
                        year = when {
                            year < 1900 -> 1900
                            year > cal.get(Calendar.YEAR) -> cal.get(Calendar.YEAR)
                            else -> year
                        }
                        day = if (day > cal.getActualMaximum(Calendar.DATE)) cal.getActualMaximum(Calendar.DATE) else day
                        clean = String.format("%02d%02d%02d", day, mon, year)
                    }

                    clean = String.format(
                        "%s.%s.%s", clean.substring(0, 2),
                        clean.substring(2, 4),
                        clean.substring(4, 8)
                    )

                    sel = if (sel < 0) 0 else sel
                    current = clean
                    binding.etDate.setText(current)
                    binding.etDate.setSelection(if (sel < current.count()) sel else current.count())
                    dateFormatted = binding.etDate.text.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        }
    }


    override fun initMasks() {
        val phoneFormats: MutableList<String> = ArrayList()
        phoneFormats.add("8 ([000]) [000]-[00]-[00]")
        val dateFormats: MutableList<String> = ArrayList()
        dateFormats.add("00.00.0000")
        MaskedTextChangedListener.installOn(
            binding.phoneEt,
            "+7 ([000]) [000]-[00]-[00]",
            phoneFormats,
            AffinityCalculationStrategy.PREFIX,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String, formattedValue: String) {
                    phoneFormatted = "7$extractedValue"
                }
            })
    }

    override fun showAlertDialogFragment() {
        AlertDialog.Builder(context)
            .setMessage(getString(R.string.reg_success_dialog_fragment))
            .setPositiveButton(getString(R.string.OK)) { dialog, _ ->
                dialog.dismiss()
                presenter.onDialogFragmentBtnClicked()
            }.show()
    }

    override fun getGenderText(): String {
        return when (binding.regRadioGroup.checkedRadioButtonId) {
            R.id.rb_man -> this.getString(R.string.radio_male)
            R.id.rb_woman -> this.getString(R.string.radio_female)
            else -> ""
        }
    }

    override fun onStop() {
        binding.etDate.removeTextChangedListener(dateTextListener)
        super.onStop()
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

    override fun showConditionsOfUseDialog(textConditionsOfUse: String) {
        val dialog = ConditionsOfUseDialog(textConditionsOfUse, this)
        dialog.isCancelable = false
        dialog.show(childFragmentManager, "ConditionsOfUse")
    }

    override fun onConditionsOfUseAccepted() {
        presenter.onAcceptedConditionsOfUse()
    }

    override fun onConditionsOfUseCancelled() {
        presenter.onCancelledConditionsOfUse()
    }
}
