package com.bonch_dev.spinit.presentation.modules.blog.presenter

import android.os.Bundle
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.interactors.article.IArticleInteractor
import com.bonch_dev.spinit.domain.interactors.broadcast.IBroadcastInteractor
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.blog.view.IBlogFragment
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class BlogPresenter @Inject constructor(
    private val articleInteractor: IArticleInteractor,
    private val broadcastInteractor: IBroadcastInteractor,
    private val router: MainRouter
) : IBlogPresenter, BasePresenter<IBlogFragment>() {

    override fun onBroadcastItemClick(bundle: Bundle, blogFragment: BlogFragment) {
        router.toBroadcastFragment(blogFragment, bundle)
    }

    override fun getCategories() {
        getView()?.getSpinnerParentPb()?.visible()
        articleInteractor.getCategories(
            result = { getView()?.initDropDownMenu(it) },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun onArticleError(err: String?) {
        getView()?.run {
            showArticleNoInternet()
            hideArticleSplash()
        }
        (getView() as Fragment).showToast(err)
    }

    override fun onBroadcastError(err: String?) {
        getView()?.run {
            showBroadcastNoInternet()
            hideBroadcastSplash()
            (this as Fragment).showToast(err)
        }
    }

    override fun onStart() {
        getCategories()
        getBroadcasts()
        getPosts()
    }

    override fun getPosts(category: String?) {
        if (NetworkConnection.isInternetAvailable())
            articleInteractor.getPosts(
                category,
                result = { getView()?.onArticleResult(it) },
                err = ::onArticleError
            )
        else onArticleError()
    }

    override fun getBroadcasts() {
        if (NetworkConnection.isInternetAvailable())
            broadcastInteractor.getBroadCast(
                result = { getView()?.onBroadcastResult(it) },
                err = ::onBroadcastError
            )
        else onBroadcastError()
    }

    override fun onItemSelected(categories: List<ArticleCategory>, position: Int) {
        getView()?.run {
            hideArticleRecycler()
            showArticleSplash()
            hideArticleNoInternet()
        }
        getPosts(categories[position].slug)
    }

    override fun onActicleClick(id: Int, liked: Boolean, blogFragment: BlogFragment) {
        val bottomNavigationFragment = getView()?.getBottomNavFragment()
        if (bottomNavigationFragment != null)
            router.toFullArticleActivity(bottomNavigationFragment, id, liked)
    }

    override fun onLikeBTNClicked(liked: Boolean, id: Int, likeBTN: ImageView) {
        if (!liked) {
            likeBTN.setImageResource(R.drawable.ic_heart_filled)
            articleInteractor.likePost(id,
                result = {
                    getPosts()
                    BottomNavigationFragment.refresh()
                },
                err = {
                    (getView() as Fragment).showToast(it)
                    likeBTN.setImageResource(R.drawable.ic_heart_unfilled)
                }
            )
        } else {
            likeBTN.setImageResource(R.drawable.ic_heart_unfilled)
            articleInteractor.unlikePost(id,
                result = {
                    getPosts()
                    BottomNavigationFragment.refresh()
                },
                err = {
                    (getView() as Fragment).showToast(it)
                    likeBTN.setImageResource(R.drawable.ic_heart_filled)
                }
            )
        }
    }
}
