package com.bonch_dev.spinit.presentation.modules.blog.presenter

import android.os.Bundle
import android.widget.ImageView
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment

interface IBlogPresenter {
    fun getCategories()
    fun onActicleClick(id: Int, liked: Boolean, blogFragment: BlogFragment)
    fun onArticleError(err: String? = null)
    fun onBroadcastError(err: String? = null)
    fun onBroadcastItemClick(bundle: Bundle, blogFragment: BlogFragment)
    fun onStart()
    fun getPosts(category: String? = null)
    fun onLikeBTNClicked(liked: Boolean, id: Int, likeBTN: ImageView)
    fun getBroadcasts()
    fun onItemSelected(categories: List<ArticleCategory>, position: Int)
}