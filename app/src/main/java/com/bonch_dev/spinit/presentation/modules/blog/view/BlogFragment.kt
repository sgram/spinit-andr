package com.bonch_dev.spinit.presentation.modules.blog.view

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.ColorInt
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentBlogBinding
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.domain.utils.Keys.GET_BROADCASTS
import com.bonch_dev.spinit.domain.utils.Keys.GET_CATEGORIES
import com.bonch_dev.spinit.domain.utils.Keys.GET_POSTS
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticleCategoryAdapter
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticlesAdapter
import com.bonch_dev.spinit.presentation.modules.blog.presenter.BlogPresenter
import com.bonch_dev.spinit.presentation.modules.blog.presenter.IBlogPresenter
import com.bonch_dev.spinit.presentation.modules.broadcast.adapters.BroadcastsAdapter
import java.util.*
import javax.inject.Inject

class BlogFragment : Fragment(), ArticlesAdapter.ArticleListener, AdapterView.OnItemSelectedListener, IBlogFragment,
    BroadcastsAdapter.BroadCastListener {

    @Inject
    lateinit var presenter: IBlogPresenter

    private lateinit var bottomNavigationFragment: BottomNavigationFragment

    private lateinit var binding: FragmentBlogBinding

    private lateinit var spinnerAdapter: ArticleCategoryAdapter
    private var categories: ArrayList<ArticleCategory> = arrayListOf(ArticleCategory(0, "По умолчанию", null))

    init {
        App.appComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentBlogBinding.inflate(inflater, container, false)
        bottomNavigationFragment = parentFragment as BottomNavigationFragment
        (presenter as BlogPresenter).attachView(this)

        initRecyclerParams()

        setListeners()

        showBroadcastSplash()
        showSplash()
        hideConnErr()
        hideBroadcastNoInternet()
        firstSpinnerInit()
        presenter.onStart()

        return binding.root
    }

    private fun firstSpinnerInit() {
        spinnerAdapter = ArticleCategoryAdapter(requireContext(), R.layout.spinner_item_parent, categories)
        binding.dropDownTypeSort.apply {
            adapter = spinnerAdapter
            setSelection(0, false)
            onItemSelectedListener = this@BlogFragment
        }
    }

    override fun onBroadcastItemClick(bundle: Bundle) {
        presenter.onBroadcastItemClick(bundle, this)
    }

    override fun setListeners() {
        binding.articleNoInternet.repeatConnectionBtn.setOnClickListener {
            hideConnErr()
            showSplash()
            presenter.getPosts(binding.dropDownTypeSort.selectedItem.toString().toLowerCase(Locale.ROOT))
        }
        binding.broadcastNoInternet.repeatConnectionBtn.setOnClickListener {
            hideBroadcastNoInternet()
            showBroadcastSplash()
            presenter.getBroadcasts()
        }
    }

    override fun hideBroadcastNoInternet() = binding.broadcastNoInternet.root.gone()

    override fun showBroadcastNoInternet() = binding.broadcastNoInternet.root.visible()

    fun showConnErr() = binding.articleNoInternet.root.visible()

    fun hideConnErr() = binding.articleNoInternet.root.gone()

    override fun onArticleClick(id: Int, liked: Boolean) {
        presenter.onActicleClick(id, liked, this)
    }

    override fun onLikeButtonClick(id: Int, liked: Boolean, likeBTN: ImageView) {
        presenter.onLikeBTNClicked(liked, id, likeBTN)
    }

    override fun getBottomNavFragment(): BottomNavigationFragment = parentFragment as BottomNavigationFragment

    override fun getArticleAdapter() = binding.blogRecycler.adapter as? ArticlesAdapter

    override fun hideArticleRecycler() = binding.blogRecycler.gone()

    override fun hideBroadcastRecycler() = binding.broadcastsRecycler.gone()

    override fun showBroadcastRecycler() = binding.broadcastsRecycler.visible()

    override fun showArticleRecycler() = binding.blogRecycler.visible()

    fun hideSplash() = binding.articleSplash.root.gone()

    fun showSplash() = binding.articleSplash.root.visible()

    override fun showBroadcastSplash() = binding.broadcastSplash.root.visible()

    override fun hideBroadcastSplash() = binding.broadcastSplash.root.gone()

    override fun onArticleResult(data: List<Post?>) {
        getArticleAdapter()?.addItem(data)
        showArticleRecycler()
        hideSplash()
    }

    override fun onBroadcastResult(data: List<Broadcast>) {
        if (data.isNullOrEmpty()) {
            hideBroadcastRecycler()
            hideBroadcastSplash()
        } else {
            val adapter = binding.broadcastsRecycler.adapter as? BroadcastsAdapter
            adapter?.addItem(data)
            showBroadcastRecycler()
            hideBroadcastSplash()
        }
    }


    override fun getSpinnerParentPb(): ProgressBar = binding.dropDownTypeSort.selectedView.findViewById(R.id.spinner_pb)


    private fun initRecyclerParams() {
        binding.run {
            blogRecycler.adapter = ArticlesAdapter(arrayListOf(), this@BlogFragment)
            broadcastsRecycler.adapter = BroadcastsAdapter(arrayListOf(), this@BlogFragment)
        }
    }

    override fun initDropDownMenu(categories: List<ArticleCategory>) {
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item)
        Log.e(javaClass.simpleName, this.categories.size.toString())
        spinnerAdapter.addItems(categories)
        Log.e(javaClass.simpleName, this.categories.size.toString())
        Log.e(javaClass.simpleName, categories.size.toString())
        getSpinnerParentPb().gone()
    }

    override fun showArticleNoInternet() {
        binding.articleNoInternet.root.visible()
    }

    override fun showArticleSplash() {
        binding.articleSplash.root.visible()
    }

    override fun hideArticleNoInternet() {
        binding.articleNoInternet.root.gone()
    }

    override fun hideArticleSplash() {
        binding.articleSplash.root.gone()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        presenter.onItemSelected(categories, position)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) styleComponents()
    }

    private fun styleComponents() {
        bottomNavigationFragment.run {
            val typedValue = TypedValue()
            val theme = context?.theme
            theme?.resolveAttribute(R.attr.toolbarColor, typedValue, true)
            @ColorInt val color = typedValue.data
            binding.toolbar.run {
                setBackgroundColor(color)
                title = "Блог"
                setTitleTextColor(Color.WHITE)
            }
        }
        Log.e("hide status", "${javaClass.simpleName} visible")
    }

    override fun onDestroyView() {
        cancelJobs(GET_BROADCASTS, GET_POSTS, GET_CATEGORIES)
        (presenter as BlogPresenter).detachView()
        super.onDestroyView()
    }
}
