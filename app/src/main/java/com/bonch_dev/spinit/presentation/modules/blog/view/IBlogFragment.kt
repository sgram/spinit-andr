package com.bonch_dev.spinit.presentation.modules.blog.view

import android.widget.ProgressBar
import com.bonch_dev.spinit.domain.entity.response.article.ArticleCategory
import com.bonch_dev.spinit.domain.entity.response.article.Post
import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.article.adapters.ArticlesAdapter

interface IBlogFragment : IBaseView {
    fun getSpinnerParentPb(): ProgressBar
    fun hideBroadcastRecycler()
    fun showBroadcastRecycler()
    fun hideArticleRecycler()
    fun showArticleRecycler()
    fun getArticleAdapter(): ArticlesAdapter?
    fun showBroadcastNoInternet()
    fun hideBroadcastNoInternet()
    fun showBroadcastSplash()
    fun hideBroadcastSplash()
    fun showArticleNoInternet()
    fun hideArticleNoInternet()
    fun showArticleSplash()
    fun hideArticleSplash()
    fun initDropDownMenu(categories: List<ArticleCategory>)
    fun onArticleResult(it: List<Post?>)
    fun onBroadcastResult(data: List<Broadcast>)
    fun getBottomNavFragment(): BottomNavigationFragment
}