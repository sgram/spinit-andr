package com.bonch_dev.spinit.presentation.modules.broadcast.adapters

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.ItemBroadcastBinding
import com.bonch_dev.spinit.domain.entity.response.broadcast.Broadcast
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class BroadcastsAdapter(private val items: ArrayList<Broadcast>?, private val broadcastListener: BroadCastListener) :
    RecyclerView.Adapter<BroadcastsAdapter.BroadcastsViewHolder>() {
    @Inject
    lateinit var mainRouter: MainRouter

    init {
        App.appComponent.inject(this)
    }

    interface BroadCastListener {
        fun onBroadcastItemClick(bundle: Bundle)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BroadcastsViewHolder =
        BroadcastsViewHolder(ItemBroadcastBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun getItemCount(): Int = items?.size ?: 0

    override fun onBindViewHolder(holder: BroadcastsViewHolder, position: Int) = holder.bind(position)

    inner class BroadcastsViewHolder(val binding: ItemBroadcastBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        fun bind(position: Int) {
            val item = items?.get(position)
            binding.run {
                itemTv.text = item?.title
                when (item?.live) {
                    1 -> showOnlineLabel()
                    0 -> hideOnlineLabel()
                }
                val shapeDrawable = itemIv.background as GradientDrawable
                when (item?.category) {
                    "design" -> shapeDrawable.setStroke(4, Color.parseColor("#2D8B00"))
                    "seo" -> shapeDrawable.setStroke(4, Color.parseColor("#D2A400"))
                    "mobile" -> shapeDrawable.setStroke(4, Color.parseColor("#006A8B"))
                    "front-end" -> shapeDrawable.setStroke(4, Color.parseColor("#905700"))
                    "back-end" -> shapeDrawable.setStroke(4, Color.parseColor("#8B0086"))
                    "smm" -> shapeDrawable.setStroke(4, Color.parseColor("#008B72"))
                }
            }

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val bundle = Bundle()
            val item = items?.get(position)
            bundle.run {
                putString("yt_video", item?.yt_video)
                putString("title", item?.title)
                putString("desc", item?.description)
                putInt("live", item?.live ?: 0)
                putString("date",item?.created_at)
            }
            broadcastListener.onBroadcastItemClick(bundle)
        }

        private fun showOnlineLabel() = binding.broadcastOnlineLabel.visible()

        private fun hideOnlineLabel() = binding.broadcastOnlineLabel.gone()
    }

    fun addItem(broadcasts: List<Broadcast>) {
        items?.clear()
        items?.addAll(0, broadcasts)
        notifyItemRangeChanged(0, broadcasts.size - 1)
    }
}