package com.bonch_dev.spinit.presentation.modules.broadcast.presenter

import android.content.pm.ActivityInfo
import android.view.OrientationEventListener
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.broadcast.view.IBroadCastFragment
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener

class BroadcastPresenter : IBroadcastPresenter, BasePresenter<IBroadCastFragment>() {

    override fun initOrientationListener(): OrientationEventListener {
        return object : OrientationEventListener(getView()?.getCtx()) {
            override fun onOrientationChanged(orientation: Int) {
                when (orientation) {
                    0 -> {
                        getView()?.run {
                            if (getVideoPlayer().isFullScreen()) {
                                getVideoPlayer().exitFullScreen()
                                showTB()
                                showStatusBar()
                            }
                        }
                    }
                    90 -> {
                        getView()?.run {
                            hideStatusBar()
                            getMainActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                            hideTB()
                            getVideoPlayer().enterFullScreen()
                        }
                    }
                    270 -> {
                        getView()?.run {
                            hideStatusBar()
                            getMainActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                            hideTB()
                            getVideoPlayer().enterFullScreen()
                        }
                    }
                }
            }
        }
    }

    override fun initFullScreenListener(): YouTubePlayerFullScreenListener {
        return object : YouTubePlayerFullScreenListener {
            override fun onYouTubePlayerEnterFullScreen() {
                getView()?.run {
                    val act = getMainActivity()
                    when (act.requestedOrientation) {
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT -> act.requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE -> act.requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE -> act.requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                    }
                    hideStatusBar()
                    hideTB()
                }
            }

            override fun onYouTubePlayerExitFullScreen() {
                getView()?.run {
                    getMainActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    showStatusBar()
                    showTB()
                }
            }
        }
    }
}