package com.bonch_dev.spinit.presentation.modules.broadcast.presenter

import android.view.OrientationEventListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener

interface IBroadcastPresenter {
    fun initOrientationListener(): OrientationEventListener
    fun initFullScreenListener(): YouTubePlayerFullScreenListener
}