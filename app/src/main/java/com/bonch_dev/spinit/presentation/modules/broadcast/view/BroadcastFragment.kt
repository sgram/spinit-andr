package com.bonch_dev.spinit.presentation.modules.broadcast.view

import android.content.Context
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.util.Log
import android.util.TypedValue
import android.view.*
import androidx.annotation.ColorInt
import androidx.core.view.isGone
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentBroadCastBinding
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.broadcast.presenter.BroadcastPresenter
import com.bonch_dev.spinit.presentation.modules.broadcast.presenter.IBroadcastPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Named

class BroadcastFragment : BaseFragment(),
    IBroadCastFragment {
    private var ytURL = ""
    private lateinit var currentFragListener: CurrentFragListener
    lateinit var act: MainActivity
    private var player: YouTubePlayer? = null
    private lateinit var playerListener: AbstractYouTubePlayerListener
    private lateinit var fullScreenListener: YouTubePlayerFullScreenListener
    private var orListener: OrientationEventListener? = null
    private var live: Int = 0
    private val fullScreenFlag = WindowManager.LayoutParams.FLAG_FULLSCREEN

    private lateinit var binding: FragmentBroadCastBinding

    @Inject
    lateinit var presenter: IBroadcastPresenter

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentBroadCastBinding.inflate(inflater, container, false)
        toolbar = binding.broadcastToolbar
        retainInstance = true
        act = activity as MainActivity
        currentFragListener = act
        currentFragListener.selectedFragment(this)
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        (presenter as BroadcastPresenter).attachView(this)
        orListener = presenter.initOrientationListener().apply { enable() }
        setTheme()
        getDataFromFragment()
        initYoutubeFrag(ytURL)
        setListeners()
        return binding.root
    }

    private fun setTheme() {
        val typedValue = TypedValue()
        val theme = context?.theme
        theme?.resolveAttribute(R.attr.toolbarColor, typedValue, true)
        @ColorInt val color = typedValue.data
        binding.ytExpandArrow.setColorFilter(color)
    }

    private fun getDataFromFragment() {
        arguments?.run {
            ytURL = getString("yt_video") ?: ""
            binding.broadcastTitle.text = getString("title")
            val dateString = getString("date") ?: ""
            val formatter: DateFormat = SimpleDateFormat("yyyy-MM-dd T HH:mm:ss.SSS Z", Locale.getDefault())
            val date = formatter.parse(dateString)
            binding.broadcastDate.text =
                String.format(this@BroadcastFragment.getString(R.string.broadcast_began), formatter.format(date))
            binding.desc.text = getString("desc")
            live = getInt("live")
        }
    }

    override fun getVideoPlayer(): YouTubePlayerView = binding.broadcastPlayer
    override fun getCtx(): Context = requireContext()
    override fun getMainActivity(): MainActivity = act

    override fun initYoutubeFrag(url: String) {
        firstPlayerInit()
        fullScreenListener = presenter.initFullScreenListener()
        getVideoPlayer().addFullScreenListener(fullScreenListener)

        playerListener = object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                super.onReady(youTubePlayer)
                youTubePlayer.loadVideo(ytURL.split("/").last(), 0f)
            }

            override fun onError(youTubePlayer: YouTubePlayer, error: PlayerConstants.PlayerError) {
                super.onError(youTubePlayer, error)
                showToast(error.name)
            }
        }
        getVideoPlayer().addYouTubePlayerListener(playerListener)
    }

    private fun firstPlayerInit() {
        val uiController = getVideoPlayer().getPlayerUiController()
        uiController.run {
            showYouTubeButton(false)
            showCustomAction1(false)
            showCustomAction2(false)
            showMenuButton(false)
            showVideoTitle(false)
            if (live == 1)
                enableLiveVideoUi(true)
            else enableLiveVideoUi(false)
        }
        act.lifecycle.addObserver(getVideoPlayer())
    }

    override fun hideTB() = binding.broadcastToolbar.gone()

    override fun showTB() = binding.broadcastToolbar.visible()

    override fun hideStatusBar() = act.window.setFlags(fullScreenFlag, fullScreenFlag)

    override fun showStatusBar() = act.window.clearFlags(fullScreenFlag)

    override fun setListeners() {
        binding.run {
            expandingContainer.setOnClickListener {
                if (hidingContainer.isGone) {
                    ViewAnimation.expand(hidingContainer)
                    ytExpandArrow.animate().setDuration(200).rotation(180F)
                } else {
                    ViewAnimation.collapse(hidingContainer)
                    ytExpandArrow.animate().setDuration(200).rotation(0F)
                }
            }
        }

    }

    private fun lockPortraitOrientation() {
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
    }

    private fun stopPlayer() {
        act.lifecycle.removeObserver(getVideoPlayer())
        player?.pause()
        getVideoPlayer().removeFullScreenListener(fullScreenListener)
        getVideoPlayer().removeYouTubePlayerListener(playerListener)
        getVideoPlayer().release()
    }

    override fun onStop() {
        Log.e("lectionFragment", "stop")
        lockPortraitOrientation()
        stopPlayer()
        super.onStop()
    }

    override fun onDestroyView() {
        (presenter as BroadcastPresenter).detachView()
        super.onDestroyView()
    }
}