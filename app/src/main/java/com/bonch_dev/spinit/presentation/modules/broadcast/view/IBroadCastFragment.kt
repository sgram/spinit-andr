package com.bonch_dev.spinit.presentation.modules.broadcast.view

import android.content.Context
import android.view.View
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

interface IBroadCastFragment : IBaseView {
    fun getMainActivity(): MainActivity
    fun getCtx(): Context
    fun getVideoPlayer(): YouTubePlayerView
    fun showStatusBar()
    fun hideStatusBar()
    fun showTB()
    fun hideTB()
    fun initYoutubeFrag(url: String)
}