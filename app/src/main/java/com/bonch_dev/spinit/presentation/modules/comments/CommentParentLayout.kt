package com.bonch_dev.spinit.presentation.modules.comments

import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CommentParentLayout {

    private lateinit var commentDate: TextView
    private lateinit var commentName: TextView
    private lateinit var commentContent: TextView
    private lateinit var thumbsDownCount: TextView
    private lateinit var thumbsUpCount: TextView
    private lateinit var commentIV: ImageView
    private lateinit var thumbsDownIV: ImageView
    private lateinit var thumbsUpIV: ImageView
    private var parentCommentID = -1
    private lateinit var presenter: ICommentsPresenter

    fun initParentLayout(parentLayout: LinearLayout?, presenter: ICommentsPresenter) {
        this.presenter = presenter
        parentLayout?.run {
            commentDate = findViewById(R.id.comment_date)
            commentName = findViewById(R.id.comment_name)
            commentContent = findViewById(R.id.comment_content)
            thumbsDownCount = findViewById(R.id.thumbs_down_count)
            thumbsUpCount = findViewById(R.id.thumbs_up_count)
            commentIV = findViewById(R.id.comment_iv)
            thumbsDownIV = findViewById(R.id.thumbs_down_iv)
            thumbsUpIV = findViewById(R.id.thumbs_up_iv)
            findViewById<TextView>(R.id.reply_btn).setOnClickListener {
                presenter.onReplyCommentBtnClick(null)
            }
        }
    }

    fun clearData() {
        removeDislikeImage()
        removeLikeImage()
        thumbsUpCount.text = 0.toString()
        thumbsDownCount.text = 0.toString()
    }

    fun clearRate(liked: Boolean) {
        presenter.setUserRate(null)
        removeDislikeImage()
        removeLikeImage()
        if (liked) decLikeCount()
        else decDislikeCount()
    }

    fun bindParentLayout(item: Comment?) {
        item?.run {
            id?.let { parentCommentID = it }
            commentDate.text = created_at
            author?.run {
                commentName.text = "$first_name $last_name"
                Glide.with(App.applicationContext()).load(avatar_url).apply(RequestOptions().circleCrop()).into(commentIV)
            }
            commentContent.text = text
            rates?.run {
                thumbsDownCount.text = dislikes.toString()
                thumbsUpCount.text = likes.toString()
                presenter.setUserRate(liked)
            }
            thumbsDownIV.setOnClickListener {
                presenter.onDislikeBtnClick(null, null, true, parentCommentID)
            }
            thumbsUpIV.setOnClickListener {
                presenter.onLikeBtnClick(null, null, true, parentCommentID)
            }
        }
        when (presenter.getUserRate()) {
            1 -> thumbsUpIV.setImageResource(R.drawable.ic_like_clicked)
            0 -> thumbsDownIV.setImageResource(R.drawable.ic_dislike_clicked)
            null -> {
                removeLikeImage()
                removeDislikeImage()
            }
        }
    }

    fun setLike() {
        thumbsUpIV.setImageResource(R.drawable.ic_like_clicked)
        if (presenter.getUserRate() == 0) {
            thumbsDownIV.setImageResource(R.drawable.ic_dislike)
            decDislikeCount()
        }
        incLikeCount()
        presenter.setUserRate(1)
    }

    fun incLikeCount() {
        thumbsUpCount.text = (thumbsUpCount.text.toString().toInt() + 1).toString()
    }

    fun decLikeCount() {
        thumbsUpCount.text = (thumbsUpCount.text.toString().toInt() - 1).toString()
    }

    fun incDislikeCount() {
        thumbsDownCount.text = (thumbsDownCount.text.toString().toInt() + 1).toString()
    }

    fun decDislikeCount() {
        thumbsDownCount.text = (thumbsDownCount.text.toString().toInt() - 1).toString()
    }

    fun removeLikeImage() = thumbsUpIV.setImageResource(R.drawable.ic_like)

    fun setDislike() {
        thumbsDownIV.setImageResource(R.drawable.ic_dislike_clicked)
        if (presenter.getUserRate() == 1) {
            removeLikeImage()
            decLikeCount()
        }
        presenter.setUserRate(0)
        incDislikeCount()
    }

    fun removeDislikeImage() = thumbsDownIV.setImageResource(R.drawable.ic_dislike)
}