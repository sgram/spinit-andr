package com.bonch_dev.spinit.presentation.modules.comments

import android.view.View
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.comments.adapter.CommentsAdapter
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior

interface IComment : IBaseView {
    fun getRepliesRV(): RecyclerView
    fun getFM(): FragmentManager
    fun clearET()
    fun hideKB()
    fun getComments()
    fun openBottomSheet()
    fun getBottomSheetState(): Int
    fun getAdapter(): CommentsAdapter?
    fun bindComments(data: List<Comment>)
    fun bindReplies(data: List<Comment>, repliesAdapter: RepliesAdapter?)
    fun getBSBehaviour():BottomSheetBehavior<View>
}