package com.bonch_dev.spinit.presentation.modules.comments.adapter

import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.utils.CurrentUser
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import javax.inject.Inject
import javax.inject.Named

class CommentsAdapter(var list: List<Comment>?, OnCommentClickListener: OnCommentClickListener) :
    RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder>() {
    @Inject
    lateinit var presenter: ICommentsPresenter

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    private var mOnCommentListener: OnCommentClickListener = OnCommentClickListener
    var parentCommentID: Int? = null

    interface OnCommentClickListener {
        fun onCommentClick(
            position: Int,
            viewID: Int?,
            commentItem: Comment?
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder =
        CommentsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0

    inner class CommentsViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private val moreInfoIcon: ImageView = view.findViewById(R.id.comment_more_info_icon)
        private val likeLayout: LinearLayout = view.findViewById(R.id.like_layout)
        private val unlikeLayout: LinearLayout = view.findViewById(R.id.unlike_layout)
        private val thumbsUpIV: ImageView = view.findViewById(R.id.thumbs_up_iv)
        private val thumbsDownIV: ImageView = view.findViewById(R.id.thumbs_down_iv)
        private val thumbsUpCount: TextView = view.findViewById(R.id.thumbs_up_count)
        private var thumbsDownCount: TextView = view.findViewById(R.id.thumbs_down_count)
        private val commentIV: ImageView = view.findViewById(R.id.comment_iv)
        private val commentName: TextView = view.findViewById(R.id.comment_name)
        private val commentDate: TextView = view.findViewById(R.id.comment_date)
        private val commentContent: TextView = view.findViewById(R.id.comment_content)
        private val replyBTN: TextView = view.findViewById(R.id.reply_btn)
        private val expandRepliesLayout: LinearLayout = view.findViewById(R.id.expand_replies_layout)

        fun bind(position: Int) {
            val item = list?.get(position)
            setColors(commentName, item?.moderator)
            item?.run {
                if (author?.id?.toDouble() == CurrentUser.id && (functions?.edit == true || functions?.delete == true)) moreInfoIcon.visible()
                else moreInfoIcon.gone()
            }
            moreInfoIcon.setOnClickListener(this)
            likeLayout.setOnClickListener(this)
            unlikeLayout.setOnClickListener(this)
            expandRepliesLayout.setOnClickListener(this)
            replyBTN.setOnClickListener(this)
            val replies = list?.get(position)?.replies
            if (replies != null && replies > 0) expandRepliesLayout.visible()
            else expandRepliesLayout.gone()
            Glide.with(App.applicationContext()).load(item?.author?.avatar_url)
                .apply(RequestOptions().circleCrop()).into(commentIV)
            when (item?.rates?.liked) {
                1 -> {
                    thumbsUpIV.setImageResource(R.drawable.ic_like_clicked)
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike)
                }
                0 -> {
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike_clicked)
                    thumbsUpIV.setImageResource(R.drawable.ic_like)
                }
                null -> {
                    thumbsUpIV.setImageResource(R.drawable.ic_like)
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike)
                }
            }
            item?.run {
                thumbsDownCount.text = rates?.dislikes.toString()
                thumbsUpCount.text = rates?.likes.toString()
                commentName.text = author?.first_name + " " + author?.last_name
                commentDate.text = created_at
                commentContent.text = text
            }
        }

        override fun onClick(v: View?) {
            parentCommentID = list?.get(adapterPosition)?.id
            mOnCommentListener.onCommentClick(adapterPosition, v?.id, list?.get(adapterPosition))
        }
    }

    private fun setColors(commentName: TextView, moderator: Boolean?) {
        val defaultColor = when (sp.getInt(Keys.THEME, R.style.LightTheme)) {
            R.style.DarkTheme -> Color.WHITE
            else -> Color.BLACK
        }
        commentName.setTextColor(if (moderator == true) Color.parseColor("#EA4949") else defaultColor)
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) = holder.bind(position)
}