package com.bonch_dev.spinit.presentation.modules.comments.adapter

import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.utils.CurrentUser
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import javax.inject.Inject
import javax.inject.Named

class RepliesAdapter(var list: List<Comment>?, onReplyClickListener: OnReplyClickListener) :
    RecyclerView.Adapter<RepliesAdapter.RepliesViewHolder>() {
    private var mOnReplyClickListener = onReplyClickListener

    @Inject
    @Named(Keys.MAIN)
    lateinit var sp: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    interface OnReplyClickListener {
        fun onReplyClick(
            viewID: Int?,
            commentItem: Comment?,
            repliesAdapter: RepliesAdapter
        )
    }

    override fun onBindViewHolder(holder: RepliesAdapter.RepliesViewHolder, position: Int) =
        holder.bind(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepliesAdapter.RepliesViewHolder =
        RepliesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0
    inner class RepliesViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val moreInfoIcon:ImageView=view.findViewById(R.id.comment_more_info_icon)
        private val likeLayout: LinearLayout = view.findViewById(R.id.like_layout)
        private val unlikeLayout: LinearLayout = view.findViewById(R.id.unlike_layout)
        private val thumbsUpIV: ImageView = view.findViewById(R.id.thumbs_up_iv)
        private val thumbsDownIV: ImageView = view.findViewById(R.id.thumbs_down_iv)
        private val thumbsUpCount: TextView = view.findViewById(R.id.thumbs_up_count)
        private val thumbsDownCount: TextView = view.findViewById(R.id.thumbs_down_count)
        private val commentIV: ImageView = view.findViewById(R.id.comment_iv)
        private val commentName: TextView = view.findViewById(R.id.comment_name)
        private val commentDate: TextView = view.findViewById(R.id.comment_date)
        private val commentContent: TextView = view.findViewById(R.id.comment_content)
        private val replyBTN: TextView = view.findViewById(R.id.reply_btn)
        fun bind(position: Int) {
            val item = list?.get(position)
            setColors(commentName, item?.moderator)
            item?.run {
                if (author?.id?.toDouble() == CurrentUser.id && (functions?.edit == true || functions?.delete == true)) moreInfoIcon.visible()
                else moreInfoIcon.gone()
            }
            moreInfoIcon.setOnClickListener(this)
            unlikeLayout.setOnClickListener(this)
            likeLayout.setOnClickListener(this)
            replyBTN.setOnClickListener(this)
            when (list?.get(position)?.rates?.liked) {
                1 -> {
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike)
                    thumbsUpIV.setImageResource(R.drawable.ic_like_clicked)
                }
                0 -> {
                    thumbsUpIV.setImageResource(R.drawable.ic_like)
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike_clicked)
                }
                null -> {
                    thumbsUpIV.setImageResource(R.drawable.ic_like)
                    thumbsDownIV.setImageResource(R.drawable.ic_dislike)
                }
            }
            Glide.with(App.applicationContext()).load(list?.get(position)?.author?.avatar_url)
                .apply(RequestOptions().circleCrop()).into(commentIV)
            val listItem = list?.get(position)
            listItem?.run {
                thumbsDownCount.text = rates?.dislikes.toString()
                thumbsUpCount.text = rates?.likes.toString()
                commentName.text = "${author?.first_name} ${list?.get(position)?.author?.last_name}"
                commentDate.text = created_at
                commentContent.text = text
            }
        }

        private fun setColors(commentName: TextView, moderator: Boolean?) {
            val defaultColor = when (sp.getInt(Keys.THEME, R.style.LightTheme)) {
                R.style.DarkTheme -> Color.WHITE
                else -> Color.BLACK
            }
            commentName.setTextColor(if (moderator == true) Color.parseColor("#EA4949") else defaultColor)
        }

        override fun onClick(v: View?) {
            mOnReplyClickListener.onReplyClick(v?.id,  list?.get(adapterPosition), this@RepliesAdapter)
        }
    }
}