package com.bonch_dev.spinit.presentation.modules.comments.presenter

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.interactors.comments.ICommentsInteractor
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.domain.utils.toRequestBody
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.comments.CommentParentLayout
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.view.CommentETDialog
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import javax.inject.Inject

class CommentsPresenter @Inject constructor(private val interactor: ICommentsInteractor) : ICommentsPresenter,
    BasePresenter<IComment>() {

    private var iComment: IComment? = null

    private val isLiked = 1
    private val isUnliked = 0
    private var parentLayout: LinearLayout? = null
    private var parentCommentID: Int = -1
    private var userRate: Int? = null
    private val commentParentLayout: CommentParentLayout by lazy { CommentParentLayout() }

    override fun getUserRate(): Int? = userRate

    override fun getParentCommentID(): Int = parentCommentID

    override fun setUserRate(value: Int?) {
        userRate = value
    }

    override fun initParentLayout(parentLayout: LinearLayout) {
        this.parentLayout = parentLayout
    }

    override fun getIComm(): IComment? = iComment

    override fun onMoreInfoClick(commentItem: Comment?, parentCommentID: Int?, ctx: Context) {
        val layout = LayoutInflater.from(ctx).inflate(R.layout.layout_comment_more_info, null)
        val commentMoreInfoDialog = BottomSheetDialog(ctx)
        commentMoreInfoDialog.apply {
            setContentView(layout)
            val isEditAvailable = commentItem?.functions?.edit ?: false
            val isDeleteAvailable = commentItem?.functions?.delete ?: false
            when {
                !isEditAvailable -> findViewById<LinearLayout>(R.id.bottom_dialog_edit)?.gone()
                !isDeleteAvailable -> findViewById<LinearLayout>(R.id.bottom_dialog_delete)?.gone()
            }
            findViewById<LinearLayout>(R.id.bottom_dialog_cancel)?.setOnClickListener { dismiss() }
            findViewById<LinearLayout>(R.id.bottom_dialog_edit)?.setOnClickListener {
                val bundle = Bundle().apply {
                    commentItem?.id?.let { putInt("commentID", it) }
                    parentCommentID?.let { putInt("parentCommentID", it) }
                    putBoolean("commentIsChanging", true)
                    putString("text", commentItem?.text)
                }
                showDialog(bundle)
                dismiss()
            }
            findViewById<LinearLayout>(R.id.bottom_dialog_delete)?.setOnClickListener {
                dismiss()
                AlertDialog.Builder(ctx)
                    .setTitle("Удалить")
                    .setMessage("Вы уверены что хотите удалить комментарий?")
                    .setPositiveButton(ResourceManager.getString(R.string.yes)) { _, _ ->
                        dismiss()
                        deleteComment(commentItem?.id ?: -1)
                    }
                    .setNegativeButton(ResourceManager.getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
                    .create().show()
            }
        }
        commentMoreInfoDialog.show()
    }

    override fun onReplyCommentBtnClick(commentItem: Comment?) {
        val bundle = Bundle().apply {
            commentItem?.id?.let { putInt("commentID", it) }
            putInt("parentCommentID", parentCommentID)
            putBoolean("commentIsChanging", false)
        }
        showDialog(bundle)
    }

    private fun showDialog(bundle: Bundle) {
        val fm = getView()?.getFM()
        fm?.let {
            val dlg = CommentETDialog()
            dlg.arguments = bundle
            dlg.show(it, "comment")
        }
    }

    override fun deleteComment(commentID: Int) {
        interactor.deleteComment(
            commentID,
            result = {
                getView()?.run {
                    if (isBottomSheetExpanded())
                        getReplies(parentCommentID, null)
                    else getComments()
                    clearET()
                    (this as Fragment).showToast("Комментарий удален")
                }
            },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun updateComment(commentID: Int, text: String, dlg: CommentETDialog, icom: IComment?, parentCommentID: Int) {
        if (icom != null) attachView(icom)

        if (this.parentCommentID == -1)
            this.parentCommentID = commentID
        else
            this.parentCommentID = parentCommentID

        dlg.dismiss()
        getView()?.hideKB()
        interactor.updateComment(
            if (commentID == -1) parentCommentID else commentID, text,
            result = {
                getView()?.run {
                    if (isBottomSheetExpanded())
                        getReplies(parentCommentID, null)
                    else getComments()
                    clearET()
                    (this as Fragment).showToast("Комментарий изменен")
                }
            },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun sendReply(commentID: Int?, text: String, dlg: CommentETDialog, icom: IComment?, parentCommentID: Int) {
        if (icom != null) attachView(icom)
        this.parentCommentID = parentCommentID
        getView()?.hideKB()
        interactor.replyComment(
            if (commentID == -1) parentCommentID else commentID, text.toRequestBody(),
            result = { onSendReplyResult(dlg) },
            err = (getView() as Fragment)::showToast
        )
    }

    private fun onSendReplyResult(dlg: CommentETDialog) {
        getView()?.run {
            if (isBottomSheetExpanded())
                getReplies(parentCommentID, null)
            else getComments()
            clearET()
        }
        dlg.dismiss()
    }

    override fun onLikeBtnClick(
        commentID: Int?,
        repliesAdapter: RepliesAdapter?,
        parentLayout: Boolean,
        parentCommentID: Int?
    ) {
        interactor.likeComment(
            parentCommentID ?: commentID,
            result = { onLikedCommentResult(parentLayout, repliesAdapter) },
            err = (getView() as Fragment)::showToast
        )
    }

    private fun onLikedCommentResult(parentLayout: Boolean, repliesAdapter: RepliesAdapter?) {
        when {
            isBottomSheetHidden() -> {
                getView()?.getComments()
                return
            }
            parentLayout -> updateLikedParentLayout()
            else -> getReplies(parentCommentID, repliesAdapter)
        }
        getView()?.getComments()
    }

    override fun onDislikeBtnClick(
        commentID: Int?,
        repliesAdapter: RepliesAdapter?,
        parentLayout: Boolean,
        parentCommentID: Int?
    ) {
        interactor.dislikeComment(
            parentCommentID ?: commentID,
            result = { onUnLikedCommentResult(parentLayout, repliesAdapter) },
            err = (getView() as Fragment)::showToast
        )
    }

    private fun onUnLikedCommentResult(parentLayout: Boolean, repliesAdapter: RepliesAdapter?) {
        when {
            isBottomSheetHidden() -> {
                getView()?.getComments()
                return
            }
            parentLayout -> updateUnLikedParentLayout()
            else -> getReplies(parentCommentID, repliesAdapter)
        }
        getView()?.getComments()
    }

    private fun updateLikedParentLayout() {
        getView()?.run {
            when (userRate) {
                null, isUnliked -> commentParentLayout.setLike()
                isLiked -> commentParentLayout.clearRate(true)
            }
        }
    }

    private fun updateUnLikedParentLayout() {
        getView()?.run {
            when (userRate) {
                null, isLiked -> commentParentLayout.setDislike()
                isUnliked -> commentParentLayout.clearRate(false)
            }
        }
    }

    private fun isBottomSheetHidden(): Boolean = getView()?.getBottomSheetState() == BottomSheetBehavior.STATE_HIDDEN

    private fun isBottomSheetExpanded(): Boolean = getView()?.getBottomSheetState() == BottomSheetBehavior.STATE_EXPANDED

    override fun onSendLessonTextCommentBTNClicked(lessonID: Int, commentText: String) {
        getView()?.run {
            hideKB()
            clearET()
        }
        interactor.sendLessonTextComment(
            lessonID, commentText.toRequestBody(),
            result = { getView()?.getComments() },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun onSendArticleCommentBTNClicked(postId: Int, text: String) {
        getView()?.run {
            clearET()
            hideKB()
        }
        interactor.sendArticleComment(
            postId, text.toRequestBody(),
            result = { getView()?.getComments() },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun onSendLessonVideoCommentBTNClicked(lessonID: Int, commentText: String) {
        getView()?.run {
            clearET()
            hideKB()
        }
        interactor.sendLessonVideoComment(
            lessonID, commentText.toRequestBody(),
            result = { getView()?.getComments() },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun onShowRepliesBtnClick(commentItem: Comment?) {
        commentItem?.run {
            parentCommentID = id ?: -1
            userRate = rates?.liked
        }
        Log.e("comment", "comment: " + (commentItem?.id ?: -1).toString())
        Log.e("comment", "parent: $parentCommentID")
        getView()?.openBottomSheet()
        commentParentLayout.initParentLayout(parentLayout, this)
        commentParentLayout.bindParentLayout(commentItem)
        getReplies(commentItem?.id, null)
    }

    override fun onBottomSheetHidden() {
        commentParentLayout.clearData()
        getView()?.getRepliesRV()?.adapter = null
    }

    override fun getLessonTextComments(lessonID: Int) {
        interactor.getLessonTextComments(
            lessonID,
            result = { getView()?.bindComments(it.data) },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun getLessonVideoComments(lessonID: Int) {
        interactor.getLessonVideoComments(
            lessonID,
            result = { getView()?.bindComments(it.data) },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun getArticleComments(postId: Int) {
        interactor.getArticleComments(
            postId,
            result = { getView()?.bindComments(it.data) },
            err = (getView() as Fragment)::showToast
        )
    }

    override fun getReplies(commentID: Int?, repliesAdapter: RepliesAdapter?) {
        interactor.getReplies(
            commentID,
            result = { getView()?.bindReplies(it, repliesAdapter) },
            err = (getView() as Fragment)::showToast
        )
    }
}