package com.bonch_dev.spinit.presentation.modules.comments.presenter

import android.content.Context
import android.widget.LinearLayout
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.view.CommentETDialog

interface ICommentsPresenter {
    fun getUserRate():Int?
    fun onBottomSheetHidden()
    fun setUserRate(value:Int?)
    fun getParentCommentID():Int
    fun initParentLayout(parentLayout: LinearLayout)
    fun onMoreInfoClick(commentItem: Comment?, parentCommentID: Int?, ctx: Context)
    fun getIComm(): IComment?
    fun onReplyCommentBtnClick(commentItem: Comment?)
    fun sendReply(commentID: Int?, text: String, dlg: CommentETDialog, icom: IComment?, parentCommentID: Int)
    fun deleteComment(commentID: Int)
    fun updateComment(commentID: Int, text: String, dlg: CommentETDialog, icom: IComment?, parentCommentID: Int)
    fun onLikeBtnClick(commentID: Int?, repliesAdapter: RepliesAdapter?, parentLayout: Boolean, parentCommentID: Int?)
    fun onDislikeBtnClick(commentID: Int?, repliesAdapter: RepliesAdapter?, parentLayout: Boolean, parentCommentID: Int?)
    fun onSendLessonTextCommentBTNClicked(lessonID: Int, commentText: String)
    fun getLessonTextComments(lessonID: Int)
    fun getLessonVideoComments(lessonID: Int)
    fun getArticleComments(postId: Int)
    fun getReplies(commentID: Int?, repliesAdapter: RepliesAdapter?)
    fun onSendArticleCommentBTNClicked(postId: Int, text: String)
    fun onSendLessonVideoCommentBTNClicked(lessonID: Int, commentText: String)
    fun onShowRepliesBtnClick(commentItem: Comment?)
}