package com.bonch_dev.spinit.presentation.modules.comments.view

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.CommentDialogEtBinding
import com.bonch_dev.spinit.domain.utils.Keyboard
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import javax.inject.Inject

class CommentETDialog : DialogFragment() {

    private lateinit var binding:CommentDialogEtBinding
    private lateinit var textWatcher: TextWatcher

    @Inject
    lateinit var commentsPresenter: ICommentsPresenter

    init {
        App.appComponent.inject(this)
    }

    private var commentID: Int = -1
    private var commentIsChanging: Boolean = false
    private var parentCommentID: Int = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.CustomDialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= CommentDialogEtBinding.inflate(inflater,container,false)
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        arguments?.run {
            commentID = getInt("commentID", -1)
            parentCommentID = getInt("parentCommentID", -1)
            commentIsChanging = getBoolean("commentIsChanging", false)
        }
        binding.commentDialogEt.setText(arguments?.getString("text"))
        initTextWatcher()
        setListeners()
        binding.commentDialogEt.requestFocus()
        val imm: InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(binding.commentDialogEt, InputMethodManager.SHOW_IMPLICIT)
        loadAvatar()
        return binding.root
    }

    private fun loadAvatar() {
        if (Keys.avatarURL.isNotEmpty())
            Glide.with(this).load(Keys.avatarURL).apply(RequestOptions().circleCrop()).into(binding.dlgIv)
    }

    fun setListeners() {
       binding.sendCommentIv.setOnClickListener {
            val commentText = binding.commentDialogEt.text.toString()
            Keyboard.hide(this)
            val icom: IComment = parentFragment as IComment
            if (!commentIsChanging)
                commentsPresenter.sendReply(commentID, commentText, this, icom, parentCommentID)
            else commentsPresenter.updateComment(commentID, commentText, this, icom, parentCommentID)
        }
    }


    private fun initTextWatcher() {
        textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (it.isNotEmpty()) binding.sendCommentIv.visible()
                    else binding.sendCommentIv.gone()
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.run {
            val lp = attributes
            lp?.horizontalMargin = 0f
            attributes = lp
            setGravity(Gravity.BOTTOM)
            setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        }
        binding.commentDialogEt.addTextChangedListener(textWatcher)
    }

    override fun onStop() {
        Keyboard.hide(this)
        binding.commentDialogEt.removeTextChangedListener(textWatcher)
        binding.sendCommentIv.setOnClickListener(null)
        super.onStop()
    }
}