package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.R

class AboutCourseSubThemesAdapter(val list: List<String?>?) :
    RecyclerView.Adapter<AboutCourseSubThemesAdapter.AboutCourseSubThemesViewHolder>() {

    inner class AboutCourseSubThemesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val numberTV: TextView = view.findViewById(R.id.tv_number)
        private val titleTV: TextView = view.findViewById(R.id.tv_sub_title)
        fun bind(position: Int) {
            val item = list?.get(position)
            numberTV.text = (position + 1).toString()
            titleTV.text = item
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutCourseSubThemesViewHolder =
        AboutCourseSubThemesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_about_course_hiding, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: AboutCourseSubThemesViewHolder, position: Int) =
        holder.bind(position)
}