package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoLessons

class AboutCourseThemesAdapter(val list: List<AboutCourseInfoLessons>?, private val subThemeListener: SubThemesListener) :
    RecyclerView.Adapter<AboutCourseThemesAdapter.AboutCourseViewHolder>() {

    interface SubThemesListener {
        fun onSubThemeClick(subThemesLayout: LinearLayout, arrow: ImageView)
    }

    inner class AboutCourseViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val subThemesLayout: LinearLayout = view.findViewById(R.id.sub_themes_layout)
        private val layout: LinearLayout = view.findViewById(R.id.about_course_ib)
        private val subThemesRecycler: RecyclerView = view.findViewById(R.id.sub_themes_recycler)
        private val aboutTitle: TextView = view.findViewById(R.id.about_course_title)
        private val arrow: ImageView = view.findViewById(R.id.arrow_iv)
        fun bind(position: Int) {
            subThemesRecycler.run {
                layoutManager = LinearLayoutManager(App.applicationContext())
                isNestedScrollingEnabled = false
                adapter = AboutCourseSubThemesAdapter(list?.get(position)?.themes)
            }
            aboutTitle.text = list?.get(position)?.title
            layout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            subThemeListener.onSubThemeClick(subThemesLayout, arrow)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutCourseViewHolder =
        AboutCourseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_about_course, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: AboutCourseViewHolder, position: Int) =
        holder.bind(position)
}