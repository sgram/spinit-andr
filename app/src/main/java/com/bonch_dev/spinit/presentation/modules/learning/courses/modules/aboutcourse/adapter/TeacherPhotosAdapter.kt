package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.course.Teachers

class TeacherPhotosAdapter(val list: List<Teachers?>?) :
    RecyclerView.Adapter<TeacherPhotosAdapter.AboutCourseTeacherViewHolder>() {
    inner class AboutCourseTeacherViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val iv: ImageView = view.findViewById(R.id.teacher_iv)
        private val nameTV: TextView = view.findViewById(R.id.teacher_name_tv)
        private val descTV: TextView = view.findViewById(R.id.teacher_desc_tv)
        fun bind(position: Int) {
            val item = list?.get(position)
            nameTV.text = item?.name
            descTV.text = item?.position
            Glide.with(App.applicationContext()).load(item?.avatar_url ?: item?.localAvatar).apply(RequestOptions().circleCrop())
                .into(iv)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AboutCourseTeacherViewHolder =
        AboutCourseTeacherViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_about_course_photos, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: AboutCourseTeacherViewHolder, position: Int) = holder.bind(position)
}