package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ClickableSpan
import android.text.style.ImageSpan
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.view.isGone
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.interactors.course.ICourseInteractor
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view.AboutCourseFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view.IAboutCourseFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.view.AttendCourseDialog
import ru.yandex.money.android.sdk.*
import java.math.BigDecimal
import java.util.*
import javax.inject.Inject

class AboutCoursePresenter @Inject constructor(
    private val interactor: ICourseInteractor
) : IAboutCoursePresenter, BasePresenter<IAboutCourseFragment>(), IDataCompletion<AboutCourseInfoData> {

    private val SHOP_ID = ""
    private val YANDEX_APP_KEY = ""
    private val REQUEST_CODE_TOKENIZE = 2000

    override fun onEnrollBtnClicked(courseID: Int, title: String, fm: FragmentManager) {
        interactor.attend(courseID,
            result = {
                BottomNavigationFragment.refresh()
                getView()?.getMainAct()?.onBackPressed()
                (getView() as Fragment).showToast("Вы записались на курс")
            },
            err = {
                (getView() as Fragment).showToast(it)
            })        /*val dlg = AttendCourseDialog()
        val bundle = Bundle().apply {
            putInt(Keys.COURSE_ID, courseID)
            putString(Keys.COURSE_TITLE, title)
        }
        dlg.arguments = bundle
        dlg.show(fm, "attend_dlg")*/
    }

    override fun getOfferText(ctx: Context): SpannableString {
        val spannedString =
            SpannableString(ResourceManager.getString(R.string.public_offer_text))
        val imageSpan = ImageSpan(ctx, R.drawable.ic_info)
        spannedString.setSpan(imageSpan, 0, 1, SpannableString.SPAN_INCLUSIVE_INCLUSIVE)
        for (word in spannedString.split(" ")) {
            if (word == "оферты") {
                val clickableSpan = object : ClickableSpan() {
                    override fun onClick(widget: View) {
                        val intent = Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(Keys.OFFER_LINK) }
                        ctx?.startActivity(intent)
                    }
                }
                spannedString.setSpan(
                    clickableSpan,
                    spannedString.indexOf(word),
                    spannedString.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )
            }
        }
        return spannedString
    }

    override fun onAttendBTNClicked(courseID: Int, attendCourseDialog: AttendCourseDialog) {
        val aboutCourseFragment = attendCourseDialog.parentFragment as AboutCourseFragment
        val cost = BigDecimal.valueOf(1000)
        val currency = Currency.getInstance("RUB")
        val amount = Amount(cost, currency)
        val paymentParameters =
            PaymentParameters(amount, "Название товара", "Описание товара", YANDEX_APP_KEY, SHOP_ID, SavePaymentMethod.OFF)
        val testParameters = TestParameters()
        attendCourseDialog.colorScheme?.let {
            val uiParameters = UiParameters(showLogo = true, colorScheme = ColorScheme(it))
        }
        val paymentIntent = Checkout.createTokenizeIntent(
            aboutCourseFragment.requireContext(),
            paymentParameters,
            testParameters
        )
        getView()?.getMainAct()?.startActivityForResult(paymentIntent, REQUEST_CODE_TOKENIZE)
        interactor.attend(courseID,
            result = {
                attendCourseDialog.dismiss()
                BottomNavigationFragment.refresh()
                getView()?.getMainAct()?.onBackPressed()
                (getView() as Fragment).showToast("Вы записались на курс")
            },
            err = {
                attendCourseDialog.dismiss()
                (getView() as Fragment).showToast(it)
            })
    }

    override fun getCourseInfo(courseID: Int) {
        getView()?.run {
            (this as InternetFragment).showSplash()
            (this as InternetFragment).hideConnErr()
        }
        if (NetworkConnection.isInternetAvailable())
            interactor.getCourseInfo(
                courseID,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: AboutCourseInfoData) {
        getView()?.run {
            bindData(data)
            (this as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onSubThemeClick(subThemesLayout: LinearLayout, arrow: ImageView) {
        if (subThemesLayout.isGone) {
            ViewAnimation.expand(subThemesLayout)
            arrow.animate().setDuration(200).rotation(180F)
        } else {
            ViewAnimation.collapse(subThemesLayout)
            arrow.animate().setDuration(200).rotation(0F)
        }
    }
}