package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter

import android.content.Context
import android.text.SpannableString
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import com.bonch_dev.spinit.presentation.modules.learning.courses.view.AttendCourseDialog

interface IAboutCoursePresenter {
    fun getOfferText(ctx: Context): SpannableString
    fun onEnrollBtnClicked(courseID: Int, title: String, fm: FragmentManager)
    fun onAttendBTNClicked(courseID: Int, attendCourseDialog: AttendCourseDialog)
    fun getCourseInfo(courseID: Int)
    fun onSubThemeClick(subThemesLayout: LinearLayout, arrow: ImageView)
}