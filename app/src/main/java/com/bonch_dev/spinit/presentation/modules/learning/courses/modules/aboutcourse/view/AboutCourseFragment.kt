package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view

import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentAboutCourseBinding
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.ATTEND
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_ID
import com.bonch_dev.spinit.domain.utils.Keys.GET_COURSE_INFO
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter.AboutCourseThemesAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.adapter.TeacherPhotosAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter.AboutCoursePresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.presenter.IAboutCoursePresenter
import javax.inject.Inject

class AboutCourseFragment : InternetFragment(), AboutCourseThemesAdapter.SubThemesListener,
    IAboutCourseFragment, View.OnClickListener {

    @Inject
    lateinit var presenter: IAboutCoursePresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentAboutCourseBinding

    var courseID: Int = -1
    var title = ""

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentAboutCourseBinding.inflate(inflater, container, false)
        toolbar = binding.aboutCourseToolbar
        bindOfferText()
        initRecyclerParams()
        (presenter as AboutCoursePresenter).attachView(this)
        getDataFromArgs()
        presenter.getCourseInfo(courseID)
        setListeners()
        return binding.root
    }

    private fun getDataFromArgs() {
        courseID = arguments?.getInt(COURSE_ID, -1) ?: -1
    }


    override fun bindData(data: AboutCourseInfoData) {
        data.data?.run {
            attendance?.let {
                binding.priceLayout.run {
                    if (it)
                        gone()
                    else
                        visible()
                }
            }

            binding.run {
                aboutCourseToolbar.title = title
                aboutCourseDesc.text = description

                themesRecycler.adapter = AboutCourseThemesAdapter(lessons, this@AboutCourseFragment)

                teacherRecycler.adapter = TeacherPhotosAdapter(teachers)
            }


            this@AboutCourseFragment.title = title.toString()

        }
    }

    override fun onSubThemeClick(subThemesLayout: LinearLayout, arrow: ImageView) {
        presenter.onSubThemeClick(subThemesLayout, arrow)
    }

    private fun bindOfferText() {
        binding.termsOfUseTv.movementMethod = LinkMovementMethod.getInstance()
        binding.termsOfUseTv.text = presenter.getOfferText(requireContext())
    }

    private fun initRecyclerParams() {
        binding.run {
            teacherRecycler.run {
                layoutManager = LinearLayoutManager(context)

                isNestedScrollingEnabled = false
            }
            themesRecycler.run {
                isNestedScrollingEnabled = false
                layoutManager = LinearLayoutManager(context)
            }
        }

    }

    override fun tSection(bt: View, lyt: View) {
        val show: Boolean = tArrow(bt)
        if (show) ViewAnimation.expand(lyt)
        else ViewAnimation.collapse(lyt)
    }

    override fun tArrow(view: View): Boolean {
        return if (view.rotation == 0.0f) {
            view.animate().setDuration(200).rotation(180F)
            true
        } else {
            view.animate().setDuration(200).rotation(0F)
            false
        }
    }

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun setListeners() {
        binding.enrollCourseBtn.setOnClickListener(this)
        binding.aboutCourseToolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.getCourseInfo(courseID)
        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.enroll_course_btn -> presenter.onEnrollBtnClicked(courseID, title, childFragmentManager)
        }
    }

    override fun onDestroyView() {
        cancelJobs(GET_COURSE_INFO, ATTEND)
        (presenter as AboutCoursePresenter).detachView()
        super.onDestroyView()
    }
}