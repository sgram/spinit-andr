package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view

import android.view.View
import com.bonch_dev.spinit.domain.entity.response.course.AboutCourseInfoData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface IAboutCourseFragment : IBaseView {
    fun getMainAct(): MainActivity
    fun tArrow(view: View): Boolean
    fun tSection(bt: View, lyt: View)
    fun bindData(data: AboutCourseInfoData)
}