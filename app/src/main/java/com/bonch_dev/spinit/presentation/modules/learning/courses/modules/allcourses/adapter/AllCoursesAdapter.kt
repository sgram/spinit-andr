package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.adapter

import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.THEME
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject
import javax.inject.Named

class AllCoursesAdapter(var coursesList: MutableList<Course>?, private val onCourseClickListener: OnCourseClickListener) :
    RecyclerView.Adapter<AllCoursesAdapter.AllCoursesViewHolder>() {
    @Inject
    lateinit var mainRouter: MainRouter

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    interface OnCourseClickListener {
        fun onMoreInfoClicked(id: Int?)
    }

    init {
        App.appComponent.inject(this)
    }

    override fun onBindViewHolder(holder: AllCoursesViewHolder, position: Int) =
        holder.bind(position)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllCoursesViewHolder =
        AllCoursesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_course, parent, false))

    override fun getItemCount(): Int = coursesList?.size ?: 0

    inner class AllCoursesViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val courseName: TextView = view.findViewById(R.id.all_course_name)
        private val courseIV: ImageView = view.findViewById(R.id.all_course_iv)
        private val courseContent: TextView = view.findViewById(R.id.all_course_content)
        private val allCourseMoreInfo: TextView = view.findViewById(R.id.all_course_more_info)
        fun bind(position: Int) {
            val item = coursesList?.get(position)
            allCourseMoreInfo.setOnClickListener(this)
            val ctx = App.applicationContext()
            Glide.with(ctx).load(item?.icon_url).into(courseIV)
            val theme = sp.getInt(THEME, R.style.LightTheme)
            if (theme == R.style.DarkTheme) courseIV.setColorFilter(Color.WHITE)
            courseName.text = item?.title
            courseContent.text = item?.description
        }

        override fun onClick(v: View?) {
            onCourseClickListener.onMoreInfoClicked(coursesList?.get(adapterPosition)?.id)
        }
    }
}