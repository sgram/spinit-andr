package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter

import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.interactors.course.ICourseInteractor
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.AllCoursesFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.IAllCoursesFragment
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class AllCoursesPresenter @Inject constructor(
    private val interactor: ICourseInteractor,
    private val router: MainRouter
) : IAllCoursesPresenter, BasePresenter<IAllCoursesFragment>(), IDataCompletion<MutableList<Course>> {

    override fun onStart() {
        getView()?.run {
            (this as InternetFragment).showSplash()
            (this as InternetFragment).hideConnErr()
            getAllCourses()
        }
    }

    override fun getAllCourses() {
        if (NetworkConnection.isInternetAvailable())
            interactor.getAllcourses(
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: MutableList<Course>) {
        getView()?.run {
            list = data
            queryTextListener = initSVQueryListener(list)
            getBottomNavFragment().searchView?.setOnQueryTextListener(queryTextListener)
            bindRecycler(list)
            (this as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onMoreInfoClicked(id: Int?, allCoursesFragment: AllCoursesFragment) {
        router.toAboutCourseInfo(allCoursesFragment, id)
    }

    override fun initSVQueryListener(list: MutableList<Course>): androidx.appcompat.widget.SearchView.OnQueryTextListener {
        return object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    val filtered = filter(list, it)
                    getView()?.bindRecycler(filtered)
                }
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    val filtered = filter(list, it)
                    getView()?.bindRecycler(filtered)
                }
                return true
            }
        }
    }

    override fun filter(list: List<Course>, query: String?): MutableList<Course> {
        val filteredList = mutableListOf<Course>()
        list.forEach {
            val text = it.title?.toLowerCase()
            if (text != null && query != null && text.startsWith(query.toLowerCase())) {
                filteredList.add(it)
            }
        }
        return filteredList
    }
}