package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter

import android.widget.SearchView
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.AllCoursesFragment

interface IAllCoursesPresenter {
    fun onStart()
    fun onMoreInfoClicked(id: Int?, allCoursesFragment: AllCoursesFragment)
    fun filter(list: List<Course>, query: String?): MutableList<Course>
    fun initSVQueryListener(list: MutableList<Course>): androidx.appcompat.widget.SearchView.OnQueryTextListener
    fun getAllCourses()
}