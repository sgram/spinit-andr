package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view

import android.graphics.Color
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.annotation.AnimRes
import androidx.annotation.ColorInt
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentCourseBinding
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.domain.utils.Keys.GET_ALL_COURSES
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.adapter.AllCoursesAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter.AllCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.presenter.IAllCoursesPresenter
import javax.inject.Inject

class AllCoursesFragment : InternetFragment(), IAllCoursesFragment, AllCoursesAdapter.OnCourseClickListener {

    override var list: MutableList<Course> = mutableListOf()

    @Inject
    lateinit var presenter: IAllCoursesPresenter

    init {
        App.appComponent.inject(this)
    }

    override lateinit var queryTextListener: androidx.appcompat.widget.SearchView.OnQueryTextListener

    private lateinit var binding: FragmentCourseBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentCourseBinding.inflate(inflater, container, false)
        (presenter as AllCoursesPresenter).attachView(this)

        presenter.onStart()
        setListeners()


        return binding.root
    }

    override fun getBottomNavFragment(): BottomNavigationFragment = parentFragment as BottomNavigationFragment

    override fun setListeners() {}

    override fun onMoreInfoClicked(id: Int?) {
        presenter.onMoreInfoClicked(id, this)
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.onStart()
        }
    }


    override fun bindRecycler(list: MutableList<Course>) {
        @AnimRes val resId: Int = R.anim.fall_down_layout_animation
        val animation: LayoutAnimationController = AnimationUtils.loadLayoutAnimation(requireContext(), resId)
        binding.allCoursesRecycler.run {
            layoutAnimation = animation
            scheduleLayoutAnimation()
            adapter = AllCoursesAdapter(list, this@AllCoursesFragment)
        }
    }


    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden)
            styleComponents()
        else {
            Log.e("hide status", "${javaClass.simpleName} visible")
        }
    }

    private fun styleComponents() {
        val typedValue = TypedValue()
        val theme = context?.theme
        theme?.resolveAttribute(R.attr.toolbarColor, typedValue, true)
        @ColorInt val color = typedValue.data
        getBottomNavFragment().binding.toolbar.run {
            setBackgroundColor(color)
            title = "Все курсы"
            setTitleTextColor(Color.WHITE)
        }
        getBottomNavFragment().searchItem?.isVisible = true
    }

    override fun onDestroyView() {
        cancelJobs(GET_ALL_COURSES)
        (presenter as AllCoursesPresenter).detachView()
        super.onDestroyView()
    }
}