package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view

import android.widget.SearchView
import com.bonch_dev.spinit.domain.entity.response.course.Course
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment

interface IAllCoursesFragment : IBaseView {
    var list: MutableList<Course>
    var queryTextListener: androidx.appcompat.widget.SearchView.OnQueryTextListener
    fun bindRecycler(list: MutableList<Course>)
    fun getBottomNavFragment(): BottomNavigationFragment
}