package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.adapter

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.THEME
import com.bonch_dev.spinit.domain.utils.ViewAnimation
import javax.inject.Inject
import javax.inject.Named

class MyCoursesAdapter(private val list: List<MyCourseInfo>?, private val onMyCoursesClickListener: OnMyCoursesClickListener) :
    RecyclerView.Adapter<MyCoursesAdapter.MyCoursesViewHolder>() {

    interface OnMyCoursesClickListener {
        fun onMyCourseItemClick(title: String?, id: Int?)
    }

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    inner class MyCoursesViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnTouchListener {
        private val myCoursesIV: ImageView = view.findViewById(R.id.my_course_iv)
        private val myCoursesNameTV: TextView = view.findViewById(R.id.my_course_name_tv)
        private val myCoursesDescTV: TextView = view.findViewById(R.id.my_course_desc)
        private val cardView: CardView = view.findViewById(R.id.my_course_cv)

        @SuppressLint("ClickableViewAccessibility")
        fun bind(position: Int) {
            val item = list?.get(position)

            cardView.setOnTouchListener(this)

            val ctx = App.applicationContext()

            Glide.with(ctx).load(item?.icon_url).into(myCoursesIV)

            val theme = sp.getInt(THEME, R.style.LightTheme)
            if (theme == R.style.DarkTheme) myCoursesIV.setColorFilter(Color.WHITE)

            myCoursesNameTV.text = item?.title
            myCoursesDescTV.text = "Продолжай обучение"
        }

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            if (v?.id == R.id.my_course_cv) {
                val scaleUp = ViewAnimation.initScaleUpAnim(v, 1.03f, 1.03f, 0)
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> scaleUp.start()
                    MotionEvent.ACTION_MOVE -> {
                        ViewAnimation.clearAnim(scaleUp)
                        ViewAnimation.initScaleDownAnim(v, 1.0f, 1.0f, 0).start()
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        ViewAnimation.clearAnim(scaleUp)
                        ViewAnimation.initScaleDownAnim(v, 1.0f, 1.0f, 0).start()
                        onMyCoursesClickListener.onMyCourseItemClick(
                            list?.get(adapterPosition)?.title,
                            list?.get(adapterPosition)?.id
                        )
                    }
                }
            }
            return false
        }

        override fun onClick(v: View?) =
            onMyCoursesClickListener.onMyCourseItemClick(list?.get(adapterPosition)?.title, list?.get(adapterPosition)?.id)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyCoursesViewHolder =
        MyCoursesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_mycourses, parent, false))

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: MyCoursesViewHolder, position: Int) = holder.bind(position)
}