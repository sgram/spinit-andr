package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.presenter

import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view.MyCoursesFragment

interface IMyCoursesPresenter {
    fun getMyCourses(myCoursesFragment: MyCoursesFragment)
}