package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view

import android.view.View
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo

interface IMyCoursesFragment {
    fun bindData(courses: List<MyCourseInfo>?)
}