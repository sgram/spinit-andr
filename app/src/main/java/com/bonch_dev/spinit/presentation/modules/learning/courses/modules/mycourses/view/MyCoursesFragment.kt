package com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view

import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.annotation.AnimRes
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentMycoursesBinding
import com.bonch_dev.spinit.domain.entity.response.course.MyCourseInfo
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_THEME
import com.bonch_dev.spinit.domain.utils.Keys.ID
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.adapter.MyCoursesAdapter
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.presenter.IMyCoursesPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.IMainFragment
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.MainFragment
import javax.inject.Inject
import javax.inject.Named

class MyCoursesFragment : BaseFragment(), IMyCoursesFragment, MyCoursesAdapter.OnMyCoursesClickListener {

    private var mAdapter: MyCoursesAdapter? = null

    private lateinit var mainFragment: IMainFragment

    private lateinit var binding: FragmentMycoursesBinding

    @Inject
    lateinit var presenter: IMyCoursesPresenter

    @Inject
    @Named(MAIN)
    lateinit var spEditor: SharedPreferences.Editor

    init {
        App.appComponent.inject(this)
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentMycoursesBinding.inflate(inflater, container, false)
        mainFragment = parentFragment as MainFragment

        return binding.root
    }

    override fun onMyCourseItemClick(title: String?, id: Int?) {

        spEditor.putString(COURSE_THEME, title)?.apply()
        mainFragment.bindTheme(title)

        val intent = Intent(context, LessonActivity::class.java).apply {
            putExtra(ID, id)
        }
        startActivity(intent)
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }


    override fun onDestroyView() {
        mAdapter = null
        super.onDestroyView()
    }

    private fun showNoContentMessage(msg: String) {
        binding.noContent.noContentTv.text = msg
        binding.noContent.noContentTv.visible()
    }

    private fun hideNoContentMessage() = binding.noContent.noContentTv.gone()

    override fun bindData(courses: List<MyCourseInfo>?) {
        if (courses.isNullOrEmpty()) {
            showNoContentMessage("Вы не записаны ни на один курс")
        } else {
            hideNoContentMessage()
            @AnimRes val resId: Int = R.anim.fall_down_layout_animation
            val animation: LayoutAnimationController = AnimationUtils.loadLayoutAnimation(requireContext(), resId)
            binding.myCoursesRecycler?.run {
                layoutAnimation = animation
                scheduleLayoutAnimation()
                adapter = MyCoursesAdapter(courses, this@MyCoursesFragment)
            }
        }
    }
}