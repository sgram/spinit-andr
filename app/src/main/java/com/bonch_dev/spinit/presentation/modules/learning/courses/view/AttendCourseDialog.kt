package com.bonch_dev.spinit.presentation.modules.learning.courses.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.DialogAttendBinding
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_ID
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_TITLE
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view.AboutCourseFragment

class AttendCourseDialog : DialogFragment(), View.OnClickListener {
    private var courseID = -1
    private var title = ""
    var colorScheme: Int? = null

    private lateinit var binding:DialogAttendBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding= DialogAttendBinding.inflate(inflater,container,false)

        colorScheme = ResourceManager.getColor(R.color.mainColor)
        getDataFromParentFrag()

        return binding.root
    }

    private fun setListeners() {
        binding.toPaymentBtn.setOnClickListener(this)
        binding.closeDialogIv.setOnClickListener(this)
    }

    private fun getDataFromParentFrag() {
        arguments?.run {
            courseID = getInt(COURSE_ID)
            title = getString(COURSE_TITLE, "")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.attendDialogTitle.text = "Получение доступа к курсу $title"
        setListeners()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.to_payment_btn -> (parentFragment as AboutCourseFragment).presenter.onAttendBTNClicked(courseID, this)
            R.id.close_dialog_iv -> this.dismiss()
        }
    }
}