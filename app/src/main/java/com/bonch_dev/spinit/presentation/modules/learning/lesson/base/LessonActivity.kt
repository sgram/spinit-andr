package com.bonch_dev.spinit.presentation.modules.learning.lesson.base

import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.ID
import com.bonch_dev.spinit.domain.utils.Keys.LESSON_ID
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.THEME
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.view.LectionFragment
import com.bonch_dev.spinit.router.LessonRouter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import javax.inject.Inject
import javax.inject.Named

class LessonActivity : AppCompatActivity(), CurrentFragListener {
    @Inject
    lateinit var router: LessonRouter

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    private var selectedFrag: Fragment? = null
    var homeworkStatus: HomeworkCompletionStatus = HomeworkCompletionStatus.NONE
    var testStatus: TestCompletionStatus = TestCompletionStatus.NONE

    init {
        App.appComponent.inject(this)
    }

    var id = -1
    var lessonId = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        when (sp.getInt(THEME, R.style.LightTheme)) {
            R.style.DarkTheme -> setTheme(R.style.DarkTheme)
            R.style.LightTheme -> setTheme(R.style.LightTheme)
        }
        setContentView(R.layout.activity_lesson)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        router.controller = findNavController(R.id.lesson_host_fragment)
        if (savedInstanceState != null) {
            savedInstanceState.run {
                id = getInt(ID, -1)
                lessonId = getInt(LESSON_ID)
            }
        } else id = intent.getIntExtra(ID, -1)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == HomeworkFragment.HOMEWORK_CODE && selectedFrag is HomeworkFragment && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            val frag = selectedFrag as HomeworkFragment
            frag.presenter.onAttachmentBTNClicked(frag)
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onBackPressed() {
        when (selectedFrag) {
            is IComment -> {
                val iComment = selectedFrag as IComment
                iComment.run {
                    if (getBSBehaviour().state == BottomSheetBehavior.STATE_EXPANDED)
                        getBSBehaviour().state = BottomSheetBehavior.STATE_HIDDEN
                    else super.onBackPressed()
                }
                return
            }
            is LectionFragment -> {
                val lectionFragment = selectedFrag as LectionFragment
                lectionFragment.run {
                    when {
                        getVideoPlayer().isFullScreen() -> {
                            getVideoPlayer().exitFullScreen()
                            getLessonActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                        else -> super.onBackPressed()
                    }
                    return
                }
            }
            else -> {
                Keys.cancelJobs(Keys.GET_COURSE_STATUS)
                super.onBackPressed()
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.run {
            putInt(ID, id)
            putInt(LESSON_ID, lessonId)
        }
    }

    override fun selectedFragment(fragment: Fragment) {
        selectedFrag = fragment
    }

    override fun onStart() {
        super.onStart()
        Log.e("${javaClass.simpleName} lifecycle", "start")
    }

    override fun onPause() {
        super.onPause()
        Log.e("${javaClass.simpleName} lifecycle", "pause")
    }

    override fun onStop() {
        Log.e("${javaClass.simpleName} lifecycle", "stop")
        super.onStop()
    }

    override fun onDestroy() {
        Log.e("${javaClass.simpleName} lifecycle", "destroy")
        selectedFrag = null
        super.onDestroy()
    }
}

interface CurrentFragListener {
    fun selectedFragment(fragment: Fragment)
}
