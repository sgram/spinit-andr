package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.domain.utils.Keys.MAX_HOMEWORK_DOCS_COUNT
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.SizeChangeListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment

class HomeworkDocsAdapter(val list: MutableList<Document>?, private val fragment: HomeworkFragment) :
    RecyclerView.Adapter<HomeworkDocsAdapter.HomeworkDocsViewHolder>() {

    var sizeChangeListener: SizeChangeListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeworkDocsAdapter.HomeworkDocsViewHolder =
        HomeworkDocsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_document, parent, false))

    override fun getItemCount(): Int = if (list?.size ?: 0 > MAX_HOMEWORK_DOCS_COUNT) MAX_HOMEWORK_DOCS_COUNT else list?.size ?: 0

    override fun onBindViewHolder(holder: HomeworkDocsAdapter.HomeworkDocsViewHolder, position: Int) = holder.bind(position)

    inner class HomeworkDocsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val uploadFileTV: TextView = view.findViewById(R.id.upload_file_tv)
        private val uploadFileExtension: TextView = view.findViewById(R.id.upload_file_extension)
        private val deleteDocIV: ImageView = view.findViewById(R.id.delete_doc_icon)
        fun bind(position: Int) {
            val item = list?.get(position)
            item?.run {
                uploadFileTV.text = name
                uploadFileExtension.text = "$ext $sizeFormatted"
            }

            deleteDocIV.setOnClickListener {
                removeItem(position)
            }
        }
    }

    fun removeItem(position: Int) {
        list?.get(position)?.sizeInBytes?.let { sizeChangeListener?.onRemoveItem(it) }
        list?.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
        if (list?.size == 0 || list?.isEmpty() == true) {
            val animation = AnimationUtils.loadAnimation(fragment.requireContext(), R.anim.scale_anim_hide)
            animation.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationEnd(animation: Animation?) = fragment.clearDocsAdapter()
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationStart(animation: Animation?) {}
            })
            fragment.animateContainer(animation)
        }
    }

    fun addItems(item: List<Document>) {
        Log.e("new items in adapter", item.size.toString())
        list?.addAll(0, item)
        notifyItemRangeInserted(0, item.size)
        notifyItemRangeChanged(0, itemCount)
    }
}