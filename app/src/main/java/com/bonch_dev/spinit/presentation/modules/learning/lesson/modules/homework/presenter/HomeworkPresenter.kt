package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter

import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.Gravity
import android.view.ViewGroup
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.documentfile.provider.DocumentFile
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.domain.entity.response.lesson.LessonHomework
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.MAX_HOMEWORK_DOCS_COUNT
import com.bonch_dev.spinit.domain.utils.ResourceManager.getString
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.adapter.HomeworkDocsAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.IHomeworkFragment
import com.bonch_dev.spinit.router.LessonRouter
import com.bumptech.glide.Glide
import kotlinx.coroutines.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.text.DecimalFormat
import javax.inject.Inject

class HomeworkPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    IHomeworkPresenter, BasePresenter<IHomeworkFragment>(), IDataCompletion<LessonHomework>, SizeChangeListener {
    private val REQUEST_CODE_DOC = 5
    private val homeworkFilesUriJob = Job()
    private val MAX_FILE_SIZE_IN_BYTES: Long = 15000000
    private val MAX_MUTUAL_FILES_SIZE_IN_BYTES: Long = 50000000
    private var globalSize: Long = 0


    override fun onRemoveItem(size: Long) {
        Log.e(javaClass.simpleName, "Before: $globalSize - $size=${globalSize - size}")
        globalSize -= size
        Log.e(javaClass.simpleName, "After: $globalSize")
    }

    override fun cancelJob() {
        if (homeworkFilesUriJob.isActive) homeworkFilesUriJob.cancelChildren()
        Log.e(javaClass.simpleName, "homeworkFilesUriJob cancelled")
    }

    override fun getLessonHomework(id: Int) {
        getView()?.run {
            (this as InternetFragment).showSplash()
            (this as InternetFragment).hideConnErr()
        }
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonHomework(
                id,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onResult(data: LessonHomework) {
        val doc = Jsoup.parse(data.homework)
        for (el in doc.allElements) {
            when (el.tagName()) {
                "h1" -> bindH1(el)
                "h2" -> bindH2(el)
                "h3" -> bindH3(el)
                "h4" -> bindH4(el)
                "h5" -> bindH5(el)
                "h6" -> bindH6(el)
                "img" -> bindIMG(el)
                "p" -> bindP(el)
                "ul" -> bindUL(el)
                "div" -> bindDiv(el)
            }
        }
        (getView() as InternetFragment).hideSplash()
    }

    private fun initLP(top: Int, bottom: Int, left: Int, right: Int): LinearLayout.LayoutParams? =
        LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            topMargin = top
            bottomMargin = bottom
            leftMargin = left
            rightMargin = right
        }

    private fun bindH1(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h1TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h1TextView)
    }

    private fun bindH2(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h2TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h2TextView)
    }

    private fun bindH3(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h3TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h3TextView)
    }

    private fun bindH4(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h4TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h4TextView)
    }

    private fun bindH5(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h5TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h5TextView)
    }

    private fun bindH6(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h6TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h6TextView)
    }

    private fun bindIMG(el: Element) {
        val image = el.getElementsByTag("img")
        val src = image.attr("src")
        val imageView = ImageView(getView()?.getCtx())
        val imageLP = initLP(top = 0, bottom = 20, left = 0, right = 0)
        imageView.layoutParams = imageLP
        getView()?.getCtx()?.let { Glide.with(it).load(src).into(imageView) }
        getView()?.addView(imageView)
    }

    private fun bindP(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindUL(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindDiv(el: Element) {
        val divLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val divTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = divLP
            text = getHTML(el.toString())
        }
        getView()?.addView(divTextView)
    }

    @Suppress("DEPRECATION")
    private fun getHTML(text: String): Spanned {
        return if (Build.VERSION.SDK_INT >= 24)
            HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
        else Html.fromHtml(text)
    }

    override fun onNextStepBTNClicked(lessonID: Int, text: String, files: MutableList<Document>?) {
        if (text.isEmpty()) {
            (getView() as Fragment).showToast(ResourceManager.getString(R.string.empty_field_error))
            return
        }
        val progressDialog: AlertDialog = showProgress()
        (getView() as BaseFragment).hideKB()
        interactor.sendHomework(
            text, lessonID, files,
            result = {
                progressDialog.dismiss()
                showHomeworkSentDialog()
            },
            err = (getView() as Fragment)::showToast
        )
    }

    private fun showProgress(): AlertDialog {
        val ctx = getView()?.getCtx()
        val wrapContentParam = ViewGroup.LayoutParams.WRAP_CONTENT
        val llParam = LinearLayout.LayoutParams(wrapContentParam, wrapContentParam)
        val llPadding = 30
        val linearLayout = LinearLayout(ctx).apply {
            orientation = LinearLayout.HORIZONTAL
            setPadding(llPadding, llPadding, llPadding, llPadding)
            layoutParams = llParam
            gravity = Gravity.CENTER_VERTICAL
        }

        val progressLP = LinearLayout.LayoutParams(wrapContentParam, wrapContentParam)
        val progressBar = ProgressBar(getView()?.getCtx())

        val tvLP = LinearLayout.LayoutParams(wrapContentParam, wrapContentParam).apply {
            marginStart = 30
        }
        val tvText = TextView(ctx).apply {
            text = getString(R.string.loading_progress)
            setTextColor(Color.BLACK)
            gravity = Gravity.CENTER
        }

        linearLayout.addView(progressBar, progressLP)
        linearLayout.addView(tvText, tvLP)

        val builder = AlertDialog.Builder(ctx).setView(linearLayout).setCancelable(false)
        val dialog = builder.create()
        dialog.show()
        return dialog
    }

    private fun showHomeworkSentDialog() {
        AlertDialog.Builder(getView()?.getCtx())
            .setTitle(ResourceManager.getString(R.string.homework_is_sent))
            .setCancelable(false)
            .setMessage(getString(R.string.homework_sent_desc))
            .setPositiveButton(ResourceManager.getString(R.string.OK)) { _, _ ->
                when (val act = getView()?.getActiv()) {
                    is LessonActivity -> router.controller?.popBackStack(R.id.lessonCategoryFragment, false)
                    is MainActivity -> act.onBackPressed()
                }
            }
            .create().apply {
                show()
                findViewById<TextView>(android.R.id.message).apply {
                    textSize = 14F
                    setTextColor(Color.parseColor("#666666"))
                }
            }
    }

    override fun onAttachmentBTNClicked(homeworkFragment: HomeworkFragment) {
        val intent = Intent().apply {
            action = Intent.ACTION_GET_CONTENT
            type = "application/*"
            putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        }
        homeworkFragment.startActivityForResult(intent, REQUEST_CODE_DOC)
    }

    private fun getSize(size: Long?): String {
        val measureConst = 1000.0
        if (size != null) {
            val m: Double = size / measureConst
            val k: Double = size / (measureConst * measureConst)
            val dec = DecimalFormat("0.00")
            return when {
                m > 1000 -> dec.format(k).plus(" MB")
                m > 1 -> dec.format(m).plus(" KB")
                else -> dec.format(size).plus(" B")
            }
        }
        return ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE_DOC && resultCode == Activity.RESULT_OK) {
            val adapter = getView()?.getAdapter()
            var currentDocsCount = 0
            val clipData: ClipData? = data?.clipData
            var selectedCount = 0
            if (clipData != null)
                selectedCount = clipData.itemCount
            if (adapter != null)
                currentDocsCount = adapter.list?.size ?: 0
            if (currentDocsCount + selectedCount > MAX_HOMEWORK_DOCS_COUNT) {
                showDialog(
                    title = getString(R.string.docs_count_limit_reached),
                    message = String.format(getString(R.string.docs_count_limit_reached_desc), MAX_HOMEWORK_DOCS_COUNT)
                )
                return
            }
            CoroutineScope(Dispatchers.IO + CoroutineName("HomeworkFilesUriThread") + homeworkFilesUriJob).launch {
                val itemsList = mutableListOf<Document>()
                if (clipData != null)
                    itemsList.addAll(getDocFromClipDataChildren(clipData))
                else
                    getDocFromSingleUri(data?.data, completion = itemsList::add)
                withContext(Dispatchers.Main) {
                    if (isAllFilesSizeNormal(itemsList))
                        addItemsToAdapter(itemsList)
                    else showDialog(
                        title = getString(R.string.overreach_mutual_files_size),
                        message = String.format(
                            getString(R.string.overreach_mutual_files_size_desc),
                            MAX_MUTUAL_FILES_SIZE_IN_BYTES / 1000000
                        )
                    )
                }
            }
        }
    }

    private fun isAllFilesSizeNormal(docs: List<Document>): Boolean {
        docs.forEach {
            it.sizeInBytes?.let { size ->
                if (globalSize + size > MAX_MUTUAL_FILES_SIZE_IN_BYTES)
                    return false
                globalSize += size
            }
        }
        Log.e(javaClass.simpleName, "files size: $globalSize")
        return globalSize < MAX_MUTUAL_FILES_SIZE_IN_BYTES
    }

    private suspend inline fun getDocFromSingleUri(uri: Uri?, completion: DataHandler<Document>) {
        var doc: Document? = null
        uri?.let {
            doc = getFileInfo(it)
            doc?.let { doc ->
                if (!isFileSizeNormal(doc)) {
                    withContext(Dispatchers.Main) {
                        showDialog(
                            title = getString(R.string.overreach_file_size),
                            message = String.format(
                                getString(R.string.overreach_file_size_desc),
                                (MAX_FILE_SIZE_IN_BYTES / 1000000)
                            )
                        )
                    }
                    return
                }
            }
        }
        doc?.let(completion)
    }

    private suspend fun getDocFromClipDataChildren(clipData: ClipData): MutableList<Document> {
        Log.e("coroutine", "beforeCoroutine ")
        val list = mutableListOf<Document>()
        CoroutineScope(Dispatchers.IO).launch {
            for (i in 0 until clipData.itemCount step 1) {
                launch {
                    Log.e("coroutine", "new thread inside coroutine ${this.coroutineContext}")
                    clipData.getItemAt(i)?.uri?.let {
                        val doc = getFileInfo(it)
                        if (!isFileSizeNormal(doc)) {
                            withContext(Dispatchers.Main) {
                                showDialog(
                                    title = getString(R.string.overreach_file_size),
                                    message = String.format(
                                        getString(R.string.overreach_file_size_desc),
                                        (MAX_FILE_SIZE_IN_BYTES / 1000000)
                                    )
                                )
                            }
                            return@launch
                        }
                        list.add(doc)
                    }
                }
            }
        }.join()
        return list
    }

    private fun isFileSizeNormal(doc: Document): Boolean {
        val size = doc.sizeInBytes
        val fileIsNormalSize = size ?: 0 < MAX_FILE_SIZE_IN_BYTES
        Log.e(javaClass.simpleName, "$size\nfile is Normal size: $fileIsNormalSize")
        return fileIsNormalSize
    }

    private fun addItemsToAdapter(itemsList: MutableList<Document>) {
        Log.e(this@HomeworkPresenter.javaClass.simpleName, "itemsCount: ${itemsList.size}")
        val adptr = getView()?.getAdapter()
        if (adptr == null)
            firstDocsAdapterInit(itemsList)
        else
            addItemToExistingAdapter(adptr, itemsList)
    }

    private fun getFileInfo(uri: Uri): Document {
        val file = DocumentFile.fromSingleUri(App.applicationContext(), uri)
        val nameList = file?.name?.split(".")
        val name = nameList?.first()
        val ext = nameList?.get(1)?.toUpperCase()
        val fileSizeInBytes = file?.length()
        val formattedSize = getSize(fileSizeInBytes)
        return Document(name, ".$ext", formattedSize, fileSizeInBytes, uri)
    }

    private fun addItemToExistingAdapter(adapter: HomeworkDocsAdapter, items: List<Document>) {
        adapter.addItems(items)
        getView()?.scrollToBegin()
    }

    private fun firstDocsAdapterInit(item: MutableList<Document>) {
        val animation = ResourceManager.getAnimation(R.anim.scale_anim_appear).apply {
            setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationEnd(animation: Animation?) {}
                override fun onAnimationRepeat(animation: Animation?) {}
                override fun onAnimationStart(animation: Animation?) {
                    getView()?.bindDocs(item, this@HomeworkPresenter)
                }
            })
        }
        getView()?.animateContainer(animation)
    }

    private fun showDialog(title: String, message: String) {
        AlertDialog.Builder(getView()?.getCtx())
            .setTitle(title)
            .setMessage(message).setPositiveButton(R.string.OK) { dialog, _ -> dialog.dismiss() }
            .create().show()
    }
}

interface SizeChangeListener {
    fun onRemoveItem(size: Long)
}