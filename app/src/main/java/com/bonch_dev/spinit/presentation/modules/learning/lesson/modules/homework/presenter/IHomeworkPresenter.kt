package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter

import android.content.Intent
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment

interface IHomeworkPresenter {
    fun cancelJob()
    fun onNextStepBTNClicked(lessonID: Int, text: String, files: MutableList<Document>?)
    fun getLessonHomework(id: Int)
    fun onAttachmentBTNClicked(homeworkFragment: HomeworkFragment)
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
}