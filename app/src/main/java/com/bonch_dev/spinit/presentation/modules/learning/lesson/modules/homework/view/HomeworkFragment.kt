package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentHomeworkBinding
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.GET_HOMEWORK
import com.bonch_dev.spinit.domain.utils.Keys.ID
import com.bonch_dev.spinit.domain.utils.Keys.SEND_HOMEWORK
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.adapter.HomeworkDocsAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.HomeworkPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.IHomeworkPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.SizeChangeListener
import javax.inject.Inject

class HomeworkFragment : InternetFragment(), IHomeworkFragment {

    private lateinit var act: AppCompatActivity
    private var lessonID = -1

    private lateinit var currentFragListener: CurrentFragListener
    private val readPerm = Manifest.permission.READ_EXTERNAL_STORAGE
    private val writePerm = Manifest.permission.WRITE_EXTERNAL_STORAGE

    private lateinit var binding: FragmentHomeworkBinding

    @Inject
    lateinit var presenter: IHomeworkPresenter

    init {
        App.appComponent.inject(this)
    }

    companion object {
        const val HOMEWORK_CODE = 10
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentHomeworkBinding.inflate(inflater, container, false)
        toolbar = binding.lessonHomeworkTb
        initActivity()
        currentFragListener.selectedFragment(this)
        (presenter as HomeworkPresenter).attachView(this)
        reqPerm()


        setListeners()
        presenter.getLessonHomework(lessonID)

        return binding.root
    }

    private fun initActivity() {
        when (activity) {
            is LessonActivity -> {
                lessonID = (activity as LessonActivity).lessonId
                act = activity as LessonActivity
                currentFragListener = act as LessonActivity
            }
            is MainActivity -> {
                lessonID = arguments?.getInt(ID) ?: -1
                act = activity as MainActivity
                currentFragListener = act as MainActivity
            }
        }
    }

    override fun getActiv(): AppCompatActivity = act

    private fun reqPerm() {
        val denied = PackageManager.PERMISSION_DENIED
        if (ContextCompat.checkSelfPermission(act, readPerm) == denied && ContextCompat.checkSelfPermission(
                act,
                writePerm
            ) == denied
        ) {
            ActivityCompat.requestPermissions(act, arrayOf(readPerm, writePerm), HOMEWORK_CODE)
        }
    }

    override fun getLessonID(): Int = lessonID

    override fun scrollToBegin() = binding.docsRecycler.smoothScrollToPosition(0)

    override fun getAdapter(): HomeworkDocsAdapter? = binding.docsRecycler.adapter as HomeworkDocsAdapter?

    override fun addView(view: View) {
        binding.homeworkContainer.addView(view)
    }

    override fun getCtx(): Context = requireContext()

    override fun bindDocs(items: MutableList<Document>, sizeChangeListener: SizeChangeListener) {
        val adapter = HomeworkDocsAdapter(items, this@HomeworkFragment)
        adapter.sizeChangeListener = sizeChangeListener
        binding.docsRecycler.run {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            this.adapter = adapter
        }
        binding.recyclerContainer.visible()
    }

    fun clearDocsAdapter() {
        binding.docsRecycler.adapter = null
        binding.recyclerContainer.gone()
    }

    override fun animateContainer(animation: Animation) = binding.recyclerContainer.startAnimation(animation)

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.getLessonHomework(lessonID)
        }
    }

    override fun setListeners() {
        binding.run {

            sendIv.setOnClickListener {
                presenter.onNextStepBTNClicked(lessonID, sendEt.text.toString(), getAdapter()?.list)
            }
            attachIv.setOnClickListener {
                if (ContextCompat.checkSelfPermission(act, readPerm) == PackageManager.PERMISSION_DENIED)
                    ActivityCompat.requestPermissions(act, arrayOf(readPerm, writePerm), HOMEWORK_CODE)
                else
                    presenter.onAttachmentBTNClicked(this@HomeworkFragment)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun onDestroyView() {
        presenter.cancelJob()
        cancelJobs(GET_HOMEWORK, SEND_HOMEWORK)
        (presenter as HomeworkPresenter).detachView()
        super.onDestroyView()
    }
}