package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view

import android.content.Context
import android.view.View
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import com.bonch_dev.spinit.domain.entity.Document
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.adapter.HomeworkDocsAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.presenter.SizeChangeListener

interface IHomeworkFragment : IBaseView {
    fun bindDocs(items: MutableList<Document>, sizeChangeListener: SizeChangeListener)
    fun animateContainer(animation: Animation)
    fun scrollToBegin()
    fun getCtx(): Context
    fun getAdapter(): HomeworkDocsAdapter?
    fun getActiv(): AppCompatActivity
    fun getLessonID(): Int
    fun addView(view: View)
}