package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter

import android.text.TextWatcher
import android.view.OrientationEventListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener

interface ILectionPresenter {
    fun initOrientationListener(): OrientationEventListener
    fun initTextWatcher(): TextWatcher
    fun onNextStepBTNClicked(lessonId: Int)
    fun initFullScreenListener(): YouTubePlayerFullScreenListener
    fun getLessonVideo(id: Int)
}