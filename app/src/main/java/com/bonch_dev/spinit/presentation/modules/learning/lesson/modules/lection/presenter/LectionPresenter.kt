package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter

import android.app.AlertDialog
import android.content.pm.ActivityInfo
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.OrientationEventListener
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.Video
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.view.ILectionFragment
import com.bonch_dev.spinit.router.LessonRouter
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener
import javax.inject.Inject

class LectionPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    ILectionPresenter, BasePresenter<ILectionFragment>(), IDataCompletion<Video> {

    override fun initFullScreenListener(): YouTubePlayerFullScreenListener {
        val act = getView()?.getLessonActivity()
        return object : YouTubePlayerFullScreenListener {
            override fun onYouTubePlayerEnterFullScreen() {
                act?.run {
                    when (requestedOrientation) {
                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT -> requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE -> requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE -> requestedOrientation =
                            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                    }
                }
                getView()?.run {
                    hideStatusBar()
                    hideTB()
                }
            }

            override fun onYouTubePlayerExitFullScreen() {
                act?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                getView()?.run {
                    showStatusBar()
                    showTB()
                }
            }
        }
    }

    override fun initOrientationListener(): OrientationEventListener {
        return object : OrientationEventListener(getView()?.getCtx()) {
            override fun onOrientationChanged(orientation: Int) {
                when (orientation) {
                    0 -> {
                        getView()?.run {
                            if (getVideoPlayer().isFullScreen()) {
                                getVideoPlayer().exitFullScreen()
                                showTB()
                                showStatusBar()
                            }
                        }
                    }
                    90 -> {
                        getView()?.run {
                            hideStatusBar()
                            getLessonActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                            hideTB()
                            getVideoPlayer().enterFullScreen()
                        }
                    }
                    270 -> {
                        getView()?.run {
                            hideStatusBar()
                            getLessonActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                            hideTB()
                            getVideoPlayer().enterFullScreen()
                        }
                    }
                }
            }
        }
    }

    override fun initTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (s.isNotEmpty()) getView()?.showSendCommentIV()
                    else getView()?.hideSendCommentIV()
                }
            }
        }
    }

    override fun onNextStepBTNClicked(lessonId: Int) {
        interactor.getLessonStatus(
            lessonId,
            result = {
                when {
                    it.test == TestCompletionStatus.NONE -> router.fromLectionToTest()
                    it.test == TestCompletionStatus.FAILED -> showFailDialog(it.time)
                    it.test == TestCompletionStatus.COMPLETED && it.status == HomeworkCompletionStatus.NONE -> router.fromLectionToHomework()
                }
            },
            err = ::onError
        )
    }

    private fun showFailDialog(time: String?) {
        val failLayout = LayoutInflater.from(getView()?.getCtx()).inflate(R.layout.dialog_test_failed, null)
        failLayout.findViewById<TextView>(R.id.time_text).text =
            String.format(ResourceManager.getString(R.string.next_try_will_be_available_in), time)
        val dlg = AlertDialog.Builder(getView()?.getCtx())
            .setView(failLayout)
            .create()
        failLayout.findViewById<Button>(R.id.get_back_btn).setOnClickListener { dlg.dismiss() }
        dlg.show()
    }

    override fun getLessonVideo(id: Int) {
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonVideo(
                id,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: Video) {
        getView()?.run {
            val act = getLessonActivity()
            if (act.testStatus == TestCompletionStatus.COMPLETED && (act.homeworkStatus == HomeworkCompletionStatus.WAITING || act.homeworkStatus == HomeworkCompletionStatus.ACCEPTED))
                hideNextStepBtn()
            initPlayerListener(data.video.split("/").last())
            (this as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }
}