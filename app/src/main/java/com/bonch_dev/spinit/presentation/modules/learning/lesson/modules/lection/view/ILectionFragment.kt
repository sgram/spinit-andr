package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.view

import android.content.Context
import android.view.View
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

interface ILectionFragment : IBaseView {
    fun initPlayerListener(url: String)
    fun getVideoPlayer(): YouTubePlayerView
    fun getCtx(): Context
    fun showTB()
    fun hideTB()
    fun hideStatusBar()
    fun showStatusBar()
    fun showSendCommentIV()
    fun hideSendCommentIV()
    fun getLessonActivity(): LessonActivity
    fun initViews(view: View)
    fun hideNextStepBtn()
}