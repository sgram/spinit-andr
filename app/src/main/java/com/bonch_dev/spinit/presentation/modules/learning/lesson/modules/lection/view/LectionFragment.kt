package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.view

import android.content.Context
import android.content.pm.ActivityInfo
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentLectionBinding
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.GET_LESSON_VIDEO_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_VIDEO
import com.bonch_dev.spinit.domain.utils.Keys.SEND_LESSON_VIDEO_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.CommentsAdapter
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.CommentsPresenter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter.ILectionPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lection.presenter.LectionPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import javax.inject.Inject

class LectionFragment : InternetFragment(),
    ILectionFragment, CommentsAdapter.OnCommentClickListener, RepliesAdapter.OnReplyClickListener, IComment {

    private lateinit var currentFragListener: CurrentFragListener
    private val flag = WindowManager.LayoutParams.FLAG_FULLSCREEN
    private lateinit var mYoutubePlayerFragment: YouTubePlayerView
    private lateinit var playerListener: YouTubePlayerListener
    private lateinit var fullScreenListener: YouTubePlayerFullScreenListener
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var textWatcher: TextWatcher
    private var orListener: OrientationEventListener? = null
    private lateinit var parentLayout: LinearLayout


    private lateinit var binding: FragmentLectionBinding


    @Inject
    lateinit var presenter: ILectionPresenter

    @Inject
    lateinit var commentsPresenter: ICommentsPresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var tb: Toolbar
    lateinit var act: LessonActivity


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentLectionBinding.inflate(inflater, container, false)
        toolbar = binding.lessonLectionTb
        retainInstance = true
        act = activity as LessonActivity
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        attachViews()
        orListener = presenter.initOrientationListener()
        orListener?.enable()
        currentFragListener = act

        textWatcher = presenter.initTextWatcher()
        currentFragListener.selectedFragment(this)
        loadAvatar()
        firstPlayerInit()
        initRecyclerParams()
        initBehaviour()
        setListeners()
        hideConnErr()
        showSplash()
        presenter.getLessonVideo(act.lessonId)
        getComments()

        return binding.root
    }

    override fun getBSBehaviour(): BottomSheetBehavior<View> = bottomSheetBehavior

    private fun attachViews() {
        (presenter as LectionPresenter).attachView(this)
        (commentsPresenter as CommentsPresenter).attachView(this)
    }


    override fun hideNextStepBtn() = binding.lectionDoneBtn.invisible()

    override fun bindReplies(data: List<Comment>, repliesAdapter: RepliesAdapter?) {
        if (repliesAdapter == null)
            getRepliesRV().adapter = RepliesAdapter(data, this)
        else {
            repliesAdapter.list = data
            repliesAdapter.notifyDataSetChanged()
        }
    }

    override fun bindComments(data: List<Comment>) {
        binding.commentContainer.commentsCount.text = data.size.toString()
        val ad = getAdapter()
        if (ad == null) {
            val adapter = CommentsAdapter(data, this)
            getCommentsRV().adapter = adapter
        } else {
            ad.list = data
            ad.notifyDataSetChanged()
        }
    }

    override fun getVideoPlayer(): YouTubePlayerView = mYoutubePlayerFragment

    override fun getAdapter(): CommentsAdapter? = binding.commentContainer.commentRecycler.adapter as CommentsAdapter?

    override fun onReplyClick(viewID: Int?, commentItem: Comment?, repliesAdapter: RepliesAdapter) {
        val commentID: Int? = commentItem?.id
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun openBottomSheet() {
        commentsPresenter.initParentLayout(parentLayout)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {}

            override fun onStateChanged(p0: View, p1: Int) {
                if (p1 == BottomSheetBehavior.STATE_HIDDEN)
                    commentsPresenter.onBottomSheetHidden()
            }
        })
    }

    override fun getRepliesRV(): RecyclerView = binding.replyContainer.repliesRv

    private fun getCommentsRV(): RecyclerView = binding.commentContainer.commentRecycler

    override fun initTbParams() {
        act.run {
            tb.initBackArrow(this)
            binding.replyContainer.repliesToolbar.post { binding.replyContainer.repliesToolbar.initBackArrow(this) }
        }
    }

    override fun onCommentClick(position: Int, viewID: Int?, commentItem: Comment?) {
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentItem?.id, null, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentItem?.id, null, false, null)
            R.id.expand_replies_layout -> commentsPresenter.onShowRepliesBtnClick(commentItem)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun clearET() {
        binding.commentContainer.sendCommentEt.apply {
            clearFocus()
            setText("")
        }
    }

    override fun getComments() = commentsPresenter.getLessonVideoComments(act.lessonId)

    override fun showSendCommentIV() = binding.commentContainer.sendCommentIv.visible()

    override fun getFM(): FragmentManager = childFragmentManager

    override fun hideSendCommentIV() = binding.commentContainer.sendCommentIv.gone()

    override fun getCtx(): Context = requireContext()

    override fun initPlayerListener(url: String) {
        playerListener = object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                youTubePlayer.loadVideo(url, 0f)
            }

            override fun onError(
                youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
                error: PlayerConstants.PlayerError
            ) = showToast(error.name)
        }
        binding.youTubePlayerView.addYouTubePlayerListener(playerListener)
    }

    override fun getLessonActivity(): LessonActivity = act

    override fun hideTB() = binding.lessonLectionTb.gone()

    override fun getBottomSheetState(): Int = bottomSheetBehavior.state

    override fun showTB() = binding.lessonLectionTb.visible()

    override fun initViews(view: View) {
        view.run {
            parentLayout = findViewById(R.id.parent_layout)
            tb = findViewById(R.id.lesson_lection_tb)
            mYoutubePlayerFragment = findViewById(R.id.youTubePlayerView)
        }
    }

    private fun firstPlayerInit() {
        mYoutubePlayerFragment.getPlayerUiController().run {
            showYouTubeButton(false)
            showCustomAction1(false)
            showCustomAction2(false)
            showMenuButton(false)
            showVideoTitle(false)
            enableLiveVideoUi(false)
        }
        fullScreenListener = presenter.initFullScreenListener()
        act.lifecycle.addObserver(binding.youTubePlayerView)
        binding.youTubePlayerView.addFullScreenListener(fullScreenListener)
    }

    private fun initBehaviour() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.replyContainer.root)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun loadAvatar() {
        if (Keys.avatarURL.isNotEmpty())
            Glide.with(this).load(Keys.avatarURL).apply(RequestOptions().circleCrop()).into(binding.commentContainer.userIv)
    }

    private fun initRecyclerParams() {
        getCommentsRV().run {
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = false
        }
        getRepliesRV().run {
            layoutManager = LinearLayoutManager(context)
            isNestedScrollingEnabled = false
        }
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            onStart()
        }
    }

    override fun setListeners() {
        binding.run {

            replyContainer.replyBtn.setOnClickListener {
                commentsPresenter.onReplyCommentBtnClick(null)
            }
            lectionDoneBtn.setOnClickListener {
                presenter.onNextStepBTNClicked(act.lessonId)
            }
            commentContainer.sendCommentEt.addTextChangedListener(textWatcher)
            commentContainer.sendCommentIv.setOnClickListener {
                commentsPresenter.onSendLessonVideoCommentBTNClicked(act.lessonId, commentContainer.sendCommentEt.text.toString())
            }
        }

    }

    override fun hideStatusBar() = act.window.setFlags(flag, flag)

    override fun showStatusBar() = act.window.clearFlags(flag)

    private fun lockPortraitOrientation() {
        act.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_NOSENSOR
    }

    private fun stopPlayer() {
        mYoutubePlayerFragment.run {
            act.lifecycle.removeObserver(binding.youTubePlayerView)
            removeYouTubePlayerListener(playerListener)
            removeFullScreenListener(fullScreenListener)
            release()
        }
    }

    override fun onStop() {
        Log.e("lectionFragment", "stop")
        lockPortraitOrientation()
        cancelJobs(GET_LESSON_VIDEO_COMMENT, SEND_LESSON_VIDEO_COMMENT, GET_VIDEO)
        binding.commentContainer.sendCommentEt.removeTextChangedListener(textWatcher)
        super.onStop()
    }

    override fun onDestroyView() {
        stopPlayer()
        (presenter as LectionPresenter).detachView()
        (commentsPresenter as CommentsPresenter).detachView()
        super.onDestroyView()
    }
}