package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.DrawableRes
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.invisible
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.router.LessonRouter
import javax.inject.Inject

class LessonCategoryAdapter(private val data: HomeworkStatus, private val lessonCategoryListener: LessonCategoryListener) :
    RecyclerView.Adapter<LessonCategoryAdapter.CourseContentViewHolder>() {

    @Inject
    lateinit var router: LessonRouter


    init {
        App.appComponent.inject(this)
    }

    private val list = ResourceManager.getStringArray(R.array.lesson_category_array)

    inner class CourseContentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val courseContentIV: ImageView = view.findViewById(R.id.course_content_iv)
        private val courseContentCard: CardView = view.findViewById(R.id.course_content_card)
        private val courseContentTV: TextView = view.findViewById(R.id.course_content_tv)
        private val courseContentCheck: ImageView = view.findViewById(R.id.course_content_check)
        fun bind(position: Int) {
            val item = list[position]
            courseContentTV.text = item
            when (position) {
                0 -> bindMaterials()
                1 -> bindLection()
                2 -> bindTest()
                3 -> bindHomework()
            }
        }

        private fun lock(@DrawableRes resource: Int) {
            setRes(resource)
            hideCheck()
        }

        private fun unlock(@DrawableRes resource: Int) {
            setRes(resource)
            showCheck()
        }

        private fun showCheck() = courseContentCheck.visible()

        private fun hideCheck() = courseContentCheck.invisible()

        private fun setRes(res: Int) = courseContentIV.setImageResource(res)

        private fun setCheckRes(res: Int) = courseContentCheck.setImageResource(res)

        private fun bindMaterials() {
            Log.e("category", "materials")
            if (data.progress > 0) unlock(R.drawable.ic_materials_unlocked)
            else lock(R.drawable.ic_materials_locked)

            courseContentCard.setOnClickListener {
                router.fromCategoryToMaterials()
            }
        }

        private fun bindLection() {
            Log.e("category", "lection")
            if (data.progress > 1) unlock(R.drawable.ic_lection_unlocked)
            else lock(R.drawable.ic_lection_locked)

            courseContentCard.setOnClickListener {
                router.fromCategoryToLection()
            }
        }

        private fun bindTest() {
            Log.e("category", "test")
            courseContentIV.translationX = 15f
            when {
                data.progress > 2 && data.test == TestCompletionStatus.COMPLETED -> {
                    unlock(R.drawable.ic_test_unlocked)
                }
                data.progress >= 2 && data.test == TestCompletionStatus.FAILED-> {
                    courseContentIV.setImageResource(R.drawable.ic_test_unlocked)
                    courseContentCheck.setImageResource(R.drawable.ic_lesson_homework_not_accepted)
                }
                else -> lock(R.drawable.ic_test_locked)
            }

            courseContentCard.setOnClickListener { lessonCategoryListener.onTestIconClick() }
        }

        private fun setHomeworkStatus() {
            when (data.status) {
                HomeworkCompletionStatus.NONE -> {
                    lock(R.drawable.ic_homework_locked)
                    courseContentCard.setOnClickListener { router.fromCategoryToHomework() }
                }
                HomeworkCompletionStatus.WAITING -> {
                    setRes(R.drawable.ic_homework_unlocked)
                    setCheckRes(R.drawable.ic_lesson_homework_wait)
                    courseContentCard.setOnClickListener { showToast(ResourceManager.getString(R.string.homework_is_pending)) }
                }
                HomeworkCompletionStatus.ACCEPTED -> {
                    setRes(R.drawable.ic_homework_unlocked)
                    setCheckRes(R.drawable.ic_arrow_done)
                    courseContentCard.setOnClickListener { showToast(ResourceManager.getString(R.string.homework_is_accepted)) }
                }
                HomeworkCompletionStatus.NOT_ACCEPTED -> {
                    setRes(R.drawable.ic_homework_unlocked)
                    setCheckRes(R.drawable.ic_lesson_homework_not_accepted)
                    courseContentCard.setOnClickListener { router.fromCategoryToHomework() }
                }
            }
        }

        private fun bindHomework() {
            Log.e("category", "homework")
            when (data.progress) {
                4 -> setHomeworkStatus()
                3 -> {
                    lock(R.drawable.ic_homework_locked)
                    courseContentCard.setOnClickListener { router.fromCategoryToHomework() }
                }
                in 0..2 -> {
                    lock(R.drawable.ic_homework_locked)
                    courseContentCard.setOnClickListener { showToast("Вы не прошли тест") }
                }
            }
        }
    }

    private fun showToast(msg: String) = Toast.makeText(App.applicationContext(), msg, Toast.LENGTH_SHORT).show()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseContentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_course_content, parent, false)
        view.viewTreeObserver.addOnGlobalLayoutListener {
            val lp = view.layoutParams
            lp.height = lp.width
            view.layoutParams = lp
        }
        return CourseContentViewHolder(view)
    }

    override fun getItemCount(): Int = list?.size ?: 0

    override fun onBindViewHolder(holder: CourseContentViewHolder, position: Int) = holder.bind(position)

    interface LessonCategoryListener {
        fun onTestIconClick()
    }
}
