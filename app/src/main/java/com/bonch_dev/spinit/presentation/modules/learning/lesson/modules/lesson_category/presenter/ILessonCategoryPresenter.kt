package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter

interface ILessonCategoryPresenter {
    fun getStatus(id: Int)
    fun onStart()
}