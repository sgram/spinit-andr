package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.Button
import android.widget.TextView
import androidx.annotation.AnimRes
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.adapters.LessonCategoryAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.view.ILessonCategoryFragment
import com.bonch_dev.spinit.router.LessonRouter
import javax.inject.Inject

class LessonCategoryPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    ILessonCategoryPresenter, BasePresenter<ILessonCategoryFragment>(), IDataCompletion<HomeworkStatus>,
    LessonCategoryAdapter.LessonCategoryListener {

    override fun onStart() {
        getView()?.let {
            (it as InternetFragment).showSplash()
            (it as InternetFragment).hideConnErr()
            getStatus(it.getLessonId())
        }
    }

    override fun onTestIconClick() {
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonStatus(
                getView()?.getLessonId() ?: -1,
                result = ::onCheckTestAvailableResult,
                err = ::onError
            )
        else onError()
    }


    private fun onCheckTestAvailableResult(status: HomeworkStatus) {
        when (status.test) {
            TestCompletionStatus.NONE -> router.fromCategoryToTest()
            TestCompletionStatus.COMPLETED -> showSuccessDialog()
            TestCompletionStatus.FAILED -> showFailDialog(status.time)
        }
    }


    private fun showFailDialog(time: String?) {
        val failLayout = LayoutInflater.from(getView()?.getCtx()).inflate(R.layout.dialog_test_failed, null)
        failLayout.findViewById<TextView>(R.id.time_text).text =
            String.format(ResourceManager.getString(R.string.next_try_will_be_available_in), time)
        val dlg = AlertDialog.Builder(getView()?.getCtx())
            .setView(failLayout)
            .create()
        failLayout.findViewById<Button>(R.id.get_back_btn).setOnClickListener { dlg.dismiss() }
        dlg.show()
    }

    private fun showSuccessDialog() {
        val completedLayout = LayoutInflater.from(getView()?.getCtx()).inflate(R.layout.dialog_test_completed, null)
        val dlg = AlertDialog.Builder(getView()?.getCtx())
            .setView(completedLayout)
            .create()
        completedLayout.findViewById<Button>(R.id.ok_btn).setOnClickListener { dlg.dismiss() }
        dlg.show()
    }

    override fun getStatus(id: Int) {
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonStatus(
                id,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: HomeworkStatus) {
        getView()?.run {
            setHomeworkStatus(data.status, data.test)
            @AnimRes val resId: Int = R.anim.fall_down_layout_animation
            val animation: LayoutAnimationController = AnimationUtils.loadLayoutAnimation(getCtx(), resId)
            getRV().run {
                layoutAnimation = animation
                scheduleLayoutAnimation()
                adapter = LessonCategoryAdapter(data, this@LessonCategoryPresenter)
            }
            (getView() as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }
}