package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.view

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

interface ILessonCategoryFragment : IBaseView {
    fun getCtx(): Context
    fun getRV(): RecyclerView
    fun getLessonId(): Int
    fun setHomeworkStatus(status: HomeworkCompletionStatus, test: TestCompletionStatus)
}