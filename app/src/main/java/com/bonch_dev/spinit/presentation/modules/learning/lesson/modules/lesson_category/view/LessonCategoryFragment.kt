package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentLessonCategoryBinding
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.entity.response.lesson.TestCompletionStatus
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.Keys.GET_HOMEWORK_STATUS
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.initBackArrow
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter.ILessonCategoryPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_category.presenter.LessonCategoryPresenter
import javax.inject.Inject

class LessonCategoryFragment : InternetFragment(), ILessonCategoryFragment {

    private lateinit var act: LessonActivity
    private lateinit var currentFragListener: CurrentFragListener

    private lateinit var binding: FragmentLessonCategoryBinding

    @Inject
    lateinit var presenter: ILessonCategoryPresenter

    init {
        App.appComponent.inject(this)
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentLessonCategoryBinding.inflate(inflater, container, false)
        act = (activity as LessonActivity)
        currentFragListener = act
        currentFragListener.selectedFragment(this)
        (presenter as LessonCategoryPresenter).attachView(this)
        presenter.onStart()
        setListeners()
        return binding.root
    }

    override fun setHomeworkStatus(status: HomeworkCompletionStatus, test: TestCompletionStatus) {
        act.homeworkStatus = status
        act.testStatus = test
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.onStart()
        }
    }

    override fun getLessonId(): Int = act.lessonId

    override fun getRV(): RecyclerView = binding.recycler

    override fun setListeners() {}

    override fun getCtx(): Context = requireContext()

    override fun initTbParams() {
        binding.lessonCategoryTb.title = arguments?.getString("title")
        binding.lessonCategoryTb.initBackArrow(act)
    }

    override fun onDestroyView() {
        cancelJobs(GET_HOMEWORK_STATUS)
        (presenter as LessonCategoryPresenter).detachView()
        super.onDestroyView()
    }
}
