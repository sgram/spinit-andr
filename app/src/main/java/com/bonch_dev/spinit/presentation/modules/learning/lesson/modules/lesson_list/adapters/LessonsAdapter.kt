package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.adapters

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.Lesson
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.domain.utils.gone

class LessonsAdapter(private val statusData: StatusData, private val mOnLessonClick: OnLessonClickListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val lessonsList = statusData.lessons

    private val ctx = App.applicationContext()

    private val lessonItemType = 1
    private val lastItemType = 2

    interface OnLessonClickListener {
        fun onLessonClick(
            completed: Boolean?,
            currentLessonNumber: Int?,
            currentLessonProgress: Int?,
            item: Lesson?
        )
    }

    init {
        App.appComponent.inject(this)
    }

    inner class LessonLastElementViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tv: TextView = view.findViewById(R.id.final_tv)
        fun bind(position: Int) {
            tv.text = lessonsList?.get(position)?.title
        }
    }

    inner class LessonViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        private val tv: TextView = view.findViewById(R.id.single_tv)
        private val iv: ImageView = view.findViewById(R.id.single_iv)
        private val cardView: CardView = view.findViewById(R.id.single_card)
        private val progressBar: ProgressBar = view.findViewById(R.id.single_pb)
        private val pbTV: TextView = view.findViewById(R.id.single_pb_tv)

        fun bind(position: Int) {
            val item = lessonsList?.get(position)
            val isCompleted = item?.status?.completed ?: false
            val currentLessonNumber = statusData.current_lesson?.lesson_number ?: -1
            val currentLessonProgress = statusData.current_lesson?.progress

            cardView.setOnClickListener(this)

            when {
                item?.number == currentLessonNumber -> {
                    pbTV.text = "$currentLessonProgress/4"
                    progressBar.progress = currentLessonProgress ?: 0
                    cardView.setCardBackgroundColor(Color.parseColor("#424242"))
                    Glide.with(ctx).load(item.mobile_img).into(iv)
                    iv.setColorFilter(Color.WHITE)
                }
                item?.number ?: 1 > currentLessonNumber -> {
                    hideProgressBar()
                    cardView.setCardBackgroundColor(Color.parseColor("#E0E0E0"))
                    Glide.with(ctx).load(item?.mobile_img).into(iv)
                }
                isCompleted -> {
                    cardView.setCardBackgroundColor(Color.parseColor("#2D8B00"))
                    hideProgressBar()
                    Glide.with(ctx).load(item?.mobile_img).into(iv)
                    iv.setColorFilter(Color.WHITE)
                }
            }

            tv.text = lessonsList?.get(position)?.title
        }

        private fun hideProgressBar() {
            progressBar.gone()
            pbTV.gone()
        }

        override fun onClick(v: View?) {
            mOnLessonClick.onLessonClick(
                lessonsList?.get(adapterPosition)?.status?.completed,
                statusData.current_lesson?.lesson_number,
                statusData.current_lesson?.progress,
                lessonsList?.get(adapterPosition)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            lessonItemType -> LessonViewHolder(inflater.inflate(R.layout.item_lesson_single, parent, false))

            lastItemType -> LessonLastElementViewHolder(inflater.inflate(R.layout.item_lesson_last_element, parent, false))

            else -> throw IllegalArgumentException("No such view type")

        }
    }

    override fun getItemCount(): Int = statusData.lessons?.size ?: 0

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LessonViewHolder)
            (holder as LessonViewHolder).bind(position)
        else if (holder is LessonLastElementViewHolder)
            (holder as LessonLastElementViewHolder).bind(position)
    }

    override fun getItemViewType(position: Int): Int {
        val size = lessonsList?.size ?: 0
        return if (position == size - 1) lastItemType
        else lessonItemType
    }
}
