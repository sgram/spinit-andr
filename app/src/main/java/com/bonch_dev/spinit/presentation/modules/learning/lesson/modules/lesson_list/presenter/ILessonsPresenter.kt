package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter

import com.bonch_dev.spinit.domain.entity.response.lesson.Lesson

interface ILessonsPresenter {
    fun onStart(courseId: Int)
    fun getStatus(id: Int)
    fun onLessonClicked(
        isCompleted: Boolean?,
        currentLessonNumber: Int?,
        currentLessonProgress: Int?,
        item: Lesson?
    )
}