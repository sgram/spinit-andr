package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter

import android.app.AlertDialog
import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.Lesson
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.view.ILessonsFragment
import com.bonch_dev.spinit.router.LessonRouter
import javax.inject.Inject

class LessonsPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    ILessonsPresenter, BasePresenter<ILessonsFragment>(), IDataCompletion<StatusData> {

    override fun onStart(courseId: Int) {
        getView()?.run {
            (this as InternetFragment).hideConnErr()
            (this as InternetFragment).showSplash()
            getStatus(courseId)
        }
    }

    override fun getStatus(id: Int) {
        if (NetworkConnection.isInternetAvailable())
            interactor.getStatus(
                id,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    private fun initLayoutManager(lessons: Int) {
        val manager = GridLayoutManager(getView()?.getCtx(), 2).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == lessons - 1 || position == 0 || (lessons % 2 != 0)) 2
                    else 1
                }
            }
        }
        getView()?.getRV()?.layoutManager = manager
    }

    override fun onResult(data: StatusData) {
        getView()?.bindViews(data)
        initLayoutManager(data.lessons?.size ?: 0 + 1)
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onLessonClicked(
        isCompleted: Boolean?,
        currentLessonNumber: Int?,
        currentLessonProgress: Int?,
        item: Lesson?
    ) {
        val ctx = App.applicationContext()
        val lockedDialog =
            AlertDialog.Builder(getView()?.getCtx()).setTitle(ctx.getString(R.string.theme_unavailable))
                .setMessage(ctx.getString(R.string.to_get_access_to_theme))
                .setPositiveButton(ctx.getString(R.string.OK)) { dialog, _ -> dialog.dismiss() }.create()

        when {
            currentLessonNumber != null && isCompleted != null && item?.number != null && !isCompleted && item.number > currentLessonNumber -> {
                lockedDialog.show()
                val alertMSG = lockedDialog.findViewById<TextView>(android.R.id.message)
                alertMSG.textSize = 14F
                alertMSG.setTextColor(Color.parseColor("#666666"))
            }
            item?.number == currentLessonNumber -> {
                val bundle = Bundle()
                bundle.run {
                    putString("title", item?.title)
                    putInt("lesson_id", item?.id ?: -1)
                }
                getView()?.getLessonAct()?.lessonId = item?.id ?: -1
                router.fromLessonsToCategory(bundle)
            }
        }
    }
}