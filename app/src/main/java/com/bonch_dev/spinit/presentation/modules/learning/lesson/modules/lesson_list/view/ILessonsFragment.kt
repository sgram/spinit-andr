package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.view

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity

interface ILessonsFragment : IBaseView {
    fun getLessonAct(): LessonActivity
    fun getCtx(): Context
    fun bindViews(data: StatusData)
    fun getRV(): RecyclerView
}