package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.annotation.AnimRes
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentLessonsBinding
import com.bonch_dev.spinit.domain.entity.response.lesson.Lesson
import com.bonch_dev.spinit.domain.entity.response.lesson.StatusData
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.adapters.LessonsAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter.ILessonsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.lesson_list.presenter.LessonsPresenter
import javax.inject.Inject

class LessonsFragment : InternetFragment(), ILessonsFragment, LessonsAdapter.OnLessonClickListener {

    private lateinit var act: LessonActivity

    private lateinit var binding: FragmentLessonsBinding

    @Inject
    lateinit var presenter: ILessonsPresenter

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentLessonsBinding.inflate(inflater, container, false)
        toolbar=binding.lessonsTb

        (presenter as LessonsPresenter).attachView(this)
        act = activity as LessonActivity

        val backHandle = act
        backHandle.selectedFragment(this)

        setListeners()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        presenter.onStart(act.id)

    }


    override fun getLessonAct(): LessonActivity = act

    override fun getCtx(): Context = requireContext()


    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.onStart(act.id)
        }
    }

    override fun setListeners() {}

    override fun bindViews(data: StatusData) {
        val lastItem = Lesson(null, null, ResourceManager.getString(R.string.final_project_development), null, null)

        val list = data.lessons
        list?.add(lastItem)

        val statusData = StatusData(
            id = data.id,
            title = data.title,
            type = data.type,
            current_lesson = data.current_lesson,
            lessons = list
        )

        binding.lessonsTb.title = "Курс по ${data.title}"

        binding.recycler.run {
            isNestedScrollingEnabled = false
            layoutAnimation = initFallDownAnim()
            scheduleLayoutAnimation()
            adapter = LessonsAdapter(statusData, this@LessonsFragment)
        }

        hideSplash()
    }

    private fun initFallDownAnim(): LayoutAnimationController? {
        @AnimRes val resId: Int = R.anim.fall_down_layout_animation
        return AnimationUtils.loadLayoutAnimation(context, resId)
    }

    override fun getRV(): RecyclerView = binding.recycler

    override fun onLessonClick(completed: Boolean?, currentLessonNumber: Int?, currentLessonProgress: Int?, item: Lesson?) {
        presenter.onLessonClicked(completed, currentLessonNumber, currentLessonProgress, item)
    }

    override fun onDestroyView() {
        Log.e("lifecycle", "ondestroyview")
        (presenter as LessonsPresenter).detachView()
        super.onDestroyView()
    }
}