package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter

import android.text.TextWatcher
import com.bonch_dev.spinit.domain.entity.response.lesson.LessonText

interface IMaterialsPresenter {
    fun initTextWatcher(): TextWatcher
    fun createMaterials(html: LessonText)
    fun onNextStepBTNClicked()
    fun getLessonText(id: Int)
}