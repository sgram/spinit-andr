package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter

import android.os.Build
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bonch_dev.spinit.domain.entity.response.lesson.LessonText
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.view.IMaterialsFragment
import com.bonch_dev.spinit.router.LessonRouter
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import javax.inject.Inject

class MaterialsPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    IMaterialsPresenter, BasePresenter<IMaterialsFragment>(), IDataCompletion<LessonText> {

    override fun onNextStepBTNClicked() = router.fromMaterialsToLection()

    override fun initTextWatcher(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let {
                    if (it.isNotEmpty()) getView()?.showSendCommentIV()
                    else getView()?.hideSendCommentIV()
                }
            }
        }
    }

    override fun getLessonText(id: Int) {
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonMaterials(
                id,
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: LessonText) {
        createMaterials(data)
        (getView() as InternetFragment).hideSplash()
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).showConnErr()
            (this as InternetFragment).   hideSplash()
            (this as Fragment).showToast(msg)
        }
    }

    override fun createMaterials(html: LessonText) {
        val doc = Jsoup.parse(html.text)
        for (el in doc.allElements) {
            when (el.tagName()) {
                "h1" -> bindH1(el)
                "h2" -> bindH2(el)
                "h3" -> bindH3(el)
                "h4" -> bindH4(el)
                "h5" -> bindH5(el)
                "h6" -> bindH6(el)
                "img" -> bindIMG(el)
                "p" -> bindP(el)
                "ul" -> bindUL(el)
                "div" -> bindDiv(el)
            }
        }
    }

    private fun initLP(top: Int, bottom: Int, left: Int, right: Int): LinearLayout.LayoutParams? {
        return LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
            topMargin = top
            bottomMargin = bottom
            leftMargin = left
            rightMargin = right
        }
    }

    @Suppress("DEPRECATION")
    private fun getHTML(text: String): Spanned {
        return if (Build.VERSION.SDK_INT >= 24)
            HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_COMPACT)
        else Html.fromHtml(text)
    }

    private fun bindH1(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h1TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h1TextView)
    }

    private fun bindH2(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h2TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h2TextView)
    }

    private fun bindH3(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h3TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h3TextView)
    }

    private fun bindH4(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h4TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h4TextView)
    }

    private fun bindH5(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h5TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h5TextView)
    }

    private fun bindH6(el: Element) {
        val lp = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val h6TextView = TextView(getView()?.getCtx()).apply {
            text = getHTML(el.toString())
            layoutParams = lp
        }
        getView()?.addView(h6TextView)
    }

    private fun bindIMG(el: Element) {
        val image = el.getElementsByTag("img")
        val src = image.attr("src")
        val imageView = ImageView(getView()?.getCtx())
        val imageLP = initLP(top = 0, bottom = 0, left = 0, right = 0)
        imageView.layoutParams = imageLP
        getView()?.getCtx()?.let { Glide.with(it).load(src).into(imageView) }
        getView()?.addView(imageView)
    }

    private fun bindP(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindUL(el: Element) {
        val pLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val pTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = pLP
            text = getHTML(el.toString())
        }
        getView()?.addView(pTextView)
    }

    private fun bindDiv(el: Element) {
        val divLP = initLP(top = 0, bottom = 0, left = 38, right = 38)
        val divTextView = TextView(getView()?.getCtx()).apply {
            layoutParams = divLP
            text = getHTML(el.toString())
        }
        getView()?.addView(divTextView)
    }
}
