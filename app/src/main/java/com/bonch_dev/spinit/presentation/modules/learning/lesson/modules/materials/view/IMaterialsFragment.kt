package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.view

import android.content.Context
import android.view.View
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

interface IMaterialsFragment : IBaseView {
    fun showSendCommentIV()
    fun hideSendCommentIV()
    fun getCtx(): Context
    fun addView(view: View)
}