package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.view

import android.content.Context
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentMaterialsBinding
import com.bonch_dev.spinit.domain.entity.response.article.Comment
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.Keys.GET_LESSON_TEXT_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.GET_TEXT
import com.bonch_dev.spinit.domain.utils.Keys.SEND_LESSON_TEXT_COMMENT
import com.bonch_dev.spinit.domain.utils.Keys.avatarURL
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.initBackArrow
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.comments.IComment
import com.bonch_dev.spinit.presentation.modules.comments.adapter.CommentsAdapter
import com.bonch_dev.spinit.presentation.modules.comments.adapter.RepliesAdapter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.CommentsPresenter
import com.bonch_dev.spinit.presentation.modules.comments.presenter.ICommentsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter.IMaterialsPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.materials.presenter.MaterialsPresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import javax.inject.Inject

class MaterialsFragment : InternetFragment(),
    IMaterialsFragment, CommentsAdapter.OnCommentClickListener, RepliesAdapter.OnReplyClickListener, IComment {

    private lateinit var currentFragListener: CurrentFragListener
    private lateinit var textWatcher: TextWatcher
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private var lessonID: Int = -1
    private lateinit var act: LessonActivity
    private lateinit var parentLayout: LinearLayout

    private lateinit var binding: FragmentMaterialsBinding

    @Inject
    lateinit var presenter: IMaterialsPresenter

    @Inject
    lateinit var commentsPresenter: ICommentsPresenter

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentMaterialsBinding.inflate(inflater, container, false)
        toolbar = binding.lessonMaterialsTb
        act = activity as LessonActivity

        currentFragListener = act
        lessonID = act.lessonId

        (commentsPresenter as CommentsPresenter).attachView(this)
        (presenter as MaterialsPresenter).attachView(this)

        textWatcher = presenter.initTextWatcher()

        loadAvatar()
        initBehaviour()
        initRecyclerParams()
        setListeners()

        return binding.root
    }

    override fun getBSBehaviour(): BottomSheetBehavior<View> = bottomSheetBehavior

    override fun getBottomSheetState(): Int = bottomSheetBehavior.state


    override fun onReplyClick(viewID: Int?, commentItem: Comment?, repliesAdapter: RepliesAdapter) {
        val commentID: Int? = commentItem?.id
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentID, repliesAdapter, false, null)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun onStart() {
        super.onStart()
        hideConnErr()
        showSplash()
        getComments()
        presenter.getLessonText(lessonID)
        currentFragListener.selectedFragment(this)
    }

    override fun getComments() {
        commentsPresenter.getLessonTextComments(lessonID)
    }

    override fun getCtx(): Context = requireContext()

    override fun addView(view: View) = binding.materialsContainer.addView(view)

    override fun getAdapter(): CommentsAdapter? = getCommentsRV().adapter as CommentsAdapter?

    override fun bindComments(data: List<Comment>) {
        binding.commentContainer.commentsCount.text = data.size.toString()
        val ad = getAdapter()
        if (ad == null) {
            val adapter = CommentsAdapter(data, this)
            getCommentsRV().adapter = adapter
        } else {
            ad.list = data
            ad.notifyDataSetChanged()
        }
    }

    override fun bindReplies(data: List<Comment>, repliesAdapter: RepliesAdapter?) {
        if (repliesAdapter == null) getRepliesRV().adapter = RepliesAdapter(data, this)
        else {
            repliesAdapter.list = data
            repliesAdapter.notifyDataSetChanged()
        }

    }

    override fun initTbParams() {
        act.run {
            binding.lessonMaterialsTb.initBackArrow(this)
            binding.replyContainer.repliesToolbar.post { binding.replyContainer.repliesToolbar.initBackArrow(this) }
        }
    }

    override fun openBottomSheet() {
        commentsPresenter.initParentLayout(parentLayout)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    override fun onCommentClick(position: Int, viewID: Int?, commentItem: Comment?) {
        val commentID: Int? = commentItem?.id
        when (viewID) {
            R.id.comment_more_info_icon -> commentsPresenter.onMoreInfoClick(commentItem, null, requireContext())
            R.id.like_layout -> commentsPresenter.onLikeBtnClick(commentID, null, false, null)
            R.id.unlike_layout -> commentsPresenter.onDislikeBtnClick(commentID, null, false, null)
            R.id.expand_replies_layout -> commentsPresenter.onShowRepliesBtnClick(commentItem)
            R.id.reply_btn -> commentsPresenter.onReplyCommentBtnClick(commentItem)
        }
    }

    override fun getFM(): FragmentManager = childFragmentManager

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            onStart()
        }
    }

    override fun setListeners() {

        binding.materialsDoneBtn.setOnClickListener {
            presenter.onNextStepBTNClicked()
        }

        binding.replyContainer.replyBtn.setOnClickListener {
            commentsPresenter.onReplyCommentBtnClick(null)
        }

        binding.commentContainer.run {
            sendCommentEt.addTextChangedListener(textWatcher)

            sendCommentIv.setOnClickListener {
                commentsPresenter.onSendLessonTextCommentBTNClicked(lessonID, sendCommentEt.text.toString())
            }
        }

    }

    override fun clearET() {
        binding.commentContainer.sendCommentEt.run {
            clearFocus()
            setText("")
        }
    }

    override fun getRepliesRV(): RecyclerView = binding.replyContainer.repliesRv

    private fun initBehaviour() {
        bottomSheetBehavior = BottomSheetBehavior.from(binding.replyContainer.root)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(p0: View, p1: Float) {}

            override fun onStateChanged(p0: View, p1: Int) {
                if (p1 == BottomSheetBehavior.STATE_HIDDEN)
                    commentsPresenter.onBottomSheetHidden()
            }
        })
    }

    private fun loadAvatar() {
        if (avatarURL.isNotEmpty())
            Glide.with(this).load(avatarURL).apply(RequestOptions().circleCrop()).into(binding.commentContainer.userIv)
    }

    private fun initRecyclerParams() {

        getCommentsRV().run {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }
        getRepliesRV().run {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }
    }

    private fun getCommentsRV(): RecyclerView = binding.commentContainer.commentRecycler

    override fun showSendCommentIV() = binding.commentContainer.sendCommentIv.visible()

    override fun hideSendCommentIV() = binding.commentContainer.sendCommentIv.gone()

    override fun onStop() {
        cancelJobs(GET_LESSON_TEXT_COMMENT, SEND_LESSON_TEXT_COMMENT, GET_TEXT)
        binding.commentContainer.sendCommentEt.removeTextChangedListener(textWatcher)
        super.onStop()
    }

    override fun onDestroyView() {
        (presenter as MaterialsPresenter).detachView()
        (commentsPresenter as CommentsPresenter).detachView()
        super.onDestroyView()
    }
}