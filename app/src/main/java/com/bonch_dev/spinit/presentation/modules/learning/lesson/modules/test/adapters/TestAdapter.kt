package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.adapters

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.TestData
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.view.TestFragment
import kotlin.collections.set

class TestAdapter(val testItems: List<TestData>?, val testFragment: TestFragment) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_SELECT = 3
    private val TYPE_CHOOSE = 2
    private val TYPE_INPUT = 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            1 -> InputViewHolder(inflater.inflate(R.layout.fragment_test_question, parent, false))
            2 -> ChooseViewHolder(inflater.inflate(R.layout.fragment_test_quiz, parent, false))
            3 -> SelectViewHolder(inflater.inflate(R.layout.fragment_test_select, parent, false))
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun getItemCount(): Int {
        testFragment.testItemCount = testItems?.size ?: 0
        return testItems?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            TYPE_INPUT -> (holder as InputViewHolder).bind(position)
            TYPE_CHOOSE -> (holder as ChooseViewHolder).bind(position)
            TYPE_SELECT -> (holder as SelectViewHolder).bind(position)
        }
    }

    inner class SelectViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val selectTitle: TextView = view.findViewById(R.id.select_title)
        private val cbLayout: LinearLayout = view.findViewById(R.id.check_box_layout)
        fun bind(position: Int) {

            itemView.setOnClickListener{
                hideKB()
            }

            val checked = mutableListOf<Int>()
            val item = testItems?.get(position)
            selectTitle.text = item?.title
            val taskID = item?.id ?: -1
            val options = item?.boxes
            options?.forEach {

                val cb = CheckBox(testFragment.context).apply {
                    text = it.title
                    id = it.id ?: -1
                }

                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    .apply { bottomMargin = 24 }
                cbLayout.addView(cb, lp)
                cb.setOnCheckedChangeListener { v, isChecked ->
                    hideKB()

                    if (isChecked)
                        checked.add(v.id)
                    else
                        checked.remove(v.id)

                    if (checked.isNullOrEmpty())
                        testFragment.testCheckedItems.remove(taskID)
                    else {
                        checked.sort()
                        testFragment.testCheckedItems[taskID] = checked
                    }

                    Log.e("map", testFragment.testCheckedItems.toString())
                }
            }
        }
    }

    private fun hideKB(){
        (testFragment as InternetFragment).hideKB()
    }

    inner class InputViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val inputTV: TextView = view.findViewById(R.id.input_title)
        private val answerET: EditText = view.findViewById(R.id.answer_et)
        fun bind(position: Int) {
            answerET.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    val testItem = testItems?.get(position)?.id
                    testItem?.let {
                        testFragment.testCheckedItems[it] = answerET.text.toString()
                    }
                    if (answerET.text.toString().isNullOrEmpty()) {
                        testItems?.get(position)?.id?.let {
                            testFragment.testCheckedItems.remove(it)
                        }
                    }
                    Log.e("map", testFragment.testCheckedItems.toString())
                }
            })
            inputTV.text = testItems?.get(position)?.title
        }
    }

    inner class ChooseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val chooseTitle: TextView = view.findViewById(R.id.choose_title)
        private val rg: RadioGroup = view.findViewById(R.id.choose_rg)
        fun bind(position: Int) {

            itemView.setOnClickListener{
                hideKB()
            }

            val item = testItems?.get(position)
            val taskID = item?.id ?: -1
            chooseTitle.text = item?.title
            val options = item?.boxes
            options?.forEach {
                val rb = RadioButton(testFragment.context).apply {
                    text = it.title
                    id = it.id ?: -1
                }
                val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                    .apply { bottomMargin = 24 }
                rg.addView(rb, lp)
                rg.setOnCheckedChangeListener { group, checkedId ->
                    hideKB()
                    testFragment.testCheckedItems[taskID] = checkedId
                    Log.e("map", testFragment.testCheckedItems.toString())
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (testItems?.get(position)?.type) {
            1 -> TYPE_INPUT
            2 -> TYPE_CHOOSE
            3 -> TYPE_SELECT
            else -> throw IllegalArgumentException("Invalid viewType")
        }
    }
}
