package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter

interface ITestPresenter {
    fun onNextStepBTNClicked(lessonID: Int)
    fun getLessonTest(id: Int)
    fun onSkippedBTNClicked()
}