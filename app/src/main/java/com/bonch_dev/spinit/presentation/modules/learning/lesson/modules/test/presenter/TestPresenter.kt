package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter

import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.lesson.LessonTest
import com.bonch_dev.spinit.domain.entity.response.lesson.SendTest
import com.bonch_dev.spinit.domain.entity.response.lesson.Task
import com.bonch_dev.spinit.domain.interactors.lesson.ILessonInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.DataHandler
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.view.ITestFragment
import com.bonch_dev.spinit.router.LessonRouter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TestPresenter @Inject constructor(private val interactor: ILessonInteractor, private val router: LessonRouter) :
    ITestPresenter, BasePresenter<ITestFragment>(), IDataCompletion<LessonTest> {

    private fun showFailDialog() {
        val failLayout = LayoutInflater.from(getView()?.getLessonActivity()).inflate(R.layout.dialog_test_failed, null)
        val dlg = AlertDialog.Builder(getView()?.getLessonActivity())
            .setCancelable(false)
            .setView(failLayout)
            .create()
        failLayout.findViewById<TextView>(R.id.time_text).text =
            String.format(ResourceManager.getString(R.string.next_try_will_be_available_in), "24 часа")
        failLayout.findViewById<Button>(R.id.get_back_btn).setOnClickListener {
            router.controller?.popBackStack(R.id.lessonCategoryFragment, false)
            dlg.dismiss()
        }
        dlg.show()
    }

    private fun showSuccessDialog() {
        val completedLayout = LayoutInflater.from(getView()?.getLessonActivity()).inflate(R.layout.dialog_test_completed, null)
        val dlg = AlertDialog.Builder(getView()?.getLessonActivity())
            .setCancelable(false)
            .setView(completedLayout)
            .create()
        completedLayout.findViewById<Button>(R.id.ok_btn).setOnClickListener {
            router.fromSuccessTestToHomework()
            dlg.dismiss()
        }
        dlg.show()
    }

    private suspend fun handleTest(listToSend: DataHandler<SendTest?>) {
        val checkedItems = getView()?.getCheckedItems()
        checkedItems?.forEach {
            Log.e("it value", it.toString())
            when (it.value) {
                is String -> {
                    Log.e("type", "String")
                    val inputString = it.value as String
                    if (inputString.isNotEmpty()) getView()?.addTask(Task(it.key, inputString))
                }
                is Int -> {
                    Log.e("type", "Int")
                    val rbId = it.value as Int
                    getView()?.addTask(Task(it.key, rbId))
                }
                is List<*> -> {
                    Log.e("type", "List")
                    val list = it.value as List<Int>
                    if (list.isNotEmpty()) getView()?.addTask(Task(it.key, list))
                }
            }
        }

        val checkedItemsCount = getView()?.getCheckedItems()?.size ?: -1
        val allTestItems = getView()?.getTestItemsCount() ?: 0
        withContext(Dispatchers.Main) {
            if (checkedItemsCount < allTestItems) (getView() as Fragment).showToast(
                App.applicationContext().getString(R.string.empty_field_error)
            )
            else listToSend(getView()?.getListToSend())
        }
    }

    override fun onNextStepBTNClicked(lessonID: Int) {
        getView()?.hideKB()
        CoroutineScope(Dispatchers.IO).launch {
            handleTest(listToSend = { list ->
                if (list != null) {
                    interactor.sendTest(
                        list, lessonID,
                        result = { data, correctCount ->
                            if (data == correctCount) showSuccessDialog()
                            else showFailDialog()
                        },
                        err = { (getView() as Fragment).showToast(it) })
                }
            })
        }
    }

    override fun getLessonTest(id: Int) {
        if (NetworkConnection.isInternetAvailable())
            interactor.getLessonTest(
                id,
                skip = {},
                notComplete = {},
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onResult(data: LessonTest) {
        getView()?.run {
            bindTest(data.test)
            (this as InternetFragment).hideSplash()
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun onSkippedBTNClicked() = router.fromCategoryToHomework()

}