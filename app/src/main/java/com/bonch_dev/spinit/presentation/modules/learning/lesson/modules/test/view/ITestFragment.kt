package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.view

import com.bonch_dev.spinit.domain.entity.response.lesson.SendTest
import com.bonch_dev.spinit.domain.entity.response.lesson.Task
import com.bonch_dev.spinit.domain.entity.response.lesson.TestData
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity

interface ITestFragment : IBaseView {
    fun bindTest(list: List<TestData>)
    fun hideKB()
    fun getListToSend(): SendTest
    fun getTestItemsCount(): Int
    fun getLessonActivity(): LessonActivity
    fun addTask(task: Task)
    fun getCheckedItems(): HashMap<Int, Any>
}