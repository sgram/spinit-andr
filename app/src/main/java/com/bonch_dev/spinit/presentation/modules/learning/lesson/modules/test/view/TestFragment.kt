package com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.view

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentTestBinding
import com.bonch_dev.spinit.domain.entity.response.lesson.SendTest
import com.bonch_dev.spinit.domain.entity.response.lesson.Task
import com.bonch_dev.spinit.domain.entity.response.lesson.TestData
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.GET_TEST
import com.bonch_dev.spinit.domain.utils.Keys.SEND_TEST
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.LessonActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.adapters.TestAdapter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter.ITestPresenter
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.test.presenter.TestPresenter
import com.bonch_dev.spinit.router.LessonRouter
import javax.inject.Inject

class TestFragment : InternetFragment(), ITestFragment {

    private val tests = mutableListOf<Task?>()
    var testCheckedItems = hashMapOf<Int, Any>()
    var testItemCount = 0
    private lateinit var currentFragListener: CurrentFragListener
    private lateinit var act: LessonActivity

    private lateinit var binding: FragmentTestBinding

    @Inject
    lateinit var router: LessonRouter

    @Inject
    lateinit var presenter: ITestPresenter

    init {
        App.appComponent.inject(this)
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentTestBinding.inflate(inflater, container, false)
        toolbar=binding.lessonTestTb
        act = activity as LessonActivity
        currentFragListener = act
        currentFragListener.selectedFragment(this)
        (presenter as TestPresenter).attachView(this)

        initRecyclerParams()
        setListeners()
        showSplash()
        hideConnErr()
        presenter.getLessonTest(act.lessonId)

        return binding.root
    }


    override fun getListToSend(): SendTest = SendTest(tests as List<Task>)

    override fun getLessonActivity(): LessonActivity = act

    override fun getCheckedItems(): HashMap<Int, Any> = testCheckedItems

    override fun addTask(task: Task) {
        tests.add(task)
    }

    override fun getTestItemsCount(): Int = testItemCount

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.getLessonTest(act.lessonId)
        }
    }

    override fun setListeners() {

        binding.testDoneBtn.setOnClickListener {
            presenter.onNextStepBTNClicked(act.lessonId)
        }

    }

    private fun initRecyclerParams() {

        binding.testRecycler.run {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    override fun bindTest(list: List<TestData>) {
        binding.run {

            testRecycler.setOnTouchListener { _, _ ->
                hideKB()
                true
            }

            testNestedScroll.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {

                override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
                    hideKB()
                }

            })

            testRecycler.adapter = TestAdapter(list, this@TestFragment)
        }
    }

    override fun onStop() {
        cancelJobs(GET_TEST, SEND_TEST)
        super.onStop()
    }

    override fun onDestroyView() {
        (presenter as TestPresenter).detachView()
        super.onDestroyView()
    }
}
