package com.bonch_dev.spinit.presentation.modules.notification.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.presentation.modules.notification.view.NotificationFragment

class NotificationsAdapter(val ctx: NotificationFragment, val items: ArrayList<Notification>?) :
    RecyclerView.Adapter<NotificationsAdapter.OriginalViewHolder>() {

    var removedPosition: Int = 0
    var removedItem: Notification? = null

    inner class OriginalViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        private val notificationImage: ImageView = v.findViewById(R.id.item_notification_icon)
        private val notificationMainText: TextView = v.findViewById(R.id.item_notification_main_text)
        private val notificationTimeCreated: TextView = v.findViewById(R.id.item_notification_time_created_text)
        fun bind(position: Int) {
            val item = items?.get(position)
            notificationMainText.text = item?.data?.title
            notificationTimeCreated.text = item?.created_at
            Glide.with(ctx).load(item?.data?.icon_url).apply(RequestOptions().circleCrop()).into(notificationImage)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OriginalViewHolder =
        OriginalViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false))

    override fun onBindViewHolder(holder: OriginalViewHolder, position: Int) = holder.bind(position)

    override fun getItemCount(): Int = items?.size ?: 0

    fun removeItem(holder: RecyclerView.ViewHolder) {
        removedPosition = holder.adapterPosition
        removedItem = items?.get(holder.adapterPosition)
        removedItem?.id?.let { ctx.presenter.deleteNotification(it, this) }
    }

    fun onDeleteItem(position: Int) {
        items?.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

    fun addItem(notification: List<Notification>) {
        items?.addAll(0, notification)
        notifyItemRangeInserted(0, notification.size)
    }
}
