package com.bonch_dev.spinit.presentation.modules.notification.adapters

import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import javax.inject.Inject
import javax.inject.Named

class PushNotificationsAdapter : RecyclerView.Adapter<PushNotificationsAdapter.PushNotificationsViewHolder>() {
    private val list = listOf("Достижения", "Трансляции", "Ответ преподавателя", "Новые задания")

    @Inject
    @Named("Notification")
    lateinit var pushNotificationsSP: SharedPreferences

    @Inject
    @Named("Notification")
    lateinit var pushNotificationsSPEditor: SharedPreferences.Editor

    init {
        App.appComponent.inject(this)
    }

    inner class PushNotificationsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val pushTV: TextView = view.findViewById(R.id.push_item_tv)
        private val pushSwitch: Switch = view.findViewById(R.id.push_switch)
        private val achievementStatus = pushNotificationsSP.getBoolean("newAchievement", true)
        private val broadCastStatus = pushNotificationsSP.getBoolean("newBroadcast", true)
        private val teacherAnswerStatus = pushNotificationsSP.getBoolean("newAnswer", true)
        private val newTaskStatus = pushNotificationsSP.getBoolean("newTask", true)
        private val checkedStatusList = listOf(achievementStatus, broadCastStatus, teacherAnswerStatus, newTaskStatus)
        private val checkedList = listOf("newAchievement", "newBroadcast", "newAnswer", "newTask")
        fun bind(position: Int) {
            if (checkedStatusList[position]) pushSwitch.isChecked = true
            pushSwitch.setOnCheckedChangeListener { _, isChecked ->
                pushNotificationsSPEditor.run {
                    if (isChecked) putBoolean(checkedList[position], true).apply()
                    else putBoolean(checkedList[position], false).apply()
                }
            }
            pushTV.text = list[position]
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PushNotificationsViewHolder =
        PushNotificationsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_pushnotif, parent, false))

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: PushNotificationsViewHolder, position: Int) = holder.bind(position)
}