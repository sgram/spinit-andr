package com.bonch_dev.spinit.presentation.modules.notification.presenter

import com.bonch_dev.spinit.presentation.modules.notification.adapters.NotificationsAdapter

interface INotificationPresenter {
    fun onStart()
    fun getNotifications()
    fun deleteNotification(
        notificationID: String,
        notificationsAdapter: NotificationsAdapter
    )
}