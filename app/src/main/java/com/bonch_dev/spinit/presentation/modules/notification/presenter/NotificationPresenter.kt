package com.bonch_dev.spinit.presentation.modules.notification.presenter

import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.domain.entity.response.notification.Notification
import com.bonch_dev.spinit.domain.interactors.notification.INotificationInteractor
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.NetworkConnection
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.notification.adapters.NotificationsAdapter
import com.bonch_dev.spinit.presentation.modules.notification.view.INotificationFragment
import javax.inject.Inject

class NotificationPresenter @Inject constructor(private val interactor: INotificationInteractor) :
    INotificationPresenter, BasePresenter<INotificationFragment>(), IDataCompletion<List<Notification>> {

    override fun getNotifications() {
        if (NetworkConnection.isInternetAvailable())
            interactor.getNotifications(
                result = ::onResult,
                err = ::onError
            )
        else onError()
    }

    override fun onStart() {
        getView()?.run {
            hideNoContentTV()
            (this as InternetFragment).hideConnErr()
            (this as InternetFragment).showSplash()
        }
        getNotifications()
    }

    override fun onResult(data: List<Notification>) {
        if (data.isNullOrEmpty()) {
            getView()?.run {
                showNoContentTV(text = "У вас нет уведомлений")
                hideRecycler()
                (this as InternetFragment).hideSplash()
            }
        } else {
            getView()?.run {
                hideNoContentTV()
                (this as InternetFragment).hideSplash()
                showRecycler()
                getAdapter()?.items?.clear()
                getAdapter()?.addItem(data)
            }
        }
    }

    override fun onError(msg: String?) {
        getView()?.run {
            (this as InternetFragment).hideSplash()
            (this as InternetFragment).showConnErr()
            (this as Fragment).showToast(msg)
        }
    }

    override fun deleteNotification(notificationID: String, notificationsAdapter: NotificationsAdapter) {
        val removedItem = notificationsAdapter.removedItem
        val pos = notificationsAdapter.removedPosition
        if (NetworkConnection.isInternetAvailable()) {
            interactor.deleteNotification(notificationID,
                result = {
                    getView()?.getAdapter()?.run {
                        onDeleteItem(pos)
                        if (items != null && items.isEmpty()) getView()?.showNoContentTV(text = "У вас нет уведомлений")
                    }
                },
                err = {
                    getView()?.getAdapter()?.run {
                        onDeleteItem(pos)
                        removedItem?.let { item -> items?.add(pos, item) }
                        notifyItemInserted(pos)
                        notifyItemRangeChanged(pos, itemCount)
                    }
                    (getView() as Fragment).showToast(it)
                })
        } else {
            getView()?.getAdapter()?.run {
                onDeleteItem(pos)
                removedItem?.let { item -> items?.add(pos, item) }
                notifyItemInserted(pos)
                notifyItemRangeChanged(pos, itemCount)
            }
            onError()
        }
    }
}