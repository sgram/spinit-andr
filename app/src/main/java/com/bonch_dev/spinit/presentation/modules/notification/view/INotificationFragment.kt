package com.bonch_dev.spinit.presentation.modules.notification.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.notification.adapters.NotificationsAdapter

interface INotificationFragment : IBaseView {
    fun showNoContentTV(text: String)
    fun hideNoContentTV()
    fun hideRecycler()
    fun showRecycler()
    fun getAdapter(): NotificationsAdapter?
}