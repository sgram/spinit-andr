package com.bonch_dev.spinit.presentation.modules.notification.view

import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentNotificationsBinding
import com.bonch_dev.spinit.presentation.base.view.InternetFragment
import com.bonch_dev.spinit.domain.utils.Keys.DELETE_NOTIFICATION
import com.bonch_dev.spinit.domain.utils.Keys.GET_NOTIFICATIONS
import com.bonch_dev.spinit.domain.utils.Keys.READ_NOTIFICATION
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.notification.adapters.NotificationsAdapter
import com.bonch_dev.spinit.presentation.modules.notification.presenter.INotificationPresenter
import com.bonch_dev.spinit.presentation.modules.notification.presenter.NotificationPresenter
import javax.inject.Inject

class NotificationFragment : InternetFragment(), INotificationFragment {


    private lateinit var adapter: NotificationsAdapter
    private var deleteIcon = ContextCompat.getDrawable(App.applicationContext(), R.drawable.ic_delete)
    private val intrinsicWidth = deleteIcon?.intrinsicWidth
    private val intrinsicHeight = deleteIcon?.intrinsicHeight
    private val background = ColorDrawable()
    private val backgroundColor = Color.parseColor("#F44336")
    private val clearPaint = Paint().apply { xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR) }

    private lateinit var binding: FragmentNotificationsBinding

    @Inject
    lateinit var presenter: INotificationPresenter

    init {
        App.appComponent.inject(this)
    }


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        toolbar = binding.notificationsToolbar
        val currentFragListener: CurrentFragListener = activity as MainActivity
        currentFragListener.selectedFragment(this)
        (presenter as NotificationPresenter).attachView(this)
        setListeners()
        initRecyclerParams()
        presenter.onStart()

        return binding.root
    }

    override fun setListeners() {}


    private fun initItemTouchHelper(adapter: NotificationsAdapter) {
        deleteIcon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_delete)
        val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean = false

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) =
                adapter.removeItem(viewHolder as NotificationsAdapter.OriginalViewHolder)

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val itemView = viewHolder.itemView
                val itemHeight = itemView.bottom - itemView.top
                val isCanceled = dX == 0f && !isCurrentlyActive
                if (isCanceled) {
                    clearCanvas(
                        c,
                        itemView.right + dX,
                        itemView.top.toFloat(),
                        itemView.right.toFloat(),
                        itemView.bottom.toFloat()
                    )
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                    return
                }
                var deleteIconTop = 0
                var deleteIconMargin = 0
                var deleteIconLeft = 0
                var deleteIconRight = 0
                var deleteIconBottom = 0
                // Draw the red delete background
                background.color = backgroundColor
                if (dX < 0) {
                    background.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
                    // Calculate position of delete icon
                    intrinsicHeight?.let {
                        deleteIconTop = itemView.top + (itemHeight - intrinsicHeight) / 2
                        deleteIconMargin = (itemHeight - intrinsicHeight) / 2
                        intrinsicWidth?.let {
                            deleteIconLeft = itemView.right - deleteIconMargin - intrinsicWidth
                        }
                        deleteIconRight = itemView.right - deleteIconMargin
                        deleteIconBottom = deleteIconTop + intrinsicHeight
                    }
                } else {
                    background.setBounds(itemView.left, itemView.top, itemView.left + dX.toInt(), itemView.bottom)
                    // Calculate position of delete icon
                    intrinsicHeight?.let {
                        deleteIconTop = itemView.top + (itemHeight - it) / 2
                        deleteIconMargin = (itemHeight - it) / 2
                        deleteIconLeft = itemView.left + deleteIconMargin
                        intrinsicWidth?.let {
                            deleteIconRight = itemView.left + deleteIconMargin + it
                        }
                        deleteIconBottom = deleteIconTop + it
                    }
                }
                background.draw(c)
                // Draw the delete icon
                deleteIcon?.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom)
                deleteIcon?.draw(c)
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

            private fun clearCanvas(c: Canvas?, left: Float, top: Float, right: Float, bottom: Float) =
                c?.drawRect(left, top, right, bottom, clearPaint)
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(binding.notificationsRecyclerView)
    }


    override fun getAdapter(): NotificationsAdapter? = binding.notificationsRecyclerView.adapter as NotificationsAdapter?

    override fun showNoContentTV(text: String) {
        binding.noContent.noContentTv.visible()
        binding.noContent.noContentTv.text = text
    }

    override fun hideNoContentTV() = binding.noContent.noContentTv.gone()

    private fun initRecyclerParams() {
        adapter = NotificationsAdapter(this, items = arrayListOf())
        initItemTouchHelper(adapter)
        binding.notificationsRecyclerView.adapter = adapter
    }

    override fun onStop() {
        cancelJobs(GET_NOTIFICATIONS, DELETE_NOTIFICATION, READ_NOTIFICATION)
        super.onStop()
    }

    override fun repeatConnBtnListener() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            presenter.onStart()
        }
    }


    override fun hideRecycler() = binding.notificationsRecyclerView.gone()

    override fun showRecycler() = binding.notificationsRecyclerView.visible()

    override fun onDetach() {
        (presenter as NotificationPresenter).detachView()
        super.onDetach()
    }
}