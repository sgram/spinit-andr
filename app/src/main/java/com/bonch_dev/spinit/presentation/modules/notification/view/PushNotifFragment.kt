package com.bonch_dev.spinit.presentation.modules.notification.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.databinding.FragmentPushNotifBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.modules.notification.adapters.PushNotificationsAdapter

class PushNotifFragment : BaseFragment() {

    private lateinit var binding: FragmentPushNotifBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentPushNotifBinding.inflate(inflater, container, false)
        toolbar = binding.pushToolbar
        binding.pushRecycler.adapter = PushNotificationsAdapter()
        return binding.root
    }

}