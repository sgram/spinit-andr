package com.bonch_dev.spinit.presentation.modules.onboarding.presenter

interface IOnBoardingPresenter {
    fun animateViewPager()
    fun onSignInBtnClicked()
    fun onRegBtnClicked()
    fun onDestroy()
    fun setSelectedPage(position:Int)
}