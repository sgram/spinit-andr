package com.bonch_dev.spinit.presentation.modules.onboarding.presenter

import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.onboarding.view.IOnBoardingFragment
import com.bonch_dev.spinit.router.MainRouter
import kotlinx.coroutines.*
import javax.inject.Inject

class OnBoardingPresenter @Inject constructor(private val router: MainRouter) :
    IOnBoardingPresenter, BasePresenter<IOnBoardingFragment>() {

    private lateinit var animatePagerJob: Job
    private var currentPage: Int = 0

    override fun setSelectedPage(position: Int) {
        currentPage = position
    }

    override fun animateViewPager() {
        animatePagerJob = CoroutineScope(Dispatchers.IO).launch {
            while (true) {
                delay(3000)
                withContext(Dispatchers.Main) {
                    getView()?.changeCurrentItem(
                        if (currentPage == 2) 0 else currentPage + 1
                    )
                }
            }
        }
    }

    override fun onSignInBtnClicked() = router.navigateToSignInFragment()

    override fun onRegBtnClicked() = router.navigateToRegFragment1()

    override fun onDestroy() {
        animatePagerJob.cancel()
        detachView()
    }
}