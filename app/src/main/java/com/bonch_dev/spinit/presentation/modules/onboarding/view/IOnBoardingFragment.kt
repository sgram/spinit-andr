package com.bonch_dev.spinit.presentation.modules.onboarding.view

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView

interface IOnBoardingFragment : IBaseView {
    fun getCtx(): Context
    fun getFragManager(): FragmentManager
    fun initVPParams()
    fun showButtonLayout()
    fun changeCurrentItem(index: Int)
}