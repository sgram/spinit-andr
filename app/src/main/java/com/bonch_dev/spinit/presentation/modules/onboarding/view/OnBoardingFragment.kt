package com.bonch_dev.spinit.presentation.modules.onboarding.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentOnboardingBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.AUTH_GOOGLE
import com.bonch_dev.spinit.domain.utils.Keys.AUTH_VK
import com.bonch_dev.spinit.domain.utils.Keys.SIGN_IN
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.onboarding.adapter.PagerAdapter
import com.bonch_dev.spinit.presentation.modules.onboarding.presenter.IOnBoardingPresenter
import com.bonch_dev.spinit.presentation.modules.onboarding.presenter.OnBoardingPresenter
import javax.inject.Inject

class OnBoardingFragment : BaseFragment(),
    IOnBoardingFragment, ViewPager.OnPageChangeListener {

    @Inject
    lateinit var presenter: IOnBoardingPresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentOnboardingBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentOnboardingBinding.inflate(inflater, container, false)
        (presenter as OnBoardingPresenter).attachView(this)
        setListeners()
        initVPParams()
        binding.tabDots.setupWithViewPager(binding.pager, true)
        return binding.root
    }

    override fun getCtx(): Context = requireContext()

    override fun setListeners() {
        binding.loginButton.setOnClickListener {
            presenter.onSignInBtnClicked()
        }
        binding.createAccountButton.setOnClickListener {
            presenter.onRegBtnClicked()
        }
    }

    override fun initVPParams() {
        val adapter = PagerAdapter(this.requireContext())
        binding.pager.adapter = adapter
        binding.pager.addOnPageChangeListener(this)
        presenter.animateViewPager()
    }

    override fun showButtonLayout() = binding.buttonsLayout.visible()

    override fun onDestroyView() {
        cancelJobs(AUTH_VK, AUTH_GOOGLE, SIGN_IN)
        binding.pager.removeOnPageChangeListener(this)
        presenter.onDestroy()
        super.onDestroyView()
    }

    override fun getFragManager(): FragmentManager = this.childFragmentManager

    override fun changeCurrentItem(index: Int) {
        binding.pager.currentItem = index
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    override fun onPageSelected(position: Int) {
        presenter.setSelectedPage(position)
    }

    override fun onPageScrollStateChanged(state: Int) {}
}