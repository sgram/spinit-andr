package com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.presentation.modules.achievements.view.AchievementsFragment
import com.bonch_dev.spinit.presentation.modules.article.view.SavedArticlesFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.mycourses.view.MyCoursesFragment
import com.bonch_dev.spinit.presentation.modules.teachermsg.view.TeacherMessagesFragment

class MainFragPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val frags: Array<Fragment> =
        arrayOf(MyCoursesFragment(), SavedArticlesFragment(), AchievementsFragment(), TeacherMessagesFragment())

    override fun getItem(position: Int): Fragment = frags[position]

    override fun getCount(): Int = frags.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> ResourceManager.getString(R.string.my_courses)
            1 -> ResourceManager.getString(R.string.saved_articles)
            2 -> ResourceManager.getString(R.string.achievements)
            3 -> ResourceManager.getString(R.string.msg_from_teacher)
            else -> ""
        }
    }
}