package com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view

import androidx.annotation.DrawableRes
import com.bonch_dev.spinit.domain.entity.UserInfo
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.adapter.MainFragPagerAdapter

interface IMainFragment : IBaseView {
    fun bindTheme(text: String?)
    fun getViewPagerAdapter(): MainFragPagerAdapter?
    fun setProfileBackground(@DrawableRes res: Int)
    fun initViewPagerAdapter()
    fun initTabLayout()
    fun showConnErr()
    fun hideConnErr()
    fun bindData(data: UserInfo)
}