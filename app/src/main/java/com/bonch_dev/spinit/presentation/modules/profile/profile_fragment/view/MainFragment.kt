package com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view

import android.graphics.Color
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentMainBinding
import com.bonch_dev.spinit.domain.entity.UserInfo
import com.bonch_dev.spinit.domain.utils.*
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_THEME
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.adapter.MainFragPagerAdapter
import com.bonch_dev.spinit.router.SettingsRouter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.signature.ObjectKey
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import javax.inject.Inject
import javax.inject.Named

class MainFragment : BaseFragment(), IMainFragment {

    @Inject
    lateinit var router: SettingsRouter

    @Inject
    @Named(COURSE_THEME)
    lateinit var theme: String

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentMainBinding


    lateinit var bottomNavigationFragment: BottomNavigationFragment
    private var offsetChangedListener: AppBarLayout.OnOffsetChangedListener? = null


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        bottomNavigationFragment = parentFragment as BottomNavigationFragment
        initViewPagerAdapter()
        bindTheme(theme)
        initTB()
        setListeners()

        return binding.root
    }

    override fun bindTheme(text: String?) {
        binding.mainFragmentThemeTv.text = text
    }


    override fun setListeners() {
        binding.noInternet.repeatConnectionBtn.setOnClickListener {
            showSplash()

            bottomNavigationFragment.run {
                if (NetworkConnection.isInternetAvailable()) {
                    presenter.onStart()
                    presenter.getUserInfo()
                }
            }

        }

        offsetChangedListener = AppBarLayout.OnOffsetChangedListener { _, p1 ->
            binding.adminMainFragmentProfileInfoLayout.alpha = 1.0f + p1 / 288f
        }

        binding.mainAppbar.addOnOffsetChangedListener(offsetChangedListener)

        binding.mainFragProfileIv.setOnClickListener {
            router.toEditProfile(bottomNavigationFragment)
        }
    }

    override fun showConnErr() {
        binding.noInternet.root.visible()
        binding.collapsingTb.gone()
    }

    override fun hideConnErr() {
        binding.noInternet.root.gone()
        binding.collapsingTb.visible()
    }

    fun showSplash() {
        binding.collapsingTb.gone()
        binding.splash.root.visible()
    }

    fun hideSplash() {
        binding.collapsingTb.visible()
        binding.splash.root.gone()
    }

    private fun initTB() {
        val cTb = binding.collapsingTb
        cTb.setExpandedTitleColor(Color.WHITE)
        cTb.setCollapsedTitleTextColor(Color.WHITE)
        initTabLayout()
    }

    override fun setProfileBackground(res: Int) = binding.mainFragProfileIv.setBackgroundResource(res)

    override fun getViewPagerAdapter(): MainFragPagerAdapter? = binding.mainFragViewPager.adapter as MainFragPagerAdapter?

    override fun initTabLayout() {
        binding.mainFragTabLayout.run {
            tabMode = TabLayout.MODE_SCROLLABLE
            setSelectedTabIndicatorColor(ResourceManager.getColor(R.color.mainColor))
            setupWithViewPager(binding.mainFragViewPager)
        }
    }

    override fun bindData(data: UserInfo) {
        Glide.with(this).load(data.avatar_url)
            .apply(RequestOptions().signature(ObjectKey(System.currentTimeMillis().toString())).centerCrop().circleCrop())
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    (activity as MainActivity).removeSplash()
                    hideSplash()
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    (activity as MainActivity).removeSplash()
                    hideSplash()
                    return false
                }
            }).into(binding.mainFragProfileIv)
        binding.collapsingTb.title = data.first_name + " " + data.last_name
    }

    override fun initViewPagerAdapter() {
        val vp = binding.mainFragViewPager
        val mainFragPagerAdapter = MainFragPagerAdapter(childFragmentManager)
        vp.offscreenPageLimit = 4
        vp.adapter = mainFragPagerAdapter
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        if (!hidden) {
            bottomNavigationFragment.run {
                binding.toolbar.run {
                    setBackgroundColor(Color.TRANSPARENT)
                    title = ""
                    setTitleTextColor(Color.TRANSPARENT)
                }
            }
            Log.e("hide status", "${javaClass.simpleName} visible")
        }
    }

    override fun onStop() {
        binding.mainAppbar.removeOnOffsetChangedListener(offsetChangedListener)
        super.onStop()
    }
}
