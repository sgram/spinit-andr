package com.bonch_dev.spinit.presentation.modules.profile.update_info.model

data class UserData(
    var name: String?,
    var lastName: String?,
    var email: String?,
    var phone: String?,
    var gender: String?
) {
    fun isAllFieldsNull(): Boolean {
        return name.isNullOrEmpty() &&
                lastName.isNullOrEmpty() &&
                email.isNullOrEmpty() &&
                phone.isNullOrEmpty() &&
                gender.isNullOrEmpty()
    }
}