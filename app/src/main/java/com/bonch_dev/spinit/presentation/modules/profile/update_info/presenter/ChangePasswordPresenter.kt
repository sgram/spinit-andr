package com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter

import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.interactors.update_info.IUpdateInfoInteractor
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.interfaces.IDataCompletion
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.IChangePasswordFragment
import javax.inject.Inject

class ChangePasswordPresenter @Inject constructor(private val interactor: IUpdateInfoInteractor) :
    IChangePasswordPresenter, BasePresenter<IChangePasswordFragment>(), IDataCompletion<String> {

    override fun changePassword(oldPass: String, newPass: String, confirmPass: String) {
        val ctx = App.applicationContext()
        val toast: (String) -> Unit = { (getView() as Fragment).showToast(it) }
        when {
            oldPass.isEmpty() or newPass.isEmpty() or confirmPass.isEmpty() -> toast(ctx.getString(R.string.empty_field_error))
            newPass != confirmPass -> toast(ctx.getString(R.string.passes_dont_match_error))
            confirmPass.length < 6 -> toast(ctx.getString(R.string.pass_length_less_6_error))
            else -> {
                interactor.changePassword(
                    oldPass, newPass, confirmPass,
                    result = { onResult(confirmPass) },
                    err = ::onError
                )
            }
        }
    }

    override fun onResult(data: String) {
        getView()?.run {
            hideKB()
            getMainAct().onBackPressed()
            BottomNavigationFragment.refresh()
            (this as Fragment).showToast("Пароль изменен")
        }
    }

    override fun onError(msg: String?) {
        (getView() as Fragment).showToast(msg)
    }
}