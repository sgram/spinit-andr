package com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter

import android.net.Uri
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.interactors.update_info.IUpdateInfoInteractor
import com.bonch_dev.spinit.domain.utils.CorrectInput
import com.bonch_dev.spinit.domain.utils.CurrentUser
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.IEditProfileFragment
import javax.inject.Inject

class EditProfilePresenter @Inject constructor(private val interactor: IUpdateInfoInteractor) :
    IEditProfilePresenter, BasePresenter<IEditProfileFragment>() {

    private lateinit var oldUserData: UserData

    override fun onStart() {
        bindOldData()
    }

    override fun bindOldData() {
        CurrentUser.apply {
            oldUserData = UserData(
                name = name,
                lastName = lastName,
                email = email,
                phone = phone,
                gender = gender
            )
            getView()?.bindOldData(oldUserData)
        }
    }

    override fun onChangeDataBtnClicked(userData: UserData, uri: Uri?, avatarHasBeenChanged: Boolean) {
        if (checkCorrectInput(userData)) {
            checkChangedData(userData)
            if (!userData.isAllFieldsNull() || avatarHasBeenChanged) {
                interactor.updateData(userData, uri,
                    result = { getView()?.onDataChanged() },
                    err = { (getView() as Fragment).showToast(it) }
                )
            } else getView()?.getMainAct()?.onBackPressed()
        }
    }

    override fun checkCorrectInput(userData: UserData): Boolean {
        val email = userData.email
        val phone = userData.phone
        val toast: (Int) -> Unit = { (getView() as Fragment).showToast(ResourceManager.getString(it)) }
        return when {
            userData.name.isNullOrEmpty() || userData.lastName.isNullOrEmpty() -> {
                toast(R.string.empty_field_error)
                false
            }
            email != null && !CorrectInput.isCorrectEmail(email) && !email.isNullOrEmpty() -> {
                toast(R.string.incorrect_email_error)
                false
            }
            phone != null && phone.length in 1..17 -> {
                toast(R.string.incorrect_phone_error)
                false
            }
            else -> true
        }
    }

    override fun checkChangedData(userData: UserData) {
        val formattedPhone = userData.phone?.replace(Regex("[+() -]"), "")
        userData.apply {
            if (name == oldUserData.name) name = null
            if (lastName == oldUserData.lastName) lastName = null
            phone = if (formattedPhone == oldUserData.phone || formattedPhone.isNullOrEmpty()) null else formattedPhone
            if (gender == oldUserData.gender || gender.isNullOrEmpty()) gender = null
            if (email == oldUserData.email || email.isNullOrEmpty()) email = null
        }
    }
}