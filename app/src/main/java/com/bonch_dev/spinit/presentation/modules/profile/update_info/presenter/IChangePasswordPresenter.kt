package com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter

interface IChangePasswordPresenter {
    fun changePassword(oldPass: String, newPass: String, confirmPass: String)
}