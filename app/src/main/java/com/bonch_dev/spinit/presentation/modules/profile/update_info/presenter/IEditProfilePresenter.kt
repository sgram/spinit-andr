package com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter

import android.net.Uri
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData

interface IEditProfilePresenter {
    fun onStart()
    fun bindOldData()
    fun onChangeDataBtnClicked(userData: UserData, uri: Uri?, avatarHasBeenChanged: Boolean)
    fun checkChangedData(userData: UserData)
    fun checkCorrectInput(userData: UserData): Boolean
}