package com.bonch_dev.spinit.presentation.modules.profile.update_info.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentChangePasswordBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.CHANGE_PASS
import com.bonch_dev.spinit.domain.utils.Keys.FORGOT
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.ChangePasswordPresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.IChangePasswordPresenter
import javax.inject.Inject

class ChangePasswordFragment : BaseFragment(), IChangePasswordFragment {

    @Inject
    lateinit var presenter: IChangePasswordPresenter

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentChangePasswordBinding


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentChangePasswordBinding.inflate(inflater, container, false)
        toolbar = binding.changePasswordToolbar
        (presenter as ChangePasswordPresenter).attachView(this)
        setListeners()
        return binding.root
    }

    override fun setListeners() {
        binding.run {
            changePasswordBtn.setOnClickListener {
                val oldPass = etOldPassword.text.toString()
                val newPass = etNewPassword.text.toString()
                val confirmPass = etNewConfirmPassword.text.toString()
                presenter.changePassword(oldPass, newPass, confirmPass)
            }
        }
    }

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun onDestroyView() {
        cancelJobs(CHANGE_PASS, FORGOT)
        (presenter as ChangePasswordPresenter).detachView()
        super.onDestroyView()
    }
}