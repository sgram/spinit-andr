package com.bonch_dev.spinit.presentation.modules.profile.update_info.view

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentEditProfileBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.FEMALE
import com.bonch_dev.spinit.domain.utils.Keys.MALE
import com.bonch_dev.spinit.domain.utils.Keys.RESET_AVATAR
import com.bonch_dev.spinit.domain.utils.Keys.UPDATE_AVATAR
import com.bonch_dev.spinit.domain.utils.Keys.UPDATE_DATA
import com.bonch_dev.spinit.domain.utils.Keys.avatarURL
import com.bonch_dev.spinit.domain.utils.Keys.cancelJobs
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.learning.lesson.base.CurrentFragListener
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.EditProfilePresenter
import com.bonch_dev.spinit.presentation.modules.profile.update_info.presenter.IEditProfilePresenter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import javax.inject.Inject

class EditProfileFragment : BaseFragment(), IEditProfileFragment {

    @Inject
    lateinit var presenter: IEditProfilePresenter
    var avatarHasBeenChanged = false
        private set

    private lateinit var currentFragListener: CurrentFragListener

    init {
        App.appComponent.inject(this)
    }

    private val readPerm = Manifest.permission.READ_EXTERNAL_STORAGE
    private val writePerm = Manifest.permission.WRITE_EXTERNAL_STORAGE

    private val PERMISSION_CODE = 10

    private lateinit var act: MainActivity

    private lateinit var binding: FragmentEditProfileBinding

    private var uri: Uri? = null
    private lateinit var phoneextracted: String


    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentEditProfileBinding.inflate(inflater, container, false)
        toolbar = binding.editProfileToolbar
        act = activity as MainActivity
        currentFragListener = act
        currentFragListener.selectedFragment(this)
        reqPerm()
        setListeners()
        initMasks()
        (presenter as EditProfilePresenter).attachView(this)

        binding.sv.setOnTouchListener { _, _ ->
            hideKB()
            false
        }

        presenter.onStart()
        return binding.root
    }

    override fun reqPerm() {
        if (ContextCompat.checkSelfPermission(act, readPerm) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(
                act,
                arrayOf(
                    readPerm,
                    writePerm
                ), PERMISSION_CODE
            )
        }
    }

    override fun initViews(view: View) {
        loadAvatar()
    }

    override fun loadAvatar() {
        if (avatarURL.isNotEmpty())
            Glide.with(this).load(avatarURL).apply(RequestOptions().circleCrop()).into(binding.editProfilePhoto)
    }

    override fun getMainAct(): MainActivity = act

    override fun initMasks() {
        val phoneFormats: MutableList<String> = ArrayList()
        phoneFormats.add("8 ([000]) [000]-[00]-[00]")
        val dateFormats: MutableList<String> = ArrayList()
        dateFormats.add("00.00.0000")
        MaskedTextChangedListener.installOn(
            binding.editProfilePhoneEt,
            "+7 ([000]) [000]-[00]-[00]",
            phoneFormats,
            AffinityCalculationStrategy.PREFIX,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(maskFilled: Boolean, extractedValue: String, formattedValue: String) {
                    phoneextracted = "7$extractedValue"
                }
            })
    }

    override fun getGender(): String =
        act.findViewById<RadioButton>(binding.editProfileRadioGroup.checkedRadioButtonId)?.text.toString()

    override fun setListeners() {
        binding.run {
            binding.editProfileSaveChangesBtn.setOnClickListener {
                onChangeDataBtnClicked()
            }
            binding.editProfileChangeAvatarBtn.setOnClickListener {
                if (ContextCompat.checkSelfPermission(act, readPerm) == PackageManager.PERMISSION_DENIED)
                    ActivityCompat.requestPermissions(act, arrayOf(readPerm, writePerm), GALLERY_CODE)
                else
                    openGallery()
            }
        }

    }

    fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK).apply { type = "image/*" }
        val mimeTypes = arrayListOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_CODE)
    }

    override fun onChangeDataBtnClicked() {
        binding.run {
            val userData = UserData(
                email = editProfileEmailEt.text.toString(),
                lastName = editProfileEtLastname.text.toString(),
                name = editProfileEtName.text.toString(),
                phone = editProfilePhoneEt.text.toString(),
                gender = when (editProfileRadioGroup.checkedRadioButtonId) {
                    R.id.rb_man -> MALE
                    R.id.rb_woman -> FEMALE
                    else -> null
                }
            )


            presenter.onChangeDataBtnClicked(userData, uri, avatarHasBeenChanged)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY_CODE && resultCode == Activity.RESULT_OK && data != null) {
            avatarHasBeenChanged = true
            uri = data.data
            Log.e("uri", uri.toString())
            Glide.with(this).load(uri).apply(RequestOptions().circleCrop()).into(binding.editProfilePhoto)
        }
    }

    override fun onStop() {
        cancelJobs(UPDATE_DATA, UPDATE_AVATAR, RESET_AVATAR)
        super.onStop()
    }

    override fun onDestroyView() {
        (presenter as EditProfilePresenter).detachView()
        super.onDestroyView()
    }

    override fun bindOldData(userData: UserData) {
        binding.run {
            userData.apply {
                editProfileEtName.setText(name)
                editProfileEtLastname.setText(lastName)
                editProfileEmailEt.setText(email)
                if (phone != null) editProfilePhoneEt.setText(phone)
                when (gender) {
                    MALE -> editProfileRadioGroup.check(R.id.rb_man)
                    FEMALE -> editProfileRadioGroup.check(R.id.rb_woman)
                }
            }
        }

    }

    override fun onDataChanged() {
        hideKB()
        BottomNavigationFragment.refresh()
        showToast(getString(R.string.data_has_been_updated))
        act.onBackPressed()
    }

    companion object {
        const val GALLERY_CODE = 1
    }
}