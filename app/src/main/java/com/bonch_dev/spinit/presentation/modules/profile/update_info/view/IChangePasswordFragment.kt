package com.bonch_dev.spinit.presentation.modules.profile.update_info.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface IChangePasswordFragment : IBaseView {
    fun getMainAct(): MainActivity
    fun hideKB()
}