package com.bonch_dev.spinit.presentation.modules.profile.update_info.view

import android.view.View
import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.profile.update_info.model.UserData

interface IEditProfileFragment : IBaseView {
    fun reqPerm()
    fun initViews(view: View)
    fun loadAvatar()
    fun initMasks()
    fun bindOldData(userData: UserData)
    fun onDataChanged()
    fun onChangeDataBtnClicked()
    fun getGender(): String
    fun getMainAct(): MainActivity
}