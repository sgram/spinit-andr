package com.bonch_dev.spinit.presentation.modules.settings.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment

class SettingsAdapter(val settingsFragment: SettingsFragment) :
    RecyclerView.Adapter<SettingsAdapter.SettingsViewHolder>() {

    var settingsPresenter = settingsFragment.presenter

    val list = if (Keys.havePassword) listOf("Редактировать профиль", "Изменить пароль", "Push-уведомления")
    else listOf("Редактировать профиль", "Push-уведомления")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SettingsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_settings, parent, false)
        return SettingsViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: SettingsViewHolder, position: Int) = holder.bind(position)

    inner class SettingsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val settingsTV: TextView = view.findViewById(R.id.settings_item_tv)
        fun bind(position: Int) {
            settingsTV.text = list[position]
            itemView.setOnClickListener() {
                settingsPresenter.onItemClick(list.size, position, settingsFragment)
            }
        }
    }
}