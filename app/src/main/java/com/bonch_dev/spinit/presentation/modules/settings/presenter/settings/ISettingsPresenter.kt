package com.bonch_dev.spinit.presentation.modules.settings.presenter.settings

import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment

interface ISettingsPresenter {
    fun onItemClick(size: Int, position: Int, settingsFragment: SettingsFragment)
    fun logout()
    fun logoutFromOtherServices()
    fun toStartActivity()
    fun clearSP()
    fun onLogoutBTNClicked()
}