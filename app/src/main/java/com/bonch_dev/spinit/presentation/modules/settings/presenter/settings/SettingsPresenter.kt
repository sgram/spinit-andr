package com.bonch_dev.spinit.presentation.modules.settings.presenter.settings

import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.fragment.app.Fragment
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.domain.interactors.auth.logout.ILogoutInteractor
import com.bonch_dev.spinit.domain.utils.Keys
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_THEME
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.TOKEN
import com.bonch_dev.spinit.domain.utils.Keys.USER
import com.bonch_dev.spinit.domain.utils.showToast
import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.settings.view.ISettingsFragment
import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment
import com.bonch_dev.spinit.router.SettingsRouter
import com.google.android.gms.auth.api.Auth
import com.vk.sdk.VKSdk
import javax.inject.Inject
import javax.inject.Named

class SettingsPresenter @Inject constructor(private val interactor: ILogoutInteractor, private val router: SettingsRouter) :
    ISettingsPresenter, BasePresenter<ISettingsFragment>() {

    @Inject
    @Named(USER)
    lateinit var spEditor: SharedPreferences.Editor

    @Inject
    @Named(MAIN)
    lateinit var mainSPEditor: SharedPreferences.Editor

    @Inject
    @Named(MAIN)
    lateinit var mainSP: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    override fun onItemClick(size: Int, position: Int, settingsFragment: SettingsFragment) {
        when (size) {
            3 -> {
                when (position) {
                    0 -> router.toEditProfile(settingsFragment)
                    1 -> router.toChangePassword(settingsFragment)
                    2 -> router.toPushNotif(settingsFragment)
                }
            }
            2 -> {
                when (position) {
                    0 -> router.toEditProfile(settingsFragment)
                    1 -> router.toPushNotif(settingsFragment)
                }
            }
        }
    }

    override fun logout() {
        interactor.logout(
            result = {
                (getView() as Fragment).showToast("Вы вышли из аккаунта")
                logoutFromOtherServices()
                toStartActivity()
                clearSP()
            },
            err = { (getView() as Fragment).showToast(it) })
    }

    override fun logoutFromOtherServices() {
        when {
            VKSdk.isLoggedIn() -> VKSdk.logout()
            Keys.GAC.isConnected -> {
                Auth.GoogleSignInApi?.signOut(Keys.GAC)
                Keys.GAC.disconnect()
            }
        }
    }

    override fun toStartActivity() {
        val act = getView()?.getMainAct()
        val intent = Intent(act, MainActivity::class.java)
        act?.finishAffinity()
        act?.startActivity(intent)
    }

    override fun clearSP() {
        spEditor.clear()?.apply()
        mainSPEditor.putString(COURSE_THEME, "").apply()
        mainSPEditor.putString(TOKEN, "").apply()
        Log.e("test", "logout after clear: " + mainSP.getString(TOKEN, ""))
    }

    override fun onLogoutBTNClicked() {
        getView()?.showLogoutDialog()
    }
}