package com.bonch_dev.spinit.presentation.modules.settings.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.base.view.Base
import com.bonch_dev.spinit.presentation.base.view.MainActivity

interface ISettingsFragment : IBaseView {
    fun getMainAct(): MainActivity
    fun showLogoutDialog()
    fun getBaseFragment(): Base
}