package com.bonch_dev.spinit.presentation.modules.settings.view

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.FragmentSettingsBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.Keys.MAIN
import com.bonch_dev.spinit.domain.utils.Keys.THEME
import com.bonch_dev.spinit.presentation.base.view.Base
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.bonch_dev.spinit.presentation.modules.settings.adapters.SettingsAdapter
import com.bonch_dev.spinit.presentation.modules.settings.presenter.settings.ISettingsPresenter
import com.bonch_dev.spinit.presentation.modules.settings.presenter.settings.SettingsPresenter
import javax.inject.Inject
import javax.inject.Named

class SettingsFragment : BaseFragment(), ISettingsFragment {

    @Inject
    lateinit var presenter: ISettingsPresenter

    @Inject
    @Named(MAIN)
    lateinit var spEditor: SharedPreferences.Editor

    @Inject
    @Named(MAIN)
    lateinit var sp: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    private lateinit var binding: FragmentSettingsBinding

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        toolbar = binding.settingsToolbar
        (presenter as SettingsPresenter).attachView(this)
        initRecyclerParams()
        setListeners()
        return binding.root
    }

    override fun showLogoutDialog() {
        val alert = createLogoutDialog()
        alert?.show()
        alert?.findViewById<TextView>(android.R.id.message)?.apply {
            textSize = 14F
            setTextColor(Color.parseColor("#666666"))
        }
    }

    private fun createLogoutDialog(): AlertDialog? = AlertDialog.Builder(context)
        .setTitle(getString(R.string.are_your_sure_to_exit))
        .setMessage(getString(R.string.logout_desc))
        .setNegativeButton(getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
        .setPositiveButton(getString(R.string.yes)) { _, _ -> presenter.logout() }.create()

    private fun initRecyclerParams() {
        binding.settingsRecycler?.adapter = SettingsAdapter(this@SettingsFragment)
    }

    override fun setListeners() {
        binding.blackThemeBtn?.setOnClickListener {
            when (sp.getInt(THEME, R.style.LightTheme)) {
                R.style.LightTheme -> spEditor.putInt(THEME, R.style.DarkTheme).apply()
                R.style.DarkTheme -> spEditor.putInt(THEME, R.style.LightTheme).apply()
            }
            val intent = Intent(context, MainActivity::class.java)
            activity?.finishAffinity()
            startActivity(intent)
        }
        binding.logoutBtn.setOnClickListener {
            presenter.onLogoutBTNClicked()
        }
    }

    override fun getBaseFragment(): Base = parentFragment as Base

    override fun getMainAct(): MainActivity = activity as MainActivity

    override fun onDetach() {
        (presenter as SettingsPresenter).detachView()
        super.onDetach()
    }
}