package com.bonch_dev.spinit.presentation.modules.teachermsg.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.ItemHomeworkSectionBinding
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible

class HomeworkSectionsAdapter(
    private var courses: List<String>,
    private val clickListener: OnClickListener
) : RecyclerView.Adapter<HomeworkSectionsAdapter.HomeworkSectionVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeworkSectionVH {
        val inflater = LayoutInflater.from(parent.context)
        return HomeworkSectionVH(ItemHomeworkSectionBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return courses.size
    }

    override fun onBindViewHolder(holder: HomeworkSectionVH, position: Int) {
        holder.onBindViewHolder(position)
    }

    inner class HomeworkSectionVH(val binding: ItemHomeworkSectionBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var homeworksAdapter: HomeworksAdapter

        private var position: Int? = null
        private var expanded: Boolean = false

        private fun setExpanded() {
            expanded = !expanded
        }

        fun onBindViewHolder(position: Int) {
            this.position = position

            bindData()
            setClickers()
        }

        private fun bindData() {
            position?.let {
                binding.courseName.text = courses[it]
            }
        }

        private fun setClickers() {
            binding.expandSectionBtn.setOnClickListener {
                setExpanded()
                if (expanded)
                    it.animate().setDuration(200).rotation(180F)
                else
                    it.animate().setDuration(200).rotation(0F)

                clickListener.onExpandHomeworksBtnClick(this, binding.courseName.text.toString())
            }

            itemView.setOnClickListener {
                setExpanded()
                if (expanded)
                    binding.expandSectionBtn.animate().setDuration(200).rotation(180F)
                else
                    binding.expandSectionBtn.animate().setDuration(200).rotation(0F)

                clickListener.onExpandHomeworksBtnClick(this, binding.courseName.text.toString())
            }
        }

        fun onExpand(
            homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>,
            clickListener: HomeworksAdapter.OnClickListener
        ) {
            homeworksAdapter = HomeworksAdapter(homeworks, clickListener)

            setHomeworksRecyclerVisible()
        }

        private fun setHomeworksRecyclerVisible() {
            if (expanded) {
                TransitionManager.beginDelayedTransition(binding.homeworksLayout, AutoTransition())
                binding.homeworksLayout.visible()
            } else {
                TransitionManager.beginDelayedTransition(binding.homeworksLayout, AutoTransition())
                binding.homeworksLayout.gone()
            }
            binding.homeworksRecycler.apply {
                adapter = homeworksAdapter
                isNestedScrollingEnabled = false
                layoutManager = LinearLayoutManager(App.applicationContext())
            }
        }
    }

    interface OnClickListener {
        fun onExpandHomeworksBtnClick(holder: HomeworkSectionVH, courseTitle: String)
    }
}