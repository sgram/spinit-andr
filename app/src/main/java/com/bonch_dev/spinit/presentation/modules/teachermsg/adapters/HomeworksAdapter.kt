package com.bonch_dev.spinit.presentation.modules.teachermsg.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.databinding.ItemHomeworkItemBinding
import com.bonch_dev.spinit.domain.entity.response.lesson.HomeworkCompletionStatus
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.ResourceManager.context
import com.bonch_dev.spinit.domain.utils.visible

class HomeworksAdapter(
    private val homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>,
    private val clickListener: OnClickListener
) : RecyclerView.Adapter<HomeworksAdapter.HomeworkVH>() {

    inner class HomeworkVH(val binding: ItemHomeworkItemBinding) : RecyclerView.ViewHolder(binding.root) {

        private var position: Int? = null

        fun onBindViewHolder(position: Int) {
            this.position = position

            setClickers()
            bindData()
        }

        private fun bindData() {
            itemView.apply {
                position?.let { pos ->
                    homeworks[pos].lesson?.title?.let {
                        binding.homeworkNumber.text = it
                    }

                    binding.answersRv.run {
                        layoutManager = LinearLayoutManager(App.applicationContext())
                        adapter = TeacherMSGAdapter(homeworks[pos].answers)
                        isNestedScrollingEnabled = false
                    }
                }
            }

            bindStatus()
            setShowHomeworkBtnVisible()
        }

        private fun setClickers() {
            binding.showHomeworkBtn.setOnClickListener {
                position?.let {
                    homeworks[it].lesson?.id?.let {
                        clickListener.onShowHomeworkBtnClicked(it)
                    }
                }
            }
        }

        private fun setShowHomeworkBtnVisible() {
            position?.let {
                if (homeworks[it].status == HomeworkCompletionStatus.NOT_ACCEPTED) {
                    binding.showHomeworkBtn.visible()
                }
            }
        }

        private fun bindStatus() {
            position?.let {
                when (homeworks[it].status) {
                    HomeworkCompletionStatus.WAITING -> setStatusExpectChecking()
                    HomeworkCompletionStatus.ACCEPTED -> setStatusAccepted()
                    HomeworkCompletionStatus.NOT_ACCEPTED -> setStatusNotAccepted()
                }
            }
        }

        private fun setStatusExpectChecking() {
            binding.run {
                statusIv.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_homework_expect_checking))

                homeworkStatus.apply {
                    text = ResourceManager.getString(R.string.homework_expect_checking)
                    setTextColor(ResourceManager.getColor(R.color.homework_expect_checking))
                }
            }
        }

        private fun setStatusAccepted() {
            binding.apply {
                statusIv.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_accepted))

                homeworkStatus.apply {
                    text = ResourceManager.getString(R.string.homework_accepted)
                    setTextColor(ResourceManager.getColor(R.color.homework_accepted))
                }
            }
        }

        private fun setStatusNotAccepted() {
            binding.apply {
                statusIv.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_not_accepted))

                homeworkStatus.apply {
                    text = ResourceManager.getString(R.string.homework_not_accepted)
                    setTextColor(ResourceManager.getColor(R.color.homework_not_accepted))
                }
            }
        }
    }

    interface OnClickListener {
        fun onShowHomeworkBtnClicked(lessonId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeworkVH {
        val inflater = LayoutInflater.from(parent.context)
        return HomeworkVH(ItemHomeworkItemBinding.inflate(inflater, parent, false))
    }


    override fun getItemCount(): Int = homeworks.size

    override fun onBindViewHolder(holder: HomeworkVH, position: Int) =
        holder.onBindViewHolder(position)
}