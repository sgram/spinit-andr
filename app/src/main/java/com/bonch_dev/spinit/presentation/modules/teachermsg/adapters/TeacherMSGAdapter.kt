package com.bonch_dev.spinit.presentation.modules.teachermsg.adapters

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.entity.response.userinfo.HomeworkAnswer
import com.bonch_dev.spinit.domain.utils.ResourceManager
import com.bonch_dev.spinit.domain.utils.visible

class TeacherMSGAdapter(val msgList: List<HomeworkAnswer>?) :
    RecyclerView.Adapter<TeacherMSGAdapter.TeacherMSGViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeacherMSGAdapter.TeacherMSGViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_teacher_msg, parent, false)



        return TeacherMSGViewHolder(view)
    }

    override fun getItemCount(): Int = msgList?.size ?: 0

    override fun onBindViewHolder(holder: TeacherMSGAdapter.TeacherMSGViewHolder, position: Int) =
        holder.bind(position)

    inner class TeacherMSGViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val teacherIV: ImageView = view.findViewById(R.id.teacher_msg_iv)
        private val teacherName: TextView = view.findViewById(R.id.teacher_name)
        private val teacherDate: TextView = view.findViewById(R.id.teacher_date)
        private val msgContent: TextView = view.findViewById(R.id.msg_content)
        private val showAll: TextView = view.findViewById(R.id.show_all)

        private var isShowAll: Boolean = false

        fun bind(position: Int) {
            val item = msgList?.get(position)
            teacherName.text = "${item?.moder?.first_name} ${item?.moder?.last_name}"
            teacherDate.text = item?.created_at
            Glide.with(App.applicationContext()).load(item?.moder?.avatar_url).apply(
                RequestOptions()
                    .signature(ObjectKey(System.currentTimeMillis().toString()))
                    .centerCrop()
                    .circleCrop()
            )
                .into(teacherIV)
            msgContent.text = item?.text
            setClickers()
            setShowAllVisible()
        }

        private fun setClickers() {
            showAll.setOnClickListener(this::onShowAllClick)
        }

        private fun onShowAllClick(v: View?) {
            if (isShowAll)
                hideAll()
            else
                showAll()
        }

        private fun showAll() {
            isShowAll = !isShowAll
            msgContent.maxLines = 1000
            showAll.text = ResourceManager.getString(R.string.hide_all)
        }

        private fun hideAll() {
            isShowAll = !isShowAll
            msgContent.maxLines = 6
            showAll.text = ResourceManager.getString(R.string.show_all)
        }

        private fun setShowAllVisible() {
            val vto = msgContent.viewTreeObserver
            vto.addOnGlobalLayoutListener {
                val layout = msgContent.layout

                layout?.let {
                    val lines: Int = it.lineCount
                    if (lines > 0 && isTextEllipsized(it, lines))
                        showAll.visible()
                }
            }
        }

        private fun isTextEllipsized(layout: Layout, lines: Int) = layout.getEllipsisCount(lines - 1) > 0
    }
}
