package com.bonch_dev.spinit.presentation.modules.teachermsg.presenter

import com.bonch_dev.spinit.presentation.modules.teachermsg.adapters.HomeworkSectionsAdapter

interface ITeacherMsgPresenter {
    fun onShowHomeworkBtnClicked(lessoidId: Int)
    fun onExpandHomeworksBtnClicked(holder: HomeworkSectionsAdapter.HomeworkSectionVH, courseTitle: String)
    fun onHomeworkGot(homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>)
    fun getHomeworksCoursesTitle(): MutableList<String>
    fun sortHw()
}