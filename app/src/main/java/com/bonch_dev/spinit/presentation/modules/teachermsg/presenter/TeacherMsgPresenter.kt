package com.bonch_dev.spinit.presentation.modules.teachermsg.presenter

import com.bonch_dev.spinit.presentation.base.presenters.BasePresenter
import com.bonch_dev.spinit.presentation.modules.profile.profile_fragment.view.MainFragment
import com.bonch_dev.spinit.presentation.modules.teachermsg.adapters.HomeworkSectionsAdapter
import com.bonch_dev.spinit.presentation.modules.teachermsg.view.ITeacherMessagesFragment
import com.bonch_dev.spinit.presentation.modules.teachermsg.view.TeacherMessagesFragment
import com.bonch_dev.spinit.router.MainRouter
import javax.inject.Inject

class TeacherMsgPresenter @Inject constructor(private val mainRouter: MainRouter) : ITeacherMsgPresenter, BasePresenter<ITeacherMessagesFragment>() {

    private lateinit var homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>

    private var sortedHw: MutableMap<String, MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>> = mutableMapOf()

    override fun onHomeworkGot(homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>) {
        this.homeworks = homeworks

        if (homeworks.isNullOrEmpty())
            getView()?.onEmptyHomeworksList()
        else {
            getView()?.onNotEmptyHomeworksList(getHomeworksCoursesTitle())
            sortHw()
        }
    }

    override fun sortHw() {
        getHomeworksCoursesTitle().forEach {
            val currentTitleHomeworks = mutableListOf<com.bonch_dev.spinit.domain.entity.UserHomework>()
            for (homework in homeworks) {
                val title = homework.course?.title
                if (title != null && title == it) currentTitleHomeworks.add(homework)
            }
            sortedHw[it] = currentTitleHomeworks
        }
    }

    override fun getHomeworksCoursesTitle(): MutableList<String> {
        val titles = mutableSetOf<String>()
        homeworks.forEach {
            val title = it.course?.title
            if (title != null) titles.add(title)
        }
        return titles.toMutableList()
    }

    override fun onShowHomeworkBtnClicked(lessoidId: Int) {
        mainRouter.fromMsgToHomework(lessoidId, ((getView() as TeacherMessagesFragment).parentFragment as MainFragment).bottomNavigationFragment)
    }

    override fun onExpandHomeworksBtnClicked(holder: HomeworkSectionsAdapter.HomeworkSectionVH, courseTitle: String) {
        getView()?.onExpand(holder, sortedHw.getOrElse(courseTitle, { mutableListOf() }))
    }
}