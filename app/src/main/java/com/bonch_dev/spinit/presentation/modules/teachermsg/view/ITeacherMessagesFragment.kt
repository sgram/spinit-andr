package com.bonch_dev.spinit.presentation.modules.teachermsg.view

import com.bonch_dev.spinit.presentation.base.presenters.interfaces.IBaseView
import com.bonch_dev.spinit.presentation.modules.teachermsg.adapters.HomeworkSectionsAdapter

interface ITeacherMessagesFragment : IBaseView {
    fun onExpand(holder: HomeworkSectionsAdapter.HomeworkSectionVH, homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>)
    fun setHomeworksCoursesTitle(titles: List<String>)
    fun onHomeworkGot(homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>)
    fun onEmptyHomeworksList()
    fun onNotEmptyHomeworksList(homeworks: MutableList<String>)
}