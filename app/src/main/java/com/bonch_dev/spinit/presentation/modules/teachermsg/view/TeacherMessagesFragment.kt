package com.bonch_dev.spinit.presentation.modules.teachermsg.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.databinding.FragmentTeacherMessagesBinding
import com.bonch_dev.spinit.presentation.base.view.BaseFragment
import com.bonch_dev.spinit.domain.utils.gone
import com.bonch_dev.spinit.domain.utils.visible
import com.bonch_dev.spinit.presentation.modules.teachermsg.adapters.HomeworkSectionsAdapter
import com.bonch_dev.spinit.presentation.modules.teachermsg.adapters.HomeworksAdapter
import com.bonch_dev.spinit.presentation.modules.teachermsg.presenter.ITeacherMsgPresenter
import com.bonch_dev.spinit.presentation.modules.teachermsg.presenter.TeacherMsgPresenter
import javax.inject.Inject

class TeacherMessagesFragment : BaseFragment(), ITeacherMessagesFragment, HomeworksAdapter.OnClickListener,
    HomeworkSectionsAdapter.OnClickListener {


    private lateinit var coursesTitle: List<String>

    private lateinit var binding: FragmentTeacherMessagesBinding

    @Inject
    lateinit var presenter: ITeacherMsgPresenter

    init {
        App.appComponent.inject(this)
    }

    override fun provideView(inflater: LayoutInflater, container: ViewGroup?): View? {
        binding = FragmentTeacherMessagesBinding.inflate(inflater, container, false)
        (presenter as TeacherMsgPresenter).attachView(this)
        return binding.root
    }


    private fun showNoContentTV(text: String) {
        binding.noContent.noContentTv.text = text
        binding.noContent.noContentTv.visible()
    }

    private fun hideNoContentTV() = binding.noContent.noContentTv.gone()

    override fun onHomeworkGot(homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>) {
        presenter.onHomeworkGot(homeworks)
    }

    override fun onNotEmptyHomeworksList(homeworks: MutableList<String>) {
        hideNoContentTV()

        binding.homeworkRecycler.apply {
            adapter = HomeworkSectionsAdapter(homeworks, this@TeacherMessagesFragment)
            isNestedScrollingEnabled = false
        }
    }

    override fun onEmptyHomeworksList() {
        showNoContentTV("У вас пока нет сообщений")
    }

    override fun onExpandHomeworksBtnClick(holder: HomeworkSectionsAdapter.HomeworkSectionVH, courseTitle: String) {
        presenter.onExpandHomeworksBtnClicked(holder, courseTitle)
    }

    override fun onExpand(
        holder: HomeworkSectionsAdapter.HomeworkSectionVH,
        homeworks: MutableList<com.bonch_dev.spinit.domain.entity.UserHomework>
    ) {
        holder.onExpand(homeworks, this)
    }

    override fun onShowHomeworkBtnClicked(lessonId: Int) {
        presenter.onShowHomeworkBtnClicked(lessonId)
    }

    override fun setHomeworksCoursesTitle(titles: List<String>) {
        coursesTitle = titles
    }

    override fun onDestroyView() {
        (presenter as TeacherMsgPresenter).detachView()
        super.onDestroyView()
    }

    override fun setListeners() {}

}
