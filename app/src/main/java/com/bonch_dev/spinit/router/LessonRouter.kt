package com.bonch_dev.spinit.router

import android.os.Bundle
import androidx.navigation.NavController
import com.bonch_dev.spinit.R
import javax.inject.Inject

class LessonRouter @Inject constructor() {
    var controller: NavController? = null

    fun fromLessonsToCategory(bundle: Bundle) {
        controller?.navigate(R.id.action_lessonsFragment_to_lessonCategoryFragment2, bundle)
    }

    fun fromCategoryToMaterials() {
        controller?.navigate(R.id.action_lessonCategoryFragment_to_materialsFragment2)
    }

    fun fromMaterialsToLection() {
        controller?.navigate(R.id.action_materialsFragment_to_lectionFragment)
    }

    fun fromLectionToTest() {
        controller?.navigate(R.id.action_lectionFragment_to_testFragment)
    }

    fun fromLectionToHomework() {
        controller?.navigate(R.id.action_lectionFragment_to_homeworkFragment)
    }

    fun fromCategoryToLection() {
        controller?.navigate(R.id.action_lessonCategoryFragment_to_lectionFragment2)
    }

    fun fromCategoryToTest() {
        controller?.navigate(R.id.action_lessonCategoryFragment_to_testFragment2)
    }

    fun fromCategoryToHomework() {
        controller?.navigate(R.id.action_lessonCategoryFragment_to_homeworkFragment2)
    }

    fun fromSuccessTestToHomework() {
        controller?.navigate(R.id.action_testFragment_to_homeworkFragment)
    }
}