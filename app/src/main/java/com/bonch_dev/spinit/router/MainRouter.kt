package com.bonch_dev.spinit.router

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keys.COURSE_ID
import com.bonch_dev.spinit.domain.utils.Keys.ID
import com.bonch_dev.spinit.presentation.base.view.BottomNavigationFragment
import com.bonch_dev.spinit.presentation.modules.about_app.view.AboutFragment
import com.bonch_dev.spinit.presentation.modules.about_app.view.ContactFragment
import com.bonch_dev.spinit.presentation.modules.article.view.FullArticleFragment
import com.bonch_dev.spinit.presentation.modules.blog.view.BlogFragment
import com.bonch_dev.spinit.presentation.modules.broadcast.view.BroadcastFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.aboutcourse.view.AboutCourseFragment
import com.bonch_dev.spinit.presentation.modules.learning.courses.modules.allcourses.view.AllCoursesFragment
import com.bonch_dev.spinit.presentation.modules.learning.lesson.modules.homework.view.HomeworkFragment
import com.bonch_dev.spinit.presentation.modules.notification.view.NotificationFragment
import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment
import javax.inject.Inject

class MainRouter @Inject constructor() {
    var navController: NavController? = null
    var hostFrag: NavHostFragment? = null

    private fun changeFrag(base: NavHostFragment, fragmentToShow: Class<out Fragment>, fragmentToHide: Fragment, bundle: Bundle? = null) {
        base.childFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
            .add(R.id.base_frame_container, fragmentToShow, bundle)
            .hide(fragmentToHide)
            .addToBackStack(null)
            .commit()
    }

    fun fromMsgToHomework(id: Int, bottomNavigationFragment: BottomNavigationFragment) {
        val bundle = Bundle()
        bundle.putInt(ID, id)
        hostFrag?.let { changeFrag(it, HomeworkFragment::class.java, bottomNavigationFragment, bundle) }
    }

    fun toSettingsFragment(bottomNavigationFragment: BottomNavigationFragment) {
        hostFrag?.let { changeFrag(it, SettingsFragment::class.java, bottomNavigationFragment) }
    }

    fun toContactFragment(bottomNavigationFragment: BottomNavigationFragment) {
        hostFrag?.let { changeFrag(it, ContactFragment::class.java, bottomNavigationFragment) }
    }

    fun toAboutAppFragment(bottomNavigationFragment: BottomNavigationFragment) {
        hostFrag?.let { changeFrag(it, AboutFragment::class.java, bottomNavigationFragment) }
    }

    fun toNotificationFragment(bottomNavigationFragment: BottomNavigationFragment) {
        hostFrag?.let { changeFrag(it, NotificationFragment::class.java, bottomNavigationFragment) }
    }

    fun toAboutCourseInfo(allCoursesFragment: AllCoursesFragment, courseID: Int?) {
        val bottomNavigationFragment = allCoursesFragment.parentFragment as BottomNavigationFragment
        val bundle = Bundle().apply {
            putInt(COURSE_ID, courseID ?: -1)
        }
        hostFrag?.let { changeFrag(it, AboutCourseFragment::class.java, bottomNavigationFragment, bundle) }
    }

    fun toBroadcastFragment(blogFragment: BlogFragment, bundle: Bundle) {
        val baseFragment = blogFragment.parentFragment as BottomNavigationFragment
        bundle.getString("yt_video")?.let {
            if (!it.contains("embed")) {
                val intent = Intent(Intent.ACTION_VIEW).apply { data = Uri.parse(it) }
                blogFragment.startActivity(intent)
            } else hostFrag?.let { host -> changeFrag(host, BroadcastFragment::class.java, baseFragment, bundle) }
        }
    }

    fun toFullArticleActivity(screen: BottomNavigationFragment, postID: Int, liked: Boolean) {
        val bundle = Bundle().apply {
            putInt("post_id", postID)
            putBoolean("liked", liked)
        }
        hostFrag?.let { changeFrag(it, FullArticleFragment::class.java, screen, bundle) }
    }

    fun navigateToMainActivity() {
        Log.e(javaClass.simpleName,"To BottomNavFragment")
        navController?.run {
            when (navController?.currentDestination?.id) {
                R.id.signUpStepOneFragment -> navigate(R.id.action_signUpStepOneFragment_to_mainActivity)
                R.id.startFragment -> navigate(R.id.action_startFragment_to_mainActivity)
                R.id.signInFragment -> navigate(R.id.action_signInFragment_to_mainActivity)
            }
        }
    }

    fun navigateToRegFragment1() {
        navController?.navigate(R.id.action_startFragment_to_signUpStepOneFragment)
    }

    fun navigateToRegFragment2(bundle: Bundle) {
        navController?.navigate(R.id.action_signUpStepOneFragment_to_signUpStepTwoFragment, bundle)
    }

    fun navigateToSignInFragment() {
        navController?.navigate(R.id.action_startFragment_to_signInFragment)
    }

    fun returnToSignInFragment() {
        navController?.navigate(R.id.action_signUpStepTwoFragment_to_signInFragment)
    }
}