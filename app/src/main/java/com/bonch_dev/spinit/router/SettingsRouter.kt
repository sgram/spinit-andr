package com.bonch_dev.spinit.router

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.presentation.modules.notification.view.PushNotifFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.ChangePasswordFragment
import com.bonch_dev.spinit.presentation.modules.profile.update_info.view.EditProfileFragment
import com.bonch_dev.spinit.presentation.modules.settings.view.SettingsFragment
import javax.inject.Inject

class SettingsRouter @Inject constructor() {
    private fun changeFrag(base: NavHostFragment, fragmentToShow: Class<out Fragment>, fragmentToHide: Fragment) {
        base.childFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
            .add(R.id.base_frame_container, fragmentToShow, null)
            .hide(fragmentToHide)
            .addToBackStack(null)
            .commit()
    }

    fun toEditProfile(fragment: Fragment) {
        val base = fragment.parentFragment as NavHostFragment
        changeFrag(base, EditProfileFragment::class.java, fragment)
    }

    fun toChangePassword(settingsFragment: SettingsFragment) {
        val base = settingsFragment.parentFragment as NavHostFragment
        changeFrag(base, ChangePasswordFragment::class.java, settingsFragment)
    }

    fun toPushNotif(settingsFragment: SettingsFragment) {
        val base = settingsFragment.parentFragment as NavHostFragment
        changeFrag(base, PushNotifFragment::class.java, settingsFragment)
    }
}