package com.bonch_dev.spinit.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bonch_dev.spinit.App
import com.bonch_dev.spinit.R
import com.bonch_dev.spinit.domain.utils.Keys.CHANNEL_ID
import com.bonch_dev.spinit.presentation.base.view.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import javax.inject.Inject
import javax.inject.Named


class NotifService : FirebaseMessagingService() {
    lateinit var nm: NotificationManager
    lateinit var nb: NotificationCompat.Builder

    @Inject
    @Named("Notification")
    lateinit var pushNotificationsSP: SharedPreferences

    init {
        App.appComponent.inject(this)
    }

    override fun onNewToken(p0: String) {
        Log.d("Refreshed", "Refreshed token: $p0")
        super.onNewToken(p0)
    }

    override fun onMessageReceived(p0: RemoteMessage) {
        val data = p0.data
        val title = data["title"]
        val content = data["content"]
        val category = data["type"]
        Log.e("notification", "new")
/*        val image=Glide.with(this).asBitmap().load(R.drawable.avatar).submit()
        val bitmap=image.get()*/
        super.onMessageReceived(p0)
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nb = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val nc = NotificationChannel(CHANNEL_ID, CHANNEL_ID, NotificationManager.IMPORTANCE_HIGH)
            nm.createNotificationChannel(nc)
            NotificationCompat.Builder(this, CHANNEL_ID)
        } else NotificationCompat.Builder(this)
        nb
            .setContentIntent(pendingIntent)
            .setAutoCancel(false)
            .setSmallIcon(R.mipmap.ic_launcher_round)
            .setContentTitle(title)
            .setContentText(content)
            .setPriority(NotificationCompat.PRIORITY_MAX)
        if (checkCategoryEnable(category)) nm.notify(0, nb.build())
    }

    private fun checkCategoryEnable(category: String?): Boolean {
        return pushNotificationsSP.getBoolean(category, true)
    }
}
